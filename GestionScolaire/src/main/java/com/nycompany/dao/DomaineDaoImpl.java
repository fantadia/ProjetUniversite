/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Domaine;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class DomaineDaoImpl implements DomaineDao{
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createDomainee(Domaine domaine) {
       
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(domaine);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateDomaine(Domaine domaine) {
        
         
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(domaine);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteDomaine(Domaine domaine) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Domaine cl = (Domaine) session.load(Domaine.class, domaine.getCodedomaine());
        
            session.delete(cl);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Domaine findDomaineById(String idDomaine) {
        Domaine domaine = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Domaine where codedomaine = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", idDomaine);
            domaine = (Domaine) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return domaine;
    }

    @Override
    public Domaine findDomaineByLibelle(String libelleDomaine) {
        
        Domaine domaine = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Domaine where nomDomaine = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", libelleDomaine);
            domaine = (Domaine) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return domaine;
    }

    @Override
    public List<Domaine> findAllDomaine() {
        List<Domaine> domaines = new ArrayList<Domaine>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            domaines = session.createQuery("from Domaine").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return domaines;   
    }

    @Override
    public List<Domaine> findDomaineByEtablissement(String idEtab) {
        List<Domaine> domaines = new ArrayList<Domaine>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Domaine where etablissement = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idEtab);
            domaines = (List<Domaine>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return domaines;  
    }
    
}
    
    

    
