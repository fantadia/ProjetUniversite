/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Domaine;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface DomaineDao {
    
    public void createDomainee(Domaine domaine);
    public void updateDomaine(Domaine domaine);
    public void deleteDomaine(Domaine domaine);
    public Domaine findDomaineById(String id);
    public List<Domaine> findDomaineByEtablissement(String idEtab);
    public Domaine findDomaineByLibelle(String libelle);
    public List<Domaine> findAllDomaine(); 
}
