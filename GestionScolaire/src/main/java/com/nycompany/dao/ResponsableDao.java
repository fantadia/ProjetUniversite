/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Responsable;
import com.mycompany.entities.Utilisateur;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface ResponsableDao {
    
    public void saveResponsable(Responsable responsable);
    public void updateResponsable(Responsable responsable);
    public void deleteResponsable(Responsable responsable);
    public Responsable findResponsableById(String id);
    public List<Responsable>  findUtilisateurByResponsable(String idResponsable);
    public List<Responsable> findAll(); 
}
