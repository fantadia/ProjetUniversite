/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Utilisateur;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface UtilisateurDao {
    
    public void createUtilisateur(Utilisateur utilisateur);
    public void updateUtilisateur(Utilisateur utilisateur);
    public void deleteUtilisateur(Utilisateur utilisateur);
    public Utilisateur findUtilisateurById(String id);
    public Utilisateur findUtilisateurByPassword(String id);
    public Utilisateur findUtilisateurByCIN(String id);
    public List<Utilisateur> findUtilisateurBySecretaire(String idSecretaire);
    public List<Utilisateur>  findUtilisateurByAdministrateur(String idAdministrateur);
    public List<Utilisateur>  findUtilisateurByComptable(String idComptable);
    public List<Utilisateur> findAll(); 
}
