/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Comptable;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface ComptableDao {
    
    public void createComptable(Comptable comptable);
    public void updateComptable(Comptable comptable);
    public void deleteComptable(Comptable comptable);
    public List<Comptable> findAllComptable(); 
}
