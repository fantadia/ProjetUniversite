/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Classe;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class ClasseDaoImpl implements ClasseDao{
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createClasse(Classe classe) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(classe);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateClasse(Classe classe) {
        
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(classe);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteClasse(Classe classe) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Classe cl = (Classe) session.load(Classe.class, classe.getCodeclasse());
        
            session.delete(cl);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Classe findClasseById(String idClasse) {
        
         
        Classe classe = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Classe where ﻿codeclasse = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", idClasse);
            classe = (Classe) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return classe;
    }

    @Override
    public Classe findClasseByLibelle(String libelle) {
        Classe classe = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Classe where lbelle_classe = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", libelle);
            classe = (Classe) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return classe;
    }

    @Override
    public List<Classe> findAllClasse()
    {
        List<Classe> classes = new ArrayList<Classe>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            classes = session.createQuery("from Classe").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return classes;   
    }

    @Override
    public List<Classe> findClasseByIdNiveau(String idNiveau) {
        
        List<Classe> classes = new ArrayList<Classe>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Classe where niveau = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idNiveau);
            classes = (List<Classe>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return classes;  
    }

    @Override
    public List<Classe> findClasseByIdFiliere(String idFiliere) {
       List<Classe> classes = new ArrayList<Classe>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Classe where filiere = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idFiliere);
            classes = (List<Classe>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return classes;  
    }
    
}
