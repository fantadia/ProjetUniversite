/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;


import com.mycompany.entities.Utilisateur;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class UtilisateurDaoImpl implements UtilisateurDao{
    
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createUtilisateur(Utilisateur utilisateur) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(utilisateur);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateUtilisateur(Utilisateur utilisateur) {
       Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(utilisateur);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteUtilisateur(Utilisateur utilisateur) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Utilisateur util = (Utilisateur) session.load(Utilisateur.class, utilisateur.getCodeutilisateur());
        
            session.delete(util);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Utilisateur findUtilisateurById(String id) {
        Utilisateur utilisateur = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Utilisateur where codeutilisateur = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            utilisateur = (Utilisateur) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return utilisateur;
    }

    @Override
    public List<Utilisateur> findAll() {
       List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            utilisateurs = session.createQuery("from Utilisateur").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return utilisateurs;   
    }


    @Override
    public List<Utilisateur> findUtilisateurBySecretaire(String idSecretaire) {
        List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Utilisateur where secretaire = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idSecretaire);
            utilisateurs = (List<Utilisateur>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return utilisateurs; 
    }

    

    @Override
    public List<Utilisateur> findUtilisateurByAdministrateur(String idAdministrateur) {
        
        List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Utilisateur where administrateur = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idAdministrateur);
            utilisateurs = (List<Utilisateur>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return utilisateurs; 
    }

    @Override
    public List<Utilisateur> findUtilisateurByComptable(String idComptable) {
        
        List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Utilisateur where comptable = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idComptable);
            utilisateurs = (List<Utilisateur>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return utilisateurs; 
    }

    @Override
    public Utilisateur findUtilisateurByPassword(String idPwd) {
        Utilisateur  utilisateur = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Utilisateur where password = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idPwd);
            utilisateur = (Utilisateur) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return utilisateur;
    }

    @Override
    public Utilisateur findUtilisateurByCIN(String idCin) {
         
        Utilisateur  utilisateur = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Utilisateur where cin = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idCin);
            utilisateur = (Utilisateur) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return utilisateur;
    }
}
