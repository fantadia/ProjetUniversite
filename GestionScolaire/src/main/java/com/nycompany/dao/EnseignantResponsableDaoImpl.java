/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.EnseignantResponsable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class EnseignantResponsableDaoImpl implements EnseignantResponsableDao{

     private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Override
    public void createEnseignantResponsable(EnseignantResponsable enseignantResponsable) {
       
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(enseignantResponsable);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateEnseignantResponsable(EnseignantResponsable enseignantResponsable) {
        
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(enseignantResponsable);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteEnseignantResponsable(EnseignantResponsable enseignantResponsable) {
       
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            EnseignantResponsable ens_resp =(EnseignantResponsable) 
            session.load(EnseignantResponsable.class, enseignantResponsable.getEnseignant().getCodeutilisateur());
        
            session.delete(ens_resp);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public EnseignantResponsable findEnseignantResponsableeById(String id) {
        EnseignantResponsable enseignantResponsable = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from EnseignantResponsable where enseignant = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            enseignantResponsable = (EnseignantResponsable) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return enseignantResponsable;
    }

    @Override
    public List<EnseignantResponsable> findUtilisateurByEnseignantResponsable(String idEns) {
       
        List<EnseignantResponsable> enseignantResponsables = new ArrayList<EnseignantResponsable>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from EnseignantResponsable where enseignant = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idEns);
            enseignantResponsables = (List<EnseignantResponsable>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return enseignantResponsables; 
    }

    @Override
    public List<EnseignantResponsable> findAllEnseignantResponsable() {
         List<EnseignantResponsable> enseignantResponsables = new ArrayList<EnseignantResponsable>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            enseignantResponsables = session.createQuery("from EnseignantResponsable").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return enseignantResponsables;  
    }
    
}
