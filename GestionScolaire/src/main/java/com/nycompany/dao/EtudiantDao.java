/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Etudiant;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface EtudiantDao 
{
    public void createEtudiant(Etudiant etudiant);
    public void updateEtudiant(Etudiant etudiant);
    public void deleteEtudiant(Etudiant etudiant);
    public Etudiant findEnsEtudiantById(String id);
    public List<Etudiant> findAllEtudiant(); 
}
