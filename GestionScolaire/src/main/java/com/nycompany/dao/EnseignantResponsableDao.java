/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.EnseignantResponsable;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface EnseignantResponsableDao {
    
    public void createEnseignantResponsable(EnseignantResponsable enseignantResponsable);
    public void updateEnseignantResponsable(EnseignantResponsable enseignantResponsable);
    public void deleteEnseignantResponsable(EnseignantResponsable enseignantResponsable);
    public EnseignantResponsable findEnseignantResponsableeById(String id);
    public List<EnseignantResponsable>  findUtilisateurByEnseignantResponsable(String idResponsable);
    public List<EnseignantResponsable> findAllEnseignantResponsable(); 
}
