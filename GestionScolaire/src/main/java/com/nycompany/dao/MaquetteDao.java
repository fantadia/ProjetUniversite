/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Maquette;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface MaquetteDao {
    
    public void createMaquette(Maquette maquette);
    public void updateMaquette(Maquette maquette);
    public void deleteMaquette(Maquette maquette);
    public Maquette findMaquetteById(String id);
    public List<Maquette> findAllMaquette(); 
    
}
