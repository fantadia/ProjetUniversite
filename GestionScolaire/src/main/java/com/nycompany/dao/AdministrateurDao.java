/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Administrateur;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface AdministrateurDao {
    
    public void createAdministrateur(Administrateur administrateur);
    public void updateAdministrateur(Administrateur administrateur);
    public void deleteAdministrateur(Administrateur administrateur);
    public List<Administrateur> findAllAdministrateur(); 
}
