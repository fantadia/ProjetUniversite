package com.mycompany.entities;
// Generated 13 sept. 2017 20:30:02 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * EnseignantResponsable generated by hbm2java
 */
@Entity
@Table(name="enseignant_responsable"
    ,schema="public"
)
public class EnseignantResponsable  implements java.io.Serializable {


     private String codeutilisateur;
     private Enseignant enseignant;
     private Set<Filiere> filieres = new HashSet<Filiere>(0);

    public EnseignantResponsable() {
    }

	
    public EnseignantResponsable(Enseignant enseignant) {
        this.enseignant = enseignant;
    }
    public EnseignantResponsable(Enseignant enseignant, Set<Filiere> filieres) {
       this.enseignant = enseignant;
       this.filieres = filieres;
    }
   
     @GenericGenerator(name="generator", strategy="foreign", parameters=@Parameter(name="property", value="enseignant"))@Id @GeneratedValue(generator="generator")

    
    @Column(name="codeutilisateur", unique=true, nullable=false, length=254)
    public String getCodeutilisateur() {
        return this.codeutilisateur;
    }
    
    public void setCodeutilisateur(String codeutilisateur) {
        this.codeutilisateur = codeutilisateur;
    }

@OneToOne(fetch=FetchType.LAZY)@PrimaryKeyJoinColumn
    public Enseignant getEnseignant() {
        return this.enseignant;
    }
    
    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="enseignantResponsable")
    public Set<Filiere> getFilieres() {
        return this.filieres;
    }
    
    public void setFilieres(Set<Filiere> filieres) {
        this.filieres = filieres;
    }




}


