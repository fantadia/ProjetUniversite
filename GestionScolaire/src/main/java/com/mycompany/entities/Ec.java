package com.mycompany.entities;
// Generated 13 sept. 2017 20:30:02 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Ec generated by hbm2java
 */
@Entity
@Table(name="ec"
    ,schema="public"
    , uniqueConstraints = @UniqueConstraint(columnNames="libelle_matiere") 
)
public class Ec  implements java.io.Serializable {


     private String codematiere;
     private Ue ue;
     private String libelleMatiere;
     private String quantumhoraire;
     private Integer credit;
     private String tp;
     private String td;
     private String tpe;
     private String coursmagistraux;
     private Set<Evaluer> evaluers = new HashSet<Evaluer>(0);
     private Set<Cours> courses = new HashSet<Cours>(0);

    public Ec() {
    }

	
    public Ec(String codematiere, Ue ue) {
        this.codematiere = codematiere;
        this.ue = ue;
    }
    public Ec(String codematiere, Ue ue, String libelleMatiere, String quantumhoraire, Integer credit, String tp, String td, String tpe, String coursmagistraux, Set<Evaluer> evaluers, Set<Cours> courses) {
       this.codematiere = codematiere;
       this.ue = ue;
       this.libelleMatiere = libelleMatiere;
       this.quantumhoraire = quantumhoraire;
       this.credit = credit;
       this.tp = tp;
       this.td = td;
       this.tpe = tpe;
       this.coursmagistraux = coursmagistraux;
       this.evaluers = evaluers;
       this.courses = courses;
    }
   
     @Id 

    
    @Column(name="codematiere", unique=true, nullable=false, length=254)
    public String getCodematiere() {
        return this.codematiere;
    }
    
    public void setCodematiere(String codematiere) {
        this.codematiere = codematiere;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="codeue", nullable=false)
    public Ue getUe() {
        return this.ue;
    }
    
    public void setUe(Ue ue) {
        this.ue = ue;
    }

    
    @Column(name="libelle_matiere", unique=true, length=254)
    public String getLibelleMatiere() {
        return this.libelleMatiere;
    }
    
    public void setLibelleMatiere(String libelleMatiere) {
        this.libelleMatiere = libelleMatiere;
    }

    
    @Column(name="quantumhoraire", length=254)
    public String getQuantumhoraire() {
        return this.quantumhoraire;
    }
    
    public void setQuantumhoraire(String quantumhoraire) {
        this.quantumhoraire = quantumhoraire;
    }

    
    @Column(name="credit")
    public Integer getCredit() {
        return this.credit;
    }
    
    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    
    @Column(name="tp", length=254)
    public String getTp() {
        return this.tp;
    }
    
    public void setTp(String tp) {
        this.tp = tp;
    }

    
    @Column(name="td", length=254)
    public String getTd() {
        return this.td;
    }
    
    public void setTd(String td) {
        this.td = td;
    }

    
    @Column(name="tpe", length=254)
    public String getTpe() {
        return this.tpe;
    }
    
    public void setTpe(String tpe) {
        this.tpe = tpe;
    }

    
    @Column(name="coursmagistraux", length=254)
    public String getCoursmagistraux() {
        return this.coursmagistraux;
    }
    
    public void setCoursmagistraux(String coursmagistraux) {
        this.coursmagistraux = coursmagistraux;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ec")
    public Set<Evaluer> getEvaluers() {
        return this.evaluers;
    }
    
    public void setEvaluers(Set<Evaluer> evaluers) {
        this.evaluers = evaluers;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="ec")
    public Set<Cours> getCourses() {
        return this.courses;
    }
    
    public void setCourses(Set<Cours> courses) {
        this.courses = courses;
    }




}


