/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.Domaine;

import com.mycompany.entities.Domaine;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.FiliereDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractDomaineCtl {
    
   
    protected DomaineDao domaineDao ;
    
    protected FiliereDao filiereDao;
 
    
    protected List<Domaine> domaines;
    
    protected StringBuffer domainesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Domaine selectedDomaine;
    protected Domaine domaine;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public DomaineDao getDomaineDao() {
        return domaineDao;
    }

    public void setDomaineDao(DomaineDao domaineDao) {
        this.domaineDao = domaineDao;
    }

    public List<Domaine> getDomaines() {
        return domaines;
    }

    public void setDomaines(List<Domaine> domaines) {
        this.domaines = domaines;
    }

    public StringBuffer getDomainesTableHtml() {
        return domainesTableHtml;
    }

    public void setDomainesTableHtml(StringBuffer domainesTableHtml) {
        this.domainesTableHtml = domainesTableHtml;
    }

    public Domaine getSelectedDomaine() {
        return selectedDomaine;
    }

   
    public void setSelectedClasse(Domaine selectedDomaine) {
        this.selectedDomaine = selectedDomaine;
        if (selectedDomaine == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Domaine getDomaine() {
        return domaine;
    }

    public void setDomaine(Domaine domaine) {
        this.domaine = domaine;
    }

    public FiliereDao getFiliereDao() {
        return filiereDao;
    }

    public void setFiliereDao(FiliereDao filiereDao) {
        this.filiereDao = filiereDao;
    }
    
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = domaineDao.findAllDomaine().isEmpty();
        return imprimer;
    }
    
    public StringBuffer getEtablissementsTableHtml() {
        return domainesTableHtml;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
