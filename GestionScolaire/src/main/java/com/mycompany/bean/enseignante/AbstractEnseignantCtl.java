/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.enseignante;




import com.mycompany.bean.comptable.*;
import com.mycompany.entities.Enseignant;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EnseignantDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEnseignantCtl {
    
    protected EnseignantDao enseignantDao ;
    protected UtilisateurDao utilisateurDao;
    protected CoursDao coursDao;
    protected List<Enseignant> enseignants;
    
    protected StringBuffer enseignantTableHtml = new StringBuffer("pas encore implementé") ;
    protected Enseignant selectedEnseignant;
    protected Enseignant enseignant;
    protected Utilisateur utilisateur;
    protected Utilisateur selectedUtilisateur;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public EnseignantDao getEnseignantDao() {
        return enseignantDao;
    }

    public void setEnseignantDao(EnseignantDao enseignantDao) {
        this.enseignantDao = enseignantDao;
    }

    public CoursDao getCoursDao() {
        return coursDao;
    }

    public void setCoursDao(CoursDao coursDao) {
        this.coursDao = coursDao;
    }

    public List<Enseignant> getEnseignants() {
        return enseignants;
    }

    public void setEnseignants(List<Enseignant> enseignants) {
        this.enseignants = enseignants;
    }

    public StringBuffer getEnseignantTableHtml() {
        return enseignantTableHtml;
    }

    public void setEnseignantTableHtml(StringBuffer enseignantTableHtml) {
        this.enseignantTableHtml = enseignantTableHtml;
    }

    public Enseignant getSelectedEnseignant() {
        return selectedEnseignant;
    }

    public void setSelectedEnseignant(Enseignant selectedEnseignant) {
        this.selectedEnseignant = selectedEnseignant;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public UtilisateurDao getUtilisateurDao() {
        return utilisateurDao;
    }

    public void setUtilisateurDao(UtilisateurDao utilisateurDao) {
        this.utilisateurDao = utilisateurDao;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Utilisateur getSeleUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSeleUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
        if (selectedUtilisateur == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = enseignantDao.findAll().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
