/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.enseignante;  


import beans.util.JsfUtil;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Enseignant;
import com.mycompany.entities.Utilisateur; 
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EnseignantDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="enseignantCtl")
@ViewScoped

public class EnseignantCtl extends AbstractEnseignantCtl implements EnseignantInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EnseignantCtl() {
    }
    
    @PostConstruct
    private void initEnseignant()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        enseignantDao = (EnseignantDao) context.getBean("enseignantDao");
        coursDao =(CoursDao) context.getBean("coursDao");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        utilisateur =(Utilisateur) context.getBean("utilisateur");
        selectedUtilisateur =(Utilisateur) context.getBean("utilisateur");
        enseignant = (Enseignant) context.getBean("enseignant");
        selectedEnseignant = (Enseignant) context.getBean("enseignant");
        enseignants = enseignantDao.findAll();
    }

    @Override
    public void enregistrerEnseignant() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        /*
            Avant tout faut enregistrer ds utilisateur avec comme identifiant 700
            ce qui veut dire que dans la page jsf faut utiliser l'objet utilisateur
            pour preparer les donnees a inserer
            Ensuite l'objet enseignant sera creer a partir de utilisateur
        */
        utilisateurDao.createUtilisateur(utilisateur);
        //jecrase utilisateur et je le recupere a nouveau
        //depuis la BD 
        utilisateur=utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        //j'initialise l'enseignant a inserer
        enseignant.setUtilisateur(utilisateur);
        enseignantDao.createEnseignant(enseignant);
        initEnseignant();
        JsfUtil.addSuccessMessage("l'enseignant à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEnseignant() {
        
        if (selectedEnseignant == null || selectedEnseignant.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       utilisateurDao.updateUtilisateur(selectedUtilisateur);
       //selectedEnseignant.setUtilisateur(selectedUtilisateur);
       //enseignantDao.updateEnseignant(selectedEnseignant);
       JsfUtil.addSuccessMessage("enseignant Modifier avec succes");
    }

    @Override
    public void supprimerEnseignant() {
        
         if(selectedUtilisateur == null || selectedUtilisateur.getCodeutilisateur()== null)
            return;
         List<Cours> cours =  coursDao.findCoursByEnseignant(selectedUtilisateur.getCodeutilisateur());
         if(cours!=null){
             JsfUtil.addErrorMessage("Cet enseignant ne peut pas etre supprimée, il est deja utilise dans une operation");
         }
           selectedEnseignant.setUtilisateur(selectedUtilisateur);
           enseignantDao.deleteEnseignant(selectedEnseignant);
           utilisateurDao.deleteUtilisateur(selectedUtilisateur);
          
           JsfUtil.addErrorMessage("Cet enseignant à été supprimée");
           modifier = supprimer = detail = true;}

    @Override
    public void imprimerEnseignantPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEnseignantHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
