/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.dossier;


import com.mycompany.bean.ec.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Dossier;
import com.mycompany.entities.Ec;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.DossierDao;
import com.nycompany.dao.EcDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="dossierCtl")
@ViewScoped

public class DossierCtl extends AbstractDossierCtl implements DossierInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public DossierCtl() {
    }
    
    @PostConstruct
    private void initDossier()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        dossierDao = (DossierDao) context.getBean("dossierDao");
        dossier = (Dossier) context.getBean("dossier");
        selectedDossier = (Dossier) context.getBean("dossier");
        dossiers = dossierDao.findAllDossier();
    }

    @Override
    public void enregistrerDossier() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        dossierDao.createDossier(dossier);
        initDossier();
        JsfUtil.addSuccessMessage("le dossier à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierDossier() {
        
        if (selectedDossier == null || selectedDossier.getCodedossier()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        dossierDao.updateDossier(selectedDossier);
        JsfUtil.addSuccessMessage("dossier Modifier avec succes");
    }

    @Override
    public void supprimerDossier() {
        
         if(selectedDossier == null || selectedDossier.getCodedossier()== null)
            return;
       
            dossierDao.deleteDossier(selectedDossier);
            JsfUtil.addErrorMessage("Ce dossier à été supprimée");
            modifier = supprimer = detail = true;
    }
    @Override
    public void imprimerDossierPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerDossierHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
