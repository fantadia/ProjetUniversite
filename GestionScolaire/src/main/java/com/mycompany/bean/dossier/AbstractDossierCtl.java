/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.dossier;




import com.mycompany.entities.Dossier;
import com.nycompany.dao.DossierDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractDossierCtl {
    
    
    protected DossierDao dossierDao;
   
    protected List<Dossier> dossiers;
    
    protected StringBuffer dossiersTableHtml = new StringBuffer("pas encore implementé") ;
    protected Dossier selectedDossier;
    protected Dossier dossier;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public DossierDao getDossierDao() {
        return dossierDao;
    }

    public void setDossierDao(DossierDao dossierDao) {
        this.dossierDao = dossierDao;
    }

    public List<Dossier> getDossiers() {
        return dossiers;
    }

    public void setDossiers(List<Dossier> dossiers) {
        this.dossiers = dossiers;
    }

    public StringBuffer getDossiersTableHtml() {
        return dossiersTableHtml;
    }

    public void setDossiersTableHtml(StringBuffer dossiersTableHtml) {
        this.dossiersTableHtml = dossiersTableHtml;
    }

    public Dossier getSelectedDossier() {
        return selectedDossier;
    }

    public void setSelectedDossier(Dossier selectedDossier) {
        this.selectedDossier = selectedDossier;
        if (selectedDossier == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Dossier getDossier() {
        return dossier;
    }

    public void setDossier(Dossier dossier) {
        this.dossier = dossier;
    }

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = dossierDao.findAllDossier().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
