/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.etudiant;


import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.DossierDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEtudiantCtl {
    
    protected EtudiantDao etudiantDao ;
    protected DossierDao dossierDao;
    protected UtilisateurDao utilisateurDao;
    protected List<Etudiant> etudiants;
    
    protected StringBuffer etudiantsTableHtml = new StringBuffer("pas encore implementé") ;
    protected Etudiant selectedEtudiant;
    protected Etudiant etudiant;
    protected Utilisateur utilisateur;
    protected Utilisateur selectedUtilisateur;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public EtudiantDao getEtudiantDao() {
        return etudiantDao;
    }

    public void setEtudiantDao(EtudiantDao etudiantDao) {
        this.etudiantDao = etudiantDao;
    }

    public DossierDao getDossierDao() {
        return dossierDao;
    }

    public void setDossierDao(DossierDao dossierDao) {
        this.dossierDao = dossierDao;
    }

    public List<Etudiant> getEtudiants() {
        return etudiants;
    }

    public void setEtudiants(List<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }

    public StringBuffer getEtudiantsTableHtml() {
        return etudiantsTableHtml;
    }

    public void setEtudiantsTableHtml(StringBuffer etudiantsTableHtml) {
        this.etudiantsTableHtml = etudiantsTableHtml;
    }

    public Etudiant getSelectedEtudiant() {
        return selectedEtudiant;
    }

    public void setSelectedEtudiant(Etudiant selectedEtudiant) {
        this.selectedEtudiant = selectedEtudiant;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Utilisateur getSelectedUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSelectedUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
        if (selectedUtilisateur == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    
    public UtilisateurDao getUtilisateurDao() {
        return utilisateurDao;
    }

    public void setUtilisateurDao(UtilisateurDao utilisateurDao) {
        this.utilisateurDao = utilisateurDao;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Utilisateur getSeleUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSeleUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
        
    }
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = etudiantDao.findAllEtudiant().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
