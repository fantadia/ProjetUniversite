/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.etudiant;







/**
 *
 * @author macbookpro
 */
public interface EtudiantInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerEtudiant();
    public void modifierEtudiant();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerEtudiant();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerEtudiantPdf();
    
    public void imprimerEtudiantHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
