/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.etudiant;


import beans.util.JsfUtil;
import com.mycompany.entities.Dossier;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.DossierDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="etudiantCtl")
@ViewScoped
public class EtudiantCtl extends AbstractEtudiantCtl implements EtudiantInterfaceCtl {
    
    /**
     * Creates a new instance of EnseignantCtrl
     */
    public EtudiantCtl() {
    }
    
    @PostConstruct
    private void initEtudiant()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        etudiantDao = (EtudiantDao) context.getBean("etudiantDao");
        dossierDao =(DossierDao) context.getBean("dossierDao");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        utilisateur =(Utilisateur) context.getBean("utilisateur");
        selectedUtilisateur =(Utilisateur) context.getBean("utilisateur");
        etudiant = (Etudiant) context.getBean("etudiant");
        selectedEtudiant = (Etudiant) context.getBean("etudiant");
        etudiants = etudiantDao.findAllEtudiant();
    }

    @Override
    public void enregistrerEtudiant() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        /*
            Avant tout faut enregistrer ds utilisateur avec comme identifiant 700
            ce qui veut dire que dans la page jsf faut utiliser l'objet utilisateur
            pour preparer les donnees a inserer
            Ensuite l'objet enseignant sera creer a partir de utilisateur
        */
        utilisateurDao.createUtilisateur(utilisateur);
        //jecrase utilisateur et je le recupere a nouveau
        //depuis la BD 
        utilisateur=utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        //j'initialise l'enseignant a inserer
        etudiant.setUtilisateur(utilisateur);
    
        etudiantDao.createEtudiant(etudiant);
        initEtudiant();
        JsfUtil.addSuccessMessage("l'enseignant à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEtudiant() {
        
        if (selectedUtilisateur == null || selectedUtilisateur.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       utilisateurDao.updateUtilisateur(selectedUtilisateur);
       //selectedEnseignant.setUtilisateur(selectedUtilisateur);
       //enseignantDao.updateEnseignant(selectedEnseignant);
       JsfUtil.addSuccessMessage("enseignant Modifier avec succes");
    }

    @Override
    public void supprimerEtudiant() {
        
         if(selectedUtilisateur == null || selectedUtilisateur.getCodeutilisateur()== null)
            return;
         List<Dossier> dossiers =  dossierDao.findDossierByEtudiant(selectedUtilisateur.getCodeutilisateur());
         if(dossiers!=null){
             JsfUtil.addErrorMessage("Cet etudiant ne peut pas etre supprimée, il est deja utilise dans une operation");
         }
           selectedEtudiant.setUtilisateur(selectedUtilisateur);
           etudiantDao.deleteEtudiant(selectedEtudiant);
           utilisateurDao.deleteUtilisateur(selectedUtilisateur);
          
           JsfUtil.addErrorMessage("Cet enseignant à été supprimée");
           modifier = supprimer = detail = true;}

    @Override
    public void imprimerEtudiantPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEtudiantHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
