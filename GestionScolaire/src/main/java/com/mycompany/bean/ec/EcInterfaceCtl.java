/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ec;






/**
 *
 * @author macbookpro
 */
public interface EcInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerEc();
    public void modifierEc();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerEc();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerEcPdf();
    
    public void imprimerEcHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
