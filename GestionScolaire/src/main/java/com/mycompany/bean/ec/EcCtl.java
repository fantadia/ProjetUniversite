/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ec;


import beans.util.JsfUtil;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Ec;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="ecCtl")
@ViewScoped

public class EcCtl extends AbstractEcCtl implements EcInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EcCtl() {
    }
    
    @PostConstruct
    private void initEc()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        ecDao = (EcDao) context.getBean("ecDao");
        coursDao = (CoursDao) context.getBean("coursDao");
        ec = (Ec) context.getBean("ec");
        selectedEc = (Ec) context.getBean("ec");
        ecs = ecDao.findAllEc();
    }

    @Override
    public void enregistrerEc() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        ecDao.createEc(ec);
        initEc();
        JsfUtil.addSuccessMessage("l' Ec à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEc() {
        
        if (selectedEc == null || selectedEc.getCodematiere()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        ecDao.updateEc(selectedEc);
        JsfUtil.addSuccessMessage("Ec Modifier avec succes");
    }

    @Override
    public void supprimerEc() {
        
         if(selectedEc == null || selectedEc.getCodematiere()== null)
            return;
        List<Cours>  cours= coursDao.findCoursByEc(selectedEc.getCodematiere());
         if(cours!=null){
             JsfUtil.addErrorMessage("ce Ec ne peut pas etre supprimer,car il est utilise dans une autre operation");
         }
       
            ecDao.deleteEc(selectedEc);
            JsfUtil.addErrorMessage("Ce Ec à été supprimée");
            modifier = supprimer = detail = true;
    }
    @Override
    public void imprimerEcPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEcHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
