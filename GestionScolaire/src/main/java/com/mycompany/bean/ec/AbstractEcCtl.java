/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ec;




import com.mycompany.entities.Ec;
import com.mycompany.entities.Semestre;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEcCtl {
    
    
    protected EcDao ecDao;
    protected CoursDao coursDao;
   
    protected List<Ec> ecs;
    
    protected StringBuffer ecsTableHtml = new StringBuffer("pas encore implementé") ;
    protected Ec selectedEc;
    protected Ec ec;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public EcDao getEcDao() {
        return ecDao;
    }

    public void setEcDao(EcDao ecDao) {
        this.ecDao = ecDao;
    }

    public CoursDao getCoursDao() {
        return coursDao;
    }

    public void setCoursDao(CoursDao coursDao) {
        this.coursDao = coursDao;
    }

    public List<Ec> getEcs() {
        return ecs;
    }

    public void setEcs(List<Ec> ecs) {
        this.ecs = ecs;
    }

    public StringBuffer getEcsTableHtml() {
        return ecsTableHtml;
    }

    public void setEcsTableHtml(StringBuffer ecsTableHtml) {
        this.ecsTableHtml = ecsTableHtml;
    }

    public Ec getSelectedEc() {
        return selectedEc;
    }

    public void setSelectedEc(Ec selectedEc) {
        this.selectedEc = selectedEc;
         if (selectedEc == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Ec getEc() {
        return ec;
    }

    public void setEc(Ec ec) {
        this.ec = ec;
    }

    
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = ecDao.findAllEc().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
