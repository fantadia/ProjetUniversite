/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.etablissements;

import com.mycompany.entities.Etablissement;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEtablissementCtl {
    
    
    protected EtablissementDao etablissementDao ;
    
   
    protected DomaineDao domaineDao;
    
    protected List<Etablissement> etablissements;
    
    protected StringBuffer etablissementsTableHtml = new StringBuffer("pas encore implementé") ;
    protected Etablissement selectedEtablissement;
    protected Etablissement etablissement;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public EtablissementDao getEtablissementDao() {
        return etablissementDao;
    }

    public void setEtablissementDao(EtablissementDao etablissementDao) {
        this.etablissementDao = etablissementDao;
    }

    public List<Etablissement> getEtablissements() {
        return etablissements;
    }

    public void setEtablissements(List<Etablissement> etablissements) {
        this.etablissements = etablissements;
    }

    public Etablissement getSelectedEtablissement() {
        return selectedEtablissement;
    }

    public void setSelectedEtablissement(Etablissement selectedEtablissement) {
        this.selectedEtablissement = selectedEtablissement;
        if (selectedEtablissement == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    
    public Etablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(Etablissement etablissement) {
        this.etablissement = etablissement;
    }

    public DomaineDao getDomaineDao() {
        return domaineDao;
    }

    public void setDomaineDao(DomaineDao domaineDao) {
        this.domaineDao = domaineDao;
    }
   
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = etablissementDao.findAllEtablissement().isEmpty();
        return imprimer;
    }
    
    public StringBuffer getEtablissementsTableHtml() {
        return etablissementsTableHtml;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
