/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.etablissements;


/**
 *
 * @author macbookpro
 */
public interface EtablissementInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerEtablissement();
    public void modifierEtablissement();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerEtablissement();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerEtablissementPdf();
    
    public void imprimerEtablissementHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
