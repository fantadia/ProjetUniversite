/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.comptable;







/**
 *
 * @author macbookpro
 */
public interface ComptableInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerComptable();
    public void modifierComptable();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerComptable();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerComptablePdf();
    
    public void imprimerComptableHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
