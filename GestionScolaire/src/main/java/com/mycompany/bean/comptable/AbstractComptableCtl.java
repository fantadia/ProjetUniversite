/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.comptable;




import com.mycompany.entities.Comptable;
import com.mycompany.entities.Enseignant;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.ComptableDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EnseignantDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractComptableCtl {
    
    protected ComptableDao comptableDao ;
    protected UtilisateurDao utilisateurDao;
    protected List<Comptable> comptables;
    
    protected StringBuffer comptablesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Comptable selectedComptable;
    protected Comptable comptable;
    protected Utilisateur utilisateur;
    protected Utilisateur selectedUtilisateur;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public ComptableDao getComptablesDao() {
        return comptableDao;
    }

    public void setComptablesDao(ComptableDao comptableDao) {
        this.comptableDao = comptableDao;
    }

    public List<Comptable> getComptables() {
        return comptables;
    }

    public void setComptables(List<Comptable> comptables) {
        this.comptables = comptables;
    }

    public StringBuffer getComptablesTableHtml() {
        return comptablesTableHtml;
    }

    public void setComptablesTableHtml(StringBuffer comptablesTableHtml) {
        this.comptablesTableHtml = comptablesTableHtml;
    }

    public Comptable getSelectedComptable() {
        return selectedComptable;
    }

    public void setSelectedComptable(Comptable selectedComptable) {
        this.selectedComptable = selectedComptable;
    }

    public Comptable getComptable() {
        return comptable;
    }

    public void setComptable(Comptable comptable) {
        this.comptable = comptable;
    }

    public Utilisateur getSelectedUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSelectedUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
    }

    
    
    public UtilisateurDao getUtilisateurDao() {
        return utilisateurDao;
    }

    public void setUtilisateurDao(UtilisateurDao utilisateurDao) {
        this.utilisateurDao = utilisateurDao;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Utilisateur getSeleUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSeleUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
        if (selectedUtilisateur == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = comptableDao.findAllComptable().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
