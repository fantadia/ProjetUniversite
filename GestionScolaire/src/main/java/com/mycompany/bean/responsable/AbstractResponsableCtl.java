/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.responsable;




import com.mycompany.entities.Responsable;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.ResponsableDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractResponsableCtl {
    
    
    protected ResponsableDao responsableDao ;
    protected UtilisateurDao utilisateurDao;
    protected Utilisateur utilisateur;
    protected Utilisateur selectedUtilisateur;
   
    protected EmploisDuTempsDao emploisDuTempsDao;
    
    protected List<Responsable> responsables;
    
    protected StringBuffer responsablesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Responsable selectedResponsable;
    protected Responsable responsable;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public ResponsableDao getResponsableDao() {
        return responsableDao;
    }

    public void setResponsableDao(ResponsableDao responsableDao) {
        this.responsableDao = responsableDao;
    }

    public UtilisateurDao getUtilisateurDao() {
        return utilisateurDao;
    }

    public void setUtilisateurDao(UtilisateurDao utilisateurDao) {
        this.utilisateurDao = utilisateurDao;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public Utilisateur getSelectedUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSelectedUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public EmploisDuTempsDao getEmploisDuTempsDao() {
        return emploisDuTempsDao;
    }

    public void setEmploisDuTempsDao(EmploisDuTempsDao emploisDuTempsDao) {
        this.emploisDuTempsDao = emploisDuTempsDao;
    }

    public List<Responsable> getResponsables() {
        return responsables;
    }

    public void setResponsables(List<Responsable> responsables) {
        this.responsables = responsables;
    }

    public StringBuffer getResponsablesTableHtml() {
        return responsablesTableHtml;
    }

    public void setResponsablesTableHtml(StringBuffer responsablesTableHtml) {
        this.responsablesTableHtml = responsablesTableHtml;
    }

    public Responsable getSelectedResponsable() {
        return selectedResponsable;
    }

    public void setSelectedResponsable(Responsable selectedResponsable) {
        this.selectedResponsable = selectedResponsable;
        if (selectedResponsable == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Responsable getResponsable() {
        return responsable;
    }

    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }

    
   
    
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = responsableDao.findAll().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
