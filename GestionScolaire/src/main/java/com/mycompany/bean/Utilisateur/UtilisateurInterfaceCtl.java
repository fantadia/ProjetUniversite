/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.Utilisateur;




/**
 *
 * @author macbookpro
 */
public interface UtilisateurInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerUtilisateur();
    public void modifierUtilisateur();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerUtilisateur();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerUtilisateurPdf();
    
    public void imprimerUtilisateurHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
