/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.Utilisateur;


import beans.util.JsfUtil;
import com.mycompany.entities.Message;

import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.MessageDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="utilisateurCtl")
@ViewScoped

public class UtilisateurCtl extends AbstractUtilisateurCtl implements UtilisateurInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public  UtilisateurCtl() {
    }
    
    @PostConstruct
    private void initUtilisateur()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        messageDao  = (MessageDao) context.getBean("messageDao");
        utilisateur = (Utilisateur) context.getBean("utilisateur");
        selectedUtilisateur = (Utilisateur) context.getBean("utilisateur");
        utilisateurs = utilisateurDao.findAll();
    }

    @Override
    public void enregistrerUtilisateur() {
        Utilisateur us = utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        Utilisateur user = utilisateurDao.findUtilisateurByPassword(utilisateur.getPassword());
        if ((us != null)||(user!=null)) {
            JsfUtil.addErrorMessage("enregistrement existe deja");
            modifier = supprimer = detail = true;
            return;

        }
        utilisateurDao.createUtilisateur(utilisateur);
        initUtilisateur();
        JsfUtil.addSuccessMessage("le domaine à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierUtilisateur() {
        
        if (selectedUtilisateur == null || selectedUtilisateur.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        utilisateurDao.updateUtilisateur(selectedUtilisateur);
        JsfUtil.addSuccessMessage("Domaine Modifier avec succes");
    }

    @Override
    public void supprimerUtilisateur() {
        
         if(selectedUtilisateur == null || selectedUtilisateur.getCodeutilisateur()== null)
            return;
             
        List<Message> n = (List<Message>) messageDao.findMessageByUtilisateur(selectedUtilisateur.getCodeutilisateur());
        if (!n.isEmpty()) 
        {
         //utils.JsfUtil.addWarningMessage("Le ne peut pas etre supprime : il est deja utilise par une  operation");
          JsfUtil.addErrorMessage("Impossible de supprimer cet utilisateur, car il est deja utilise");
        } 
        else{
            
            utilisateurDao.deleteUtilisateur(selectedUtilisateur);
            JsfUtil.addErrorMessage("user supprimée");
            modifier = supprimer = detail = true;
        }
            
    }

    @Override
    public void imprimerUtilisateurPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerUtilisateurHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
