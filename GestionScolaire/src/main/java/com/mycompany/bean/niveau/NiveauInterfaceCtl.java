/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.niveau;

/**
 *
 * @author macbookpro
 */
public interface NiveauInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerNiveau();
    public void modifierNiveau();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerNiveau();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerNiveauPdf();
    
    public void imprimerNiveauHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
