/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.niveau;

import com.mycompany.entities.Niveau;
import com.nycompany.dao.NiveauDao;
import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.nycompany.dao.ClasseDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="niveauCtl")
@ViewScoped

public class NiveauCtl extends AbstractNiveauCtl implements NiveauInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public NiveauCtl() {
    }
    
    @PostConstruct
    private void initNiveau()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        niveauDao = (NiveauDao) context.getBean("niveauDao");
        classDao = (ClasseDao) context.getBean("classeDao");
        niveau = (Niveau) context.getBean("niveau");
        selectedNiveau = (Niveau) context.getBean("niveau");
        niveaux = niveauDao.findAll();
    }

    @Override
    public void enregistrerNiveau() {
         Niveau nv = niveauDao.findNiveauByLibelle(niveau.getLibelle());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLibelle());
            modifier = supprimer = detail = true;
            return;

        }
        niveauDao.saveNiveau(niveau);
        initNiveau();
        JsfUtil.addSuccessMessage("le niveau à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierNiveau() {
        
        if (selectedNiveau == null || selectedNiveau.getIdniveau()== null) {
            return;
        }

       /*Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        niveauDao.updateNiveau(selectedNiveau);
        JsfUtil.addSuccessMessage("Niveau Modifier avec succes");
    }

    @Override
    public void supprimerNiveau() {
        
         if(selectedNiveau == null || selectedNiveau.getLibelle()== null)
            return;
             
        List<Classe> n = classDao.findClasseByIdNiveau(selectedNiveau.getIdniveau());
        if (!n.isEmpty()) {
            JsfUtil.addErrorMessage("Impossible de supprimer cet enregistrement, il est deja utilise dans une autre classe");
         
        } else {

            niveauDao.deleteNiveau(selectedNiveau);
            JsfUtil.addErrorMessage("Cette niveau à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerNiveauPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerNiveauHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
