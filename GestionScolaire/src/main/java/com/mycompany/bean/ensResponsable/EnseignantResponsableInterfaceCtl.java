/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ensResponsable;



/**
 *
 * @author macbookpro
 */
public interface EnseignantResponsableInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerEnseignantResponsable();
    public void modifierEnseignantResponsable();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerEnseignantResponsable();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerEnseignantResponsablePdf();
    
    public void imprimerEnseignantResponsableHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
