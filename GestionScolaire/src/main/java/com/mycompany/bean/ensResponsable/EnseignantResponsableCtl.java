/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ensResponsable;

import beans.util.JsfUtil;
import com.mycompany.entities.EnseignantResponsable;
import com.nycompany.dao.EnseignantResponsableDao;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="enseignantResponsable")
@ViewScoped

public class EnseignantResponsableCtl extends AbstractEnseignantResponsableCtl implements EnseignantResponsableInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EnseignantResponsableCtl() {
    }
    
    @PostConstruct
    private void initEnseignantResponsable()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        enseignantResponsableDao = (EnseignantResponsableDao) context.getBean("enseignantResponsableDao");
        //classeDao = (ClasseDao) context.getBean("classeDao");
        enseignantResponsable = (EnseignantResponsable) context.getBean("enseignantResponsable");
        selectedEnseignantResponsable = (EnseignantResponsable) context.getBean("enseignantResponsable");
        enseignantResponsables = enseignantResponsableDao.findAllEnseignantResponsable();
    }

    @Override
    public void enregistrerEnseignantResponsable() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        enseignantResponsableDao.createEnseignantResponsable(enseignantResponsable);
        initEnseignantResponsable();
        JsfUtil.addSuccessMessage("l'enseignantResponsable à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEnseignantResponsable() {
        
        if (selectedEnseignantResponsable == null || selectedEnseignantResponsable.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        enseignantResponsableDao.updateEnseignantResponsable(selectedEnseignantResponsable);
        JsfUtil.addSuccessMessage("enseignantResponsable Modifier avec succes");
    }

    @Override
    public void supprimerEnseignantResponsable() {
        
         if(selectedEnseignantResponsable == null || selectedEnseignantResponsable.getCodeutilisateur()== null)
            return;
             
       
            enseignantResponsableDao.deleteEnseignantResponsable(selectedEnseignantResponsable);
            JsfUtil.addErrorMessage("Cet enseignantResponsable à été supprimée");
            modifier = supprimer = detail = true;}

    @Override
    public void imprimerEnseignantResponsablePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEnseignantResponsableHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
    
