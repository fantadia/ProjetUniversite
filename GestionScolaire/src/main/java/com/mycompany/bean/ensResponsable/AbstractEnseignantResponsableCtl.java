/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ensResponsable;




import com.mycompany.entities.Enseignant;
import com.mycompany.entities.EnseignantResponsable;
import com.nycompany.dao.EnseignantDao;
import com.nycompany.dao.EnseignantResponsableDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEnseignantResponsableCtl {
    
    protected EnseignantResponsableDao enseignantResponsableDao ;
    protected List<EnseignantResponsable> enseignantResponsables;
    
    protected EnseignantDao enseignantDao;
    protected Enseignant enseignant;
    protected Enseignant selectedEnseignant;
    
    protected StringBuffer enseignantResponsableTableHtml = new StringBuffer("pas encore implementé") ;
    protected EnseignantResponsable selectedEnseignantResponsable;
    protected EnseignantResponsable enseignantResponsable;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public EnseignantResponsableDao getEnseignantResponsableDao() {
        return enseignantResponsableDao;
    }

    public void setEnseignantResponsableDao(EnseignantResponsableDao enseignantResponsableDao) {
        this.enseignantResponsableDao = enseignantResponsableDao;
    }

    public List<EnseignantResponsable> getEnseignantResponsables() {
        return enseignantResponsables;
    }

    public void setEnseignantResponsables(List<EnseignantResponsable> enseignantResponsables) {
        this.enseignantResponsables = enseignantResponsables;
    }

    public StringBuffer getEnseignantResponsableTableHtml() {
        return enseignantResponsableTableHtml;
    }

    public void setEnseignantResponsableTableHtml(StringBuffer enseignantResponsableTableHtml) {
        this.enseignantResponsableTableHtml = enseignantResponsableTableHtml;
    }

    public EnseignantResponsable getSelectedEnseignantResponsable() {
        return selectedEnseignantResponsable;
    }

    public void setSelectedEnseignantResponsable(EnseignantResponsable selectedEnseignantResponsable) {
        this.selectedEnseignantResponsable = selectedEnseignantResponsable;
        if (selectedEnseignantResponsable == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public EnseignantDao getEnseignantDao() {
        return enseignantDao;
    }

    public void setEnseignantDao(EnseignantDao enseignantDao) {
        this.enseignantDao = enseignantDao;
    }

    public Enseignant getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignant enseignant) {
        this.enseignant = enseignant;
    }

    public Enseignant getSelectedEnseignant() {
        return selectedEnseignant;
    }

    public void setSelectedEnseignant(Enseignant selectedEnseignant) {
        this.selectedEnseignant = selectedEnseignant;
    }
    
    

    public EnseignantResponsable getEnseignantResponsable() {
        return enseignantResponsable;
    }

    public void setEnseignantResponsable(EnseignantResponsable enseignantResponsable) {
        this.enseignantResponsable = enseignantResponsable;
    }

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = enseignantResponsableDao.findAllEnseignantResponsable().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
