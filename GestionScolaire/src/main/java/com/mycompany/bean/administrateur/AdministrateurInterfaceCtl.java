/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.administrateur;








/**
 *
 * @author macbookpro
 */
public interface AdministrateurInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerAdministrateur();
    public void modifierAdministrateur();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerAdministrateur();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerAdministrateurPdf();
    
    public void imprimerAdministrateurHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
