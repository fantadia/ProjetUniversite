/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.administrateur;




import com.mycompany.bean.comptable.*;
import com.mycompany.entities.Administrateur;
import com.mycompany.entities.Comptable;
import com.mycompany.entities.Enseignant;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.AdministrateurDao;
import com.nycompany.dao.ComptableDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EnseignantDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractAdministrateurCtl {
    
    protected AdministrateurDao administrateurDao ;
    protected UtilisateurDao utilisateurDao;
    protected List<Administrateur> administrateurs;
    
    protected StringBuffer administrateursTableHtml = new StringBuffer("pas encore implementé") ;
    protected Administrateur selectedAdministrateur;
    protected Administrateur administrateur;
    protected Utilisateur utilisateur;
    protected Utilisateur selectedUtilisateur;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public AdministrateurDao getAdministrateurDao() {
        return administrateurDao;
    }

    public void setAdministrateurDao(AdministrateurDao administrateurDao) {
        this.administrateurDao = administrateurDao;
    }

    public List<Administrateur> getAdministrateurs() {
        return administrateurs;
    }

    public void setAdministrateurs(List<Administrateur> administrateurs) {
        this.administrateurs = administrateurs;
    }

    public StringBuffer getAdministrateursTableHtml() {
        return administrateursTableHtml;
    }

    public void setAdministrateursTableHtml(StringBuffer administrateursTableHtml) {
        this.administrateursTableHtml = administrateursTableHtml;
    }

    public Administrateur getSelectedAdministrateur() {
        return selectedAdministrateur;
    }

    public void setSelectedAdministrateur(Administrateur selectedAdministrateur) {
        this.selectedAdministrateur = selectedAdministrateur;
    }

    public Administrateur getAdministrateur() {
        return administrateur;
    }

    public void setAdministrateur(Administrateur administrateur) {
        this.administrateur = administrateur;
    }

    
    public Utilisateur getSelectedUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSelectedUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
    }

    
    
    public UtilisateurDao getUtilisateurDao() {
        return utilisateurDao;
    }

    public void setUtilisateurDao(UtilisateurDao utilisateurDao) {
        this.utilisateurDao = utilisateurDao;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Utilisateur getSeleUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSeleUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
        if (selectedUtilisateur == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = administrateurDao.findAllAdministrateur().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
