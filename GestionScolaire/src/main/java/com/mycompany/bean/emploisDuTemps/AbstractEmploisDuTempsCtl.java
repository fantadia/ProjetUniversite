/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.emploisDuTemps;




import com.mycompany.entities.EmploisDuTemps;
import com.nycompany.dao.EmploisDuTempsDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEmploisDuTempsCtl {
    
    
    protected EmploisDuTempsDao emploisDuTempsDao;
   
    protected List<EmploisDuTemps> emploisDuTempses;
    
    protected StringBuffer emploisDuTempsTableHtml = new StringBuffer("pas encore implementé") ;
    protected EmploisDuTemps selectedEmploisDuTemps;
    protected EmploisDuTemps emploisDuTemps;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public EmploisDuTempsDao getEmploisDuTempsDao() {
        return emploisDuTempsDao;
    }

    public void setEmploisDuTempsDao(EmploisDuTempsDao emploisDuTempsDao) {
        this.emploisDuTempsDao = emploisDuTempsDao;
    }

    public List<EmploisDuTemps> getEmploisDuTempses() {
        return emploisDuTempses;
    }

    public void setEmploisDuTempses(List<EmploisDuTemps> emploisDuTempses) {
        this.emploisDuTempses = emploisDuTempses;
    }

    public StringBuffer getEmploisDuTempsTableHtml() {
        return emploisDuTempsTableHtml;
    }

    public void setEmploisDuTempsTableHtml(StringBuffer emploisDuTempsTableHtml) {
        this.emploisDuTempsTableHtml = emploisDuTempsTableHtml;
    }

    public EmploisDuTemps getSelectedEmploisDuTemps() {
        return selectedEmploisDuTemps;
    }

    public void setSelectedEmploisDuTemps(EmploisDuTemps selectedEmploisDuTemps) {
        this.selectedEmploisDuTemps = selectedEmploisDuTemps;
         if (selectedEmploisDuTemps == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public EmploisDuTemps getEmploisDuTemps() {
        return emploisDuTemps;
    }

    public void setEmploisDuTemps(EmploisDuTemps emploisDuTemps) {
        this.emploisDuTemps = emploisDuTemps;
    }
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = emploisDuTempsDao.findAllEmploisDuTemps().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
