/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.emploisDuTemps;


import beans.util.JsfUtil;
import com.mycompany.entities.EmploisDuTemps;
import com.nycompany.dao.EmploisDuTempsDao;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="emploisDuTempsCtl")
@ViewScoped

public class EmploisDuTempsCtl extends AbstractEmploisDuTempsCtl implements EmploisDuTempsInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EmploisDuTempsCtl() {
    }
    
    @PostConstruct
    private void initEmploisDuTemps()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        emploisDuTempsDao = (EmploisDuTempsDao) context.getBean("emploisdutempsDao");
        emploisDuTemps = (EmploisDuTemps) context.getBean("emploisdutemps");
        selectedEmploisDuTemps = (EmploisDuTemps) context.getBean("emploisdutemps");
        emploisDuTempses = emploisDuTempsDao.findAllEmploisDuTemps();
    }

    @Override
    public void enregistrerEmploisDuTemps() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        emploisDuTempsDao.createEmploisDuTemps(emploisDuTemps);
        initEmploisDuTemps();
        JsfUtil.addSuccessMessage("l' EmploisDuTemps à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEmploisDuTemps() {
        
        if (selectedEmploisDuTemps == null || selectedEmploisDuTemps.getCodeEmploisdutemps()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        emploisDuTempsDao.updateEmploisDuTemps(selectedEmploisDuTemps);
        JsfUtil.addSuccessMessage("Ec Modifier avec succes");
    }

    @Override
    public void supprimerEmploisDuTemps() {
        
         if(selectedEmploisDuTemps == null || selectedEmploisDuTemps.getCodeEmploisdutemps()== null)
            return;
       
            emploisDuTempsDao.deleteEmploisDuTemps(selectedEmploisDuTemps);
            JsfUtil.addErrorMessage("Ce Ec à été supprimée");
            modifier = supprimer = detail = true;
    }
    @Override
    public void imprimerEmploisDuTempsPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEmploisDuTempsHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
