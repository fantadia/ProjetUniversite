/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.classes;

import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.EmploisDuTemps;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.EmploisDuTempsDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="classeCtl")
@ViewScoped

public class ClasseCtl extends AbstractClasseCtl implements ClasseInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public ClasseCtl() {
    }
    
    @PostConstruct
    private void initClasse()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        classeDao = (ClasseDao) context.getBean("classeDao");
        emploisDuTempsDao = (EmploisDuTempsDao) context.getBean("emploisdutempsDao");
        classe = (Classe) context.getBean("classe");
        selectedClasse = (Classe) context.getBean("classe");
        classes = classeDao.findAllClasse();
    }

    @Override
    public void enregistrerClasse() {
         Classe nv = classeDao.findClasseByLibelle(classe.getLbelleClasse());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLbelleClasse());
            modifier = supprimer = detail = true;
            return;

        }
        classeDao.createClasse(classe);
        initClasse();
        JsfUtil.addSuccessMessage("la classe à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierClasse() {
        
        if (selectedClasse == null || selectedClasse.getCodeclasse()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        classeDao.updateClasse(selectedClasse);
        JsfUtil.addSuccessMessage("classe Modifier avec succes");
    }

    @Override
    public void supprimerClasse() {
        
         if(selectedClasse == null || selectedClasse.getCodeclasse()== null)
            return;
             
        List<EmploisDuTemps> e = emploisDuTempsDao.findEmploisDuTempsByClasse(selectedClasse.getCodeclasse());
        if (!e.isEmpty()) {
          JsfUtil.addErrorMessage("La classe ne peut pas etre supprime : il est deja utilise dans une  operation");
        } else {

            classeDao.deleteClasse(selectedClasse);
            JsfUtil.addErrorMessage("Cette niveau à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerClassePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerClasseHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
