/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.classes;

import com.mycompany.entities.Classe;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.EmploisDuTempsDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractClasseCtl {
    
    
    protected ClasseDao classeDao ;
    protected EmploisDuTempsDao emploisDuTempsDao;
   
    //protected ClasseDao classDao;
    
    protected List<Classe> classes;
    
    protected StringBuffer classesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Classe selectedClasse;
    protected Classe classe;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public ClasseDao getClasseDao() {
        return classeDao;
    }

    public void setClasseDao(ClasseDao classeDao) {
        this.classeDao = classeDao;
    }

    public EmploisDuTempsDao getEmploisDuTempsDao() {
        return emploisDuTempsDao;
    }

    public void setEmploisDuTempsDao(EmploisDuTempsDao emploisDuTempsDao) {
        this.emploisDuTempsDao = emploisDuTempsDao;
    }

   
    public StringBuffer getClassesTableHtml() {
        return classesTableHtml;
    }

    public void setClassesTableHtml(StringBuffer classesTableHtml) {
        this.classesTableHtml = classesTableHtml;
    }  

    public List<Classe> getClasses() {
        return classes;
    }

    public void setClasses(List<Classe> classes) {
        this.classes = classes;
    }

    public Classe getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classe selectedClasse) {
        this.selectedClasse = selectedClasse;
        if (selectedClasse == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    
    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }
   
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = classeDao.findAllClasse().isEmpty();
        return imprimer;
    }
    
    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
