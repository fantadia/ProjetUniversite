/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.classes;


/**
 *
 * @author macbookpro
 */
public interface ClasseInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerClasse();
    public void modifierClasse();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerClasse();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerClassePdf();
    
    public void imprimerClasseHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
