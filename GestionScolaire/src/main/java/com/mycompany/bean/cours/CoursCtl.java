/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.cours;


import com.mycompany.bean.ec.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Ec;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="coursCtl")
@ViewScoped

public class CoursCtl extends AbstractCoursCtl implements CoursInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public CoursCtl() {
    }
    
    @PostConstruct
    private void initCours()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        coursDao = (CoursDao) context.getBean("coursDao");
        cours = (Cours) context.getBean("cours");
        selectedCours = (Cours) context.getBean("cours");
        courses = coursDao.findAllCours();
    }

    @Override
    public void enregistrerCours() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        coursDao.createCours(cours);
        initCours();
        JsfUtil.addSuccessMessage("le Cours à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierCours() {
        
        if (selectedCours == null || selectedCours.getCodecours()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        coursDao.updateCours(selectedCours);
        JsfUtil.addSuccessMessage("Cours Modifier avec succes");
    }

    @Override
    public void supprimerCours() {
        
         if(selectedCours == null || selectedCours.getCodecours()== null)
            return;
        /* List<Cours>  cours= courDao.findCoursByEc(selectedEc.getCodesemestre());
         if(ues!=null){
             JsfUtil.addErrorMessage("ce Ec ne peut pas etre supprimer,car il est utilise dans une autre operation");
         }*/
       
            coursDao.deleteCours(selectedCours);
            JsfUtil.addErrorMessage("Ce cours à été supprimée");
            modifier = supprimer = detail = true;
    }
    @Override
    public void imprimerCoursPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerCoursHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
