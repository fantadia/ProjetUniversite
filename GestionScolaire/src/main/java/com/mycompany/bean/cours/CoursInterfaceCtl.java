/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.cours;

import com.mycompany.bean.ec.*;






/**
 *
 * @author macbookpro
 */
public interface CoursInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerCours();
    public void modifierCours();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerCours();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerCoursPdf();
    
    public void imprimerCoursHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
