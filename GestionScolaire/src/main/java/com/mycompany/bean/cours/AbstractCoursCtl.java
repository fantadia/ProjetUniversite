/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.cours;





import com.mycompany.entities.Cours;
import com.nycompany.dao.CoursDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractCoursCtl {
    
    
    protected CoursDao coursDao;
    
   
    protected List<Cours> courses;
    
    protected StringBuffer coursesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Cours selectedCours;
    protected Cours cours;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public CoursDao getCoursDao() {
        return coursDao;
    }

    public void setCoursDao(CoursDao coursDao) {
        this.coursDao = coursDao;
    }

    public List<Cours> getCourses() {
        return courses;
    }

    public void setCourses(List<Cours> courses) {
        this.courses = courses;
    }

    public StringBuffer getCoursesTableHtml() {
        return coursesTableHtml;
    }

    public void setCoursesTableHtml(StringBuffer coursesTableHtml) {
        this.coursesTableHtml = coursesTableHtml;
    }

    public Cours getSelectedCours() {
        return selectedCours;
    }

    public void setSelectedCours(Cours selectedCours) {
        this.selectedCours = selectedCours;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = coursDao.findAllCours().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
