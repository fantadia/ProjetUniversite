/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ue;


import com.mycompany.entities.Ue;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractUeCtl {
    
    
    protected UeDao ueDao ;
    protected SemestreDao semestreDao;
    protected EcDao ecDao;
       
    protected List<Ue> ues;
    
    protected StringBuffer uesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Ue selectUe;
    protected Ue ue;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public UeDao getUeDao() {
        return ueDao;
    }

    public void setUeDao(UeDao ueDao) {
        this.ueDao = ueDao;
    }

    public EcDao getEcDao() {
        return ecDao;
    }

    public void setEcDao(EcDao ecDao) {
        this.ecDao = ecDao;
    }
    
    

    public List<Ue> getUes() {
        return ues;
    }

    public void setUes(List<Ue> ues) {
        this.ues = ues;
    }

    public StringBuffer getUesTableHtml() {
        return uesTableHtml;
    }

    public void setUesTableHtml(StringBuffer uesTableHtml) {
        this.uesTableHtml = uesTableHtml;
    }

    public Ue getSelectUe() {
        return selectUe;
    }

    public void setSelectUe(Ue selectUe) {
        this.selectUe = selectUe;
    }

    public Ue getUe() {
        return ue;
    }

    public void setUe(Ue ue) {
        this.ue = ue;
    }

   

    public SemestreDao getSemestreDao() {
        return semestreDao;
    }

    public void setSemestreDao(SemestreDao semestreDao) {
        this.semestreDao = semestreDao;
    }
    
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = ueDao.findAllUe().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
