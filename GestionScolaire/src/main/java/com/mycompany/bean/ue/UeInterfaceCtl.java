/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ue;






/**
 *
 * @author macbookpro
 */
public interface UeInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerUe();
    public void modifierUe();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerUe();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerUePdf();
    
    public void imprimerUeHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
