/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.semestre;




import com.mycompany.entities.Semestre;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractSemestreCtl {
    
    
    protected SemestreDao semestreDao ;
    protected UeDao ueDao;
    protected EmploisDuTempsDao emploisDuTempsDao;
    
   
    protected List<Semestre> semestres;
    
    protected StringBuffer semestresTableHtml = new StringBuffer("pas encore implementé") ;
    protected Semestre selectedSemestre;
    protected Semestre semestre;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public SemestreDao getSemestreDao() {
        return semestreDao;
    }

    public void setSemestreDao(SemestreDao semestreDao) {
        this.semestreDao = semestreDao;
    }

    public EmploisDuTempsDao getEmploisDuTempsDao() {
        return emploisDuTempsDao;
    }

    public void setEmploisDuTempsDao(EmploisDuTempsDao emploisDuTempsDao) {
        this.emploisDuTempsDao = emploisDuTempsDao;
    }

    public List<Semestre> getSemestres() {
        return semestres;
    }

    public void setSemestres(List<Semestre> semestres) {
        this.semestres = semestres;
    }

    public StringBuffer getSemestresTableHtml() {
        return semestresTableHtml;
    }

    public void setSemestresTableHtml(StringBuffer semestresTableHtml) {
        this.semestresTableHtml = semestresTableHtml;
    }

    public Semestre getSelectedSemestre() {
        return selectedSemestre;
    }

    public void setSelectedSemestre(Semestre selectedSemestre) 
    {
        this.selectedSemestre = selectedSemestre;
        if (selectedSemestre == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public UeDao getUeDao() {
        return ueDao;
    }

    public void setUeDao(UeDao ueDao) {
        this.ueDao = ueDao;
    }
    
    

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = semestreDao.findAllSemestre().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
