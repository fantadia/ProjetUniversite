/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.semestre;


import beans.util.JsfUtil;
import com.mycompany.entities.EmploisDuTemps;

import com.mycompany.entities.Semestre;
import com.mycompany.entities.Ue;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="semestreCtl")
@ViewScoped

public class SemestreCtl extends AbstractSemestreCtl implements SemestreInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public SemestreCtl() {
    }
    
    @PostConstruct
    private void initSemestre()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        semestreDao = (SemestreDao) context.getBean("semestreDao");
        emploisDuTempsDao = (EmploisDuTempsDao) context.getBean("emploisdutempsDao");
        ueDao = (UeDao) context.getBean("ueDao");
        semestre = (Semestre) context.getBean("semestre");
        selectedSemestre = (Semestre) context.getBean("semestre");
        semestres = semestreDao.findAllSemestre();
    }

    @Override
    public void enregistrerSemestre() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        semestreDao.createSemestre(semestre);
        initSemestre();
        JsfUtil.addSuccessMessage("le semestre à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierSemestre() {
        
        if (selectedSemestre == null || selectedSemestre.getCodesemestre()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        semestreDao.updateSemestre(selectedSemestre);
        JsfUtil.addSuccessMessage("semestre Modifier avec succes");
    }

    @Override
    public void supprimerSemestre() {
        
         if(selectedSemestre == null || selectedSemestre.getCodesemestre()== null)
            return;
         List<Ue> ues = ueDao.findUeBySemestre(selectedSemestre.getCodesemestre());
         List<EmploisDuTemps> emp = emploisDuTempsDao.findEmploisDuTempsBySemestre(selectedSemestre.getCodesemestre());
         if((ues!=null)||(emp!=null))
         {
             JsfUtil.addErrorMessage("ce semestre ne peut pas etre supprimer,car il est utilise dans une autre operation");
         }
       
            semestreDao.deleteSemestre(selectedSemestre);
            JsfUtil.addErrorMessage("Ce semestre à été supprimée");
            modifier = supprimer = detail = true;
    }
        
    @Override
    public void imprimerSemestrePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerSemestreHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
