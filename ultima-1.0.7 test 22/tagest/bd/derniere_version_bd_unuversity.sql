
create table classes (
   id                   serial                         not null,
   denomination        character  varying(254)         not null,
   nbreetudiant         integer                        not null default 0,
    constraint pk_classes primary key (id),
    constraint denomination_uc unique (denomination)

);



create table compteuser (
   id                  serial                         not null,
   personnel           integer                        not null,
   login               character  varying(254)        not null,
   motdepass           character  varying(254)        not null,
   etat                 boolean                       not null default true,
   constraint pk_compteuser primary key (id),
   constraint perso_log_pass_uc unique (personnel,login,motdepass)

);



create table departement (
   id                   serial                    not null,
   nomdepat             integer                   not null,
   description         character  varying(254)    null,
   etat                 boolean                  not null default true,
   constraint pk_departement primary key (id),
   constraint nomdepat_uc unique (nomdepat)
);


create table emploidetemps (
   id                   serial                     not null,
   titre                character  varying(254)     not null,
   periodedebut         date                       not null,
   periodefin           date                       not null,
   etat                 boolean                    not null default true,
   constraint pk_emploidetemps primary key (id),
   constraint periode_debut_fin unique (periodedebut, periodefin)
);


create table enseignants (
   id                  serial                          not null,
   matricule           character  varying(254)         not null,
   premon              character  varying(254)         null,
   nom                 character  varying(254)         not null,
   age                  integer                        null,
   sexe                character  varying(254)         not null,
   datenais             date                           null,
   lieunais            character  varying(254)         null,
   paysnais            character  varying(254)         not null,
   nationalite         character  varying(254)         not null,
   situationmatrimoniale character  varying(254)       not null,
   telephone            integer                        not null,
   email1               character  varying(254)       not null,
   email                character  varying(254)        null,
    constraint pk_enseignants primary key (id),
    constraint matricule_enseignant_uc unique (matricule)
);



create table etudiants (
   id                   serial                       not null,
   classe              integer                       not null,
   annee                integer                         not null,
   numerocarte         character  varying(254)       not null,
   premon              character  varying(254)       null,
   nom                 character  varying(254)       not null,
   age                  integer                      null,
   sexe                character  varying(254)       not null,
   datenais             date                         not null,
   lieunais            character  varying(254)       not null,
   paysnais            character  varying(254)       not null,
   nationalite         character  varying(254)       not null,
   telephone            integer                      not null,
   email1               character  varying(254)       not null,
   email               character  varying(254)       null,
   constraint pk_etudiants primary key (id),
    constraint numerocarte_uc unique (numerocarte)
);


create table filieres (
   id                   serial                       not null,
   departement          integer                      not null,
   nomfiliere          character  varying(254)       not null,
   description         character  varying(254)       null,
   etat                boolean                       not null default true,
   constraint pk_filieres primary key (id),
    constraint nomfiliere_uc unique (nomfiliere)
);


create table historiquedeliberation (
   id                   serial                       not null,
   note                 integer                      not null,
   pv                   character  varying(254)      null,
   etat                 boolean                      not null default true,
   seuil                integer                      null,
    constraint pk_historiquedeliberation primary key (id)
);

create table matiersmodule (
   id                   serial                   not null,
   uniteenseignement    integer                  not null,
   nommatiere           character  varying(254)  not null,
   guotahoraire         integer                  not null default 0,
   guotahorairerestant  integer                  not null default 0,
   credit               integer                  not null default 0,
   constraint pk_matiersmodule primary key (id),
    constraint nommatiere_uc unique (nommatiere)
);


create table mois (
   id                   serial                         not null,
   nom                character  varying(254)         not null,
   constraint pk_mois primary key (id),
    constraint mois_uc unique (nom)
);


create table personnel (
   id                   serial                       not null,
   poste               integer                       not null,
   matricule           character  varying(254)       not null,
   nom                 character  varying(254)       not null,
   age                 integer                       null,
   date_naissance       date                         null,
   lieu_naissance      character  varying(254)       null,
   pays_de_naissance   character  varying(254)       null,
   nationalite         character  varying(254)       not null,
   situation_matrimoniale character  varying(254)    not null,
   telephone              integer                    not null,
   email1                character  varying(254)     not null,
   email                character  varying(254)       null,
   etat                 boolean                       not null default true,
    constraint pk_personnel primary key (id),
    constraint matricule_perso_uc unique (matricule)
);


create table postepersonnel (
   id                   serial                          not null,
   nom                  character  varying(254)         not null,
   description          character  varying(254)         null,
   constraint pk_postepersonnel primary key (id),
   constraint nomposteperso_uc unique (nom)
);

create table privilege (
   id                   serial                         not null,
   nomprivilege        character  varying(254)         null,
   description         character  varying(254)         null,
   hierarchie          character  varying(254)         null,
   etat                 boolean                        not null default true,
   constraint pk_privilege primary key (id),
    constraint nomprivilege_uc unique (nomprivilege)
);


create table privilegeperso (
   id                     serial                  not null,
   personnel              integer                 not null,
   privilege              integer                 not null,
   constraint pk_privilegeperso primary key (id),
   constraint perso_privilege unique (personnel,privilege)
);

create table roles (
   id                   serial                         not null,
   nom                 character  varying(254)         null,
   etat                 boolean                        not null default true,
    constraint pk_role primary key (id),
    constraint nom_role unique (nom)

);



create table role_privilege (
   id                     serial                  not null,
   privilege              integer                 not null,
   roles                   integer                 not null,
    constraint pk_role_privilege primary key (id)
);

   
   
create table rolepersonnel (
   id                    serial                  not null,
   personnel             integer                 not null,
   roles                  integer                 not null,
    constraint pk_rolepersonnel primary key (id),
    constraint role_peronnel_uc unique (personnel,roles)  

);



create table salles (
   id                   serial                         not null,
   nomsalle            character  varying(254)         not null,
   etat                 boolean                        not null default true,
   constraint pk_salles primary key (id),
    constraint nomsalle_uc unique (nomsalle)
);


create table seancecours (
   
   enseignant           integer                 not null,
   classe               integer                 not null,
   matiere              integer                 not null,
   emploitps            integer                 not null,
   salle                integer                 not null,
   horaire              integer                 not null,
   jour                 date                    not null,
    constraint pk_seancecours primary key (enseignant,classe,matiere,emploitps,salle,horaire),
    constraint seancecours_uc unique (salle,horaire,jour)
  
);


create table horaires(
   id                   serial               not null,
   heuredebut           integer                 null,
   heurefin             integer                 null,
    constraint pk_horaire primary key (id),
    constraint hdeb_hfin_uc unique (heuredebut,heurefin)
   
);


create table uniteenseignements (
   id                   serial                    not null,
   filiere              integer                   not null,
   nom                  character  varying(254)   not null,
   description          character  varying(254)   null,
   credit               integer                   not null default 0,
   etat                 boolean                   not null default true,
   constraint pk_uniteenseignement primary key (id),
    constraint nom_unitenseignement unique (nom)
);


create table annee_scolaire (
   id                   serial                   not null,
   annee                integer                     not null,
   semestre            character  varying(254)   null,
   etat                 boolean                  not null default true,
   constraint pk_annee_scolaire primary key (id),
    constraint annee_uc unique (annee)
);



create table classematiere (
   id                        serial                  not null,
   classe                    integer                 not null,
   matiere                   integer                 not null,
    constraint pk_classematiere primary key (id)
);



create table comptabilite (
   id                   serial               not null,
   mois                 integer              not null,
   etudiant             integer              not null,
   montant              double precision     not null default 0.0,
   montanttotal         double precision     not null default 0.0,
   dates                 date                 not null,
   etat                 boolean              not null default true,
   constraint pk_comptabilite primary key (id),
   constraint mois_payer_etudiant unique (mois,etudiant)
);


create table notes (
   id                   serial                  not null,
   enseignant           integer                 not null,
   etudiant             integer                 not null,
   matiere              integer                 not null,
   note                double precision       not null default 0.0,
   notedeliberer        double precision       not null default 0.0,
   credit               integer                not null default 0,
    constraint pk_note primary key (id),
    constraint not_etudiant_matiere unique (etudiant,matiere,note)
);
alter table compteuser
   add constraint fk_compteus_personne foreign key (personnel)
      references personnel (id)
      on delete restrict on update restrict;
      
alter table etudiants
   add constraint fk_etudiant_annee_sc foreign key (annee)
      references annee_scolaire (id)
      on delete restrict on update restrict;

alter table etudiants
   add constraint fk_etudiant_classes foreign key (classe)
      references classes (id)
      on delete restrict on update restrict;

alter table filieres
    add constraint fk_filieres_departem foreign key (departement)
      references departement (id)
      on delete restrict on update restrict;

alter table historiquedeliberation
   add constraint fk_historiq_note foreign key (note)
      references notes (id)
      on delete restrict on update restrict;

alter table matiersmodule
   add constraint fk_matiersm_uniteens foreign key (uniteenseignement)
      references uniteenseignements (id)
      on delete restrict on update restrict;

alter table personnel
    add constraint fk_personne_posteper foreign key (poste)
      references postepersonnel (id)
      on delete restrict on update restrict;


alter table privilegeperso
   add constraint fk_privileg_personne foreign key (personnel)
      references personnel (id)
      on delete restrict on update restrict;

alter table privilegeperso
   add constraint fk_privileg_privileg foreign key (privilege)
      references privilege (id)
      on delete restrict on update restrict;

alter table role_privilege
   add constraint fk_role_pri_privileg foreign key (privilege)
      references privilege (id)
      on delete restrict on update restrict;

alter table role_privilege
   add constraint fk_role_pri_role foreign key (roles)
      references roles (id)
      on delete restrict on update restrict;

alter table rolepersonnel
   add constraint fk_rolepers_personne foreign key (personnel)
      references personnel (id)
      on delete restrict on update restrict;

alter table rolepersonnel
   add constraint fk_rolepers_role foreign key (roles)
      references roles (id)
      on delete restrict on update restrict;

alter table seancecours
    add constraint fk_seanceco_horaire foreign key (horaire)
      references horaires (id)
      on delete restrict on update restrict;

alter table seancecours
   add constraint fk_seanceco_classes foreign key (classe)
      references classes (id)
      on delete restrict on update restrict;

alter table seancecours
    add constraint fk_seanceco_enseigna foreign key (enseignant)
      references enseignants (id)
      on delete restrict on update restrict;

alter table seancecours
    add constraint fk_seanceco_matiersm foreign key (matiere)
      references matiersmodule (id)
      on delete restrict on update restrict;

alter table seancecours
    add constraint fk_seanceco_emploide foreign key (emploitps)
      references emploidetemps (id)
      on delete restrict on update restrict;

alter table seancecours
     add constraint fk_seanceco_salles foreign key (salle)
      references salles (id)
      on delete restrict on update restrict;

alter table uniteenseignements
    add constraint fk_uniteens_filieres foreign key (filiere)
      references filieres (id)
      on delete restrict on update restrict;

alter table classematiere
    add constraint fk_classema_classes foreign key (classe)
      references classes (id)
      on delete restrict on update restrict;

alter table classematiere
   add  constraint fk_classema_matiersm foreign key (matiere)
      references matiersmodule (id)
      on delete restrict on update restrict;

alter table comptabilite
  add  constraint fk_comptabi_etudiant foreign key (etudiant)
      references etudiants (id)
      on delete restrict on update restrict;

alter table comptabilite
  add  constraint fk_comptabi_mois foreign key (mois)
      references mois (id)
      on delete restrict on update restrict;

alter table notes
   add constraint fk_note_enseigna foreign key (enseignant)
      references enseignants (id)
      on delete restrict on update restrict;

alter table notes
    add constraint fk_note_matiersm foreign key (matiere)
      references matiersmodule (id)
      on delete restrict on update restrict;

alter table notes
    add constraint fk_note_etudiant foreign key (etudiant)
      references etudiants (id)
      on delete restrict on update restrict;