create table charge 
(
   id                   serial                                    not null,
   libelle              character  varying(254)                   not null,
   reference_charge      character  varying(254)                  not null,
   etat                 boolean                                   not null default true,
   constraint pk_charge primary key (id),
   constraint charge_uc unique (libelle),
   constraint charge_reference_uc unique (reference_charge)
);



create table commercial 
(
   id                   serial                         not null,
   nom                  character  varying(254)        not null,
   prenom               character  varying(254)        null,
   date_entree          date                           not null,
   date_sortie          date                           null,
   telephone            character  varying(254)        null,
   email                character  varying(254)        null,
   sexe                 character  varying(254)        not null,
   constraint pk_commercial primary key (id),
    constraint commercial_uc unique (nom, prenom)
);


create table compte 
(
   id                     serial                         not null,
   devise                 integer                        not null,
   type_compte             integer                        not null,
   intitule              character  varying(254)        not null,
   solde_ouverture       double precision              not null default 0.0,
   solde_courant         double precision              not null default 0.0,
   solde_fermeture       double precision              not null default 0.0,
   date_operation        date                          not null,
   constraint pk_compte primary key (id),
   constraint intitule_uc unique (intitule)
);


create table depot 
(
   id                    serial                          not null,
   mois                 integer                         not null,
   type_depot           integer                         not null,
   compte               integer                         not null,
   devise               integer                         not null,
   montant              double precision                not null default 0.0,
   date_depot           date                            null,
   constraint pk_depot primary key (id)
);


create table devise 
(
   id                   serial                         not null,
   devise               character  varying(254)        not null,
   symbole              character  varying(254)        not null,
   constraint pk_devise primary key (id),
   constraint devise_uc unique (devise),
   constraint symbole_uc unique (symbole)
);



create table echange_devise 
(   
   id                     serial                          not null,
   devise_src             integer                         not null,
   devise_dest            integer                         not null,
   mois                   integer                         not null,
   taux_echange           double precision                not null default 0.0,
   date_echange           date                            null,
   etat                   boolean                         not null default true,
   constraint pk_echange_devise primary key (id)
);



create table mode_paiement 
(
   id                   serial                         not null,
   libelle              character  varying(254)        not null,
   constraint pk_mode_paiement primary key (id),
   constraint mode_paiement_uc unique (libelle)
);



create table mois 
(
   id                   serial                         not null,
   mois                 character  varying(254)        not null,
   constraint pk_mois primary key (id),
   constraint mois_uc unique (mois)
);



create table mouvement 
(
   id                       serial                          not null,
   compte_src               integer                         not null,
   compte_dest              integer                         not null,
   devise                   integer                         not null,
   montant                  double precision                not null default 0.0,
   constraint pk_mouvement primary key (id)
);



create table objectif 
(
   id                   serial                         not null,
   mois                 integer                        not null,
   commercial           integer                        not null,
   devise               integer                        not null,
   montant              double precision               not null default 0.0        ,
   etat                 boolean                        not null default true,
   chiffre_affaire      double precision               not null default 0.0,
   montant_realiser     double precision                not null default 0.0,
   constraint pk_objectif primary key (id)
);



create table operation_charge 
(
   id                        serial                          not null,
   charge                    integer                         not null,
   compte                    integer                         not null,
   type_operation            integer                         not null,
   commercial                integer                         not null,
   devise                    integer                         not null,
   mode_paiement             integer                         not null,
   description          character  varying(254)              null,
   commantaire          character  varying(254)              null,
   date_operation        date                                null,
   montant              double precision                     not null default 0.0,
   constraint pk_operation_charge primary key (id)
);

create table operation_produit 
(
   id                        serial                          not null,
   produit                   integer                         not null,
   compte                    integer                         not null,
   type_operation             integer                         not null,
   commercial                integer                         not null,
   devise                    integer                         not null,
   mode_paiement             integer                         not null,
   description          character  varying(254)              null,
   commantaire          character  varying(254)              null,
   date_operation        date                                null,
   montant              double precision                     not null default 0.0,
   constraint pk_operation_produit primary key (id)
);



create table prevision_charge 
(
   id                    serial                          not null,
   devise                integer                         not null,
   mois                  integer                         not null,
   charge                integer                         not null,
   montant               double precision                not null default 0.0,
   montant_realiser      double precision                not null default 0.0,
   statut                character  varying(254)         null,
   constraint pk_prevision_charge primary key (id)
);


create table prevision_produit 
(
   id                   serial                          not null,
   produit              integer                         not null,
   mois                 integer                         not null,
   devise               integer                         not null,
   montant               double precision               not null default 0.0,
   montant_realiser      double precision               not null default 0.0,
   statut               character  varying(254)         null,
   constraint pk_prevision_produit primary key (id)
);


create table produit 
(
   id                    serial                         not null,
   libelle               character  varying(254)        not null,
   reference_produit     character  varying(254)        not null,
   etat                  boolean                        not null default true,
   constraint pk_produit primary key (id),
   constraint produit_uc unique (libelle),
    constraint reference_produit_uc unique (reference_produit)
);


create table retrait 
(
   id                      serial                         not null,
   compte                  integer                        not null,
   montant                 double precision               not null default 0.0,
   date_retrait            date                           null,
   constraint pk_retrait primary key (id)
);


create table sous_traitant 
(
   id                   serial                                     not null,
   nom                  character  varying(254)                    not null,
   prenom               character  varying(254)                    not null,
   telephone            character  varying(254)                    not null,
   email                character  varying(254)                    null,
   sexe                 character  varying(254)                    not null,
   constraint pk_sous_traitant primary key (id)
   
);


create table travaille 
(
   id                       serial                          not null,
   commercial               integer                         not null,
   sous_traitant            integer                         not null,
   mois                     integer                         not null,
   montant_remunere         double precision                not null default 0.0,
   date_remuneration        date                            null,
   constraint pk_travaille primary key(id)
    
);


create table type_compte 
(
   id                   serial                         not null,
   libelle              character  varying(254)        not null,
   constraint pk_type_compte primary key (id),
   constraint type_compte_uc unique (libelle)
);


create table type_depot 
(
   id                   serial                         not null,
   libelle              character  varying(254)        not null,
   constraint pk_type_depot primary key (id),
   constraint type_depot_uc unique (libelle)
);


create table type_operation 
(
   id                   serial                         not null,
   libelle              character  varying(254)        not null,
   constraint pk_type_operation primary key (id),
   constraint type_operation_uc unique (libelle)
);


alter table compte
   add constraint fk_compte_typecompte foreign key (type_compte)
      references type_compte (id)
      on update restrict
      on delete restrict;

alter table compte
   add constraint fk_compte_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;

alter table depot
   add constraint fk_depot_typedepot foreign key (type_depot)
      references type_depot (id)
      on update restrict
      on delete restrict;

alter table depot
   add constraint fk_depot_compte foreign key (compte)
      references compte (id)
      on update restrict
      on delete restrict;

alter table depot
   add constraint fk_depot_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;


alter table depot
   add constraint fk_depot_mois foreign key (mois)
      references mois (id)
      on update restrict
      on delete restrict;

alter table echange_devise
   add constraint fk_echange__mois foreign key (mois)
      references mois (id)
      on update restrict
      on delete restrict;

alter table echange_devise
   add constraint fk_echange_devise_src foreign key (devise_src)
      references devise (id)
      on update restrict
      on delete restrict;

alter table echange_devise
   add constraint fk_echange_devise_dest foreign key (devise_dest)
      references devise (id)
      on update restrict
      on delete restrict;

alter table mouvement
   add constraint fk_mouvemen_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;

alter table mouvement
   add constraint fk_mouvemen_compte_src foreign key (compte_src)
      references compte (id)
      on update restrict
      on delete restrict;

alter table mouvement
   add constraint fk_mouvemen_compte_dest foreign key (compte_dest)
      references compte (id)
      on update restrict
      on delete restrict;	  
	  

alter table objectif
   add constraint fk_objectif_commercial foreign key (commercial)
      references commercial (id)
      on update restrict
      on delete restrict;

alter table objectif
   add constraint fk_objectif_mois foreign key (mois)
      references mois (id)
      on update restrict
      on delete restrict;

alter table objectif
   add constraint fk_objectif_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;

alter table operation_charge
   add constraint fk_operation_typeoperation foreign key (type_operation)
      references type_operation (id)
      on update restrict
      on delete restrict;

alter table operation_charge
   add constraint fk_operation_modepaiement foreign key (mode_paiement)
      references mode_paiement (id)
      on update restrict
      on delete restrict;

alter table operation_charge
   add constraint fk_operation_compte foreign key (compte)
      references compte (id)
      on update restrict
      on delete restrict;

alter table operation_charge
   add constraint fk_operation_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;

alter table operation_charge
   add constraint fk_operation_commercial foreign key (commercial)
      references commercial (id)
      on update restrict
      on delete restrict;

alter table operation_charge
   add constraint fk_operation_charge foreign key (charge)
      references charge (id)
      on update restrict
      on delete restrict;
	  
	  
alter table operation_produit
   add constraint fk_operation_typeoperation foreign key (type_operation)
      references type_operation (id)
      on update restrict
      on delete restrict;

alter table operation_produit
   add constraint fk_operation_modepaiement foreign key (mode_paiement)
      references mode_paiement (id)
      on update restrict
      on delete restrict;

alter table operation_produit
   add constraint fk_operation_compte foreign key (compte)
      references compte (id)
      on update restrict
      on delete restrict;

alter table operation_produit
   add constraint fk_operation_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;

alter table operation_produit
   add constraint fk_operation_commercial foreign key (commercial)
      references commercial (id)
      on update restrict
      on delete restrict;

alter table operation_produit
   add constraint fk_operation_produit foreign key (produit)
      references produit (id)
      on update restrict
      on delete restrict;

	  

alter table prevision_charge
   add constraint fk_prevision_mois foreign key (mois)
      references mois (id)
      on update restrict
      on delete restrict;

alter table prevision_charge
   add constraint fk_prevision_charge foreign key (charge)
      references charge (id)
      on update restrict
      on delete restrict;

alter table prevision_charge
   add constraint fk_prevision_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;

alter table prevision_produit
   add constraint fk_prevision_mois foreign key (mois)
      references mois (id)
      on update restrict
      on delete restrict;

alter table prevision_produit
   add constraint fk_prevision_produit foreign key (produit)
      references produit (id)
      on update restrict
      on delete restrict;

alter table prevision_produit
   add constraint fk_prevision_devise foreign key (devise)
      references devise (id)
      on update restrict
      on delete restrict;

alter table retrait
   add constraint fk_retrait_compte foreign key (compte)
      references compte (id)
      on update restrict
      on delete restrict;

alter table travaille
   add constraint fk_travaille_mois foreign key (mois)
      references mois (id)
      on update restrict
      on delete restrict;

alter table travaille
   add constraint fk_travaille_commercial foreign key (commercial)
      references commercial (id)
      on update restrict
      on delete restrict;

alter table travaille
   add constraint fk_travaille_soustraitant foreign key (sous_traitant)
      references sous_traitant (id)
      on update restrict
      on delete restrict;

