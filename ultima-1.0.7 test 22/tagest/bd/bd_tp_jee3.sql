create table classes (
   id                   serial               not null,
   denomination         varchar(254)         null,
   nbreetudiant         int4                 null
);

alter table classes
   add constraint pk_classes primary key (id);

create table compteuser (
   id                   serial               not null,
   per_id               int4                 not null,
   login                varchar(254)         null,
   motdepass            varchar(254)         null,
   etat                 bool                 null
);

alter table compteuser
   add constraint pk_compteuser primary key (id);

create table departement (
   id                   serial               not null,
   nomdept              varchar(254)         null,
   description          varchar(254)         null,
   etat                 int4                 null
);

alter table departement
   add constraint pk_departement primary key (id);

create table emploidetemps (
   id                   serial               not null,
   titre                varchar(254)         null,
   periodedebut         date                 null,
   periodefin           date                 null,
   etat                 bool                 null
);

alter table emploidetemps
   add constraint pk_emploidetemps primary key (id);

create table enseignants (
   id                   serial               not null,
   matricule            varchar(254)         null,
   premon               varchar(254)         null,
   nom                  varchar(254)         null,
   age                  int4                 null,
   sexe                 varchar(254)         null,
   datenais             date                 null,
   lieunais             varchar(254)         null,
   paysnais             varchar(254)         null,
   nationalite          varchar(254)         null,
   situationmatrimoniale int4                 null,
   telephone            int4                 null,
   email                varchar(254)         null
);

alter table enseignants
   add constraint pk_enseignants primary key (id);

create table etudiants (
   id                   serial               not null,
   cla_id               int4                 not null,
   ann_id               int4                 not null,
   numerocarte          varchar(254)         null,
   premon               varchar(254)         null,
   nom                  varchar(254)         null,
   age                  int4                 null,
   sexe                 varchar(254)         null,
   datenais             date                 null,
   lieunais             varchar(254)         null,
   paysnais             varchar(254)         null,
   nationalite          varchar(254)         null,
   telephone            int4                 null,
   email                varchar(254)         null
);

alter table etudiants
   add constraint pk_etudiants primary key (id);

create table filieres (
   id                   serial               not null,
   dep_id               int4                 not null,
   nomfiliere           varchar(254)         null,
   description          varchar(254)         null,
   etat                 int4                 null
);

alter table filieres
   add constraint pk_filieres primary key (id);

create table historiquedeliberation (
   id                   serial               not null,
   not_id               int4                 not null,
   pv                   varchar(254)         null,
   etat                 bool                 null,
   seuil                numeric              null
);

alter table historiquedeliberation
   add constraint pk_historiquedeliberation primary key (id);

create table horaire (
   id                   serial               not null,
   heuredebut           int4                 null,
   heurefin             int4                 null,
   jour                 date                 null
);

alter table horaire
   add constraint pk_horaire primary key (id);

create table matiersmodule (
   id                   serial               not null,
   uni_id               int4                 not null,
   nommatiere           varchar(254)         null,
   guotahoraire         int4                 null,
   guotahorairerestant  int4                 null,
   credit               int4                 null
);

alter table matiersmodule
   add constraint pk_matiersmodule primary key (id);

create table mois (
   id                   serial               not null,
   mois                 varchar(254)         null
);

alter table mois
   add constraint pk_mois primary key (id);

create table personnel (
   id                   serial               not null,
   pos_id               int4                 not null,
   matricule            varchar(254)         null,
   poste                varchar(254)         null,
   nom                  varchar(254)         null,
   titre                varchar(254)         null,
   age                  int4                 null,
   date_naissance       date                 null,
   lieu_naissance       varchar(254)         null,
   pays_de_naissance    varchar(254)         null,
   nationalite          varchar(254)         null,
   situation_matrimoniale varchar(254)         null,
   tel                  int4                 null,
   email                varchar(254)         null,
   etat                 int4                 null
);

alter table personnel
   add constraint pk_personnel primary key (id);

create table postepersonnel (
   id                   serial               not null,
   nom                  varchar(254)         null,
   description          varchar(254)         null
);

alter table postepersonnel
   add constraint pk_postepersonnel primary key (id);

create table privilege (
   id                   serial               not null,
   nomprivilege         varchar(254)         null,
   description          varchar(254)         null,
   hierarchie           varchar(254)         null,
   etat                 bool                 null
);

alter table privilege
   add constraint pk_privilege primary key (id);

create table privilegeperso (
   id                   int4                 not null,
   pri_id               int4                 not null
);

alter table privilegeperso
   add constraint pk_privilegeperso primary key (id, pri_id);

create table role (
   id                   serial               not null,
   nom                  varchar(254)         null,
   etat                 bool                 null
);

alter table role
   add constraint pk_role primary key (id);

create table rolepersonnel (
   id                   int4                 not null,
   rol_id               int4                 not null
);

alter table rolepersonnel
   add constraint pk_rolepersonnel primary key (id, rol_id);

create table role_privilege (
   id                   int4                 not null,
   rol_id               int4                 not null
);

alter table role_privilege
   add constraint pk_role_privilege primary key (id, rol_id);

create table salles (
   id                   serial               not null,
   nomsalle             varchar(254)         null,
   etat                 bool                 null
);

alter table salles
   add constraint pk_salles primary key (id);

create table seancecours (
   id                   serial               not null,
   ens_id               int4                 not null,
   hor_id               int4                 not null,
   cla_id               int4                 not null,
   mat_id               int4                 not null,
   emp_id               int4                 not null,
   sal_id               int4                 not null
);

alter table seancecours
   add constraint pk_seancecours primary key (id);

create table uniteenseignement (
   id                   serial               not null,
   fil_id               int4                 not null,
   nom                  varchar(254)         null,
   description          varchar(254)         null,
   credit               int4                 null,
   etat                 int4                 null
);

alter table uniteenseignement
   add constraint pk_uniteenseignement primary key (id);

create table annee_scolaire (
   id                   serial               not null,
   annee                date                 null,
   semestre             varchar(254)         null,
   etat                 bool                 null
);

alter table annee_scolaire
   add constraint pk_annee_scolaire primary key (id);

create table classematiere (
   cla_id               int4                 not null,
   id                   int4                 not null
);

alter table classematiere
   add constraint pk_classematiere primary key (cla_id, id);

create table comptabilite (
   id                   serial               not null,
   moi_id               int4                 not null,
   etu_id               int4                 not null,
   montant              numeric              null,
   montanttotal         numeric              null,
   date                 date                 null,
   etat                 bool                 null
);

alter table comptabilite
   add constraint pk_comptabilite primary key (id);

create table note (
   id                   serial               not null,
   ens_id               int4                 not null,
   etu_id               int4                 not null,
   mat_id               int4                 not null,
   note                 varchar(254)         null,
   notedeliberer        int4                 null,
   credit               int4                 null
);

alter table note
   add constraint pk_note primary key (id);

alter table compteuser
   add constraint fk_compteus_associati_personne foreign key (per_id)
      references personnel (id)
      on delete restrict on update restrict;

alter table etudiants
   add constraint fk_etudiant_associati_annee_sc foreign key (ann_id)
      references annee_scolaire (id)
      on delete restrict on update restrict;

alter table etudiants
   add constraint fk_etudiant_associati_classes foreign key (cla_id)
      references classes (id)
      on delete restrict on update restrict;

alter table filieres
   add constraint fk_filieres_associati_departem foreign key (dep_id)
      references departement (id)
      on delete restrict on update restrict;

alter table historiquedeliberation
   add constraint fk_historiq_associati_note foreign key (not_id)
      references note (id)
      on delete restrict on update restrict;

alter table matiersmodule
   add constraint fk_matiersm_associati_uniteens foreign key (uni_id)
      references uniteenseignement (id)
      on delete restrict on update restrict;

alter table personnel
   add constraint fk_personne_associati_posteper foreign key (pos_id)
      references postepersonnel (id)
      on delete restrict on update restrict;

alter table privilegeperso
   add constraint fk_privileg_associati_personne foreign key (id)
      references personnel (id)
      on delete restrict on update restrict;

alter table privilegeperso
   add constraint fk_privileg_associati_privileg foreign key (pri_id)
      references privilege (id)
      on delete restrict on update restrict;

alter table rolepersonnel
   add constraint fk_rolepers_associati_personne foreign key (id)
      references personnel (id)
      on delete restrict on update restrict;

alter table rolepersonnel
   add constraint fk_rolepers_associati_role foreign key (rol_id)
      references role (id)
      on delete restrict on update restrict;

alter table role_privilege
   add constraint fk_role_pri_associati_privileg foreign key (id)
      references privilege (id)
      on delete restrict on update restrict;

alter table role_privilege
   add constraint fk_role_pri_associati_role foreign key (rol_id)
      references role (id)
      on delete restrict on update restrict;

alter table seancecours
   add constraint fk_seanceco_associati_horaire foreign key (hor_id)
      references horaire (id)
      on delete restrict on update restrict;

alter table seancecours
   add constraint fk_seanceco_associati_classes foreign key (cla_id)
      references classes (id)
      on delete restrict on update restrict;

alter table seancecours
   add constraint fk_seanceco_associati_enseigna foreign key (ens_id)
      references enseignants (id)
      on delete restrict on update restrict;

alter table seancecours
   add constraint fk_seanceco_associati_matiersm foreign key (mat_id)
      references matiersmodule (id)
      on delete restrict on update restrict;

alter table seancecours
   add constraint fk_seanceco_associati_emploide foreign key (emp_id)
      references emploidetemps (id)
      on delete restrict on update restrict;

alter table seancecours
   add constraint fk_seanceco_associati_salles foreign key (sal_id)
      references salles (id)
      on delete restrict on update restrict;

alter table uniteenseignement
   add constraint fk_uniteens_associati_filieres foreign key (fil_id)
      references filieres (id)
      on delete restrict on update restrict;

alter table classematiere
   add constraint fk_classema_associati_classes foreign key (cla_id)
      references classes (id)
      on delete restrict on update restrict;

alter table classematiere
   add constraint fk_classema_associati_matiersm foreign key (id)
      references matiersmodule (id)
      on delete restrict on update restrict;

alter table comptabilite
   add constraint fk_comptabi_associati_etudiant foreign key (etu_id)
      references etudiants (id)
      on delete restrict on update restrict;

alter table comptabilite
   add constraint fk_comptabi_associati_mois foreign key (moi_id)
      references mois (id)
      on delete restrict on update restrict;

alter table note
   add constraint fk_note_associati_enseigna foreign key (ens_id)
      references enseignants (id)
      on delete restrict on update restrict;

alter table note
   add constraint fk_note_associati_matiersm foreign key (mat_id)
      references matiersmodule (id)
      on delete restrict on update restrict;

alter table note
   add constraint fk_note_associati_etudiant foreign key (etu_id)
      references etudiants (id)
      on delete restrict on update restrict;

