/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.uniteEnseignemnet;

import entities.Matiersmodule;

/**
 *
 * @author Fantastic
 */
public interface UniteenseignementsInterfaceCtrl {

    /**
     * cette fonction permet de save les filieres
     */
    public void enregistrerUniteenseignements();

    public void modifier();

    /**
     * Fantastic Supprimer le filiere
     *
     */
    public void supprimer();

    /**
     * Fantastic Méthode d'impression
     *
     */
    public void imprimerUniteenseignementssPdf();

    public void imprimerUniteenseignementssHtml();

    public String getComponentMessages(String filiereComponent, String defaultMessage);

    /**
     * Opération qui s'exécute au changement de l'UE sélectionnée
     */
    public void handleUeSelectionChange();

    /**
     * Ajoute une matière à la liste des matières de l'UE. La variable concernée
     * est <code>matiersmodule</code>
     */
    public void addNewMatiere();

    /**
     * Supprime la filière sélectionnée de la liste des matières de l'UE (et
     * aussi de la BD)
     */
    public void delSelectedMatiere();

    /**
     * Supprime la filière fournie de la liste des matières de l'UE (et aussi de
     * la BD)
     *
     * @param matiere
     */
    public void delMatiere(Matiersmodule matiere);

}
