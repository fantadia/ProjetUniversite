/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.uniteEnseignemnet;

import beans.util.JsfUtil;
import entities.Matiersmodule;
import entities.Uniteenseignements;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Fantastic
 */
@Named("uniteenseignementCtrl")
@ViewScoped
public class UniteenseignementsCtrl extends AbstractUniteenseignementsCtrl implements UniteenseignementsInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of UniteenseignementsCtrl
     */
    public UniteenseignementsCtrl() {
    }

    @PostConstruct
    private void initUniteenseignements() {
        uniteenseignements = uniteenseignementsFacadeLocal.findAll();
        selectedUniteenseignements = new Uniteenseignements();
        uniteenseignement = new Uniteenseignements();
        matiersmodule = new Matiersmodule();
    }

    @Override
    public void enregistrerUniteenseignements() {
        Uniteenseignements cl = uniteenseignementsFacadeLocal.findByNomUniteenseignements(uniteenseignement.getNom());
        if (cl != null) {
            JsfUtil.addErrorMessage("Cette uniteenseignement existe déjà" + "'" + cl.getNom());
            modifier = supprimer = detail = true;
            return;

        }
        uniteenseignementsFacadeLocal.create(uniteenseignement);
        initUniteenseignements();
        JsfUtil.addSuccessMessage("l'unité enseignement à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifier() {
        if (selectedUniteenseignements == null || selectedUniteenseignements.getId() == null) {
            return;
        }

        Uniteenseignements type = uniteenseignementsFacadeLocal.findByNomUniteenseignements(selectedUniteenseignements.getNom());
        if (type != null && !Objects.equals(type.getId(), selectedUniteenseignements.getId())) {
            JsfUtil.addErrorMessage("l'unité enseignement '" + selectedUniteenseignements.getNom() + "'" + "a été mis à jour");
        }
        uniteenseignementsFacadeLocal.edit(selectedUniteenseignements);
        JsfUtil.addSuccessMessage("Unité d'enseignements enregistrée");
    }

    @Override
    public void supprimer() {

        if (selectedUniteenseignements == null || selectedUniteenseignements.getId() == null) {
            return;
        }

        List<Matiersmodule> matires = matiersmoduleFacadeLocal.findByIdUniteenseignements(selectedUniteenseignements.getId());
        if (!matires.isEmpty()) {
            utils.JsfUtil.addWarningMessage("L'unité d'enseignement n'a pas éte supprimé : il est deja utilise par une  operation");
        } else {

            uniteenseignementsFacadeLocal.remove(selectedUniteenseignements);
            JsfUtil.addErrorMessage("Cette uniteenseignement à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

    @Override
    public void imprimerUniteenseignementssPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerUniteenseignementssHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleUeSelectionChange() {
        if (selectedUniteenseignements == null) {
            return;
        }
        matiersmodule = new Matiersmodule();
        matiersmodule.setUniteenseignement(selectedUniteenseignements);
        euMatiersmodules = matiersmoduleFacadeLocal.findByIdUniteenseignements(selectedUniteenseignements.getId());
    }

    @Override
    public void addNewMatiere() {
        if (matiersmodule == null) {
            JsfUtil.addSuccessMessage("jout impossible: Veuillez reessayer plus tard");
            return;
        }
        matiersmoduleFacadeLocal.create(matiersmodule);
        JsfUtil.addSuccessMessage("La matière a été ajoutée");
        handleUeSelectionChange();
    }

    @Override
    public void delSelectedMatiere() {
        if (selectedMatiersmodule == null) {
            JsfUtil.addErrorMessage("Veuillez sélectionner une matière pour la supprimer");
            return;
        }
        matiersmoduleFacadeLocal.remove(selectedMatiersmodule);
        JsfUtil.addSuccessMessage("La suppression a été effectuée");
        handleUeSelectionChange();
    }

    @Override
    public void delMatiere(Matiersmodule matiere) {
        if (matiere == null) {
            JsfUtil.addErrorMessage("Valeur fournie pour suppression invalide");
            return;
        }
        matiersmoduleFacadeLocal.remove(matiere);
        JsfUtil.addSuccessMessage("La suppression a été effectuée");
        handleUeSelectionChange();
    }

}
