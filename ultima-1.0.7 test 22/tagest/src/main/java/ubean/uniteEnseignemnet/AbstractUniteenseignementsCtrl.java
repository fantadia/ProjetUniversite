/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.uniteEnseignemnet;

import entities.Matiersmodule;
import entities.Uniteenseignements;
import java.util.List;
import javax.ejb.EJB;
import sessions.UniteenseignementsFacadeLocal;
import sessions.MatiersmoduleFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractUniteenseignementsCtrl {

    @EJB
    protected UniteenseignementsFacadeLocal uniteenseignementsFacadeLocal;
    @EJB
    protected MatiersmoduleFacadeLocal matiersmoduleFacadeLocal;

    protected List<Uniteenseignements> uniteenseignements;

    protected StringBuffer uniteenseignementsTableHtml = new StringBuffer("pas encore implementé");
    protected Uniteenseignements selectedUniteenseignements;
    protected Uniteenseignements uniteenseignementModifiee;
    protected Uniteenseignements uniteenseignement;
    
    protected Matiersmodule matiersmodule;
    protected Matiersmodule selectedMatiersmodule;
    protected List<Matiersmodule> euMatiersmodules;

    public Uniteenseignements getUniteenseignement() {
        return uniteenseignement;
    }

    public void setUniteenseignement(Uniteenseignements uniteenseignement) {
        this.uniteenseignement = uniteenseignement;
    }

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public List<Uniteenseignements> getUniteenseignements() {
        uniteenseignements = uniteenseignementsFacadeLocal.findAll();
        return uniteenseignements;
    }

    public Uniteenseignements getSelectedUniteenseignements() {
        return selectedUniteenseignements;
    }

    public Uniteenseignements getUniteenseignementsModifiee() {
        return uniteenseignementModifiee;
    }

    public void setUniteenseignementsModifiee(Uniteenseignements uniteenseignementModifiee) {
        this.uniteenseignementModifiee = uniteenseignementModifiee;
    }

//    public void setSelectedUniteenseignements(Uniteenseignements selectedUniteenseignements) {
//        this.selectedUniteenseignements = selectedUniteenseignements;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        uniteenseignementModifiee = new Uniteenseignements();
//        if(selectedUniteenseignements == null )
//            return;
//        uniteenseignementModifiee.setUniteenseignements(selectedUniteenseignements.getUniteenseignements());
//        uniteenseignementModifiee.setEtat(selectedUniteenseignements.getEtat());
//        uniteenseignementModifiee.setId(selectedUniteenseignements.getId());
//        uniteenseignementModifiee.setEtat(selectedUniteenseignements.getEtat());
//    }
    public void setSelectedUniteenseignements(Uniteenseignements selectedUniteenseignements) {
        this.selectedUniteenseignements = selectedUniteenseignements;
        if (selectedUniteenseignements == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = uniteenseignementsFacadeLocal.findAll().isEmpty();
        return imprimer;
    }

    public void setUniteenseignements(Uniteenseignements uniteenseignement) {
        this.uniteenseignement = uniteenseignement;
    }

    public StringBuffer getUniteenseignementsTableHtml() {
        return uniteenseignementsTableHtml;
    }

    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    public Matiersmodule getMatiersmodule() {
        return matiersmodule;
    }

    public void setMatiersmodule(Matiersmodule matiersmodule) {
        this.matiersmodule = matiersmodule;
    }

    public Matiersmodule getSelectedMatiersmodule() {
        return selectedMatiersmodule;
    }

    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
        this.selectedMatiersmodule = selectedMatiersmodule;
    }

    public List<Matiersmodule> getEuMatiersmodules() {
        return euMatiersmodules;
    }

}
