/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.histDeliberation;

import beans.util.JsfUtil;
import entities.Historiquedeliberation;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfMatiersmodule;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "historiquedeliberationCtrl")
@ViewScoped
public class HistoriquedeliberationCtrl extends AbstractHistoriqueDeliberationCtrl implements HistoriquedeliberationInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public HistoriquedeliberationCtrl() {
    }

    @PostConstruct
    private void initHistoriquedeliberation() {
        historiquesDeliberations = historiquedeliberationFacadeLocal.findAll();
        selectedHistoriquedeliberation = new Historiquedeliberation();
        historiquedeliberation = new Historiquedeliberation();

    }

    @Override
    public void enregistrerHistoriquedeliberation() {
        
        Historiquedeliberation hd = historiquedeliberationFacadeLocal.findByIdHistDelibetation(historiquedeliberation.getId());
        if (hd != null) {
            JsfUtil.addErrorMessage("historiqueDeliberation existe déjà" + "'" + hd.getId());
            modifier = supprimer = detail = true;
            return;
        }
        historiquedeliberationFacadeLocal.create(historiquedeliberation);
        initHistoriquedeliberation();
        JsfUtil.addSuccessMessage("historiqueDeliberationà été bien créé");
        modifier = supprimer = detail = true;
       
    }

    @Override
    public void modifierHistoriquedeliberation() {
       
        if ((selectedHistoriquedeliberation==null)|| (selectedHistoriquedeliberation.getId()==null)) {
            return;
        }

//        Salles type = salleFacadeLocale.findByNomSalle(selectedSalle.getNomsalle());
//        if (type != null && !Objects.equals(type.getId(), selectedSalle.getId())) {
//            JsfUtil.addErrorMessage("cette salle '" + selectedSalle.getNomsalle() + "'" + "a été mis à jour");
//        }
        historiquedeliberationFacadeLocal.edit(selectedHistoriquedeliberation);
        JsfUtil.addSuccessMessage("historique deliberation enregistrée");
    }

    @Override
    public void supprimerHistoriquedeliberation() {
        
        if(selectedHistoriquedeliberation == null || selectedHistoriquedeliberation.getId() == null)
            return;
         
        //if (!selectedHistoriquedeliberation.isEmpty()) {
           // utils.JsfUtil.addWarningMessage("Cette salle ne peut pas étre supprimé :car utilisee comme cle etrangere dans une autre cla");
       // } else 
        //{

            historiquedeliberationFacadeLocal.remove(selectedHistoriquedeliberation);
            JsfUtil.addErrorMessage("historiqueDeliberation a été supprimée");
            modifier = supprimer = detail = true;
       // }
    }
    
     @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

   

}
