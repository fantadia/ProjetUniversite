/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.histDeliberation;


/**
 *
 * @author Fantastic
 */
public interface HistoriquedeliberationInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Matiersmodule
     */
    public void enregistrerHistoriquedeliberation();
    
    
    public void modifierHistoriquedeliberation();
    
    /**
     * Fantastic
     * Supprimer le Matiersmodule
     * 
     */
    public void supprimerHistoriquedeliberation();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
   
   
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
