/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.histDeliberation;


import java.util.List;
import javax.ejb.EJB;
import sessions.HistoriquedeliberationFacadeLocal;
import entities.Historiquedeliberation;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractHistoriqueDeliberationCtrl {
    
    @EJB
    protected HistoriquedeliberationFacadeLocal historiquedeliberationFacadeLocal ;
    
    
    
    
    protected List<Historiquedeliberation> historiquesDeliberations;
    
    protected StringBuffer historiquedeliberationTableHtml= new StringBuffer("pas encore implementé") ;
    protected Historiquedeliberation selectedHistoriquedeliberation;
    protected Historiquedeliberation historiquedeliberation;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public HistoriquedeliberationFacadeLocal getHistoriquedeliberationFacadeLocal() {
        return historiquedeliberationFacadeLocal;
    }

    public void setHistoriquedeliberationFacadeLocal(HistoriquedeliberationFacadeLocal historiquedeliberationFacadeLocal) {
        this.historiquedeliberationFacadeLocal = historiquedeliberationFacadeLocal;
    }

    public List<Historiquedeliberation> getHistoriquesDeliberations() {
        return historiquesDeliberations;
    }

    public void setHistoriquesDeliberations(List<Historiquedeliberation> historiquesDeliberations) {
        this.historiquesDeliberations = historiquesDeliberations;
    }

    public StringBuffer getHistoriquedeliberationTableHtml() {
        return historiquedeliberationTableHtml;
    }

    public void setHistoriquedeliberationTableHtml(StringBuffer historiquedeliberationTableHtml) {
        this.historiquedeliberationTableHtml = historiquedeliberationTableHtml;
    }

    public Historiquedeliberation getSelectedHistoriquedeliberation() {
        return selectedHistoriquedeliberation;
    }

    public void setSelectedHistoriquedeliberation(Historiquedeliberation selectedHistoriquedeliberation) {
        this.selectedHistoriquedeliberation = selectedHistoriquedeliberation;
        if (selectedHistoriquedeliberation == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Historiquedeliberation getHistoriquedeliberation() {
        return historiquedeliberation;
    }

    public void setHistoriquedeliberation(Historiquedeliberation historiquedeliberation) {
        this.historiquedeliberation = historiquedeliberation;
    }
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    
    
    
   

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }
    
    public boolean isImprimer() {
        imprimer = historiquedeliberationFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
