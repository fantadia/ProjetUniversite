/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.filiere;

import ubean.filiere.*;


/**
 *
 * @author Fantastic
 */
public interface FiliereInterfaceCtrl {
    
    /**
     * cette fonction permet de save les filieres
     */
    public void enregistrerFilieres();
    
    
    public void modifier();
    
    /**
     * Fantastic
     * Supprimer le filiere
     * 
     */
    public void supprimer();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerFilieressPdf();
    
    public void imprimerFilieressHtml();
    
     public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
