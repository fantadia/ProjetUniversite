/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.filiere;

import ubean.uniteEnseignemnet.*;
import ubean.filiere.*;
import ubean.filiere.*;
import entities.Filieres;
import java.util.List;
import javax.ejb.EJB;
import sessions.FilieresFacadeLocal;
import sessions.EtudiantsFacadeLocal;
import sessions.MoisFacadeLocal;
import sessions.UniteenseignementsFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractFiliereCtrl {
    
    @EJB
    protected FilieresFacadeLocal filieresFacadeLocal ;
    
     @EJB
    protected UniteenseignementsFacadeLocal uniteenseignementsFacadeLocal ;
    
    protected List<Filieres> filieres;
    
    protected StringBuffer filieresTableHtml = new StringBuffer("pas encore implementé") ;
    protected Filieres selectedFilieres;
    protected Filieres filiereModifiee;
    protected Filieres filiere;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
    public List<Filieres> getFilieres() {
        filieres = filieresFacadeLocal.findAll();
        return filieres;
    }

    public Filieres getSelectedFilieres() {
        return selectedFilieres;
    }

    public Filieres getFiliere() {
        return filiere;
    }

    public void setFiliere(Filieres filiere) {
        this.filiere = filiere;
    }
    
    

    public Filieres getFilieresModifiee() {
        return filiereModifiee;
    }

    public void setFilieresModifiee(Filieres filiereModifiee) {
        this.filiereModifiee = filiereModifiee;
    }
    
//    public void setSelectedFilieres(Filieres selectedFilieres) {
//        this.selectedFilieres = selectedFilieres;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        filiereModifiee = new Filieres();
//        if(selectedFilieres == null )
//            return;
//        filiereModifiee.setFilieres(selectedFilieres.getFilieres());
//        filiereModifiee.setEtat(selectedFilieres.getEtat());
//        filiereModifiee.setId(selectedFilieres.getId());
//        filiereModifiee.setEtat(selectedFilieres.getEtat());
//    }
    
    
    
    public void setSelectedFilieres(Filieres selectedFilieres) {
        this.selectedFilieres = selectedFilieres;
        if (selectedFilieres == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = filieresFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
   

    public void setFilieres(Filieres filiere) {
        this.filiere = filiere;
    }

    public StringBuffer getFilieresTableHtml() {
        return filieresTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
