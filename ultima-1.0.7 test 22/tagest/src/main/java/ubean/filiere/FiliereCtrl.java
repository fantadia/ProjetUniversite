/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.filiere;

import ubean.uniteEnseignemnet.*;
import ubean.filiere.*;
import ubean.filiere.*;
import beans.util.JsfUtil;
import entities.Filieres;
import entities.Etudiants;
import entities.Filieres;
import entities.Uniteenseignements;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import sessions.EtudiantsFacadeLocal;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfFilieres;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "filiereCtrl")
@ViewScoped
public class FiliereCtrl extends AbstractFiliereCtrl implements FiliereInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of FilieresCtrl
     */
    public FiliereCtrl() {
    }

    @PostConstruct
    private void initFilieres() {
        filieres = filieresFacadeLocal.findAll();
        selectedFilieres = new Filieres();
        filiere = new Filieres();

    }

    @Override
    public void enregistrerFilieres() {
        Filieres cl = filieresFacadeLocal.findByNomFilieres(filiere.getNomfiliere());
        if (cl != null) {
            JsfUtil.addErrorMessage("Cette filiere existe déjà" + "'" + cl.getNomfiliere());
            modifier = supprimer = detail = true;
            return;

        }
        filieresFacadeLocal.create(filiere);
        initFilieres();
        JsfUtil.addSuccessMessage("la filiere à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifier() {
        if (selectedFilieres == null || selectedFilieres.getId() == null) {
            return;
        }

        Filieres type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        filieresFacadeLocal.edit(selectedFilieres);
        JsfUtil.addSuccessMessage("Filieres enregistrée");
    }

    @Override
    public void supprimer() {

         if(selectedFilieres == null || selectedFilieres.getId() == null)
            return;
             
        List<Uniteenseignements> l = uniteenseignementsFacadeLocal.findByIdFilieres(selectedFilieres.getId());
        if (!l.isEmpty()) {
            utils.JsfUtil.addWarningMessage("La filiere n'a pas éte supprimé : il est deja utilise par une  operation");
        } else {

            filieresFacadeLocal.remove(selectedFilieres);
            JsfUtil.addErrorMessage("Cette filiere à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

   

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

    @Override
    public void imprimerFilieressPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerFilieressHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

}
