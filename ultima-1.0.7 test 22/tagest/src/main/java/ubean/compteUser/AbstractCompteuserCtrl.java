package ubean.compteUser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import entities.Compteuser;
import java.util.List;
import javax.ejb.EJB;



import sessions.CompteuserFacadeLocal;



/**
 *
 * @author Fantastic
 */
public abstract class AbstractCompteuserCtrl {
    
    @EJB
    protected CompteuserFacadeLocal compteuserFacadeLocal ;
    
   
    protected List<Compteuser> compteusers;
    
    protected StringBuffer compteusersTableHtml = new StringBuffer("pas encore implementé") ;
    protected Compteuser selectedCompteuser;
    protected Compteuser compteuser;
    
    
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public CompteuserFacadeLocal getCompteuserFacadeLocal() {
        return compteuserFacadeLocal;
    }

    public void setCompteuserFacadeLocal(CompteuserFacadeLocal compteuserFacadeLocal) {
        this.compteuserFacadeLocal = compteuserFacadeLocal;
    }

   

    public List<Compteuser> getCompteusers() {
        return compteusers;
    }

    public void setCompteusers(List<Compteuser> compteusers) {
        this.compteusers = compteusers;
    }

    public StringBuffer getCompteusersTableHtml() {
        return compteusersTableHtml;
    }

    public void setCompteusersTableHtml(StringBuffer compteusersTableHtml) {
        this.compteusersTableHtml = compteusersTableHtml;
    }

    public Compteuser getSelectedCompteuser() {
        return selectedCompteuser;
    }

    public void setSelectedCompteuser(Compteuser selectedCompteuser) {
        this.selectedCompteuser = selectedCompteuser;
        if (selectedCompteuser == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Compteuser getCompteuser() {
        return compteuser;
    }

    public void setCompteuser(Compteuser compteuser) {
        this.compteuser = compteuser;
    }
    
    
    
    
    
  
    
  
    
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = compteuserFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
 
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
