/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.compteUser;



/**
 *
 * @author Fantastic
 */
public interface CompteuserInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Compteuser
     */
    public void enregistrerCompteuser();
    
    
    public void modifierCompteuser();
    
    /**
     * Fantastic
     * Supprimer la Compteuser
     * 
     */
    public void supprimerCompteuser();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerCompteuserPdf();
    
    public void imprimerCompteuserHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
