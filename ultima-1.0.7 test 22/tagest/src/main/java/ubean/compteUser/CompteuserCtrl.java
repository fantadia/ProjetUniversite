/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.compteUser;

import ubean.compteUser.AbstractCompteuserCtrl;
import ubean.compteUser.CompteuserInterfaceCtrl;
import beans.util.JsfUtil;
import entities.Compteuser;
import entities.Compteuser;
import entities.Seancecours;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "compteuserCtrl")
@ViewScoped
public class CompteuserCtrl extends AbstractCompteuserCtrl implements CompteuserInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public CompteuserCtrl() {
    }

    @PostConstruct
    private void initCompteuser() {

        compteusers = compteuserFacadeLocal.findAll();
        selectedCompteuser = new Compteuser();
        compteuser = new Compteuser();

    }

    @Override
    public void enregistrerCompteuser() {
        Compteuser compte = compteuserFacadeLocal.findByIdPersonnel(compteuser.getPersonnel().getId());
        if (compte != null) {
            JsfUtil.addErrorMessage("Ce personnel dispose déjà d'un compte" + "'" + compteuser.getPersonnel().getNom());
            modifier = supprimer = detail = true;
            return;

        }

        Compteuser log = compteuserFacadeLocal.findByLogin(compteuser.getLogin());
        if (log != null) {
            JsfUtil.addErrorMessage("Ce login est déjà utilisé" + "'" + compteuser.getLogin());
            modifier = supprimer = detail = true;
            return;

        }
        
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CompteuserCtrl.class.getName()).log(Level.SEVERE, null, ex);
        }
        md.update(compteuser.getMotdepass().getBytes());
        byte[] md5 = md.digest();
        
        compteuserFacadeLocal.create(compteuser);
        initCompteuser();
        JsfUtil.addSuccessMessage("le compte utilisateur à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierCompteuser() {

        if (selectedCompteuser == null || selectedCompteuser.getId() == null) {
            return;
        }

//        Compteuser type = compteuserFacadeLocal.findByIdCompteuser(selectedCompteuser.getId());
//        if (type != null && !Objects.equals(type.getId(), selectedCompteuser.getId())) {
//            JsfUtil.addErrorMessage("le compteuser '" + selectedCompteuser.getNom()+ "'" + "a été mis à jour");
//        }
        compteuserFacadeLocal.edit(selectedCompteuser);
        JsfUtil.addSuccessMessage("compteuser enregistrée");
    }

    @Override
    public void supprimerCompteuser() {
       
        compteuserFacadeLocal.remove(selectedCompteuser);
        JsfUtil.addErrorMessage("Ce compte utilisateur à été supprimée");
        modifier = supprimer = detail = true;
    }

    @Override
    public void imprimerCompteuserPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerCompteuserHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

}
