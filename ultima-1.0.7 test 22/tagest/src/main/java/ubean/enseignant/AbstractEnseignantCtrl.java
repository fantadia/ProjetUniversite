/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.enseignant;


import entities.Enseignants;
import java.util.List;
import javax.ejb.EJB;
import sessions.EnseignantsFacadeLocal;

import sessions.SeancecoursFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractEnseignantCtrl {
    
    @EJB
    protected EnseignantsFacadeLocal enseignantFacadeLocale ;
    
    @EJB
    protected SeancecoursFacadeLocal seancecoursFacadeLocale;
    
    
    
    
    protected List<Enseignants> listEnseignant;
    
    protected StringBuffer SalleTableHtml= new StringBuffer("pas encore implementé") ;
    protected Enseignants selectedEnseignant;
    protected Enseignants enseignant;
    protected List<String> sexes;
    

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public EnseignantsFacadeLocal getEnseignantFacadeLocale() {
        return enseignantFacadeLocale;
    }

    public void setEnseignantFacadeLocale(EnseignantsFacadeLocal enseignantFacadeLocale) {
        this.enseignantFacadeLocale = enseignantFacadeLocale;
    }

    public SeancecoursFacadeLocal getSeancecoursFacadeLocale() {
        return seancecoursFacadeLocale;
    }

    public void setSeancecoursFacadeLocale(SeancecoursFacadeLocal seancecoursFacadeLocale) {
        this.seancecoursFacadeLocale = seancecoursFacadeLocale;
    }

    public List<Enseignants> getListEnseignant() {
        return listEnseignant;
    }

    public void setListEnseignant(List<Enseignants> listEnseignant) {
        this.listEnseignant = listEnseignant;
    }

    public Enseignants getSelectedEnseignant() {
        return selectedEnseignant;
    }

    public void setSelectedEnseignant(Enseignants selectedEnseignant) {
        this.selectedEnseignant = selectedEnseignant;
         if (selectedEnseignant== null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
         }
           
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }

    public Enseignants getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignants enseignant) {
        this.enseignant = enseignant;
    }

    public List<String> getSexes() {
        
        return sexes;
    }

    
    
    public void setSexes(List<String> sexes) {
        this.sexes = sexes;
    }
    
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }
    
    public boolean isImprimer() {
        imprimer = enseignantFacadeLocale.findAll().isEmpty();
        return imprimer;
    }

   
    public StringBuffer getSalleTableHtml() {
        return SalleTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
