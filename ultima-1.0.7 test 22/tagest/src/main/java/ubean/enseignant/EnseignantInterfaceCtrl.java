/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.enseignant;


/**
 *
 * @author Fantastic
 */
public interface EnseignantInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Matiersmodule
     */
    public void enregistrerEnseignant();
    
    
    public void modifierEnseignant();
    
    /**
     * Fantastic
     * Supprimer le Matiersmodule
     * 
     */
    public void supprimerEnseignant();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
   
   
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
