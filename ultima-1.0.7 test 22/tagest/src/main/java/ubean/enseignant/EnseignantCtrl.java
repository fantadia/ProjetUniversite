/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.enseignant;

import beans.util.JsfUtil;
import entities.Enseignants;
import entities.Salles;
import entities.Seancecours;
import interfaces.Genre;
import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sessions.EtudiantsFacadeLocal;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfMatiersmodule;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "enseignantCtrl")
@ViewScoped
public class EnseignantCtrl extends AbstractEnseignantCtrl implements EnseignantInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public EnseignantCtrl() {
    }

    @PostConstruct
    private void initEnseignant() {
        listEnseignant = enseignantFacadeLocale.findAll();
        selectedEnseignant = new Enseignants();
         enseignant = new Enseignants();
         initGenres();

    }
    
    
    
    private void initGenres(){
        sexes = new ArrayList<>();
        sexes.add(Genre.FEMME);
        sexes.add(Genre.HOMME);
    }

    @Override
    public void enregistrerEnseignant() {
        Enseignants ens = enseignantFacadeLocale.findByMatricule(enseignant.getMatricule());
        if (ens != null) {
            JsfUtil.addErrorMessage("Cette Enseignant est déjà creee" + "'" + ens.getMatricule());
            modifier = supprimer = detail = true;
            return;
        }
        enseignantFacadeLocale.create(enseignant);
        initEnseignant();
        JsfUtil.addSuccessMessage("enseignant créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEnseignant() {
        if ((selectedEnseignant==null)|| (selectedEnseignant.getId()==null)) {
            return;
        }

        Enseignants ensei = enseignantFacadeLocale.findByMatricule(selectedEnseignant.getMatricule());
//        if (type != null && !Objects.equals(type.getId(), selectedSalle.getId())) {
//            JsfUtil.addErrorMessage("la matiersmodule '" + selectedSalle.getNomsalle() + "'" + "a été mis à jour");
//        }
        enseignantFacadeLocale.edit(selectedEnseignant);
        JsfUtil.addSuccessMessage("Enseignant modifier");
    }

    @Override
    public void supprimerEnseignant() {

         if(selectedEnseignant == null || selectedEnseignant.getId() == null)
            return;
             
        List<Seancecours> enseignants= seancecoursFacadeLocale.findByIdEnseignant(selectedEnseignant.getId());
      
        if (!enseignants.isEmpty()) {
            utils.JsfUtil.addWarningMessage("Cette Enseignant ne peut pas étre supprimé :car utilisee dans une autre operation");
        } else {

            enseignantFacadeLocale.remove(selectedEnseignant);
            JsfUtil.addErrorMessage("Cette salle a été supprimée");
            modifier = supprimer = detail = true;
        }
    }

 

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

}
