/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.note;

import entities.Notes;
import java.util.List;
import javax.ejb.EJB;

import sessions.HistoriquedeliberationFacadeLocal;

import sessions.NotesFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractNoteCtrl {

    @EJB
    protected HistoriquedeliberationFacadeLocal historiquedeliberationFacadeLocal;

    @EJB
    protected NotesFacadeLocal notesFacadeLocal;

    protected List<Notes> notes;
    protected List<String> resultatUE;

    protected StringBuffer NotesTableHtml = new StringBuffer("pas encore implementé");
    protected Notes selectedNote;
    protected Notes note;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    protected String pdfFileName;

    public Notes getNote() {
        return note;
    }

    public HistoriquedeliberationFacadeLocal getHistoriquedeliberationFacadeLocal() {
        return historiquedeliberationFacadeLocal;
    }

    public void setHistoriquedeliberationFacadeLocal(HistoriquedeliberationFacadeLocal historiquedeliberationFacadeLocal) {
        this.historiquedeliberationFacadeLocal = historiquedeliberationFacadeLocal;
    }

    public List<String> getResultatUE() {
        return resultatUE;
    }

    public void setResultatUE(List<String> resultatUE) {
        this.resultatUE = resultatUE;
    }

    public NotesFacadeLocal getNotesFacadeLocal() {
        return notesFacadeLocal;
    }

    public void setNotesFacadeLocal(NotesFacadeLocal notesFacadeLocal) {
        this.notesFacadeLocal = notesFacadeLocal;
    }

    public List<Notes> getNotes() {
        return notes;
    }

    public void setNotes(List<Notes> notes) {
        this.notes = notes;
    }

    public StringBuffer getNotesTableHtml() {
        return NotesTableHtml;
    }

    public void setNotesTableHtml(StringBuffer NotesTableHtml) {
        this.NotesTableHtml = NotesTableHtml;
    }

    public Notes getSelectedNote() {
        return selectedNote;
    }

    public void setSelectedNote(Notes selectedNote) {
        this.selectedNote = selectedNote;
        if (selectedNote == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;

    }

    public void setNote(Notes note) {
        this.note = note;

    }

//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = notesFacadeLocal.findAll().isEmpty();
        return imprimer;
    }

    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

}
