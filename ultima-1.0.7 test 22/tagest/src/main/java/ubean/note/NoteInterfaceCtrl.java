/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.note;


/**
 *
 * @author Fantastic
 */
public interface NoteInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Matiersmodule
     */
    public void enregistrerNote();
    
    
    public void modifierNote();
    
    /**
     * Fantastic
     * Supprimer le Matiersmodule
     * 
     */
    public void supprimerNote();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerNotePdf();
    
    public void imprimerNoteHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
