/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.note;

import beans.util.JsfUtil;
import entities.Historiquedeliberation;
import entities.Notes;
import interfaces.ResutatUE;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import pdf7.NotesPdfDocument;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "noteCtrl")
@ViewScoped
public class NoteCtrl extends AbstractNoteCtrl implements NoteInterfaceCtrl, Serializable {

    private static final Logger LOG = Logger.getLogger(NoteCtrl.class.getName());

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public NoteCtrl() {
    }

    @PostConstruct
    private void initNote() {
        notes = notesFacadeLocal.findAll();
        selectedNote = new Notes();
        note = new Notes();
        initResultatUE();

    }

    private void initResultatUE() {
        resultatUE = new ArrayList<>();
        resultatUE.add(ResutatUE.VALIDER);
        resultatUE.add(ResutatUE.NON_VALIDER);
        resultatUE.add(ResutatUE.VALIDER_COMPENSATION);
    }

    @Override
    public void enregistrerNote() {
        Notes n = notesFacadeLocal.findByIdNote(note.getId());
        if (n != null) {
            JsfUtil.addErrorMessage("Cette note existe déjà" + "'" + note.getNote());
            modifier = supprimer = detail = true;
            return;

        }
        notesFacadeLocal.create(note);
        initNote();
        JsfUtil.addSuccessMessage("la note à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierNote() {

        if (selectedNote == null || selectedNote.getId() == null) {
            return;
        }

        notesFacadeLocal.edit(selectedNote);
        JsfUtil.addSuccessMessage("la Note a été mis à jour");
    }

    @Override
    public void supprimerNote() {

        if (selectedNote == null || selectedNote.getId() == null) {
            return;
        }

        List<Historiquedeliberation> listNote = historiquedeliberationFacadeLocal.findByNote(selectedNote.getId());

        if (!listNote.isEmpty()) {
            utils.JsfUtil.addWarningMessage("Cette note ne peut pas etre supprimé : elle est deja utilise dans une  operation");
        } else {

            notesFacadeLocal.remove(selectedNote);
            JsfUtil.addErrorMessage("Cette matiersmodule à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerNotePdf() {
        try {
            NotesPdfDocument doc = new NotesPdfDocument(notesFacadeLocal.findAll());
            pdfFileName = doc.print();
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void imprimerNoteHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

}
