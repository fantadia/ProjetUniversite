/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.annee;

import ubean.classes.ClasseInterfaceCtrl;
import entities.AnneeScolaire;
import entities.Mois;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fanta Diakite
 */
@ManagedBean (name = "anneeCtrl")
@ViewScoped
public class AnneeCtrl extends AbstractAnneeCtrl implements AnneeInterfaceCtrl, Serializable{
    
    @PostConstruct
    private void initAnnee(){
        annees = anneeFacadeLocal.findAll();
        selectedAnnee = new AnneeScolaire();
        annee = new AnneeScolaire();
        anneeModifiee = new AnneeScolaire();
        
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
//        if (privil != null) {
//            creer = !privil.contains("ANNEE_CREER");
//            notif += creer ? "Création refusée\n" : "";
//            
//        } else {
//            System.out.println("liste de privillege vide");
//        }
//        if (!notif.equals("")) {
//            JsfUtil.addWarningMessage(notif);
//        }
    }
    
    
    @Override
    public void enregistrerAnnee() {
      //        String summary;
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "",  null);
//        FacesContext.getCurrentInstance().addMessage(null, message); 
         
        AnneeScolaire annee1 = anneeFacadeLocal.findByAnnee(annee.getAnnee());
        if(annee1 != null){
            JsfUtil.addErrorMessage("cet annee  existe déjà" +"'"+ annee.getAnnee()+"'");
            modifier = supprimer = detail = true ;
            return;
        }
//        if (annee.getAnnee()<=2010) {
//            JsfUtil.addErrorMessage("l'annee saisie doit etre superieur a 2010  '"+ annee.getAnnee()+"'");
//            modifier = supprimer = detail = true ;
//            return;
//        }
        annee.setEtat(true);
        anneeFacadeLocal.create(annee);
        initAnnee();
        JsfUtil.addSuccessMessage("L'annee a été enregistré");  
    
        modifier = supprimer = detail = true ;
        
    }
           

    @Override
    public void modifier() {
        if(selectedAnnee == null || selectedAnnee.getId() == null)
            return;
//        AnneeScolaire type = anneeFacadeLocal.findByAnnee(selectedAnnee.getAnnee());
//        if(type != null && !Objects.equals(type.getId(), selectedAnnee.getId())){
//            JsfUtil.addErrorMessage("cet annee existe déjà");
//            return;
//        }
      
//        if (selectedAnnee.getAnnee() <=2010) {
//            JsfUtil.addErrorMessage("l'annee saisie doit etre superieur a 2010  '"+ selectedAnnee.getAnnee()+"'");
//            return;
//        }
        selectedAnnee = anneeModifiee;
        anneeFacadeLocal.edit(selectedAnnee);
        JsfUtil.addSuccessMessage("l' annee '" +selectedAnnee.getAnnee()+"'"+" a été mis à jour");
    }
    

    @Override
    public void supprimer() {
//        if(selectedAnnee == null || selectedAnnee.getId() == null)
//            return;
        
//        List<Mois> moises = moisFacadeLocal.findByIdAnnee(selectedAnnee.getId());
//        if(moises.isEmpty()){//Suppression
//            anneeFacadeLocal.remove(selectedAnnee);
//            JsfUtil.addSuccessMessage("L'annee a ete supprime");
//        }else{
//            selectedAnnee.setEtat(false);
//            anneeFacadeLocal.edit(selectedAnnee);
//       
//            JsfUtil.addWarningMessage("L'annee a ete desactive: Elle est deja utilise");
//        }
    }

    @Override
    public void imprimerAnneePdf() {
        System.out.println("Impression pdf des annees");
    }

    @Override
    public void imprimerAnneeHtml() {
        System.out.println("Impression html des annees");
    }

    
   
    
    
}
