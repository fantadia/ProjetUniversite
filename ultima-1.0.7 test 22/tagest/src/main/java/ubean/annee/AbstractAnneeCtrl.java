/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.annee;

import entities.AnneeScolaire;
import java.util.List;
import javax.ejb.EJB;
import sessions.AnneeScolaireFacadeLocal;
import sessions.MoisFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fanta Diakite
 */
public abstract class AbstractAnneeCtrl {
    
    @EJB
    protected AnneeScolaireFacadeLocal anneeFacadeLocal ;

    @EJB
    protected MoisFacadeLocal moisFacadeLocal ;
    
    
    protected List<AnneeScolaire> annees;
    
    protected StringBuffer anneesTableHtml = new StringBuffer("pas encore implementé") ;
    protected AnneeScolaire selectedAnnee;
    protected AnneeScolaire anneeModifiee;
    protected AnneeScolaire annee;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
    public List<AnneeScolaire> getAnnees() {
        annees = anneeFacadeLocal.findAll();
        return annees;
    }

    public AnneeScolaire getSelectedAnnee() {
        return selectedAnnee;
    }

    public AnneeScolaire getAnneeModifiee() {
        return anneeModifiee;
    }

    public void setAnneeModifiee(AnneeScolaire anneeModifiee) {
        this.anneeModifiee = anneeModifiee;
    }
    
//    public void setSelectedAnnee(AnneeScolaire selectedAnnee) {
//        this.selectedAnnee = selectedAnnee;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        anneeModifiee = new AnneeScolaire();
//        if(selectedAnnee == null )
//            return;
//        anneeModifiee.setAnnee(selectedAnnee.getAnnee());
//        anneeModifiee.setEtat(selectedAnnee.getEtat());
//        anneeModifiee.setId(selectedAnnee.getId());
//        anneeModifiee.setEtat(selectedAnnee.getEtat());
//    }
    
    
    
    public void setSelectedAnnee(AnneeScolaire selectedAnnee) {
        this.selectedAnnee = selectedAnnee;
        if (selectedAnnee == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = anneeFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
    public AnneeScolaire getAnnee() {
        return annee;
    }

    public void setAnnee(AnneeScolaire annee) {
        this.annee = annee;
    }

    public StringBuffer getAnneesTableHtml() {
        return anneesTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
    
   

    public void setModifier(boolean modifier) {
        this.modifier = modifier;
    }

   
    public void setSupprimer(boolean supprimer) {
        this.supprimer = supprimer;
    }

    

    public void setImprimer(boolean imprimer) {
        this.imprimer = imprimer;
    }

   
    public void setDetail(boolean detail) {
        this.detail = detail;
    }

    
}
