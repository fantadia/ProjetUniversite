/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.annee;

import ubean.annee.*;

/**
 *
 * @author Fanta Diakite
 */
public interface AnneeInterfaceCtrl {
    
    /**
     * Fanta Diakite
     * Cette méthode gère l'enregistrement d'un type de Année en base de données
     */
    public void enregistrerAnnee();
    
    /**
     * Fanta Diakite
     * Modifier le type de Année sélectionné
     */
    public void modifier();
    
    /**
     * Fanta Diakite
     * Supprimer le type de Année sélectionné
     * Si le type de Année n'est pas utilisé alors il est supprimé.
     * Il serra désactivé dans le cas contraire
     */
    public void supprimer();
    
    /**
     * Fanta Diakite
     * Méthode d'impression
     * 
     */
    public void imprimerAnneePdf();
    
    /**
     * Fanta Diakite
     * Méthode d'impression
     *
     */
    public void imprimerAnneeHtml();
    
}
