/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.salle;

import beans.util.JsfUtil;
import entities.Salles;
import entities.Seancecours;
import java.io.Serializable;

import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sessions.EtudiantsFacadeLocal;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfMatiersmodule;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "salleCtrl")
@ViewScoped
public class SalleCtrl extends AbstractSalleCtrl implements SalleInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public SalleCtrl() {
    }

    @PostConstruct
    private void initSalle() {
        listSalle = salleFacadeLocale.findAll();
        selectedSalle = new Salles();
        salle = new Salles();

    }

    @Override
    public void enregistrerSalle() {
        Salles cl = salleFacadeLocale.findByNomSalle(salle.getNomsalle());
        if (cl != null) {
            JsfUtil.addErrorMessage("Cette Salle existe déjà" + "'" + cl.getNomsalle());
            modifier = supprimer = detail = true;
            return;
        }
        salleFacadeLocale.create(salle);
        initSalle();
        JsfUtil.addSuccessMessage("la Salle à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierSalle() {
        if ((selectedSalle==null)|| (salle.getId()==null)) {
            return;
        }

//        Salles type = salleFacadeLocale.findByNomSalle(selectedSalle.getNomsalle());
//        if (type != null && !Objects.equals(type.getId(), selectedSalle.getId())) {
//            JsfUtil.addErrorMessage("cette salle '" + selectedSalle.getNomsalle() + "'" + "a été mis à jour");
//        }
        salleFacadeLocale.edit(selectedSalle);
        JsfUtil.addSuccessMessage("salle enregistrée");
    }

    @Override
    public void supprimerSalle() {

         if(selectedSalle == null || selectedSalle.getId() == null)
            return;
             
        List<Seancecours> sallecCours= seancecoursFacadeLocale.findBySalle(selectedSalle.getId());
      
        if (!sallecCours.isEmpty()) {
            utils.JsfUtil.addWarningMessage("Cette salle ne peut pas étre supprimé :car utilisee comme cle etrangere dans une autre cla");
        } else {

            salleFacadeLocale.remove(selectedSalle);
            JsfUtil.addErrorMessage("Cette salle a été supprimée");
            modifier = supprimer = detail = true;
        }
    }

 

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

   

}
