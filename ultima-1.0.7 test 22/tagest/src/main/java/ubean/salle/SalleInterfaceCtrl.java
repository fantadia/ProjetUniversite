/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.salle;


/**
 *
 * @author Fantastic
 */
public interface SalleInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Matiersmodule
     */
    public void enregistrerSalle();
    
    
    public void modifierSalle();
    
    /**
     * Fantastic
     * Supprimer le Matiersmodule
     * 
     */
    public void supprimerSalle();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
   
   
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
