/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.salle;


import entities.Matiersmodule;
import entities.Salles;
import java.util.List;
import javax.ejb.EJB;

import sessions.SallesFacadeLocal;
import sessions.SeancecoursFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractSalleCtrl {
    
    @EJB
    protected SallesFacadeLocal salleFacadeLocale ;
    
    @EJB
    protected SeancecoursFacadeLocal seancecoursFacadeLocale;
    
    
    
    
    protected List<Salles> listSalle;
    
    protected StringBuffer SalleTableHtml= new StringBuffer("pas encore implementé") ;
    protected Salles selectedSalle;
    protected Salles salle;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public SallesFacadeLocal getSalleFacadeLocale() {
        return salleFacadeLocale;
    }

    public void setSalleFacadeLocale(SallesFacadeLocal salleFacadeLocale) {
        this.salleFacadeLocale = salleFacadeLocale;
    }

    public Salles getSelectedSalle() {
        return selectedSalle;
    }

    public void setSelectedSalle(Salles selectedSalle) {
        this.selectedSalle = selectedSalle;
         if (selectedSalle== null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
         }
         
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Salles getSalle() 
    {
        return salle;
    }

    public void setSalle(Salles salle) {
        this.salle = salle;
    }

    public List<Salles> getListSalle() {
        return listSalle;
    }

    public void setListSalle(List<Salles> listSalle) {
        this.listSalle = listSalle;
    }
    
    

   
    
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    
    
    
    public void setSelectedM(Matiersmodule selectedMatiersmodule) {
        this.selectedSalle = selectedSalle;
        if (selectedMatiersmodule == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }
    
    public boolean isImprimer() {
        imprimer = salleFacadeLocale.findAll().isEmpty();
        return imprimer;
    }

   
    public StringBuffer getSalleTableHtml() {
        return SalleTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
