/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.seancecours;

import ubean.emploidetemps.*;




/**
 *
 * @author Fantastic
 */
public interface SeancecoursInterfaceCtrl {
    
    /**
     * cette fonction permet de save les filieres
     */
    public void enregistrerSeancecours();
    
    
    public void modifierSeancecours();
    
    /**
     * Fantastic
     * Supprimer le filiere
     * 
     */
    public void supprimerSeancecours();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerEmpTemps();
    
    public void imprimerEmpTempssHtml();
    
     public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
