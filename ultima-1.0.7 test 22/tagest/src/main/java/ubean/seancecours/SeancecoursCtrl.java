/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.seancecours;

import beans.util.JsfUtil;
import entities.Seancecours;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "seancecoursCtrl")
@ViewScoped
public class SeancecoursCtrl extends AbstractSeancecoursCtrl implements SeancecoursInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of UniteenseignementsCtrl
     */
    public SeancecoursCtrl() {
    }

    @PostConstruct
    private void initSeancecoursCtrl() {
        ListseancesCours = seancecoursFacadeLocal.findAll();
        seancecoursModifiee = new Seancecours();
        selectedSeancecours = new Seancecours();
        seancecours = new Seancecours();
        seancecours.setJour(new Date());
    }

    @Override
    public void enregistrerSeancecours() {

        try {

            seancecoursFacadeLocal.create(seancecours);
            initSeancecoursCtrl();
            JsfUtil.addSuccessMessage("Séance enregistrée avec succes");
            modifier = supprimer = detail = true;
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Une séance occupant ces heures a deja été programmée");
        }
    }

    @Override
    public void modifierSeancecours() {
       try{
           seancecoursFacadeLocal.edit(selectedSeancecours);
           JsfUtil.addSuccessMessage("Scéance  modifiée avec succès");
       }catch(Exception e){
           JsfUtil.addErrorMessage("Une séance occupant ces heures a deja été programmée");
       }
    }

    @Override
    public void supprimerSeancecours() {

//         if(selectedSeancecours == null || selectedSeancecours.getId() == null)
//            return;
//             
//        List<Seancecours> ept = seancecoursFacadeLocal.findByIdEmp_tps(selectedSeancecours.getId());
//        if (!ept.isEmpty()) {
//            utils.JsfUtil.addWarningMessage("impossible de supprimer emploi de temps : il est deja utilise par une  operation");
//        } else {
        seancecoursFacadeLocal.remove(selectedSeancecours);
        JsfUtil.addErrorMessage("Emploi de temps supprimée");
        modifier = supprimer = detail = true;
//        }
    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

    @Override
    public void imprimerEmpTemps() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEmpTempssHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
