/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.seancecours;

import entities.Seancecours;
import java.util.List;
import javax.ejb.EJB;
import sessions.ClassesFacadeLocal;
import sessions.EnseignantsFacadeLocal;
import sessions.HorairesFacadeLocal;
import sessions.MatiersmoduleFacadeLocal;
import sessions.SallesFacadeLocal;
import sessions.SeancecoursFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractSeancecoursCtrl {

    @EJB
    protected SeancecoursFacadeLocal emploidetempsFacadeLocal;
    @EJB
    protected EnseignantsFacadeLocal enseignantsFacadeLocal;
    @EJB
    protected HorairesFacadeLocal horairesFacadeLocal;
    @EJB
    protected MatiersmoduleFacadeLocal matiersmoduleFacadeLocal;
    @EJB
    protected ClassesFacadeLocal classesFacadeLocal;
    @EJB
    protected SallesFacadeLocal sallesFacadeLocal;
    @EJB
    protected SeancecoursFacadeLocal seancecoursFacadeLocal;

    protected List<Seancecours> ListseancesCours;

    protected StringBuffer seancecoursTableHtml = new StringBuffer("pas encore implementé");
    protected Seancecours selectedSeancecours;
    protected Seancecours seancecoursModifiee;
    protected Seancecours seancecours;

    public SeancecoursFacadeLocal getSeancecoursFacadeLocal() {
        return emploidetempsFacadeLocal;
    }

    public void setSeancecoursFacadeLocal(SeancecoursFacadeLocal emploidetempsFacadeLocal) {
        this.emploidetempsFacadeLocal = emploidetempsFacadeLocal;
    }

    public List<Seancecours> getListseancesCours() {
        ListseancesCours = seancecoursFacadeLocal.findAll();
        return ListseancesCours;
    }

    public void setListseancesCours(List<Seancecours> ListseancesCours) {
        this.ListseancesCours = ListseancesCours;
    }

    public Seancecours getSelectedSeancecours() {
        return selectedSeancecours;
    }

    public void setSelectedSeancecours(Seancecours selectedSeancecours) {
        this.selectedSeancecours = selectedSeancecours;
        if (selectedSeancecours == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Seancecours getSeancecoursModifiee() {
        return seancecoursModifiee;
    }

    public void setSeancecoursModifiee(Seancecours seancecoursModifiee) {
        this.seancecoursModifiee = seancecoursModifiee;
    }

    public Seancecours getSeancecours() {
        return seancecours;
    }

    public void setSeancecours(Seancecours seancecours) {
        this.seancecours = seancecours;
    }

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

//    public void setSelectedUniteenseignements(Uniteenseignements selectedUniteenseignements) {
//        this.selectedUniteenseignements = selectedUniteenseignements;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        uniteenseignementModifiee = new Uniteenseignements();
//        if(selectedUniteenseignements == null )
//            return;
//        uniteenseignementModifiee.setUniteenseignements(selectedUniteenseignements.getUniteenseignements());
//        uniteenseignementModifiee.setEtat(selectedUniteenseignements.getEtat());
//        uniteenseignementModifiee.setId(selectedUniteenseignements.getId());
//        uniteenseignementModifiee.setEtat(selectedUniteenseignements.getEtat());
//    }
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = seancecoursFacadeLocal.findAll().isEmpty();
        return imprimer;
    }

    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

}
