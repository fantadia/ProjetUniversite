/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.personnel;


import entities.Notes;
import entities.Personnel;
import java.util.List;
import javax.ejb.EJB;
import sessions.CompteuserFacadeLocal;


import sessions.HistoriquedeliberationFacade;

import sessions.NotesFacadeLocal;
import sessions.PersonnelFacadeLocal;



/**
 *
 * @author Fantastic
 */
public abstract class AbstractPersonnelCtrl {
    
    @EJB
    protected PersonnelFacadeLocal personnelFacadeLocal ;
    
    @EJB
    protected CompteuserFacadeLocal compteuserFacadeLocal ;
    
    protected List<Personnel> personnels;
    
    protected StringBuffer personnelsTableHtml = new StringBuffer("pas encore implementé") ;
    protected Personnel selectedPersonnel;
    protected Personnel personnel;
    
    
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public PersonnelFacadeLocal getPersonnelFacadeLocal() {
        return personnelFacadeLocal;
    }

    public void setPersonnelFacadeLocal(PersonnelFacadeLocal personnelFacadeLocal) {
        this.personnelFacadeLocal = personnelFacadeLocal;
    }

    public CompteuserFacadeLocal getCompteuserFacadeLocal() {
        return compteuserFacadeLocal;
    }

    public void setCompteuserFacadeLocal(CompteuserFacadeLocal compteuserFacadeLocal) {
        this.compteuserFacadeLocal = compteuserFacadeLocal;
    }

    public List<Personnel> getPersonnels() {
        return personnels;
    }

    public void setPersonnels(List<Personnel> personnels) {
        this.personnels = personnels;
    }

    public StringBuffer getPersonnelsTableHtml() {
        return personnelsTableHtml;
    }

    public void setPersonnelsTableHtml(StringBuffer personnelsTableHtml) {
        this.personnelsTableHtml = personnelsTableHtml;
    }

    public Personnel getSelectedPersonnel() {
        return selectedPersonnel;
    }

    public void setSelectedPersonnel(Personnel selectedPersonnel) {
        this.selectedPersonnel = selectedPersonnel;
        if (selectedPersonnel == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Personnel getPersonnel() {
        return personnel;
    }

    public void setPersonnel(Personnel personnel) {
        this.personnel = personnel;
    }
    
    
    
    
    
  
    
  
    
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = personnelFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
 
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
