/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.personnel;

import beans.util.JsfUtil;
import entities.Compteuser;
import entities.Matiersmodule;
import entities.Etudiants;
import entities.Historiquedeliberation;
import entities.Notes;
import entities.Personnel;
import entities.Seancecours;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import sessions.EtudiantsFacadeLocal;
import ubean.note.AbstractNoteCtrl;
import ubean.note.NoteInterfaceCtrl;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfMatiersmodule;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "personnelCtrl")
@ViewScoped
public class PersonnelCtrl extends AbstractPersonnelCtrl implements PersonnelInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public PersonnelCtrl() {
    }
    
    

    @PostConstruct
    private void initPersonnel() {
        personnels = personnelFacadeLocal.findAll();
        selectedPersonnel = new Personnel();
        personnel = new Personnel();
        personnel.setDateNaissance(new  Date());

    }
    
    @Override
    public void enregistrerPersonnel() {
        Personnel n = personnelFacadeLocal.findByIdPersonnel(personnel.getId());
        if (n != null) {
            JsfUtil.addErrorMessage("Cette note existe déjà" + "'" + personnel.getNom());
            modifier = supprimer = detail = true;
            return;

        }
        personnelFacadeLocal.create(personnel);
        initPersonnel();
        JsfUtil.addSuccessMessage("le personnel à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierPersonnel() {
        
        if (selectedPersonnel == null || selectedPersonnel.getId() == null) {
            return;
        }

        Personnel type = personnelFacadeLocal.findByIdPersonnel(selectedPersonnel.getId());
        if (type != null && !Objects.equals(type.getId(), selectedPersonnel.getId())) {
            JsfUtil.addErrorMessage("le personnel '" + selectedPersonnel.getNom()+ "'" + "a été mis à jour");
        }
        personnelFacadeLocal.edit(selectedPersonnel);
        JsfUtil.addSuccessMessage("personnel enregistrée");
    }

    @Override
    public void supprimerPersonnel() {
        
         if(selectedPersonnel == null || selectedPersonnel.getId() == null)
            return;
             
        List<Compteuser> listCompteUser = compteuserFacadeLocal.findByPersonnel(selectedPersonnel.getId());
       
        if (!listCompteUser.isEmpty()) {
            utils.JsfUtil.addWarningMessage("Ce personnel ne peut pas etre supprimé : elle est deja utilise dans une  operation");
        } else {

            personnelFacadeLocal.remove(selectedPersonnel);
            JsfUtil.addErrorMessage("Ce personnel à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerPersonnelPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerPersonnelHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

        
}

