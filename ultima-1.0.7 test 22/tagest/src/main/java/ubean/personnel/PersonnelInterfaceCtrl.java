/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.personnel;


/**
 *
 * @author Fantastic
 */
public interface PersonnelInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Matiersmodule
     */
    public void enregistrerPersonnel();
    
    
    public void modifierPersonnel();
    
    /**
     * Fantastic
     * Supprimer le Matiersmodule
     * 
     */
    public void supprimerPersonnel();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerPersonnelPdf();
    
    public void imprimerPersonnelHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
