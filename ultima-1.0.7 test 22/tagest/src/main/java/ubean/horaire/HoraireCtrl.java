/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.horaire;


import beans.util.JsfUtil;
import entities.Filieres;
import entities.Etudiants;
import entities.Filieres;
import entities.Horaires;
import entities.Seancecours;
import entities.Uniteenseignements;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "horaireCtrl")
@ViewScoped
public class HoraireCtrl extends AbstractHoraireCtrl implements HoraireInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of FilieresCtrl
     */
    public HoraireCtrl() {
    }

    @PostConstruct
    private void initHoraire() {
        horaires = horaireFacadeLocale.findAll();
        selectedHoraire = new Horaires();
        horaireModifiee=new Horaires();
        horaire = new Horaires();

    }

    @Override
    public void enregistrerHoraire() {
        Horaires hor =horaireFacadeLocale.findByIdHoraire(horaire.getId());
        if (hor != null) {
            JsfUtil.addErrorMessage("Cette Horaire existe déjà" + "'" + hor.getId());
            modifier = supprimer = detail = true;
            return;

        }
        horaireFacadeLocale.create(horaire);
        initHoraire();
        JsfUtil.addSuccessMessage("Horaire créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierHoraire() {
        if (selectedHoraire == null || selectedHoraire.getId() == null) {
            return;
        }

        Horaires type = horaireFacadeLocale.findByIdHoraire(selectedHoraire.getId());
        if (type != null && !Objects.equals(type.getId(), selectedHoraire.getId())) {
            JsfUtil.addErrorMessage("Horaire '" + selectedHoraire.getHeuredebut()+ "  " +selectedHoraire.getHeurefin()+ " a été deja mis à jour");
        }
        horaireFacadeLocale.edit(selectedHoraire);
        JsfUtil.addSuccessMessage("Horaire enregistrée");
    }

    @Override
    public void supprimerHoraire() {

         if(selectedHoraire == null || selectedHoraire.getId() == null)
            return;
             
            horaireFacadeLocale.remove(selectedHoraire);
            JsfUtil.addErrorMessage("Cette horaire à été supprimée");
            modifier = supprimer = detail = true;
        
    }

   

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

    

    @Override
    public void imprimerFilieressHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   

}
