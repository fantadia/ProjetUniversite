/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.horaire;



/**
 *
 * @author Fantastic
 */
public interface HoraireInterfaceCtrl {
    
    /**
     * cette fonction permet de save les filieres
     */
    public void enregistrerHoraire();
    
    
    public void modifierHoraire();
    
    /**
     * Fantastic
     * Supprimer le filiere
     * 
     */
    public void supprimerHoraire();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
   
    
    public void imprimerFilieressHtml();
    
     public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
