/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.horaire;


import entities.Filieres;
import entities.Horaires;
import java.util.List;
import javax.ejb.EJB;
import sessions.HorairesFacadeLocal;
import sessions.SeancecoursFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractHoraireCtrl {
    
    @EJB
    protected HorairesFacadeLocal horaireFacadeLocale;
    
     @EJB
    protected SeancecoursFacadeLocal seancecoursFacadeLocal ;
    
    protected List<Horaires> horaires;
    
    protected StringBuffer filieresTableHtml = new StringBuffer("pas encore implementé") ;
    protected Horaires selectedHoraire;
    protected Horaires horaireModifiee;
    protected Horaires horaire;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
  
    public Horaires getselectedHoraire() {
        return selectedHoraire;
    }

    public Horaires getHoraire() {
        return horaire;
    }

    public void setHoraire(Filieres filiere) {
        this.horaire = horaire;
    }

    public Horaires getSelectedHoraire() {
        return selectedHoraire;
    }

    public List<Horaires> getHoraires() {
        return horaires;
    }

    public void setHoraires(List<Horaires> horaires) {
        this.horaires = horaires;
    }
    
    
    
    

    public Horaires getHoraireModifiee() {
        return horaireModifiee;
    }

    public void setHoraireModifiee(Horaires horaire_modifie ){
        this.horaireModifiee = horaire_modifie;
    }
    
//    public void setSelectedFilieres(Filieres selectedFilieres) {
//        this.selectedFilieres = selectedFilieres;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        filiereModifiee = new Filieres();
//        if(selectedFilieres == null )
//            return;
//        filiereModifiee.setFilieres(selectedFilieres.getFilieres());
//        filiereModifiee.setEtat(selectedFilieres.getEtat());
//        filiereModifiee.setId(selectedFilieres.getId());
//        filiereModifiee.setEtat(selectedFilieres.getEtat());
//    }
    
    
    
    public void setSelectedHoraire(Horaires selectedHoraire) {
        this.selectedHoraire = selectedHoraire;
        if (selectedHoraire == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = horaireFacadeLocale.findAll().isEmpty();
        return imprimer;
    }
    
   

    public void setHoraire(Horaires horaire) {
        this.horaire = horaire;
    }

    public StringBuffer getFilieresTableHtml() {
        return filieresTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
