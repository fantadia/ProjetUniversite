/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.classes;

import beans.util.JsfUtil;
import entities.Classes;
import entities.Etudiants;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
//import pdf.PdfClasses;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "classeCtrl")
@ViewScoped
public class ClasseCtrl extends AbstractClasseCtrl implements ClasseInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of ClassesCtrl
     */
    public ClasseCtrl() {
    }

    @PostConstruct
    private void initClasses() {
        classes = classeFacadeLocal.findAll();
        selectedClasse = new Classes();
        classe = new Classes();

    }

    @Override
    public void enregistrerClasse() {
        Classes cl = classeFacadeLocal.findByClasses(classe.getDenomination());
        if (cl != null) {
            JsfUtil.addErrorMessage("Cette classe existe déjà" + "'" + cl.getDenomination());
            modifier = supprimer = detail = true;
            return;

        }
        classeFacadeLocal.create(classe);
        initClasses();
        JsfUtil.addSuccessMessage("la classe à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifier() {
        if (selectedClasse == null || selectedClasse.getId() == null) {
            return;
        }

        Classes type = classeFacadeLocal.findByClasses(selectedClasse.getDenomination());
        if (type != null && !Objects.equals(type.getId(), selectedClasse.getId())) {
            JsfUtil.addErrorMessage("la classe '" + selectedClasse.getDenomination() + "'" + "a été mis à jour");
        }
        classeFacadeLocal.edit(selectedClasse);
        JsfUtil.addSuccessMessage("Classes enregistrée");
    }

    @Override
    public void supprimer() {

         if(selectedClasse == null || selectedClasse.getId() == null)
            return;
             
        List<Etudiants> etudiant = etudiantsFacadeLocal.findByIdClasses(selectedClasse.getId());
        if (!etudiant.isEmpty()) {
            utils.JsfUtil.addWarningMessage("La classe n'a pas éte supprimé : elle est deja utilise par une  operation");
        } else {

            classeFacadeLocal.remove(selectedClasse);
            JsfUtil.addErrorMessage("Cette classe à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerClassesPdf() {
        
//        classes = classeFacadeLocal.findAll();
//        fileName = PdfClasses.etatsClasses(classes);
//        System.out.println("Impression pdf des classes");
    }

    @Override
    public void imprimerClassesHtml() {
        System.out.println("Impression html des classes");

    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

   

}
