/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.classes;

/**
 *
 * @author Fantastic
 */
public interface ClasseInterfaceCtrl {
    
    /**
     * cette fonction permet de save les classes
     */
    public void enregistrerClasse();
    
    
    public void modifier();
    
    /**
     * Fantastic
     * Supprimer le classe
     * 
     */
    public void supprimer();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerClassesPdf();
    
    public void imprimerClassesHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
