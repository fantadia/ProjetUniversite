/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.classes;

import ubean.classes.*;
import entities.Classes;
import java.util.List;
import javax.ejb.EJB;
import sessions.ClassesFacadeLocal;
import sessions.EtudiantsFacadeLocal;
import sessions.MoisFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractClasseCtrl {
    
    @EJB
    protected ClassesFacadeLocal classeFacadeLocal ;
    
    @EJB
    protected EtudiantsFacadeLocal etudiantsFacadeLocal ;

    @EJB
    protected MoisFacadeLocal moisFacadeLocal ;
    
    
    protected List<Classes> classes;
    
    protected StringBuffer classesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Classes selectedClasse;
    protected Classes classeModifiee;
    protected Classes classe;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
    public List<Classes> getClasses() {
        classes = classeFacadeLocal.findAll();
        return classes;
    }

    public Classes getSelectedClasse() {
        return selectedClasse;
    }

    public Classes getClasseModifiee() {
        return classeModifiee;
    }

    public void setClasseModifiee(Classes classeModifiee) {
        this.classeModifiee = classeModifiee;
    }
    
//    public void setSelectedClasse(Classes selectedClasse) {
//        this.selectedClasse = selectedClasse;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        classeModifiee = new Classes();
//        if(selectedClasse == null )
//            return;
//        classeModifiee.setClasse(selectedClasse.getClasse());
//        classeModifiee.setEtat(selectedClasse.getEtat());
//        classeModifiee.setId(selectedClasse.getId());
//        classeModifiee.setEtat(selectedClasse.getEtat());
//    }
    
    
    
    public void setSelectedClasse(Classes selectedClasse) {
        this.selectedClasse = selectedClasse;
        if (selectedClasse == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = classeFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
    public Classes getClasse() {
        return classe;
    }

    public void setClasse(Classes classe) {
        this.classe = classe;
    }

    public StringBuffer getClassesTableHtml() {
        return classesTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
