/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.matiere;


import entities.Matiersmodule;
import java.util.List;
import javax.ejb.EJB;
import sessions.MatiersmoduleFacadeLocal;
import sessions.EtudiantsFacadeLocal;
import sessions.MoisFacadeLocal;
import sessions.NotesFacadeLocal;
import sessions.SeancecoursFacade;
import sessions.SeancecoursFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractMatiereCtrl {
    
    @EJB
    protected MatiersmoduleFacadeLocal matiersmoduleFacadeLocal ;
    
    @EJB
    protected SeancecoursFacadeLocal seancecoursFacadeLocal ;
    
     @EJB
    protected NotesFacadeLocal notesFacadeLocal ;

    @EJB
    protected MoisFacadeLocal moisFacadeLocal ;
    
    
    protected List<Matiersmodule> Matiersmodules;
    
    protected StringBuffer MatiersmodulesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Matiersmodule selectedMatiersmodule;
    protected Matiersmodule matiersmodule;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
    
    

    public Matiersmodule getSelectedMatiersmodule() {
        return selectedMatiersmodule;
    }

    public Matiersmodule getMatiersmodule() {
        return matiersmodule;
    }

    public void setMatiersmodule(Matiersmodule matiersmodule) {
        this.matiersmodule = matiersmodule;
    }

    public List<Matiersmodule> getMatiersmodules() {
        return Matiersmodules;
    }

    public void setMatiersmodules(List<Matiersmodule> Matiersmodules) {
        this.Matiersmodules = Matiersmodules;
    }
    
    
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    
    
    
    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
        this.selectedMatiersmodule = selectedMatiersmodule;
        if (selectedMatiersmodule == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = matiersmoduleFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
 

    public StringBuffer getMatiersmoduleTableHtml() {
        return MatiersmodulesTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
