/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.matiere;


/**
 *
 * @author Fantastic
 */
public interface MatiereInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Matiersmodule
     */
    public void enregistrerMatiersmodule();
    
    
    public void modifier();
    
    /**
     * Fantastic
     * Supprimer le Matiersmodule
     * 
     */
    public void supprimer();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerMatiersmodulesPdf();
    
    public void imprimerMatiersmodulesHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
