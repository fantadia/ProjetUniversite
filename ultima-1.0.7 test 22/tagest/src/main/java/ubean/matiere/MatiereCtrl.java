/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.matiere;

import beans.util.JsfUtil;
import entities.Matiersmodule;
import entities.Etudiants;
import entities.Notes;
import entities.Seancecours;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import sessions.EtudiantsFacadeLocal;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfMatiersmodule;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "matiersmoduleCtrl")
@ViewScoped
public class MatiereCtrl extends AbstractMatiereCtrl implements MatiereInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public MatiereCtrl() {
    }

    @PostConstruct
    private void initMatiersmodule() {
        Matiersmodules = matiersmoduleFacadeLocal.findAll();
        selectedMatiersmodule = new Matiersmodule();
        matiersmodule = new Matiersmodule();

    }

    @Override
    public void enregistrerMatiersmodule() {
        Matiersmodule cl = matiersmoduleFacadeLocal.findByMatiersmodule(matiersmodule.getNommatiere());
        if (cl != null) {
            JsfUtil.addErrorMessage("Cette matiersmodule existe déjà" + "'" + cl.getNommatiere());
            modifier = supprimer = detail = true;
            return;

        }
        matiersmoduleFacadeLocal.create(matiersmodule);
        initMatiersmodule();
        JsfUtil.addSuccessMessage("la matiersmodule à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifier() {
        if (selectedMatiersmodule == null || selectedMatiersmodule.getId() == null) {
            return;
        }

        Matiersmodule type = matiersmoduleFacadeLocal.findByMatiersmodule(selectedMatiersmodule.getNommatiere());
        if (type != null && !Objects.equals(type.getId(), selectedMatiersmodule.getId())) {
            JsfUtil.addErrorMessage("la matiersmodule '" + selectedMatiersmodule.getNommatiere() + "'" + "a été mis à jour");
        }
        matiersmoduleFacadeLocal.edit(selectedMatiersmodule);
        JsfUtil.addSuccessMessage("Matiersmodule enregistrée");
    }

    @Override
    public void supprimer() {

         if(selectedMatiersmodule == null || selectedMatiersmodule.getId() == null)
            return;
             
        List<Seancecours> seanceCours = seancecoursFacadeLocal.findByIdMatiersmodule(selectedMatiersmodule.getId());
        List<Notes> notes = notesFacadeLocal.findByIdMatiersmodule(selectedMatiersmodule.getId());
        if ((!seanceCours.isEmpty())||(!notes.isEmpty())) {
            utils.JsfUtil.addWarningMessage("Cette matier n'a pas éte supprimé : elle est deja utilise par une  operation");
        } else {

            matiersmoduleFacadeLocal.remove(selectedMatiersmodule);
            JsfUtil.addErrorMessage("Cette matiersmodule à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerMatiersmodulesPdf() {
        
//        Matiersmodules = matiersmoduleFacadeLocal.findAll();
//        fileName = PdfMatiersmodule.etatsMatiersmodule(Matiersmodules);
//        System.out.println("Impression pdf des Matiersmodules");
    }

    @Override
    public void imprimerMatiersmodulesHtml() {
        System.out.println("Impression html des Matiersmodules");

    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

   

}
