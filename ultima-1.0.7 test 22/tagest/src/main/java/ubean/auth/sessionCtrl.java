/*
 * To Ulrich TIAYO NGNINGAHE change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.auth;

import entities.Compteuser;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import sessions.CompteuserFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author server
 */
@Named("sessionCtrl")
@SessionScoped
public class sessionCtrl implements Serializable {

    private static final Logger LOG = Logger.getLogger(sessionCtrl.class.getName());

    @EJB
    private CompteuserFacadeLocal compteuserFacadeLocal;

    private Compteuser userAccount;
    private Compteuser userconnected;
    private String language = "fr";
    private String login;
    private String motDePasse;
    private boolean connecte = false;

    public sessionCtrl() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    @PostConstruct
    public void init() {
        userAccount = new Compteuser();
    }

    public void switch_fr() {
        language = "fr";
    }

    public void switch_en() {
        language = "en";
    }

    public void authentification() {
        try {
            userAccount = compteuserFacadeLocal.findByLoginAndPassword(login, motDePasse);
            String sc = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
            if (userAccount != null) {
                UtilitaireSession.getInstance().setuser(userAccount);
                JsfUtil.addSuccessMessage("Bienvenue dans votre espace de travail");
                FacesContext.getCurrentInstance().getExternalContext().redirect(sc + "/dashboard.xhtml");

            } else {
                JsfUtil.addErrorMessage("Echec d'authentification, verifiez vos paramètres de connexion");
                userAccount = new Compteuser();
                motDePasse = null;
            }
        } catch (IOException e) {
            LOG.log(Level.SEVERE, null, e);
            JsfUtil.addErrorMessage("Erreur!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            System.err.println("erreur de recherche d: " + e.getMessage());
        }
    }

    public void deconnexion() {
        try {
            String sc = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();

            UtilitaireSession.getInstance().setuser(null);
            FacesContext.getCurrentInstance().getExternalContext().redirect(sc + "/login.xhtml");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Uitlisateur déconnecté ", ""));
        } catch (IOException ex) {
            Logger.getLogger(sessionCtrl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isConnecte() {
        return connecte;
    }

    public void setConnecte(boolean connecte) {
        this.connecte = connecte;
    }

    public Object getUser() {
        return userAccount;
    }

    public void setUser(Object user) {
        this.userAccount = (Compteuser) user;
    }

    public Compteuser getUtilisateur() {
        return userAccount;
    }

    public void setUtilisateur(Compteuser utilisateur) {
        this.userAccount = utilisateur;
    }

    public Compteuser getUserconnected() {
        userconnected = UtilitaireSession.getInstance().getuser();
        String sc = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        if (userconnected == null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(sc + "/login.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(sessionCtrl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return userconnected;
    }

    public void watcher() {
        System.out.println("Watcher");
        List<Compteuser> fonctionnaires = compteuserFacadeLocal.findAll();
        try {
            if (fonctionnaires.isEmpty()) {
                String sc = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
                ((FacesContext.getCurrentInstance()).getExternalContext()).redirect(sc + "/parametrage/index.xhtml");
                JsfUtil.addSuccessMessage("Veuillez Créer un utilisateur pour commencer");
            }
            String sc = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
            if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("user")) {
                JsfUtil.addErrorMessage("Veuillez vous connecter!");
                ((FacesContext.getCurrentInstance()).getExternalContext()).redirect(sc + "/login.xhtml");
                JsfUtil.addErrorMessage("Veuillez vous connecter!");

            }
        } catch (IOException e) {
        }
    }

    public void watcher2() {
        List<Compteuser> fonctionnaires = compteuserFacadeLocal.findAll();
        try {
            if (fonctionnaires.isEmpty()) {
                return;
            }
            String sc = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
            if (!FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey("user")) {
                JsfUtil.addErrorMessage("Veuillez vous connecter!");
                ((FacesContext.getCurrentInstance()).getExternalContext()).redirect(sc + "/login.xhtml");
                JsfUtil.addErrorMessage("Veuillez vous connecter!");

            }
        } catch (IOException e) {
            LOG.log(Level.SEVERE, null, e);
        }
    }

    public Compteuser getCompteuser() {
        return userAccount;
    }

    public void setCompteuser(Compteuser fonctionnaire) {
        this.userAccount = fonctionnaire;
    }

}
