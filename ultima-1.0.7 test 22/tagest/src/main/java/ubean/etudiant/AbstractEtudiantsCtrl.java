/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.etudiant;

import entities.AnneeScolaire;
import entities.Classes;
import entities.Etudiants;
import java.util.List;
import javax.ejb.EJB;
import sessions.AnneeScolaireFacadeLocal;
import sessions.ClassesFacadeLocal;

import sessions.MoisFacadeLocal;
import sessions.EtudiantsFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractEtudiantsCtrl {

    @EJB
    protected ClassesFacadeLocal classesFacadeLocal;
    protected Classes classe;

    @EJB
    protected EtudiantsFacadeLocal etudiantFacadeLocal;

    @EJB
    protected MoisFacadeLocal moisFacadeLocal;

    @EJB
    protected AnneeScolaireFacadeLocal anneeScolaireFacadeLocal;

    protected List<AnneeScolaire> anneeScolaires;
    protected List<Classes> classes;
    protected List<String> sexes;
    protected String pdfFileName;

    public List<Classes> getClasses() {
        classes = classesFacadeLocal.findAll();
        return classes;
    }

    public void setClasses(List<Classes> classes) {
        this.classes = classes;
    }

//    protected List<Charge> chargesCreation;
//    protected List<Charge> chargesEdition;
    protected AnneeScolaire selectedAnneeScolaire;
    protected AnneeScolaire anneeScolaire;

    protected List<Etudiants> etudiants;

    protected Etudiants selectedEtudiants;

    protected Classes selectedClasse;

    protected Etudiants etudiant;
    protected Classes classes1;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public List<Etudiants> getEtudiants() {
        return etudiants;
    }

    public void setEtudiantses(List<Etudiants> etudiants) {
        this.etudiants = etudiants;
    }

    public Classes getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classes selectedClasse) {
        this.selectedClasse = selectedClasse;
    }

    public Classes getClasses1() {
        return classes1;
    }

    public void setClasses1(Classes classes1) {
        this.classes1 = classes1;
    }

    public List<AnneeScolaire> getAnneeScolaires() {
        anneeScolaire = (AnneeScolaire) anneeScolaireFacadeLocal.findAll();
        return anneeScolaires;
    }

    public void setAnneeScolaires(List<AnneeScolaire> anneeScolaires) {
        this.anneeScolaires = anneeScolaires;
    }

    public AnneeScolaire getAnneeScolaire() {
        return anneeScolaire;
    }

    public void setAnneeScolaire(AnneeScolaire anneeScolaire) {
        this.anneeScolaire = anneeScolaire;
    }

    public List<String> getSexes() {
        return sexes;
    }

//    public List<Compte> getComptesEdition() {
//        if (selectedEtudiants.getDevise() != null) {
//            comptesEdition = compteFacadeLocal.findByIdDevise(selectedEtudiants.getDevise().getId());
//        } else {
//            comptesEdition = new ArrayList<>();
//        }
//        return comptesEdition;
//    }
//    
//    public List<Compte> getComptes() {
//        if (etudiant.getDevise() != null) {
//            comptes = compteFacadeLocal.findByIdDevise(etudiant.getDevise().getId());
//        } else {
//            comptes = new ArrayList<>();
//        }
//        return comptes;
//    }
    public void setSexes(List<String> sexes) {
        this.sexes = sexes;
    }

    public Etudiants getSelectedEtudiants() {
        return selectedEtudiants;
    }

    public void setSelectedEtudiants(Etudiants selectedEtudiants) {
        this.selectedEtudiants = selectedEtudiants;
        if (selectedEtudiants == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

//    public Etudiants getEtudiants() {
//        return etudiant;
//    }
    public void setEtudiants(Etudiants etudiant) {
        this.etudiant = etudiant;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public void setModifier(boolean modifier) {
        this.modifier = modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public void setSupprimer(boolean supprimer) {
        this.supprimer = supprimer;
    }

    public boolean isImprimer() {
        imprimer = !etudiantFacadeLocal.findAll().isEmpty();
        return !imprimer;
    }

    public void setImprimer(boolean imprimer) {
        this.imprimer = imprimer;
    }

    public boolean isDetail() {
        return !detail;
    }

    public void setDetail(boolean detail) {
        this.detail = detail;
    }

    public Etudiants getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiants etudiant) {
        this.etudiant = etudiant;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

}
