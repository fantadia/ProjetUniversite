/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.etudiant;


/**
 *
 * @author fantastic
 */
public interface EtudiantsInterfaceCtrl {
    
    /**
     * @author Fantastic
     * Cette méthode gère l'enregistrement d'un etudiant
     */
    public void enregistrerEtudiants();
    
    /**
     * @author Fantastic
     * Modifier le mois sélectionné
     */
    public void modifier();
    
    /**
     * @author Fantastic
     * Supprimer l'etudiant sélectionné
     * Si l'etudiant n'est pas utilisé alors il est supprimé.
     */
    public void supprimer();
    
    /**
     * @author Fantastic
     * Méthode d'impression
     */
    public void imprimerEtudiantsPdf();
    /**
     * Méthode qui affiche les etudiants par classe
     */
    
    public void listeEtudiantsClasse();
    
    /**
     * @author Fantastic
     * Méthode d'impression
     */
    public void imprimerEtudiantsHtml();
    
    /**
     * @author Fantastic
     * Méthode de l'info bulle
     * @param etudiantComponent
     * @param defaultMessage
     * @return 
     */
     public String getComponentMessages(String clientComponent, String defaultMessage);
     
     /**
      * Permet, losque la charge sélectionnée change d'initialiser la description
      */
     public void handleEtudiantChange();
    
}
