/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.etudiant;

import entities.Classes;
import entities.Etudiants;
import interfaces.Genre;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import pdf7.EtudiantsParClassePdfDocument;
import utils.JsfUtil;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "etudiantCtrl")
@ViewScoped
public class EtudiantsCtrl extends AbstractEtudiantsCtrl implements EtudiantsInterfaceCtrl, Serializable {

    private static final Logger LOG = Logger.getLogger(EtudiantsCtrl.class.getName());

    public EtudiantsCtrl() {
    }

    @PostConstruct
    private void initEtudiants() {
        selectedEtudiants = new Etudiants();
        initGenres();
        etudiants = etudiantFacadeLocal.findAll();
        etudiant = new Etudiants();
        classes1 = new Classes();
        selectedClasse = null;
        anneeScolaires = anneeScolaireFacadeLocal.findAll();
//       etudiant.setAnnee(new Date());
    }

    private void initGenres() {
        sexes = new ArrayList<>();
        sexes.add(Genre.FEMME);
        sexes.add(Genre.HOMME);
    }

    @Override
    public void enregistrerEtudiants() {
        Etudiants et = etudiantFacadeLocal.findByINumerocarte(etudiant.getNumerocarte());
        if (et != null) {
            JsfUtil.addErrorMessage("Cet etudiant existe déjà" + "'" + et.getNumerocarte());
            modifier = supprimer = detail = true;
            return;
        }

        etudiant.setClasse(classesFacadeLocal.find(1));
        etudiantFacadeLocal.create(etudiant);
        initEtudiants();
        JsfUtil.addSuccessMessage("Etudiant enregistré");
        modifier = supprimer = detail = true;

    }

    @Override
    public void modifier() {
        if (selectedEtudiants == null || selectedEtudiants.getId() == null) {
            return;
        }
//        Etudiants type = etudiantFacadeLocal.findByINumerocarte(selectedEtudiants.getNumerocarte());
//        if (type != null && !Objects.equals(type.getNumerocarte(), selectedEtudiants.getId())) {
//            JsfUtil.addErrorMessage("l'etudiant '" + selectedEtudiants.getNumerocarte() + " ' existe déjà ");
//            return;
//        }

        etudiantFacadeLocal.edit(selectedEtudiants);
        JsfUtil.addSuccessMessage("l'udiant de matricule'" + selectedEtudiants.getNumerocarte() + "'" + " a été mis à jour");
    }

    @Override
    public void supprimer() {
        etudiantFacadeLocal.remove(selectedEtudiants);
        JsfUtil.addSuccessMessage("L'etudiant a ete supprime");
        modifier = supprimer = detail = true;

    }

    @Override
    public void imprimerEtudiantsPdf() {
        try {
            EtudiantsParClassePdfDocument doc = new EtudiantsParClassePdfDocument(etudiantFacadeLocal.findAllSortByClasse());
            pdfFileName = doc.print();
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void imprimerEtudiantsHtml() {
        System.out.println("Impression html des etudiants");
    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

//    @Override
//    public void handleEtudiantChange() {
//        if (etudiant.getEtudiants != null) {
//            etudiant.setDescription(etudiant.getEtudiant().getLibelle() + ": ");
//        } else {
//            etudiant.setDescription("");
//        }
//    }
    @Override
    public void handleEtudiantChange() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void listeEtudiantsClasse() {
        if (selectedClasse == null) {
            etudiants = etudiantFacadeLocal.findAll();
            return;
        }
        etudiants = etudiantFacadeLocal.findEtudiantByclasse(selectedClasse.getId());
        if (etudiants.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun etudiant enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
    }

}
