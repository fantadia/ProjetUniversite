/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.emploidetemps;


import beans.util.JsfUtil;
import entities.Emploidetemps;
import entities.Matiersmodule;
import entities.Seancecours;
import entities.Uniteenseignements;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
//import pdf.PdfUniteenseignements;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "emploiDeTpsCrtl")
@ViewScoped
public class EmploidetempsCtrl extends AbstractEmploiDeTpsCtrl implements EmploidetempsInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of UniteenseignementsCtrl
     */
    public EmploidetempsCtrl() {
    }

    @PostConstruct
    private void initEmploidetempsCtrl() {
        
        emplois = emploidetempsFacadeLocal.findAll();
        emploiDeTempsModifiee = new Emploidetemps();
        selectedEmploiDeTemps = new Emploidetemps();
        emploiDeTemps=new Emploidetemps();
        emploiDeTemps.setPeriodedebut(new  Date());
        emploiDeTemps.setPeriodefin(new Date());;

    }

    @Override
    public void enregistrerEmpDeTemps() {
        Emploidetemps cl = emploidetempsFacadeLocal.findByIdEmploiDeTps(emploiDeTemps.getId());
        if (cl != null) {
            JsfUtil.addErrorMessage("Emploi de temps existe déjà" + "'" + cl.getTitre());
            modifier = supprimer = detail = true;
            return;

        }
        emploidetempsFacadeLocal.create(emploiDeTemps);
        initEmploidetempsCtrl();
        JsfUtil.addSuccessMessage("Emploi de temps créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEmpDeTemps() {
        if (selectedEmploiDeTemps == null || selectedEmploiDeTemps.getId() == null) {
            return;
        }

        Emploidetemps type = emploidetempsFacadeLocal.findByIdEmploiDeTps(selectedEmploiDeTemps.getId());
        if (type != null && !Objects.equals(type.getId(), selectedEmploiDeTemps.getId())) {
            JsfUtil.addErrorMessage("l'unité enseignement '" + selectedEmploiDeTemps.getTitre()+ "'" + "a été mis à jour");
        }
        emploidetempsFacadeLocal.edit(selectedEmploiDeTemps);
        JsfUtil.addSuccessMessage("Emploi de temps modifie");
    }

    @Override
    public void supprimerEmpDeTemps() {

         if(selectedEmploiDeTemps == null || selectedEmploiDeTemps.getId() == null)
            return;
             
        List<Seancecours> ept = seancecoursFacadeLocal.findByIdEmp_tps(selectedEmploiDeTemps.getId());
        if (!ept.isEmpty()) {
            utils.JsfUtil.addWarningMessage("impossible de supprimer emploi de temps : il est deja utilise par une  operation");
        } else {

            emploidetempsFacadeLocal.remove(selectedEmploiDeTemps);
            JsfUtil.addErrorMessage("Emploi de temps supprimée");
            modifier = supprimer = detail = true;
        }
    }

   

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

    @Override
    public void imprimerEmpTemps() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEmpTempssHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

}
