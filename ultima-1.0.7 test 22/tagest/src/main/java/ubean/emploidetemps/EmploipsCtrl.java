/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.emploidetemps;

import entities.Emploidetemps;
import entities.Seancecours;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Fantastic
 */
@Named("gestionEmploiTpsCrtl")
@SessionScoped
public class EmploipsCtrl extends AbstractEmploiDeTpsCtrl implements Serializable {

    private ScheduleModel eventModel;
    private ScheduleModel lazyEventModel;
    private Seancecours seancecours;
    private Seancecours selectedSeance = null;
    private int nombreSeances = 1;

    private ScheduleEvent event = new DefaultScheduleEvent();

    /**
     * Creates a new instance of UniteenseignementsCtrl
     */
    public EmploipsCtrl() {
    }

    @PostConstruct
    private void initEmploidetempsCtrl() {
        emplois = emploidetempsFacadeLocal.findAll();
        emploiDeTempsModifiee = new Emploidetemps();
        selectedEmploiDeTemps = new Emploidetemps();
        emploiDeTemps = new Emploidetemps();
        emploiDeTemps.setPeriodedebut(new Date());
        emploiDeTemps.setPeriodefin(new Date());

        eventModel = new DefaultScheduleModel();
        seancecours = new Seancecours();
        classes = classesFacadeLocal.findAll();
        enseignants = enseignantsFacadeLocal.findAll();
        horaires = horairesFacadeLocal.findAll();
        matiersmodules = matiersmoduleFacadeLocal.findAll();
        salles = sallesFacadeLocal.findAll();
//        eventModel.addEvent(new DefaultScheduleEvent("Champions League Match", previousDay8Pm(), previousDay11Pm()));
//        eventModel.addEvent(new DefaultScheduleEvent("Birthday Party", today1Pm(), today6Pm()));
//        eventModel.addEvent(new DefaultScheduleEvent("Breakfast at Tiffanys", nextDay9Am(), nextDay11Am()));
//        eventModel.addEvent(new DefaultScheduleEvent("Plant the new garden stuff", theDayAfter3Pm(), fourDaysLater3pm()));
//         
//        lazyEventModel = new LazyScheduleModel() {
//             
//            @Override
//            public void loadEvents(Date start, Date end) {
//                Date random = getRandomDate(start);
//                addEvent(new DefaultScheduleEvent("Lazy Event 1", random, random));
//                 
//                random = getRandomDate(start);
//                addEvent(new DefaultScheduleEvent("Lazy Event 2", random, random));
//            }   
//        };

    }

    public void handleEmploiDuTempsSelection() {
        eventModel = new DefaultScheduleModel();
        seancecours = new Seancecours();
        if(selectedEmploiDeTemps == null){
            return;
        }
        List<Seancecours> seances = seancecoursFacadeLocal.findByIdEmp_tps(selectedEmploiDeTemps.getId());
        seances.forEach((s) -> {
            Date deb = (Date) s.getDatesceance().clone();
            Calendar end = Calendar.getInstance();
            end.setTime(s.getDatesceance());
            int duree = 120;
            if (s.getDuree() != null) {
                duree = s.getDuree();
            }
            end.add(Calendar.MINUTE, duree);
            eventModel.addEvent(new DefaultScheduleEvent(s.getMatiere().getNommatiere(), deb, end.getTime(), s));
        });
        seancecours.setEmploitps(selectedEmploiDeTemps);
    }

    public void handleEmploiDuTempsUnSelection() {
        eventModel = new DefaultScheduleModel();
    }

    public void addSeanceCours() {
        Calendar c = Calendar.getInstance();
        c.setTime(seancecours.getJour());
        for (int i = 0; i < nombreSeances; i++) {
            Seancecours sc = new Seancecours();
            sc.setJour(c.getTime());
            sc.setDatesceance(c.getTime());
            sc.setDuree(seancecours.getDuree());
            sc.setEnseignant(seancecours.getEnseignant());
            sc.setMatiere(seancecours.getMatiere());
            sc.setSalle(seancecours.getSalle());
            sc.setEmploitps(seancecours.getEmploitps());
            seancecoursFacadeLocal.create(sc);
            c.add(Calendar.DAY_OF_MONTH, 7);
        }
        seancecours = new Seancecours();
        seancecours.setEmploitps(selectedEmploiDeTemps);
        handleEmploiDuTempsSelection();
    }

    public void editSeanceCours() {
        selectedSeance.setDatesceance(selectedSeance.getJour());
        seancecoursFacadeLocal.edit(selectedSeance);
        selectedSeance = null;
        handleEmploiDuTempsSelection();
    }

    public Date getRandomDate(Date base) {
        Calendar date = Calendar.getInstance();
        date.setTime(base);
        date.add(Calendar.DATE, ((int) (Math.random() * 30)) + 1);    //set random day of month

        return date.getTime();
    }

    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar.getTime();
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public ScheduleModel getLazyEventModel() {
        return lazyEventModel;
    }

    private Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar;
    }

    private Date previousDay8Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 8);

        return t.getTime();
    }

    private Date previousDay11Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date today1Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 1);

        return t.getTime();
    }

    private Date theDayAfter3Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 2);
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    private Date today6Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 6);

        return t.getTime();
    }

    private Date nextDay9Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 9);

        return t.getTime();
    }

    private Date nextDay11Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date fourDaysLater3pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 4);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public void addEvent(ActionEvent actionEvent) {
        if (event.getId() == null) {
            eventModel.addEvent(event);
        } else {
            eventModel.updateEvent(event);
        }

        event = new DefaultScheduleEvent();
    }

    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
        selectedSeance = (Seancecours) event.getData();
    }

    public void onDateSelect(SelectEvent selectEvent) {
        selectedSeance = null;
        seancecours.setJour((Date) selectEvent.getObject());
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
    }

    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public Seancecours getSeancecours() {
        return seancecours;
    }

    public void setSeancecours(Seancecours seancecours) {
        this.seancecours = seancecours;
    }

    public int getNombreSeances() {
        return nombreSeances;
    }

    public void setNombreSeances(int nombreSeances) {
        this.nombreSeances = nombreSeances;
    }

    public Seancecours getSelectedSeance() {
        return selectedSeance;
    }

}
