/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.emploidetemps;




/**
 *
 * @author Fantastic
 */
public interface EmploidetempsInterfaceCtrl {
    
    /**
     * cette fonction permet de save les filieres
     */
    public void enregistrerEmpDeTemps();
    
    
    public void modifierEmpDeTemps();
    
    /**
     * Fantastic
     * Supprimer le filiere
     * 
     */
    public void supprimerEmpDeTemps();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerEmpTemps();
    
    public void imprimerEmpTempssHtml();
    
     public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
