/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.emploidetemps;

import entities.Classes;
import entities.Emploidetemps;
import entities.Enseignants;
import entities.Horaires;
import entities.Matiersmodule;
import entities.Salles;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import sessions.ClassesFacadeLocal;
import sessions.EmploidetempsFacadeLocal;
import sessions.EnseignantsFacadeLocal;
import sessions.HorairesFacadeLocal;
import sessions.MatiersmoduleFacadeLocal;
import sessions.SallesFacadeLocal;
import sessions.SeancecoursFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractEmploiDeTpsCtrl implements Serializable {

    @EJB
    protected EmploidetempsFacadeLocal emploidetempsFacadeLocal;
    @EJB
    protected SeancecoursFacadeLocal seancecoursFacadeLocal;
    @EJB
    protected ClassesFacadeLocal classesFacadeLocal;
    @EJB
    protected EnseignantsFacadeLocal enseignantsFacadeLocal;
    @EJB
    protected HorairesFacadeLocal horairesFacadeLocal;
    @EJB
    protected MatiersmoduleFacadeLocal matiersmoduleFacadeLocal;
    @EJB
    protected SallesFacadeLocal sallesFacadeLocal;

    protected List<Emploidetemps> emplois;
    protected List<Classes> classes;
    protected List<Enseignants> enseignants;
    protected List<Horaires> horaires;
    protected List<Matiersmodule> matiersmodules;
    protected List<Salles> salles;

    protected StringBuffer emploidetempsTableHtml = new StringBuffer("pas encore implementé");
    protected Emploidetemps selectedEmploiDeTemps;
    protected Emploidetemps emploiDeTempsModifiee;
    protected Emploidetemps emploiDeTemps;

    public EmploidetempsFacadeLocal getEmploidetempsFacadeLocal() {
        return emploidetempsFacadeLocal;
    }

    public void setEmploidetempsFacadeLocal(EmploidetempsFacadeLocal emploidetempsFacadeLocal) {
        this.emploidetempsFacadeLocal = emploidetempsFacadeLocal;
    }

    public SeancecoursFacadeLocal getSeancecoursFacadeLocal() {
        return seancecoursFacadeLocal;
    }

    public void setSeancecoursFacadeLocal(SeancecoursFacadeLocal seancecoursFacadeLocal) {
        this.seancecoursFacadeLocal = seancecoursFacadeLocal;
    }

    public List<Emploidetemps> getEmplois() {
        this.emplois = emploidetempsFacadeLocal.findAll();
        return emplois;
    }

    public void setEmplois(List<Emploidetemps> emplois) {
        this.emplois = emplois;
    }

    public Emploidetemps getSelectedEmploiDeTemps() {
        return selectedEmploiDeTemps;
    }

    public void setSelectedEmploiDeTemps(Emploidetemps selectedEmploiDeTemps) {
        this.selectedEmploiDeTemps = selectedEmploiDeTemps;
        if (selectedEmploiDeTemps == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Emploidetemps getEmploiDeTempsModifiee() {
        return emploiDeTempsModifiee;
    }

    public void setEmploiDeTempsModifiee(Emploidetemps emploiDeTempsModifiee) {
        this.emploiDeTempsModifiee = emploiDeTempsModifiee;
    }

    public Emploidetemps getEmploiDeTemps() {
        return emploiDeTemps;
    }

    public void setEmploiDeTemps(Emploidetemps emploiDeTemps) {
        this.emploiDeTemps = emploiDeTemps;
    }

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

//    public void setSelectedUniteenseignements(Uniteenseignements selectedUniteenseignements) {
//        this.selectedUniteenseignements = selectedUniteenseignements;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        uniteenseignementModifiee = new Uniteenseignements();
//        if(selectedUniteenseignements == null )
//            return;
//        uniteenseignementModifiee.setUniteenseignements(selectedUniteenseignements.getUniteenseignements());
//        uniteenseignementModifiee.setEtat(selectedUniteenseignements.getEtat());
//        uniteenseignementModifiee.setId(selectedUniteenseignements.getId());
//        uniteenseignementModifiee.setEtat(selectedUniteenseignements.getEtat());
//    }
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = emploidetempsFacadeLocal.findAll().isEmpty();
        return imprimer;
    }

    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

   

    public List<Enseignants> getEnseignants() {
        return enseignants;
    }

    public List<Horaires> getHoraires() {
        return horaires;
    }

    public List<Matiersmodule> getMatiersmodules() {
        return matiersmodules;
    }

    public List<Salles> getSalles() {
        return salles;
    }

}
