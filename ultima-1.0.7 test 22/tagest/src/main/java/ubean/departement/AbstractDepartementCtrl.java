/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.departement;

import entities.Departement;
import java.util.List;
import javax.ejb.EJB;
import sessions.DepartementFacadeLocal;
import sessions.EtudiantsFacadeLocal;
import sessions.FilieresFacadeLocal;
import sessions.MoisFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractDepartementCtrl {
    
    @EJB
    protected DepartementFacadeLocal departementFacadeLocal ;
    
    @EJB
    protected FilieresFacadeLocal filieresFacadeLocal ;
    
    protected List<Departement> departements;
    
    protected StringBuffer departementsTableHtml = new StringBuffer("pas encore implementé") ;
    protected Departement selectedDepartement;
    protected Departement departementModifiee;
    protected Departement departement;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }
    
    
    
    
    

    public Departement getSelectedDepartement() {
        return selectedDepartement;
    }

    public Departement getDepartementModifiee() {
        return departementModifiee;
    }

    public void setDepartementModifiee(Departement departementModifiee) {
        this.departementModifiee = departementModifiee;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    public void setDepartements(List<Departement> departements) {
        this.departements = departements;
    }
    
    
    
    
    
//    public void setSelectedDepartement(Departement selectedDepartement) {
//        this.selectedDepartement = selectedDepartement;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        departementModifiee = new Departement();
//        if(selectedDepartement == null )
//            return;
//        departementModifiee.setDepartement(selectedDepartement.getDepartement());
//        departementModifiee.setEtat(selectedDepartement.getEtat());
//        departementModifiee.setId(selectedDepartement.getId());
//        departementModifiee.setEtat(selectedDepartement.getEtat());
//    }
    
    
    
    public void setSelectedDepartement(Departement selectedDepartement) {
        this.selectedDepartement = selectedDepartement;
        if (selectedDepartement == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = departementFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
  

    public StringBuffer getDepartementTableHtml() {
        return departementsTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
