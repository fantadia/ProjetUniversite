/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.departement;

import beans.util.JsfUtil;
import entities.Departement;
import entities.Filieres;
import java.io.Serializable;

import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sessions.FilieresFacadeLocal;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfDepartement;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "departementCtrl")
@ViewScoped
public class DepartementCtrl extends AbstractDepartementCtrl implements DepartementInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of DepartementCtrl
     */
    public DepartementCtrl() {
    }

    @PostConstruct
    private void initDepartement() {
        departements = departementFacadeLocal.findAll();
        selectedDepartement = new Departement();
        departement = new Departement();
        

    }

    @Override
    public void enregistrerDepartement() {
        Departement cl = departementFacadeLocal.findByNomDepartement(departement.getNomdepat());
        if (cl != null) {
            JsfUtil.addErrorMessage("Cette departement existe déjà" + "'" + cl.getNomdepat());
            modifier = supprimer = detail = true;
            return;

        }
        //departement.setEtat(true);
        departementFacadeLocal.create(departement);
        initDepartement();
        JsfUtil.addSuccessMessage("la departement à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifier() {
        if (selectedDepartement == null || selectedDepartement.getId() == null) {
            return;
        }

        Departement type = departementFacadeLocal.findByNomDepartement(selectedDepartement.getNomdepat());
        if (type != null && !Objects.equals(type.getId(), selectedDepartement.getId())) {
            JsfUtil.addErrorMessage("la departement '" + selectedDepartement.getNomdepat() + "'" + "a été mis à jour");
        }
        departementFacadeLocal.edit(selectedDepartement);
        JsfUtil.addSuccessMessage("Departement enregistrée");
    }

    @Override
    public void supprimer() {

         if(selectedDepartement == null || selectedDepartement.getId() == null)
            return;
             
        List<Filieres> filieres = filieresFacadeLocal.findByIdDepartement(selectedDepartement.getId());
        if (!filieres.isEmpty()) {
            utils.JsfUtil.addWarningMessage("La departement n'a pas éte supprimé : il est deja utilise par une  operation");
        } else {

            departementFacadeLocal.remove(selectedDepartement);
            JsfUtil.addErrorMessage("Cette departement à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

   

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

    @Override
    public void imprimerDepartementsPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerDepartementsHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

}
