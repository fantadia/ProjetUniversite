/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.departement;


/**
 *
 * @author Fantastic
 */
public interface DepartementInterfaceCtrl {
    
    /**
     * cette fonction permet de save les departements
     */
    public void enregistrerDepartement();
    
    
    public void modifier();
    
    /**
     * Fantastic
     * Supprimer le departement
     * 
     */
    public void supprimer();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerDepartementsPdf();
    
    public void imprimerDepartementsHtml();
    
     public String getComponentMessages(String departementComponent, String defaultMessage);
    
}
