/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.comptabilite;

import beans.util.JsfUtil;
import entities.Compteuser;
import entities.Comptabilite;
import entities.Seancecours;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "comptabiliteCtrl")
@ViewScoped
public class ComptabiliteCtrl extends AbstractComptabiliteCtrl implements ComptabiliteInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public ComptabiliteCtrl() {
    }

    @PostConstruct
    private void initComptabilite() {
        comptabilites = comptabiliteFacadeLocal.findAll();
        selectedComptabilite = new Comptabilite();
         selectedClasse = null;
        comptabilite = new Comptabilite();
        comptabilite.setDates(new  Date());

    }
    
    @Override
    public void enregistrerComptabilite() {
        comptabiliteFacadeLocal.create(comptabilite);
        initComptabilite();
        JsfUtil.addSuccessMessage("le comptabilite à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierComptabilite() {
        
        if (selectedComptabilite == null || selectedComptabilite.getId() == null) {
            return;
        }

//        Comptabilite type = comptabiliteFacadeLocal.findByIdComptabilite(selectedComptabilite.getId());
//        if (type != null && !Objects.equals(type.getId(), selectedComptabilite.getId())) {
//            JsfUtil.addErrorMessage("le comptabilite '" + selectedComptabilite.getNom()+ "'" + "a été mis à jour");
//        }
        comptabiliteFacadeLocal.edit(selectedComptabilite);
        JsfUtil.addSuccessMessage("comptabilite enregistrée");
    }

    @Override
    public void supprimerComptabilite() {
          

            comptabiliteFacadeLocal.remove(selectedComptabilite);
            JsfUtil.addErrorMessage("Ce comptabilite à été supprimée");
            modifier = supprimer = detail = true;
        
    }
    
    @Override
    public void listeEtudiantsClasse() {
        if (selectedClasse == null) {
            etudiants = etudiantFacadeLocal.findAll();
            return;
        }
        etudiants = etudiantFacadeLocal.findEtudiantByclasse(selectedClasse.getId());
        if (etudiants.isEmpty()) {
            utils.JsfUtil.addErrorMessage("Aucun etudiant enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerComptabilitePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerComptabiliteHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

        
}

