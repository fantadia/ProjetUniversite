/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.comptabilite;


import entities.Classes;
import ubean.comptabilite.*;
import entities.Comptabilite;
import entities.Etudiants;
import java.util.List;
import javax.ejb.EJB;
import sessions.ClassesFacadeLocal;
import sessions.CompteuserFacadeLocal;



import sessions.ComptabiliteFacadeLocal;
import sessions.EtudiantsFacadeLocal;



/**
 *
 * @author Fantastic
 */
public abstract class AbstractComptabiliteCtrl {
    
    @EJB
    protected ComptabiliteFacadeLocal comptabiliteFacadeLocal ;
    
    @EJB
    protected ClassesFacadeLocal classesFacadeLocal;
    protected Classes classe;
      @EJB
    protected EtudiantsFacadeLocal etudiantFacadeLocal;

    
    @EJB
    protected CompteuserFacadeLocal compteuserFacadeLocal ;
    
    protected List<Comptabilite> comptabilites;
    
    protected StringBuffer comptabilitesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Comptabilite selectedComptabilite;
    protected Comptabilite comptabilite;
     protected Classes selectedClasse;
      protected List<Etudiants> etudiants;
    
    
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public ComptabiliteFacadeLocal getComptabiliteFacadeLocal() {
        return comptabiliteFacadeLocal;
    }

    public void setComptabiliteFacadeLocal(ComptabiliteFacadeLocal comptabiliteFacadeLocal) {
        this.comptabiliteFacadeLocal = comptabiliteFacadeLocal;
    }

    public CompteuserFacadeLocal getCompteuserFacadeLocal() {
        return compteuserFacadeLocal;
    }

    public void setCompteuserFacadeLocal(CompteuserFacadeLocal compteuserFacadeLocal) {
        this.compteuserFacadeLocal = compteuserFacadeLocal;
    }

    public List<Comptabilite> getComptabilites() {
        return comptabilites;
    }

    public void setComptabilites(List<Comptabilite> comptabilites) {
        this.comptabilites = comptabilites;
    }

    public StringBuffer getComptabilitesTableHtml() {
        return comptabilitesTableHtml;
    }

    public void setComptabilitesTableHtml(StringBuffer comptabilitesTableHtml) {
        this.comptabilitesTableHtml = comptabilitesTableHtml;
    }

    public Comptabilite getSelectedComptabilite() {
        return selectedComptabilite;
    }

    public void setSelectedComptabilite(Comptabilite selectedComptabilite) {
        this.selectedComptabilite = selectedComptabilite;
        if (selectedComptabilite == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Comptabilite getComptabilite() {
        return comptabilite;
    }

    public void setComptabilite(Comptabilite comptabilite) {
        this.comptabilite = comptabilite;
    }

    public Classes getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classes selectedClasse) {
        this.selectedClasse = selectedClasse;
    }

    public List<Etudiants> getEtudiants() {
        return etudiants;
    }

    public void setEtudiants(List<Etudiants> etudiants) {
        this.etudiants = etudiants;
    }
    
    
    
    
    
  
    
  
    
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = comptabiliteFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
 
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
