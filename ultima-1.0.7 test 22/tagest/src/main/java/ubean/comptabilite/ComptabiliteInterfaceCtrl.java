/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.comptabilite;

import ubean.comptabilite.*;
import ubean.personnel.*;


/**
 *
 * @author Fantastic
 */
public interface ComptabiliteInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Comptabilite
     */
    public void enregistrerComptabilite();
    
    
    public void modifierComptabilite();
    
    /**
     * Fantastic
     * Supprimer la Comptabilite
     * 
     */
    public void supprimerComptabilite();
    
    
     public void listeEtudiantsClasse() ;
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerComptabilitePdf();
    
    public void imprimerComptabiliteHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
