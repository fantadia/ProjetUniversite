/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.postepersonnel;

import entities.Postepersonnel;
import java.util.List;
import javax.ejb.EJB;
import sessions.EtudiantsFacadeLocal;
import sessions.MoisFacadeLocal;
import sessions.PersonnelFacadeLocal;
import sessions.PostepersonnelFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractPostepersonnelCtrl {
    
    @EJB
    protected PostepersonnelFacadeLocal postepersonnelFacadeLocal ;
    
    @EJB
    protected PersonnelFacadeLocal personnelFacadeLocal ;

    @EJB
    protected MoisFacadeLocal moisFacadeLocal ;
    
    
    protected List<Postepersonnel> postepersonnels;
    
    protected StringBuffer postepersonnelsTableHtml = new StringBuffer("pas encore implementé") ;
    protected Postepersonnel selectedPostepersonnel;
    protected Postepersonnel postepersonnelModifiee;
    protected Postepersonnel postepersonnel;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
    public List<Postepersonnel> getPostepersonnels() {
        postepersonnels = postepersonnelFacadeLocal.findAll();
        return postepersonnels;
    }

    public Postepersonnel getSelectedPostepersonnel() {
        return selectedPostepersonnel;
    }

    public Postepersonnel getPostepersonnelModifiee() {
        return postepersonnelModifiee;
    }

    public void setPostepersonnelModifiee(Postepersonnel postepersonnelModifiee) {
        this.postepersonnelModifiee = postepersonnelModifiee;
    }
    
//    public void setSelectedPostepersonnel(Postepersonnels selectedPostepersonnel) {
//        this.selectedPostepersonnel = selectedPostepersonnel;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        postepersonnelModifiee = new Postepersonnels();
//        if(selectedPostepersonnel == null )
//            return;
//        postepersonnelModifiee.setPostepersonnel(selectedPostepersonnel.getPostepersonnel());
//        postepersonnelModifiee.setEtat(selectedPostepersonnel.getEtat());
//        postepersonnelModifiee.setId(selectedPostepersonnel.getId());
//        postepersonnelModifiee.setEtat(selectedPostepersonnel.getEtat());
//    }
    
    
    
    public void setSelectedPostepersonnel(Postepersonnel selectedPostepersonnel) {
        this.selectedPostepersonnel = selectedPostepersonnel;
        if (selectedPostepersonnel == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = postepersonnelFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
    
    public Postepersonnel getPostepersonnel() {
        return postepersonnel;
    }

    public void setPostepersonnel(Postepersonnel postepersonnel) {
        this.postepersonnel = postepersonnel;
    }

    public StringBuffer getPostepersonnelsTableHtml() {
        return postepersonnelsTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
