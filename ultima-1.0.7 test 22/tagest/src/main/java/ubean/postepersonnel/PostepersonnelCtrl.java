/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.postepersonnel;

import beans.util.JsfUtil;
import entities.Etudiants;
import entities.Personnel;
import entities.Postepersonnel;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sessions.PersonnelFacadeLocal;
//import pdf.PdfPostepersonnels;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "postepersonnelCtrl")
@ViewScoped
public class PostepersonnelCtrl extends AbstractPostepersonnelCtrl implements PostepersonnelInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of PostepersonnelsCtrl
     */
    public PostepersonnelCtrl() {
    }

    @PostConstruct
    private void initPostepersonnels() {
        postepersonnels = postepersonnelFacadeLocal.findAll();
        selectedPostepersonnel = new Postepersonnel();
        postepersonnel = new Postepersonnel();

    }

    @Override
    public void enregistrerPostepersonnel() {
        Postepersonnel cl = postepersonnelFacadeLocal.findByPostepersonnel(postepersonnel.getNom());
        if (cl != null) {
            JsfUtil.addErrorMessage("Ce poste existe déjà" + "'" + cl.getNom());
            modifier = supprimer = detail = true;
            return;

        }
        postepersonnelFacadeLocal.create(postepersonnel);
        initPostepersonnels();
        JsfUtil.addSuccessMessage("le poste à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifier() {
        if (selectedPostepersonnel == null || selectedPostepersonnel.getId() == null) {
            return;
        }

        Postepersonnel type = postepersonnelFacadeLocal.findByPostepersonnel(selectedPostepersonnel.getNom());
        if (type != null && !Objects.equals(type.getId(), selectedPostepersonnel.getId())) {
            JsfUtil.addErrorMessage("la poste'" + selectedPostepersonnel.getNom() + "'" + "a été mis à jour");
        }
        postepersonnelFacadeLocal.edit(selectedPostepersonnel);
        JsfUtil.addSuccessMessage("Poste enregistrée");
    }

    @Override
    public void supprimer() {

         if(selectedPostepersonnel == null || selectedPostepersonnel.getId() == null)
            return;
             
        List<Personnel> peronnel = personnelFacadeLocal.findByIdPostepersonnel(selectedPostepersonnel.getId());
        if (!peronnel.isEmpty()) {
            utils.JsfUtil.addWarningMessage("Le poste n'a pas éte supprimé : elle est deja utilise par une  operation");
        } else {

            postepersonnelFacadeLocal.remove(selectedPostepersonnel);
            JsfUtil.addErrorMessage("Ce poste à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerPostepersonnelsPdf() {
        
//        postepersonnels = postepersonnelFacadeLocal.findAll();
//        fileName = PdfPostepersonnels.etatsPostepersonnels(postepersonnels);
//        System.out.println("Impression pdf des postepersonnels");
    }

    @Override
    public void imprimerPostepersonnelsHtml() {
        System.out.println("Impression html des postepersonnels");

    }

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

   

}
