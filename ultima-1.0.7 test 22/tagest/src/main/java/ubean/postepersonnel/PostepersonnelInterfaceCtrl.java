/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.postepersonnel;

import ubean.classes.*;

/**
 *
 * @author Fantastic
 */
public interface PostepersonnelInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Postes
     */
    public void enregistrerPostepersonnel();
    
    
    public void modifier();
    
    /**
     * Fantastic
     * Supprimer le Poste
     * 
     */
    public void supprimer();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerPostepersonnelsPdf();
    
    public void imprimerPostepersonnelsHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
