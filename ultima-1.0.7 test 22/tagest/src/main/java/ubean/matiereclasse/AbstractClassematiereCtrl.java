/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.matiereclasse;

import entities.Classematiere;
import java.util.List;
import javax.ejb.EJB;
import sessions.ClassematiereFacadeLocal;
import sessions.EtudiantsFacadeLocal;
import sessions.MoisFacadeLocal;
import utils.JsfUtil;
import utils.UtilitaireSession;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractClassematiereCtrl {
    
    @EJB
    protected ClassematiereFacadeLocal classematiereFacadeLocal ;
    
    @EJB
    protected EtudiantsFacadeLocal etudiantsFacadeLocal ;

    @EJB
    protected MoisFacadeLocal moisFacadeLocal ;
    
    
    protected List<Classematiere> classematieres;
    
    protected StringBuffer classematieresTableHtml = new StringBuffer("pas encore implementé") ;
    protected Classematiere selectedClassematiere;
    protected Classematiere classematiere;
//    protected Classematiere classematiere;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
    public List<Classematiere> getClassematieres() {
        classematieres = classematiereFacadeLocal.findAll();
        return classematieres;
    }
    
    public ClassematiereFacadeLocal getClassematiereFacadeLocal() {
        return classematiereFacadeLocal;
    }

    public void setClassematiereFacadeLocal(ClassematiereFacadeLocal classematiereFacadeLocal) {
        this.classematiereFacadeLocal = classematiereFacadeLocal;
    }

    
    
    public Classematiere getSelectedClassematiere() {
        return selectedClassematiere;
    }

   
    
//    public void setSelectedClassematiere(Classematiere selectedClassematiere) {
//        this.selectedClassematiere = selectedClassematiere;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        classematiere = new Classematiere();
//        if(selectedClassematiere == null )
//            return;
//        classematiere.setClassematiere(selectedClassematiere.getClassematiere());
//        classematiere.setEtat(selectedClassematiere.getEtat());
//        classematiere.setId(selectedClassematiere.getId());
//        classematiere.setEtat(selectedClassematiere.getEtat());
//    }
    
    
    
    public void setSelectedClassematiere(Classematiere selectedClassematiere) {
        this.selectedClassematiere = selectedClassematiere;
        if (selectedClassematiere == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = classematiereFacadeLocal.findAll().isEmpty();
        return imprimer;
    }
  

    public void setClassematiere(Classematiere classematiere) {
        this.classematiere = classematiere;
    }

    public StringBuffer getClassematiereTableHtml() {
        return classematieresTableHtml;
    }
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
