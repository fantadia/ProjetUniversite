/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.matiereclasse;

import ubean.classes.*;
import beans.util.JsfUtil;
import entities.Classematiere;
import entities.Etudiants;
import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sessions.ClassematiereFacadeLocal;
//import pdf.PdfClassematieres;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "classematiereCtrl")
@ViewScoped
public class ClassematiereCtrl extends AbstractClassematiereCtrl implements ClassematiereInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of ClassematieresCtrl
     */
    public ClassematiereCtrl() {
    }

    @PostConstruct
    private void initClassematieres() {
        classematieres = classematiereFacadeLocal.findAll();
        selectedClassematiere = new Classematiere();
        classematiere = new Classematiere();

    }

    @Override
    public void enregistrerClassematiere() {
       
        classematiereFacadeLocal.create(classematiere);
        initClassematieres();
        JsfUtil.addSuccessMessage("Enregistrement effectué avec succes");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifier() {
        if (selectedClassematiere == null || selectedClassematiere.getId() == null) {
            return;
        }

        classematiereFacadeLocal.edit(selectedClassematiere);
        JsfUtil.addSuccessMessage("Classematieres enregistrée");
    }

    @Override
    public void supprimer() {

         if(selectedClassematiere == null || selectedClassematiere.getId() == null)
            return;
             
//        List<Etudiants> etudiant = etudiantsFacadeLocal.findByIdClassematieres(selectedClassematiere.getId());
//        if (!etudiant.isEmpty()) {
//            utils.JsfUtil.addWarningMessage("La classematiere n'a pas éte supprimé : elle est deja utilise par une  operation");
//        } else {

            classematiereFacadeLocal.remove(selectedClassematiere);
            JsfUtil.addErrorMessage("Cette classematiere à été supprimée");
            modifier = supprimer = detail = true;
        }

    @Override
    public void imprimerClassematieresPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerClassematieresHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String classeComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     @Override
    public void listeMatiresClasse() {
        if (selectedClassematiere == null) {
            classematieres = classematiereFacadeLocal.findAll();
            return;
        }
        classematieres = classematiereFacadeLocal.findMatiereByclasse(selectedClassematiere.getId());
        if (classematieres.isEmpty()) {
            utils.JsfUtil.addErrorMessage("Aucun matière enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
    }
    }

   
   


