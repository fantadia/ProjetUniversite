/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubean.matiereclasse;

import ubean.classes.*;

/**
 *
 * @author Fantastic
 */
public interface ClassematiereInterfaceCtrl {
    
    /**
     * cette fonction permet de save les classes
     */
    public void enregistrerClassematiere();
    
    
    public void modifier();
    
    /**
     * Fantastic
     * Supprimer le classe
     * 
     */
    public void supprimer();
    
    
    
    public void listeMatiresClasse();
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
    public void imprimerClassematieresPdf();
    
    public void imprimerClassematieresHtml();
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
