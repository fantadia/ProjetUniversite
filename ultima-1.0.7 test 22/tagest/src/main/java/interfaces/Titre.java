/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author ulrich
 */
public interface Titre {
    
    public static final String MONSIEUR = "Monsieur"; 
    public static final String MADAME = "Madame"; 
    public static final String DOCTEUR = "Docteur"; 
    public static final String PROFESSEUR = "Professeur";
    public static final String MADEMOISELLE = "Mademoiselle"; 
    public static final String Mr = "Monsieur"; 
    public static final String Mme = "Mme"; 
    public static final String MLLE = "Mlle"; 
    public static final String Dr = "Dr"; 
    public static final String Pr = "Pr";
}
