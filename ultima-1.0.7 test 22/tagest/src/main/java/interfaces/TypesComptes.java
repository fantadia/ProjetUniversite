/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author ulrich
 */
public interface TypesComptes {
    
    public static final int PRINCIPAL = 1;
    public static final int LOYER = 2;
    public static final int EAU = 3;
    public static final int ELECTRICITE = 4;
    public static final int PENALITE = 5;
    public static final int REMISE = 6;
    public static final int CHARGE = 7;
    public static final int CABLE = 8;
}
