/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author ulrich
 */
public interface StatusCaution {
    
    public static final String REMBOURSE = "Remboursée";
    public static final String ENREGISTREE = "Enregistrée";
    public static final String RETENUE = "Retenue";
}
