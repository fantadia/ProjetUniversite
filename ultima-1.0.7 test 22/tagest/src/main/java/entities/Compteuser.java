/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "compteuser", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"personnel", "login", "motdepass"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compteuser.findAll", query = "SELECT c FROM Compteuser c")
    , @NamedQuery(name = "Compteuser.findById", query = "SELECT c FROM Compteuser c WHERE c.id = :id")
    , @NamedQuery(name = "Compteuser.findByLogin", query = "SELECT c FROM Compteuser c WHERE c.login = :login")
    , @NamedQuery(name = "Compteuser.findByMotdepass", query = "SELECT c FROM Compteuser c WHERE c.motdepass = :motdepass")
    , @NamedQuery(name = "Compteuser.findByEtat", query = "SELECT c FROM Compteuser c WHERE c.etat = :etat")})
public class Compteuser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "login", nullable = false, length = 254)
    private String login;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "motdepass", nullable = false, length = 254)
    private String motdepass;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @JoinColumn(name = "personnel", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Personnel personnel;

    public Compteuser() {
    }

    public Compteuser(Integer id) {
        this.id = id;
    }

    public Compteuser(Integer id, String login, String motdepass, boolean etat) {
        this.id = id;
        this.login = login;
        this.motdepass = motdepass;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotdepass() {
        return motdepass;
    }

    public void setMotdepass(String motdepass) {
        this.motdepass = motdepass;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public Personnel getPersonnel() {
        return personnel;
    }

    public void setPersonnel(Personnel personnel) {
        this.personnel = personnel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compteuser)) {
            return false;
        }
        Compteuser other = (Compteuser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Compteuser[ id=" + id + " ]";
    }
    
}
