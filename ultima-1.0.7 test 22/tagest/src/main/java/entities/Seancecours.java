/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "seancecours", catalog = "university", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seancecours.findAll", query = "SELECT s FROM Seancecours s")
    , @NamedQuery(name = "Seancecours.findByJour", query = "SELECT s FROM Seancecours s WHERE s.jour = :jour")
    , @NamedQuery(name = "Seancecours.findById", query = "SELECT s FROM Seancecours s WHERE s.id = :id")
    , @NamedQuery(name = "Seancecours.findByDuree", query = "SELECT s FROM Seancecours s WHERE s.duree = :duree")
    , @NamedQuery(name = "Seancecours.findByDatesceance", query = "SELECT s FROM Seancecours s WHERE s.datesceance = :datesceance")})
public class Seancecours implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "jour", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date jour;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "duree")
    private Integer duree = 120;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datesceance", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date datesceance;
    @JoinColumn(name = "emploitps", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Emploidetemps emploitps;
    @JoinColumn(name = "enseignant", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Enseignants enseignant;
    @JoinColumn(name = "matiere", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Matiersmodule matiere;
    @JoinColumn(name = "salle", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Salles salle;

    public Seancecours() {
    }

    public Seancecours(Integer id) {
        this.id = id;
    }

    public Seancecours(Integer id, Date jour, Date datesceance) {
        this.id = id;
        this.jour = jour;
        this.datesceance = datesceance;
    }

    public Date getJour() {
        return jour;
    }

    public void setJour(Date jour) {
        this.jour = jour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public Date getDatesceance() {
        return datesceance;
    }

    public void setDatesceance(Date datesceance) {
        this.datesceance = datesceance;
    }

    public Emploidetemps getEmploitps() {
        return emploitps;
    }

    public void setEmploitps(Emploidetemps emploitps) {
        this.emploitps = emploitps;
    }

    public Enseignants getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignants enseignant) {
        this.enseignant = enseignant;
    }

    public Matiersmodule getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiersmodule matiere) {
        this.matiere = matiere;
    }

    public Salles getSalle() {
        return salle;
    }

    public void setSalle(Salles salle) {
        this.salle = salle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seancecours)) {
            return false;
        }
        Seancecours other = (Seancecours) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Seancecours[ id=" + id + " ]";
    }
    
}
