/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "emploidetemps", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"periodedebut", "periodefin"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Emploidetemps.findAll", query = "SELECT e FROM Emploidetemps e")
    , @NamedQuery(name = "Emploidetemps.findById", query = "SELECT e FROM Emploidetemps e WHERE e.id = :id")
    , @NamedQuery(name = "Emploidetemps.findByTitre", query = "SELECT e FROM Emploidetemps e WHERE e.titre = :titre")
    , @NamedQuery(name = "Emploidetemps.findByPeriodedebut", query = "SELECT e FROM Emploidetemps e WHERE e.periodedebut = :periodedebut")
    , @NamedQuery(name = "Emploidetemps.findByPeriodefin", query = "SELECT e FROM Emploidetemps e WHERE e.periodefin = :periodefin")
    , @NamedQuery(name = "Emploidetemps.findByEtat", query = "SELECT e FROM Emploidetemps e WHERE e.etat = :etat")})
public class Emploidetemps implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "titre", nullable = false, length = 254)
    private String titre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodedebut", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date periodedebut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodefin", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date periodefin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @JoinColumn(name = "classe", referencedColumnName = "id")
    @ManyToOne
    private Classes classe;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emploitps")
    private List<Seancecours> seancecoursList;

    public Emploidetemps() {
    }

    public Emploidetemps(Integer id) {
        this.id = id;
    }

    public Emploidetemps(Integer id, String titre, Date periodedebut, Date periodefin, boolean etat) {
        this.id = id;
        this.titre = titre;
        this.periodedebut = periodedebut;
        this.periodefin = periodefin;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Date getPeriodedebut() {
        return periodedebut;
    }

    public void setPeriodedebut(Date periodedebut) {
        this.periodedebut = periodedebut;
    }

    public Date getPeriodefin() {
        return periodefin;
    }

    public void setPeriodefin(Date periodefin) {
        this.periodefin = periodefin;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public Classes getClasse() {
        return classe;
    }

    public void setClasse(Classes classe) {
        this.classe = classe;
    }

    @XmlTransient
    public List<Seancecours> getSeancecoursList() {
        return seancecoursList;
    }

    public void setSeancecoursList(List<Seancecours> seancecoursList) {
        this.seancecoursList = seancecoursList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Emploidetemps)) {
            return false;
        }
        Emploidetemps other = (Emploidetemps) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Emploidetemps[ id=" + id + " ]";
    }
    
}
