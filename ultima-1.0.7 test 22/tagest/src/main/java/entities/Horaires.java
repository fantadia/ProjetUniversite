/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "horaires", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"heuredebut", "heurefin"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Horaires.findAll", query = "SELECT h FROM Horaires h")
    , @NamedQuery(name = "Horaires.findById", query = "SELECT h FROM Horaires h WHERE h.id = :id")
    , @NamedQuery(name = "Horaires.findByHeuredebut", query = "SELECT h FROM Horaires h WHERE h.heuredebut = :heuredebut")
    , @NamedQuery(name = "Horaires.findByHeurefin", query = "SELECT h FROM Horaires h WHERE h.heurefin = :heurefin")})
public class Horaires implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "heuredebut")
    private Integer heuredebut;
    @Column(name = "heurefin")
    private Integer heurefin;

    public Horaires() {
    }

    public Horaires(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHeuredebut() {
        return heuredebut;
    }

    public void setHeuredebut(Integer heuredebut) {
        this.heuredebut = heuredebut;
    }

    public Integer getHeurefin() {
        return heurefin;
    }

    public void setHeurefin(Integer heurefin) {
        this.heurefin = heurefin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horaires)) {
            return false;
        }
        Horaires other = (Horaires) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Horaires[ id=" + id + " ]";
    }
    
}
