/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "historiquedeliberation", catalog = "university", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Historiquedeliberation.findAll", query = "SELECT h FROM Historiquedeliberation h")
    , @NamedQuery(name = "Historiquedeliberation.findById", query = "SELECT h FROM Historiquedeliberation h WHERE h.id = :id")
    , @NamedQuery(name = "Historiquedeliberation.findByPv", query = "SELECT h FROM Historiquedeliberation h WHERE h.pv = :pv")
    , @NamedQuery(name = "Historiquedeliberation.findByEtat", query = "SELECT h FROM Historiquedeliberation h WHERE h.etat = :etat")
    , @NamedQuery(name = "Historiquedeliberation.findBySeuil", query = "SELECT h FROM Historiquedeliberation h WHERE h.seuil = :seuil")})
public class Historiquedeliberation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 254)
    @Column(name = "pv", length = 254)
    private String pv;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @Column(name = "seuil")
    private Integer seuil;
    @JoinColumn(name = "note", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Notes note;

    public Historiquedeliberation() {
    }

    public Historiquedeliberation(Integer id) {
        this.id = id;
    }

    public Historiquedeliberation(Integer id, boolean etat) {
        this.id = id;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPv() {
        return pv;
    }

    public void setPv(String pv) {
        this.pv = pv;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public Integer getSeuil() {
        return seuil;
    }

    public void setSeuil(Integer seuil) {
        this.seuil = seuil;
    }

    public Notes getNote() {
        return note;
    }

    public void setNote(Notes note) {
        this.note = note;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Historiquedeliberation)) {
            return false;
        }
        Historiquedeliberation other = (Historiquedeliberation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Historiquedeliberation[ id=" + id + " ]";
    }
    
}
