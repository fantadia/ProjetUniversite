/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "departement", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nomdepat"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Departement.findAll", query = "SELECT d FROM Departement d")
    , @NamedQuery(name = "Departement.findById", query = "SELECT d FROM Departement d WHERE d.id = :id")
    , @NamedQuery(name = "Departement.findByNomdepat", query = "SELECT d FROM Departement d WHERE d.nomdepat = :nomdepat")
    , @NamedQuery(name = "Departement.findByDescription", query = "SELECT d FROM Departement d WHERE d.description = :description")
    , @NamedQuery(name = "Departement.findByEtat", query = "SELECT d FROM Departement d WHERE d.etat = :etat")})
public class Departement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nomdepat", nullable = false, length = 254)
    private String nomdepat;
    @Size(max = 254)
    @Column(name = "description", length = 254)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departement")
    private List<Filieres> filieresList;

    public Departement() {
    }

    public Departement(Integer id) {
        this.id = id;
    }

    public Departement(Integer id, String nomdepat, boolean etat) {
        this.id = id;
        this.nomdepat = nomdepat;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomdepat() {
        return nomdepat;
    }

    public void setNomdepat(String nomdepat) {
        this.nomdepat = nomdepat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Filieres> getFilieresList() {
        return filieresList;
    }

    public void setFilieresList(List<Filieres> filieresList) {
        this.filieresList = filieresList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Departement)) {
            return false;
        }
        Departement other = (Departement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Departement[ id=" + id + " ]";
    }
    
}
