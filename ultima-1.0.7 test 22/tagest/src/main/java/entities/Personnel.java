/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "personnel", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"matricule"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personnel.findAll", query = "SELECT p FROM Personnel p")
    , @NamedQuery(name = "Personnel.findById", query = "SELECT p FROM Personnel p WHERE p.id = :id")
    , @NamedQuery(name = "Personnel.findByMatricule", query = "SELECT p FROM Personnel p WHERE p.matricule = :matricule")
    , @NamedQuery(name = "Personnel.findByNom", query = "SELECT p FROM Personnel p WHERE p.nom = :nom")
    , @NamedQuery(name = "Personnel.findByAge", query = "SELECT p FROM Personnel p WHERE p.age = :age")
    , @NamedQuery(name = "Personnel.findByDateNaissance", query = "SELECT p FROM Personnel p WHERE p.dateNaissance = :dateNaissance")
    , @NamedQuery(name = "Personnel.findByLieuNaissance", query = "SELECT p FROM Personnel p WHERE p.lieuNaissance = :lieuNaissance")
    , @NamedQuery(name = "Personnel.findByPaysDeNaissance", query = "SELECT p FROM Personnel p WHERE p.paysDeNaissance = :paysDeNaissance")
    , @NamedQuery(name = "Personnel.findByNationalite", query = "SELECT p FROM Personnel p WHERE p.nationalite = :nationalite")
    , @NamedQuery(name = "Personnel.findBySituationMatrimoniale", query = "SELECT p FROM Personnel p WHERE p.situationMatrimoniale = :situationMatrimoniale")
    , @NamedQuery(name = "Personnel.findByTelephone", query = "SELECT p FROM Personnel p WHERE p.telephone = :telephone")
    , @NamedQuery(name = "Personnel.findByEmail1", query = "SELECT p FROM Personnel p WHERE p.email1 = :email1")
    , @NamedQuery(name = "Personnel.findByEmail", query = "SELECT p FROM Personnel p WHERE p.email = :email")
    , @NamedQuery(name = "Personnel.findByEtat", query = "SELECT p FROM Personnel p WHERE p.etat = :etat")})
public class Personnel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "matricule", nullable = false, length = 254)
    private String matricule;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nom", nullable = false, length = 254)
    private String nom;
    @Column(name = "age")
    private Integer age;
    @Column(name = "date_naissance")
    @Temporal(TemporalType.DATE)
    private Date dateNaissance;
    @Size(max = 254)
    @Column(name = "lieu_naissance", length = 254)
    private String lieuNaissance;
    @Size(max = 254)
    @Column(name = "pays_de_naissance", length = 254)
    private String paysDeNaissance;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nationalite", nullable = false, length = 254)
    private String nationalite;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "situation_matrimoniale", nullable = false, length = 254)
    private String situationMatrimoniale;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telephone", nullable = false)
    private int telephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "email1", nullable = false, length = 254)
    private String email1;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 254)
    @Column(name = "email", length = 254)
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personnel")
    private List<Rolepersonnel> rolepersonnelList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personnel")
    private List<Compteuser> compteuserList;
    @JoinColumn(name = "poste", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Postepersonnel poste;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personnel")
    private List<Privilegeperso> privilegepersoList;

    public Personnel() {
    }

    public Personnel(Integer id) {
        this.id = id;
    }

    public Personnel(Integer id, String matricule, String nom, String nationalite, String situationMatrimoniale, int telephone, String email1, boolean etat) {
        this.id = id;
        this.matricule = matricule;
        this.nom = nom;
        this.nationalite = nationalite;
        this.situationMatrimoniale = situationMatrimoniale;
        this.telephone = telephone;
        this.email1 = email1;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getPaysDeNaissance() {
        return paysDeNaissance;
    }

    public void setPaysDeNaissance(String paysDeNaissance) {
        this.paysDeNaissance = paysDeNaissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public String getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(String situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Rolepersonnel> getRolepersonnelList() {
        return rolepersonnelList;
    }

    public void setRolepersonnelList(List<Rolepersonnel> rolepersonnelList) {
        this.rolepersonnelList = rolepersonnelList;
    }

    @XmlTransient
    public List<Compteuser> getCompteuserList() {
        return compteuserList;
    }

    public void setCompteuserList(List<Compteuser> compteuserList) {
        this.compteuserList = compteuserList;
    }

    public Postepersonnel getPoste() {
        return poste;
    }

    public void setPoste(Postepersonnel poste) {
        this.poste = poste;
    }

    @XmlTransient
    public List<Privilegeperso> getPrivilegepersoList() {
        return privilegepersoList;
    }

    public void setPrivilegepersoList(List<Privilegeperso> privilegepersoList) {
        this.privilegepersoList = privilegepersoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personnel)) {
            return false;
        }
        Personnel other = (Personnel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Personnel[ id=" + id + " ]";
    }
    
}
