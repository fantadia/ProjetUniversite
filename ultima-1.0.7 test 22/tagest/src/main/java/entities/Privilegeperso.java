/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "privilegeperso", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"personnel", "privilege"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Privilegeperso.findAll", query = "SELECT p FROM Privilegeperso p")
    , @NamedQuery(name = "Privilegeperso.findById", query = "SELECT p FROM Privilegeperso p WHERE p.id = :id")})
public class Privilegeperso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @JoinColumn(name = "personnel", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Personnel personnel;
    @JoinColumn(name = "privilege", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Privilege privilege;

    public Privilegeperso() {
    }

    public Privilegeperso(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Personnel getPersonnel() {
        return personnel;
    }

    public void setPersonnel(Personnel personnel) {
        this.personnel = personnel;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    public void setPrivilege(Privilege privilege) {
        this.privilege = privilege;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Privilegeperso)) {
            return false;
        }
        Privilegeperso other = (Privilegeperso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Privilegeperso[ id=" + id + " ]";
    }
    
}
