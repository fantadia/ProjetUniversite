/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "matiersmodule", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nommatiere"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Matiersmodule.findAll", query = "SELECT m FROM Matiersmodule m")
    , @NamedQuery(name = "Matiersmodule.findById", query = "SELECT m FROM Matiersmodule m WHERE m.id = :id")
    , @NamedQuery(name = "Matiersmodule.findByNommatiere", query = "SELECT m FROM Matiersmodule m WHERE m.nommatiere = :nommatiere")
    , @NamedQuery(name = "Matiersmodule.findByGuotahoraire", query = "SELECT m FROM Matiersmodule m WHERE m.guotahoraire = :guotahoraire")
    , @NamedQuery(name = "Matiersmodule.findByGuotahorairerestant", query = "SELECT m FROM Matiersmodule m WHERE m.guotahorairerestant = :guotahorairerestant")
    , @NamedQuery(name = "Matiersmodule.findByCredit", query = "SELECT m FROM Matiersmodule m WHERE m.credit = :credit")})
public class Matiersmodule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nommatiere", nullable = false, length = 254)
    private String nommatiere;
    @Basic(optional = false)
    @NotNull
    @Column(name = "guotahoraire", nullable = false)
    private int guotahoraire;
    @Basic(optional = false)
    @NotNull
    @Column(name = "guotahorairerestant", nullable = false)
    private int guotahorairerestant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "credit", nullable = false)
    private int credit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matiere")
    private List<Notes> notesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matiere")
    private List<Classematiere> classematiereList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matiere")
    private List<Seancecours> seancecoursList;
    @JoinColumn(name = "uniteenseignement", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Uniteenseignements uniteenseignement;

    public Matiersmodule() {
    }

    public Matiersmodule(Integer id) {
        this.id = id;
    }

    public Matiersmodule(Integer id, String nommatiere, int guotahoraire, int guotahorairerestant, int credit) {
        this.id = id;
        this.nommatiere = nommatiere;
        this.guotahoraire = guotahoraire;
        this.guotahorairerestant = guotahorairerestant;
        this.credit = credit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNommatiere() {
        return nommatiere;
    }

    public void setNommatiere(String nommatiere) {
        this.nommatiere = nommatiere;
    }

    public int getGuotahoraire() {
        return guotahoraire;
    }

    public void setGuotahoraire(int guotahoraire) {
        this.guotahoraire = guotahoraire;
    }

    public int getGuotahorairerestant() {
        return guotahorairerestant;
    }

    public void setGuotahorairerestant(int guotahorairerestant) {
        this.guotahorairerestant = guotahorairerestant;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    @XmlTransient
    public List<Notes> getNotesList() {
        return notesList;
    }

    public void setNotesList(List<Notes> notesList) {
        this.notesList = notesList;
    }

    @XmlTransient
    public List<Classematiere> getClassematiereList() {
        return classematiereList;
    }

    public void setClassematiereList(List<Classematiere> classematiereList) {
        this.classematiereList = classematiereList;
    }

    @XmlTransient
    public List<Seancecours> getSeancecoursList() {
        return seancecoursList;
    }

    public void setSeancecoursList(List<Seancecours> seancecoursList) {
        this.seancecoursList = seancecoursList;
    }

    public Uniteenseignements getUniteenseignement() {
        return uniteenseignement;
    }

    public void setUniteenseignement(Uniteenseignements uniteenseignement) {
        this.uniteenseignement = uniteenseignement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matiersmodule)) {
            return false;
        }
        Matiersmodule other = (Matiersmodule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Matiersmodule[ id=" + id + " ]";
    }
    
}
