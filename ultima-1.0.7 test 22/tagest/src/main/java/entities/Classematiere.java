/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "classematiere", catalog = "university", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Classematiere.findAll", query = "SELECT c FROM Classematiere c")
    , @NamedQuery(name = "Classematiere.findById", query = "SELECT c FROM Classematiere c WHERE c.id = :id")})
public class Classematiere implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @JoinColumn(name = "classe", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Classes classe;
    @JoinColumn(name = "matiere", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Matiersmodule matiere;

    public Classematiere() {
    }

    public Classematiere(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Classes getClasse() {
        return classe;
    }

    public void setClasse(Classes classe) {
        this.classe = classe;
    }

    public Matiersmodule getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiersmodule matiere) {
        this.matiere = matiere;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Classematiere)) {
            return false;
        }
        Classematiere other = (Classematiere) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Classematiere[ id=" + id + " ]";
    }
    
}
