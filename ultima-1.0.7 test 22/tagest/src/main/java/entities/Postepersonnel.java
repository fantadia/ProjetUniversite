/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "postepersonnel", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nom"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Postepersonnel.findAll", query = "SELECT p FROM Postepersonnel p")
    , @NamedQuery(name = "Postepersonnel.findById", query = "SELECT p FROM Postepersonnel p WHERE p.id = :id")
    , @NamedQuery(name = "Postepersonnel.findByNom", query = "SELECT p FROM Postepersonnel p WHERE p.nom = :nom")
    , @NamedQuery(name = "Postepersonnel.findByDescription", query = "SELECT p FROM Postepersonnel p WHERE p.description = :description")})
public class Postepersonnel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nom", nullable = false, length = 254)
    private String nom;
    @Size(max = 254)
    @Column(name = "description", length = 254)
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "poste")
    private List<Personnel> personnelList;

    public Postepersonnel() {
    }

    public Postepersonnel(Integer id) {
        this.id = id;
    }

    public Postepersonnel(Integer id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<Personnel> getPersonnelList() {
        return personnelList;
    }

    public void setPersonnelList(List<Personnel> personnelList) {
        this.personnelList = personnelList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Postepersonnel)) {
            return false;
        }
        Postepersonnel other = (Postepersonnel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Postepersonnel[ id=" + id + " ]";
    }
    
}
