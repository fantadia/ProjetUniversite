/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "filieres", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nomfiliere"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Filieres.findAll", query = "SELECT f FROM Filieres f")
    , @NamedQuery(name = "Filieres.findById", query = "SELECT f FROM Filieres f WHERE f.id = :id")
    , @NamedQuery(name = "Filieres.findByNomfiliere", query = "SELECT f FROM Filieres f WHERE f.nomfiliere = :nomfiliere")
    , @NamedQuery(name = "Filieres.findByDescription", query = "SELECT f FROM Filieres f WHERE f.description = :description")
    , @NamedQuery(name = "Filieres.findByEtat", query = "SELECT f FROM Filieres f WHERE f.etat = :etat")})
public class Filieres implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nomfiliere", nullable = false, length = 254)
    private String nomfiliere;
    @Size(max = 254)
    @Column(name = "description", length = 254)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filiere")
    private List<Uniteenseignements> uniteenseignementsList;
    @JoinColumn(name = "departement", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Departement departement;

    public Filieres() {
    }

    public Filieres(Integer id) {
        this.id = id;
    }

    public Filieres(Integer id, String nomfiliere, boolean etat) {
        this.id = id;
        this.nomfiliere = nomfiliere;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomfiliere() {
        return nomfiliere;
    }

    public void setNomfiliere(String nomfiliere) {
        this.nomfiliere = nomfiliere;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Uniteenseignements> getUniteenseignementsList() {
        return uniteenseignementsList;
    }

    public void setUniteenseignementsList(List<Uniteenseignements> uniteenseignementsList) {
        this.uniteenseignementsList = uniteenseignementsList;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filieres)) {
            return false;
        }
        Filieres other = (Filieres) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Filieres[ id=" + id + " ]";
    }
    
}
