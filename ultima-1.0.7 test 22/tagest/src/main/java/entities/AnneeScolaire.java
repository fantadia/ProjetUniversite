/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "annee_scolaire", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"annee"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnneeScolaire.findAll", query = "SELECT a FROM AnneeScolaire a")
    , @NamedQuery(name = "AnneeScolaire.findById", query = "SELECT a FROM AnneeScolaire a WHERE a.id = :id")
    , @NamedQuery(name = "AnneeScolaire.findByAnnee", query = "SELECT a FROM AnneeScolaire a WHERE a.annee = :annee")
    , @NamedQuery(name = "AnneeScolaire.findBySemestre", query = "SELECT a FROM AnneeScolaire a WHERE a.semestre = :semestre")
    , @NamedQuery(name = "AnneeScolaire.findByEtat", query = "SELECT a FROM AnneeScolaire a WHERE a.etat = :etat")})
public class AnneeScolaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "annee", nullable = false)
    private int annee;
    @Size(max = 254)
    @Column(name = "semestre", length = 254)
    private String semestre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @OneToMany(mappedBy = "annee")
    private List<Notes> notesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "annee")
    private List<Etudiants> etudiantsList;

    public AnneeScolaire() {
    }

    public AnneeScolaire(Integer id) {
        this.id = id;
    }

    public AnneeScolaire(Integer id, int annee, boolean etat) {
        this.id = id;
        this.annee = annee;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Notes> getNotesList() {
        return notesList;
    }

    public void setNotesList(List<Notes> notesList) {
        this.notesList = notesList;
    }

    @XmlTransient
    public List<Etudiants> getEtudiantsList() {
        return etudiantsList;
    }

    public void setEtudiantsList(List<Etudiants> etudiantsList) {
        this.etudiantsList = etudiantsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnneeScolaire)) {
            return false;
        }
        AnneeScolaire other = (AnneeScolaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.AnneeScolaire[ id=" + id + " ]";
    }
    
}
