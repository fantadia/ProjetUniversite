/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "classes", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"denomination"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Classes.findAll", query = "SELECT c FROM Classes c")
    , @NamedQuery(name = "Classes.findById", query = "SELECT c FROM Classes c WHERE c.id = :id")
    , @NamedQuery(name = "Classes.findByDenomination", query = "SELECT c FROM Classes c WHERE c.denomination = :denomination")
    , @NamedQuery(name = "Classes.findByNbreetudiant", query = "SELECT c FROM Classes c WHERE c.nbreetudiant = :nbreetudiant")})
public class Classes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "denomination", nullable = false, length = 254)
    private String denomination;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nbreetudiant", nullable = false)
    private int nbreetudiant;
    @OneToMany(mappedBy = "classe")
    private List<Emploidetemps> emploidetempsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classe")
    private List<Etudiants> etudiantsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classe")
    private List<Classematiere> classematiereList;

    public Classes() {
    }

    public Classes(Integer id) {
        this.id = id;
    }

    public Classes(Integer id, String denomination, int nbreetudiant) {
        this.id = id;
        this.denomination = denomination;
        this.nbreetudiant = nbreetudiant;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public int getNbreetudiant() {
        return nbreetudiant;
    }

    public void setNbreetudiant(int nbreetudiant) {
        this.nbreetudiant = nbreetudiant;
    }

    @XmlTransient
    public List<Emploidetemps> getEmploidetempsList() {
        return emploidetempsList;
    }

    public void setEmploidetempsList(List<Emploidetemps> emploidetempsList) {
        this.emploidetempsList = emploidetempsList;
    }

    @XmlTransient
    public List<Etudiants> getEtudiantsList() {
        return etudiantsList;
    }

    public void setEtudiantsList(List<Etudiants> etudiantsList) {
        this.etudiantsList = etudiantsList;
    }

    @XmlTransient
    public List<Classematiere> getClassematiereList() {
        return classematiereList;
    }

    public void setClassematiereList(List<Classematiere> classematiereList) {
        this.classematiereList = classematiereList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Classes)) {
            return false;
        }
        Classes other = (Classes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Classes[ id=" + id + " ]";
    }
    
}
