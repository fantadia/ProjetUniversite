/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "comptabilite", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"mois", "etudiant"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comptabilite.findAll", query = "SELECT c FROM Comptabilite c")
    , @NamedQuery(name = "Comptabilite.findById", query = "SELECT c FROM Comptabilite c WHERE c.id = :id")
    , @NamedQuery(name = "Comptabilite.findByMontant", query = "SELECT c FROM Comptabilite c WHERE c.montant = :montant")
    , @NamedQuery(name = "Comptabilite.findByMontanttotal", query = "SELECT c FROM Comptabilite c WHERE c.montanttotal = :montanttotal")
    , @NamedQuery(name = "Comptabilite.findByDates", query = "SELECT c FROM Comptabilite c WHERE c.dates = :dates")
    , @NamedQuery(name = "Comptabilite.findByEtat", query = "SELECT c FROM Comptabilite c WHERE c.etat = :etat")})
public class Comptabilite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montant", nullable = false)
    private double montant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montanttotal", nullable = false)
    private double montanttotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dates", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dates;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @JoinColumn(name = "etudiant", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Etudiants etudiant;
    @JoinColumn(name = "mois", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Mois mois;

    public Comptabilite() {
    }

    public Comptabilite(Integer id) {
        this.id = id;
    }

    public Comptabilite(Integer id, double montant, double montanttotal, Date dates, boolean etat) {
        this.id = id;
        this.montant = montant;
        this.montanttotal = montanttotal;
        this.dates = dates;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public double getMontanttotal() {
        return montanttotal;
    }

    public void setMontanttotal(double montanttotal) {
        this.montanttotal = montanttotal;
    }

    public Date getDates() {
        return dates;
    }

    public void setDates(Date dates) {
        this.dates = dates;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public Etudiants getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiants etudiant) {
        this.etudiant = etudiant;
    }

    public Mois getMois() {
        return mois;
    }

    public void setMois(Mois mois) {
        this.mois = mois;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comptabilite)) {
            return false;
        }
        Comptabilite other = (Comptabilite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Comptabilite[ id=" + id + " ]";
    }
    
}
