/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "uniteenseignements", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nom"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Uniteenseignements.findAll", query = "SELECT u FROM Uniteenseignements u")
    , @NamedQuery(name = "Uniteenseignements.findById", query = "SELECT u FROM Uniteenseignements u WHERE u.id = :id")
    , @NamedQuery(name = "Uniteenseignements.findByNom", query = "SELECT u FROM Uniteenseignements u WHERE u.nom = :nom")
    , @NamedQuery(name = "Uniteenseignements.findByDescription", query = "SELECT u FROM Uniteenseignements u WHERE u.description = :description")
    , @NamedQuery(name = "Uniteenseignements.findByCredit", query = "SELECT u FROM Uniteenseignements u WHERE u.credit = :credit")
    , @NamedQuery(name = "Uniteenseignements.findByEtat", query = "SELECT u FROM Uniteenseignements u WHERE u.etat = :etat")})
public class Uniteenseignements implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nom", nullable = false, length = 254)
    private String nom;
    @Size(max = 254)
    @Column(name = "description", length = 254)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "credit", nullable = false)
    private int credit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @JoinColumn(name = "filiere", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Filieres filiere;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "uniteenseignement")
    private List<Matiersmodule> matiersmoduleList;

    public Uniteenseignements() {
    }

    public Uniteenseignements(Integer id) {
        this.id = id;
    }

    public Uniteenseignements(Integer id, String nom, int credit, boolean etat) {
        this.id = id;
        this.nom = nom;
        this.credit = credit;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public Filieres getFiliere() {
        return filiere;
    }

    public void setFiliere(Filieres filiere) {
        this.filiere = filiere;
    }

    @XmlTransient
    public List<Matiersmodule> getMatiersmoduleList() {
        return matiersmoduleList;
    }

    public void setMatiersmoduleList(List<Matiersmodule> matiersmoduleList) {
        this.matiersmoduleList = matiersmoduleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Uniteenseignements)) {
            return false;
        }
        Uniteenseignements other = (Uniteenseignements) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Uniteenseignements[ id=" + id + " ]";
    }
    
}
