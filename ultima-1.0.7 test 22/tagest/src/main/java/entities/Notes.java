/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "notes", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"etudiant", "matiere", "note"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notes.findAll", query = "SELECT n FROM Notes n")
    , @NamedQuery(name = "Notes.findById", query = "SELECT n FROM Notes n WHERE n.id = :id")
    , @NamedQuery(name = "Notes.findByNote", query = "SELECT n FROM Notes n WHERE n.note = :note")
    , @NamedQuery(name = "Notes.findByNotedeliberer", query = "SELECT n FROM Notes n WHERE n.notedeliberer = :notedeliberer")
    , @NamedQuery(name = "Notes.findByCredit", query = "SELECT n FROM Notes n WHERE n.credit = :credit")
    , @NamedQuery(name = "Notes.findByResultatue", query = "SELECT n FROM Notes n WHERE n.resultatue = :resultatue")
    , @NamedQuery(name = "Notes.findByCoefficient", query = "SELECT n FROM Notes n WHERE n.coefficient = :coefficient")})
public class Notes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "note", nullable = false)
    private double note;
    @Basic(optional = false)
    @NotNull
    @Column(name = "notedeliberer", nullable = false)
    private double notedeliberer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "credit", nullable = false)
    private int credit;
    @Size(max = 254)
    @Column(name = "resultatue", length = 254)
    private String resultatue;
    @Column(name = "coefficient")
    private Integer coefficient;
    @JoinColumn(name = "annee", referencedColumnName = "id")
    @ManyToOne
    private AnneeScolaire annee;
    @JoinColumn(name = "enseignant", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Enseignants enseignant;
    @JoinColumn(name = "etudiant", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Etudiants etudiant;
    @JoinColumn(name = "matiere", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Matiersmodule matiere;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "note")
    private List<Historiquedeliberation> historiquedeliberationList;

    public Notes() {
    }

    public Notes(Integer id) {
        this.id = id;
    }

    public Notes(Integer id, double note, double notedeliberer, int credit) {
        this.id = id;
        this.note = note;
        this.notedeliberer = notedeliberer;
        this.credit = credit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    public double getNotedeliberer() {
        return notedeliberer;
    }

    public void setNotedeliberer(double notedeliberer) {
        this.notedeliberer = notedeliberer;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getResultatue() {
        return resultatue;
    }

    public void setResultatue(String resultatue) {
        this.resultatue = resultatue;
    }

    public Integer getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Integer coefficient) {
        this.coefficient = coefficient;
    }

    public AnneeScolaire getAnnee() {
        return annee;
    }

    public void setAnnee(AnneeScolaire annee) {
        this.annee = annee;
    }

    public Enseignants getEnseignant() {
        return enseignant;
    }

    public void setEnseignant(Enseignants enseignant) {
        this.enseignant = enseignant;
    }

    public Etudiants getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiants etudiant) {
        this.etudiant = etudiant;
    }

    public Matiersmodule getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiersmodule matiere) {
        this.matiere = matiere;
    }

    @XmlTransient
    public List<Historiquedeliberation> getHistoriquedeliberationList() {
        return historiquedeliberationList;
    }

    public void setHistoriquedeliberationList(List<Historiquedeliberation> historiquedeliberationList) {
        this.historiquedeliberationList = historiquedeliberationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notes)) {
            return false;
        }
        Notes other = (Notes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Notes[ id=" + id + " ]";
    }
    
}
