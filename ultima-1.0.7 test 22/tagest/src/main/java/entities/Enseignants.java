/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "enseignants", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"matricule"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enseignants.findAll", query = "SELECT e FROM Enseignants e")
    , @NamedQuery(name = "Enseignants.findById", query = "SELECT e FROM Enseignants e WHERE e.id = :id")
    , @NamedQuery(name = "Enseignants.findByMatricule", query = "SELECT e FROM Enseignants e WHERE e.matricule = :matricule")
    , @NamedQuery(name = "Enseignants.findByPremon", query = "SELECT e FROM Enseignants e WHERE e.premon = :premon")
    , @NamedQuery(name = "Enseignants.findByNom", query = "SELECT e FROM Enseignants e WHERE e.nom = :nom")
    , @NamedQuery(name = "Enseignants.findByAge", query = "SELECT e FROM Enseignants e WHERE e.age = :age")
    , @NamedQuery(name = "Enseignants.findBySexe", query = "SELECT e FROM Enseignants e WHERE e.sexe = :sexe")
    , @NamedQuery(name = "Enseignants.findByDatenais", query = "SELECT e FROM Enseignants e WHERE e.datenais = :datenais")
    , @NamedQuery(name = "Enseignants.findByLieunais", query = "SELECT e FROM Enseignants e WHERE e.lieunais = :lieunais")
    , @NamedQuery(name = "Enseignants.findByPaysnais", query = "SELECT e FROM Enseignants e WHERE e.paysnais = :paysnais")
    , @NamedQuery(name = "Enseignants.findByNationalite", query = "SELECT e FROM Enseignants e WHERE e.nationalite = :nationalite")
    , @NamedQuery(name = "Enseignants.findBySituationmatrimoniale", query = "SELECT e FROM Enseignants e WHERE e.situationmatrimoniale = :situationmatrimoniale")
    , @NamedQuery(name = "Enseignants.findByTelephone", query = "SELECT e FROM Enseignants e WHERE e.telephone = :telephone")
    , @NamedQuery(name = "Enseignants.findByEmail1", query = "SELECT e FROM Enseignants e WHERE e.email1 = :email1")
    , @NamedQuery(name = "Enseignants.findByEmail", query = "SELECT e FROM Enseignants e WHERE e.email = :email")})
public class Enseignants implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "matricule", nullable = false, length = 254)
    private String matricule;
    @Size(max = 254)
    @Column(name = "premon", length = 254)
    private String premon;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nom", nullable = false, length = 254)
    private String nom;
    @Column(name = "age")
    private Integer age;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "sexe", nullable = false, length = 254)
    private String sexe;
    @Column(name = "datenais")
    @Temporal(TemporalType.DATE)
    private Date datenais;
    @Size(max = 254)
    @Column(name = "lieunais", length = 254)
    private String lieunais;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "paysnais", nullable = false, length = 254)
    private String paysnais;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nationalite", nullable = false, length = 254)
    private String nationalite;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "situationmatrimoniale", nullable = false, length = 254)
    private String situationmatrimoniale;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telephone", nullable = false)
    private int telephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "email1", nullable = false, length = 254)
    private String email1;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 254)
    @Column(name = "email", length = 254)
    private String email;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enseignant")
    private List<Notes> notesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enseignant")
    private List<Seancecours> seancecoursList;

    public Enseignants() {
    }

    public Enseignants(Integer id) {
        this.id = id;
    }

    public Enseignants(Integer id, String matricule, String nom, String sexe, String paysnais, String nationalite, String situationmatrimoniale, int telephone, String email1) {
        this.id = id;
        this.matricule = matricule;
        this.nom = nom;
        this.sexe = sexe;
        this.paysnais = paysnais;
        this.nationalite = nationalite;
        this.situationmatrimoniale = situationmatrimoniale;
        this.telephone = telephone;
        this.email1 = email1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getPremon() {
        return premon;
    }

    public void setPremon(String premon) {
        this.premon = premon;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Date getDatenais() {
        return datenais;
    }

    public void setDatenais(Date datenais) {
        this.datenais = datenais;
    }

    public String getLieunais() {
        return lieunais;
    }

    public void setLieunais(String lieunais) {
        this.lieunais = lieunais;
    }

    public String getPaysnais() {
        return paysnais;
    }

    public void setPaysnais(String paysnais) {
        this.paysnais = paysnais;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public String getSituationmatrimoniale() {
        return situationmatrimoniale;
    }

    public void setSituationmatrimoniale(String situationmatrimoniale) {
        this.situationmatrimoniale = situationmatrimoniale;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public List<Notes> getNotesList() {
        return notesList;
    }

    public void setNotesList(List<Notes> notesList) {
        this.notesList = notesList;
    }

    @XmlTransient
    public List<Seancecours> getSeancecoursList() {
        return seancecoursList;
    }

    public void setSeancecoursList(List<Seancecours> seancecoursList) {
        this.seancecoursList = seancecoursList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enseignants)) {
            return false;
        }
        Enseignants other = (Enseignants) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Enseignants[ id=" + id + " ]";
    }
    
}
