/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fanta Diakité
 */
@Entity
@Table(name = "salles", catalog = "university", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nomsalle"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Salles.findAll", query = "SELECT s FROM Salles s")
    , @NamedQuery(name = "Salles.findById", query = "SELECT s FROM Salles s WHERE s.id = :id")
    , @NamedQuery(name = "Salles.findByNomsalle", query = "SELECT s FROM Salles s WHERE s.nomsalle = :nomsalle")
    , @NamedQuery(name = "Salles.findByEtat", query = "SELECT s FROM Salles s WHERE s.etat = :etat")})
public class Salles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "nomsalle", nullable = false, length = 254)
    private String nomsalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "etat", nullable = false)
    private boolean etat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "salle")
    private List<Seancecours> seancecoursList;

    public Salles() {
    }

    public Salles(Integer id) {
        this.id = id;
    }

    public Salles(Integer id, String nomsalle, boolean etat) {
        this.id = id;
        this.nomsalle = nomsalle;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomsalle() {
        return nomsalle;
    }

    public void setNomsalle(String nomsalle) {
        this.nomsalle = nomsalle;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Seancecours> getSeancecoursList() {
        return seancecoursList;
    }

    public void setSeancecoursList(List<Seancecours> seancecoursList) {
        this.seancecoursList = seancecoursList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salles)) {
            return false;
        }
        Salles other = (Salles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Salles[ id=" + id + " ]";
    }
    
}
