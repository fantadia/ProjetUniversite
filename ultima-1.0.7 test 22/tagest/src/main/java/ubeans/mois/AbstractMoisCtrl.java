/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubeans.mois;


import static entities.Comptabilite_.mois;
import ubean.salle.*;
import entities.Matiersmodule;
import entities.Mois;
import entities.Salles;
import java.util.List;
import javax.ejb.EJB;
import sessions.ComptabiliteFacadeLocal;
import sessions.MoisFacadeLocal;

import sessions.SallesFacadeLocal;
import sessions.SeancecoursFacadeLocal;

/**
 *
 * @author Fantastic
 */
public abstract class AbstractMoisCtrl {
    
    @EJB
    protected MoisFacadeLocal moisFacadeLocale ;
    
    @EJB
    protected ComptabiliteFacadeLocal comptabiliteFacadeLocal;
    
    
    
    
    protected List<Mois> listMois;
    
    protected StringBuffer MoisTableHtml= new StringBuffer("pas encore implementé") ;
    protected Mois selectedMois;
    protected Mois mois;

    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public MoisFacadeLocal getMoisFacadeLocale() {
        return moisFacadeLocale;
    }

    public void setMoisFacadeLocale(MoisFacadeLocal moisFacadeLocale) {
        this.moisFacadeLocale = moisFacadeLocale;
    }

    public Mois getSelectedMois() {
        return selectedMois;
    }

    public void getSelectedMois(Mois selectedMois) {
        this.selectedMois = selectedMois;
         if (selectedMois== null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
         }
         
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Mois getMois() 
    {
        return mois;
    }

    public void setMois(Mois mois) {
        this.mois = mois;
    }

    public List<Mois> getListMois() {
        return listMois;
    }

    public void setListMois(List<Mois> listMois) {
        this.listMois = listMois;
    }
    
    

   
    
    
    
//    public void setSelectedMatiersmodule(Matiersmodule selectedMatiersmodule) {
//        this.selectedMatiersmodule = selectedMatiersmodule;
//         List<String> privil = (List<String>) UtilitaireSession.getInstance().get("privillege");
//        String notif = "";
////        if (privil != null) {
////            creer = !privil.contains("ANNEE_CREER");
////            notif += creer ? "Création refusée\n" : "";
////            modifier = !privil.contains("ANNEE_MODIFIER");
////            notif += modifier ? "Modification refusée\n" : "";
////            detail = !privil.contains("ANNEE_DETAIL");
////            notif += detail ? "Detail refusé\n" : "";
////            supprimer = !privil.contains("ANNEE_SUPPRIMER");
////            notif += supprimer ? "Suppression refusée\n" : "";
////            imprimer = !privil.contains("ANNEE_IMPRIMER");
////            notif += imprimer ? "impression refusée\n" : "";
////        } else {
////            System.out.println("liste de privillege vide");
////        }
////        if (!notif.equals("")) {
////            JsfUtil.addWarningMessage(notif);
////        }
//        matiersmodule = new Matiersmodule();
//        if(selectedMatiersmodule == null )
//            return;
//        matiersmodule.setMatiersmodule(selectedMatiersmodule.getMatiersmodule());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//        matiersmodule.setId(selectedMatiersmodule.getId());
//        matiersmodule.setEtat(selectedMatiersmodule.getEtat());
//    }
    
    
    
    public void setSelectedM(Matiersmodule selectedMatiersmodule) {
        this.selectedMois = selectedMois;
        if (selectedMatiersmodule == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }
    
    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }
    
    public boolean isImprimer() {
        imprimer = moisFacadeLocale.findAll().isEmpty();
        return imprimer;
    }

   
    public StringBuffer getMoisTableHtml() {
        return MoisTableHtml;
    }
    //je suis cest fanta
    
    protected boolean creer = true;

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    
}
