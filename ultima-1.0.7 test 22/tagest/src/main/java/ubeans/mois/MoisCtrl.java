/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubeans.mois;

import ubean.salle.*;
import beans.util.JsfUtil;
import entities.Comptabilite;
import static entities.Comptabilite_.mois;
import entities.Mois;
import entities.Salles;
import entities.Seancecours;
import java.io.Serializable;

import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sessions.EtudiantsFacadeLocal;
//import org.jboss.weld.jsf.JsfApiAbstraction;
//import pdf.PdfMatiersmodule;

/**
 *
 * @author Fantastic
 */
@ManagedBean(name = "moisCtrl")
@ViewScoped
public class MoisCtrl extends AbstractMoisCtrl implements MoisInterfaceCtrl, Serializable {

    /**
     * Creates a new instance of MatiersmoduleCtrl
     */
    public MoisCtrl() {
    }

    @PostConstruct
    private void initMois() {
        listMois = moisFacadeLocale.findAll();
        selectedMois = new Mois();
        mois = new Mois();

    }

    @Override
    public void enregistrerMois() {
       Mois cl = moisFacadeLocale.findByNomMois(mois.getNom());
        if (cl != null) {
            JsfUtil.addErrorMessage("Cette mois existe déjà" + "'" + cl.getNom());
            modifier = supprimer = detail = true;
            return;
        }
        moisFacadeLocale.create(mois);
        initMois();
        JsfUtil.addSuccessMessage("le mois à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierMois() {
        if ((selectedMois==null)|| (mois.getId()==null)) {
            return;
        }

      Mois type = moisFacadeLocale.findByNomMois(selectedMois.getNom());
        if (type != null && !Objects.equals(type.getId(), selectedMois.getId())) {
            JsfUtil.addErrorMessage("le mois '" + selectedMois.getNom()+ "'" + "a été mis à jour");
        }
        moisFacadeLocale.edit(selectedMois);
        JsfUtil.addSuccessMessage("Matiersmodule enregistrée");
    }

    @Override
    public void supprimerMois() {

         if(selectedMois == null || selectedMois.getId() == null)
            return;
             
        List<Comptabilite> compmois= comptabiliteFacadeLocal.findByMois(selectedMois.getId());
      
        if (!compmois.isEmpty()) {
            utils.JsfUtil.addWarningMessage("Cette salle ne peut pas étre supprimé :car utilisee comme cle etrangere dans une autre cla");
        } else {

           moisFacadeLocale.remove(selectedMois);
            JsfUtil.addErrorMessage("Cette salle a été supprimée");
            modifier = supprimer = detail = true;
        }
    }

 

    @Override
    public String getComponentMessages(String clientComponent, String defaultMessage) {
        return beans.util.JsfUtil.getComponentMessages(clientComponent, defaultMessage);
    }

   

}
