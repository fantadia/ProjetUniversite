/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ubeans.mois;

import ubean.salle.*;


/**
 *
 * @author Fantastic
 */
public interface MoisInterfaceCtrl {
    
    /**
     * cette fonction permet de save les Matiersmodule
     */
    public void enregistrerMois();
    
    
    public void modifierMois();
    
    /**
     * Fantastic
     * Supprimer le Matiersmodule
     * 
     */
    public void supprimerMois();
    
    
    
    /**
     * Fantastic
     * Méthode d'impression
     *
     */
   
   
    
     public String getComponentMessages(String classeComponent, String defaultMessage);
    
}
