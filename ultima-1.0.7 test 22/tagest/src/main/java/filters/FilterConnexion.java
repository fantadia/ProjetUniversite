/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Fantastic
 */
@WebFilter(filterName = "FilterConnexion", urlPatterns = {"/*"})// declarration d'un filtre
public class FilterConnexion implements Filter, Serializable {

    private static final Logger LOG = Logger.getLogger(FilterConnexion.class.getName());

    private static final boolean DEBUG = true;
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public FilterConnexion() {
        System.err.println("FilterConnexion -->>>>>>>>>>>>>< le constructeur");
    }

    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (DEBUG) {
            log("FilterConnexion:DoBeforeProcessing");
        }
        System.err.println("FilterConnexion--------------<<         doBeforeProcessing");
        // Write code here to process the request and/or response before
        // the rest of the filter chain is invoked.

        // For example, a logging filter might log items on the request object,
        // such as the parameters.
        /*
         for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
         String name = (String)en.nextElement();
         String values[] = request.getParameterValues(name);
         int n = values.length;
         StringBuffer buf = new StringBuffer();
         buf.append(name);
         buf.append("=");
         for(int i=0; i < n; i++) {
         buf.append(values[i]);
         if (i < n-1)
         buf.append(",");
         }
         log(buf.toString());
         }
         */
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        System.err.println("FilterConnexion--------------<<         doAfterProcessing");
        if (DEBUG) {
            log("FilterConnexion:DoAfterProcessing");
        }

        // Write code here to process the request and/or response after
        // the rest of the filter chain is invoked.
        // For example, a logging filter might log the attributes on the
        // request object after the request has been processed. 
        /*
         for (Enumeration en = request.getAttributeNames(); en.hasMoreElements(); ) {
         String name = (String)en.nextElement();
         Object value = request.getAttribute(name);
         log("attribute: " + name + "=" + value.toString());

         }
         */
        // For example, a filter might append something to the response.
        /*
         PrintWriter respOut = new PrintWriter(response.getWriter());
         respOut.println("<P><B>This has been appended by an intrusive filter.</B>");
         */
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        System.err.println("doFilter");
        if (DEBUG) {
            log("FilterConnexion:doFilter()");
        }

        doBeforeProcessing(request, response);

//        System.out.println("++++++ utilisateur connecté ++++++");
        HttpServletRequest hRequest = (HttpServletRequest) request;
        HttpServletResponse hResponse = (HttpServletResponse) response;
        HttpSession session = hRequest.getSession();
        if (hRequest.getRequestURL().toString().endsWith("landing.xhtml")//recupération de l'url de la requete en terme de chaine de caractère et on vérifie si sa fini par la page d'accueil landing.xhtml dont on a pas besoin d'etre connecter pour voir
                || hRequest.getRequestURL().toString().endsWith(hRequest.getContextPath()) // on vérifie si on est a l'accueil le mon dossier du projet target 
                || hRequest.getRequestURL().toString().endsWith(hRequest.getContextPath() + "/") //on verifie si la page d'accueil finie par  le /
                || hRequest.getRequestURL().toString().contains(hRequest.getContextPath() + "/javax.faces.resource/")) // meme que mon dossier de style
        {
            chain.doFilter(request, response);//ignore moi ces URL spécifiées plu haut
            return;
        }
//        String param = request.getParameter("j_form_connexion");
//        if (param == null || param.equals("")) {
        if (session.getAttribute("user") != null) // vérifions si l'utilisateur est connecté
        {
//            doBeforeProcessing(request, response);
            if (hRequest.getRequestURL().toString().endsWith("login.xhtml")) // alors je verifie si l'url fini par login
            {
                hResponse.sendRedirect(hRequest.getContextPath() + "/dashboard.xhtml"); // on le regirige vers dashboard
            } else {
                chain.doFilter(request, response);//l'utilisateur n'est pas connecter
            }
        } else {
            try {
                LOG.log(Level.WARNING, "Utilisateur non connect\u00e9: {0}", hRequest.getContextPath());
                request.getRequestDispatcher("/login.xhtml").forward(request, response);// on le redirige vers la page login
            } catch (IOException | ServletException t) {
                LOG.log(Level.WARNING, null, t);
            }
        }

        doAfterProcessing(request, response);

        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
    }

    /**
     * Return the filter configuration object for this filter.
     *
     * @return
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (DEBUG) {
                log("FilterConnexion:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("FilterConnexion()");
        }
        StringBuilder sb = new StringBuilder("FilterConnexion(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (IOException ex) {
            LOG.log(Level.WARNING, null, ex);
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
}
