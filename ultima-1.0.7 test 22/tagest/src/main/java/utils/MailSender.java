package utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@ManagedBean
@SessionScoped
public class MailSender {

    /**
     * abdoulsys
     * @since 25-11-2015
     * fonction qui envoit le mail a partir de l'adresse email du bailleur qu'on met dans la variable Strind email
     * @param debug
     * @param adresses
     * @param Objet
     * @param Message
     * @param fichierTel
     * @return
     * @throws MessagingException
     * @throws IOException 
     */
    public boolean envoyerMailSMTP(boolean debug,
            ArrayList<String> adresses,
            String Objet,
            String Message,
            String fichierTel)
            throws MessagingException,
            IOException {
        boolean result = false;
        final String serveur = "smtp.mail.yahoo.com", 
                email = "abdoulayeahmadou56@hotmail.fr",                
                //User = "abdoulayeahmadou56@hotmail.fr",
                //Port = "587",
                passwd = "Phacochère5432";
        try {

            Properties prop = new Properties();
            prop.put("mail.smtp.port", "587");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true");
            prop.put("mail.smtp.socketFactory.class", "import javax.net.ssl.SSLSocketFactory");
            prop.put("mail.smtp.socketFactory.fallback", "true");
            prop.put("mail.smtp.ssl.enable", "true");

            Session session = Session.getDefaultInstance(prop, null);

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));//originaire du message

            for (String adresse : adresses) {
                message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(adresse)); //ajouter les recepteur du message
            }

            if (!fichierTel.equals("")) {

                MimeBodyPart mbp2 = new MimeBodyPart();
                String file = fichierTel;//"C://Users//core i3//Pictures//arnaud/amina.jpg";
                mbp2.attachFile(file);

                /*contenu du message */
                MimeBodyPart mbp1 = new MimeBodyPart();
                mbp1.setText(Message);

                /*on regroupe tous les deux dans message*/
                MimeMultipart mp = new MimeMultipart();
                mp.addBodyPart(mbp1);
                mp.addBodyPart(mbp2);
                message.setContent(mp);

                message.setSubject(Objet);

                //message.setContent(Message, "text/plain");
                message.setSentDate(new Date());
                session.setDebug(debug);
                Transport tr = session.getTransport("smtp");

                tr.connect(serveur, email, passwd);

                message.saveChanges();

                tr.sendMessage(message, message.getAllRecipients());
                tr.close();

                result = true;
            } else {

                message.setSubject(Objet);

                message.setContent(Message, "text/plain");
                message.setSentDate(new Date());
                message.setFileName("");
                session.setDebug(debug);
                Transport tr = session.getTransport("smtp");

                tr.connect(serveur, email, passwd);

                message.saveChanges();

                tr.sendMessage(message, message.getAllRecipients());
                tr.close();

                result = true;

            }

        } catch (AddressException e) {
        }
        return result;
    }

}
