/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Seancecours;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class SeancecoursFacade extends AbstractFacade<Seancecours> implements SeancecoursFacadeLocal {

    private static final Logger LOG = Logger.getLogger(SeancecoursFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SeancecoursFacade() {
        super(Seancecours.class);
    }

    @Override
    public List<Seancecours> findByIdMatiersmodule(Integer idMatiere) {

        List<Seancecours> sec = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT s FROM Seancecours s WHERE s.");
            q.setParameter(1, idMatiere);
            sec = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sec;
    }

    @Override
    public List<Seancecours> findBySalle(Integer idSalle) {

        List<Seancecours> sec = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT s FROM Seancecours s WHERE s.seancecoursPK.salle =?1 ");
            q.setParameter(1, idSalle);
            sec = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Erreur dans findBySalle de seancecours", e);
        }
        return sec;

    }

    @Override
    public List<Seancecours> findByIdEnseignant(Integer idEns) {
        List<Seancecours> sec = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT s FROM Seancecours s WHERE s.enseignant.id =?1 ");
            q.setParameter(1, idEns);
            sec = q.getResultList();
        } catch (Exception e) {
            return null;
        }
        return sec;
    }

   

    @Override
    public List<Seancecours> findByIdEmp_tps(Integer idEmpt) {
        List<Seancecours> sec = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT s FROM Seancecours s WHERE s.emploitps.id =?1 ");
            q.setParameter(1, idEmpt);
            sec = q.getResultList();
        } catch (Exception e) {
            return null;
        }
        return sec;

    }

}
