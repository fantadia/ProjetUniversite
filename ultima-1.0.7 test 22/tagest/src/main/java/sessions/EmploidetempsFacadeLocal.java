/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Emploidetemps;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface EmploidetempsFacadeLocal {

    void create(Emploidetemps emploidetemps);

    void edit(Emploidetemps emploidetemps);

    void remove(Emploidetemps emploidetemps);

    Emploidetemps find(Object id);

    List<Emploidetemps> findAll();

    List<Emploidetemps> findRange(int[] range);
    
    Emploidetemps findByIdEmploiDeTps(Integer id);

    int count();
    
}
