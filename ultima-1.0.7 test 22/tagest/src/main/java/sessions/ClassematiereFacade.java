/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Classematiere;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class ClassematiereFacade extends AbstractFacade<Classematiere> implements ClassematiereFacadeLocal {

     private static final Logger LOG = Logger.getLogger(ClassesFacade.class.getName());
     
    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClassematiereFacade() {
        super(Classematiere.class);
    }

    @Override
    public List<Classematiere> findMatiereByclasse(Integer id) {
        List<Classematiere> matieres = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT c FROM Classematiere c WHERE c.classe.id = ?1");
            q.setParameter(1, id);
            matieres = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return matieres;
    }
    
    
    
    
}
