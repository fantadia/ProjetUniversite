/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Salles;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class SallesFacade extends AbstractFacade<Salles> implements SallesFacadeLocal {

    private static final Logger LOG = Logger.getLogger(SallesFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SallesFacade() {
        super(Salles.class);
    }

    @Override
    public Salles findByNomSalle(String annee) {
        Salles s;
        Query q;
        try {
            q = em.createQuery("SELECT s FROM Salles s WHERE s.nomsalle = ?1 ");
            q.setParameter(1, annee);
            s = (Salles) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return s;
    }

    @Override
    public Salles findByIdSalle(Integer id) {
        Salles sa = null;
        Query q;
        try {
            q = em.createQuery("SELECT s FROM Salles s WHERE s.id = ?1");
            q.setParameter(1, id);
            sa = (Salles) q.getSingleResult();
        } catch (Exception e) {
//            LOG.log(Level.WARNING, null, e);
        }
        return sa;
    }

    @Override
    public List<Salles> findByEtatSalle(boolean etat) {
        List<Salles> sal = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT s FROM Salles s WHERE s.etat =?1 ");
            q.setParameter(1, etat);
            sal = (List<Salles>) q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return sal;
    }

}
