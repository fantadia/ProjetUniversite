/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Classes;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class ClassesFacade extends AbstractFacade<Classes> implements ClassesFacadeLocal {

    private static final Logger LOG = Logger.getLogger(ClassesFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClassesFacade() {
        super(Classes.class);
    }

    @Override
    public Classes findByClasses(String denomination) {
       
        Classes classe = null;
        Query q;
        try {
            q = em.createQuery("SELECT c FROM Classes c WHERE c.denomination = ?1");
            q.setParameter(1, denomination);
            classe = (Classes) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return classe;
    }
    
    
}
