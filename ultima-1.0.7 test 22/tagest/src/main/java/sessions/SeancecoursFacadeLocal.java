/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Seancecours;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface SeancecoursFacadeLocal {

    void create(Seancecours seancecours);

    void edit(Seancecours seancecours);

    void remove(Seancecours seancecours);

    Seancecours find(Object id);

    List<Seancecours> findAll();

    List<Seancecours> findRange(int[] range);

    int count();

    public List<Seancecours> findByIdMatiersmodule(Integer idMatiere);

    public List<Seancecours> findBySalle(Integer idSalle);

    public List<Seancecours> findByIdEnseignant(Integer idEns);


    public List<Seancecours> findByIdEmp_tps(Integer idEmpts);

}
