/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Privilegeperso;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class PrivilegepersoFacade extends AbstractFacade<Privilegeperso> implements PrivilegepersoFacadeLocal {

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrivilegepersoFacade() {
        super(Privilegeperso.class);
    }

}
