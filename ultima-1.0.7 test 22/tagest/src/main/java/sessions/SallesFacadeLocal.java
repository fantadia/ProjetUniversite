/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Salles;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface SallesFacadeLocal {

    void create(Salles salles);

    void edit(Salles salles);

    void remove(Salles salles);

    Salles find(Object id);

    List<Salles> findAll();

    List<Salles> findRange(int[] range);

    int count();
    
    /**
     *
     * @param annee
     * @return
     */
    public Salles findByNomSalle (String annee );
    
    public Salles   findByIdSalle(Integer id) ;
    
    public List<Salles> findByEtatSalle(boolean etat);
     
     

    
}
