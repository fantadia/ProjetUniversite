/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Matiersmodule;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class MatiersmoduleFacade extends AbstractFacade<Matiersmodule> implements MatiersmoduleFacadeLocal {

    private static final Logger LOG = Logger.getLogger(MatiersmoduleFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MatiersmoduleFacade() {
        super(Matiersmodule.class);
    }

    @Override
    public List<Matiersmodule> findByIdUniteenseignements(Integer iduniteEns) {

        List<Matiersmodule> unitEns = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT m FROM Matiersmodule m WHERE m.uniteenseignement.id = ?1");
            q.setParameter(1, iduniteEns);
            unitEns = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return unitEns;
    }

    @Override
    public Matiersmodule findByMatiersmodule(String nommatiere) {
        Matiersmodule m;
        Query q;
        try {
            q = em.createQuery("SELECT m FROM Matiersmodule m WHERE m.nommatiere = ?1");
            q.setParameter(1, nommatiere);
            m = (Matiersmodule) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return m;
    }

    @Override
    public int NbreMatiereUE(Integer idUE) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
