/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Personnel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class PersonnelFacade extends AbstractFacade<Personnel> implements PersonnelFacadeLocal {

    private static final Logger LOG = Logger.getLogger(PersonnelFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonnelFacade() {
        super(Personnel.class);
    }

    @Override
    public Personnel findByIdPersonnel(Integer idPersonnel) {
        Personnel p;
        Query q;
        try {
            q = em.createQuery("SELECT p FROM Personnel p WHERE p.id=?1");
            q.setParameter(1, idPersonnel);
            p = (Personnel) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return p;
    }

    @Override
    public List<Personnel> findByIdPostepersonnel(Integer idPoste) {
        List<Personnel> sec = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT p FROM Personnel p WHERE p.poste.id =?1 ");
            q.setParameter(1, idPoste);
            sec = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return sec;

    }

}
