/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Compteuser;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface CompteuserFacadeLocal {

    void create(Compteuser compteuser);

    void edit(Compteuser compteuser);

    void remove(Compteuser compteuser);

    Compteuser find(Object id);

    List<Compteuser> findAll();

    List<Compteuser> findRange(int[] range);

    int count();

    public List<Compteuser> findByPersonnel(Integer id);

    /**
     * Permet l'authentification.
     *
     * @param login
     * @param motDePasse
     * @return
     */
    public Compteuser findByLoginAndPassword(String login, String motDePasse);

    public Compteuser findByIdPersonnel(Integer id);

    public Compteuser findByLogin(String login);

}
