/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Enseignants;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class EnseignantsFacade extends AbstractFacade<Enseignants> implements EnseignantsFacadeLocal {

    private static final Logger LOG = Logger.getLogger(EnseignantsFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EnseignantsFacade() {
        super(Enseignants.class);
    }

    @Override
    public Enseignants findByMatricule(String matricule) {
        Enseignants enseignant;
        Query q;
        try {
            q = em.createQuery("SELECT e FROM Enseignants e WHERE e.matricule =?1");
            q.setParameter(1, matricule);
            enseignant = (Enseignants) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return enseignant;

    }

    @Override
    public Enseignants findByIdEns(int id) {
        Enseignants enseignant;
        Query q;
        try {
            q = em.createQuery("SELECT e FROM Enseignants e WHERE e.id =?1");
            q.setParameter(1, id);
            enseignant = (Enseignants) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return enseignant;

    }

}
