/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Compteuser;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class CompteuserFacade extends AbstractFacade<Compteuser> implements CompteuserFacadeLocal {

    private static final Logger LOG = Logger.getLogger(CompteuserFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CompteuserFacade() {
        super(Compteuser.class);
    }

    @Override
    public List<Compteuser> findByPersonnel(Integer id) {
        List<Compteuser> c;
        Query q;
        try {
            q = em.createQuery("SELECT c FROM Compteuser c WHERE c.personnel =?1");
            q.setParameter(1, id);
            c = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return c;
    }

    @Override
    public Compteuser findByLoginAndPassword(String login, String motDePasse) {
        try {
            Query q = em.createQuery("SELECT c FROM Compteuser c WHERE c.login = ?1 AND c.motdepass = ?2");
            q.setParameter(1, login);
            q.setParameter(2, motDePasse);
            return (Compteuser) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, "Echec d''authentification: {0}", login);
            return null;
        }
    }

    @Override
    public Compteuser findByIdPersonnel(Integer id) {
         Compteuser cpt;
        Query q;
        try {
            q = em.createQuery("SELECT c FROM Compteuser c WHERE c.id = ?1");
            q.setParameter(1, id);
            cpt = (Compteuser) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return cpt;
        
    }

    @Override
    public Compteuser findByLogin(String login) {
         try {
            Query q = em.createQuery("SELECT c FROM Compteuser c WHERE c.login = ?1 = ?2");
            q.setParameter(1, login);
            return (Compteuser) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
    }

}
