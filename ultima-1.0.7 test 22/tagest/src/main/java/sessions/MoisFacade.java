/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Mois;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class MoisFacade extends AbstractFacade<Mois> implements MoisFacadeLocal {

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MoisFacade() {
        super(Mois.class);
    }

    @Override
    public Mois findByNomMois(String nom) {
        Mois m;
        Query q;
        try {
            q = em.createQuery("SELECT m FROM Mois m WHERE m.nom =?1 ");
            q.setParameter(1, nom);
            m = (Mois) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return m;

    }

}
