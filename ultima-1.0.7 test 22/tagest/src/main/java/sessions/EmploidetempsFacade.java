/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Emploidetemps;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class EmploidetempsFacade extends AbstractFacade<Emploidetemps> implements EmploidetempsFacadeLocal {

    private static final Logger LOG = Logger.getLogger(EmploidetempsFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmploidetempsFacade() {
        super(Emploidetemps.class);
    }

    @Override
    public Emploidetemps findByIdEmploiDeTps(Integer id) {
        Emploidetemps ept;
        Query q;
        try {
            q = em.createQuery(" SELECT e FROM Emploidetemps e WHERE e.id  = ?1");
            q.setParameter(1, id);
            ept = (Emploidetemps) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return ept;
    }

}
