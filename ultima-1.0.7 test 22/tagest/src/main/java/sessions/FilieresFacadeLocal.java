/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Filieres;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface FilieresFacadeLocal {

    void create(Filieres filieres);

    void edit(Filieres filieres);

    void remove(Filieres filieres);

    Filieres find(Object id);

    List<Filieres> findAll();

    List<Filieres> findRange(int[] range);

    int count();
    
    List<Filieres>  findByIdDepartement(Integer idDept);

    public Filieres findByNomFilieres(String nomfiliere);
    
}
