/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Horaires;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface HorairesFacadeLocal {

    void create(Horaires horaires);

    void edit(Horaires horaires);

    void remove(Horaires horaires);

    Horaires find(Object id);

    List<Horaires> findAll();

    List<Horaires> findRange(int[] range);
    
    Horaires findByIdHoraire(Integer id);

    int count();
    
}
