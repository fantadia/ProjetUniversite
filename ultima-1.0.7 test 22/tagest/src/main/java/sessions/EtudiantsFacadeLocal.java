/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Etudiants;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface EtudiantsFacadeLocal {

    void create(Etudiants etudiants);

    void edit(Etudiants etudiants);

    void remove(Etudiants etudiants);

    Etudiants find(Object id);

    List<Etudiants> findAll();

    List<Etudiants> findRange(int[] range);

    int count();

    public Etudiants findByINumerocarte(String numerocarte);

    public List<Etudiants> findByIdEtudiant(Integer idEtudiant);

    public List<Etudiants> findByIdClasses(Integer id);

    public List<Etudiants> findEtudiantByclasse(Integer idClasse);

    /**
     * Fournis la liste des étudiants triée sur leur classe
     *
     * @return
     */
    public List<Etudiants> findAllSortByClasse();

}
