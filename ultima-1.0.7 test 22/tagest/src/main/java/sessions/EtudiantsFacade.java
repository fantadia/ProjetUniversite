/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Etudiants;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class EtudiantsFacade extends AbstractFacade<Etudiants> implements EtudiantsFacadeLocal {

    private static final Logger LOG = Logger.getLogger(EtudiantsFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EtudiantsFacade() {
        super(Etudiants.class);
    }

    /**
     *
     * @param idEtudiant
     * @return
     */
    @Override
    public List<Etudiants> findByIdEtudiant(Integer idEtudiant) {
        List<Etudiants> etudiants = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT e FROM Etudiants e WHERE e.id = ?1");
            q.setParameter(1, idEtudiant);
            etudiants = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return etudiants;
    }

    @Override
    public List<Etudiants> findByIdClasses(Integer id) {

        List<Etudiants> etudiants = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT e FROM Etudiants e WHERE e.classe.id = ?1");
            q.setParameter(1, id);
            etudiants = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return etudiants;
    }

    @Override
    public Etudiants findByINumerocarte(String numerocarte) {
        Etudiants etudiant;
        Query q;
        try {
            q = em.createQuery("SELECT e FROM Etudiants e WHERE e.numerocarte = ?1");
            q.setParameter(1, numerocarte);
            etudiant = (Etudiants) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return etudiant;
    }

    @Override
    public List<Etudiants> findEtudiantByclasse(Integer idClasse) {
        List<Etudiants> etudiants = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT e FROM Etudiants e WHERE e.classe.id = ?1");
            q.setParameter(1, idClasse);
            etudiants = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return etudiants;
    }

    @Override
    public List<Etudiants> findAllSortByClasse() {
        List<Etudiants> etudiants = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT e FROM Etudiants e ORDER BY e.classe.denomination ASC");
            etudiants = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return etudiants;
    }

}
