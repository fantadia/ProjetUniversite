/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Historiquedeliberation;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface HistoriquedeliberationFacadeLocal {

    void create(Historiquedeliberation historiquedeliberation);

    void edit(Historiquedeliberation historiquedeliberation);

    void remove(Historiquedeliberation historiquedeliberation);

    Historiquedeliberation find(Object id);

    List<Historiquedeliberation> findAll();

    List<Historiquedeliberation> findRange(int[] range);
    
    List<Historiquedeliberation> findByNote(Integer idNote);
    
    Historiquedeliberation findByIdHistDelibetation(Integer id);

    int count();
    
}
