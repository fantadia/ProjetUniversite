/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Rolepersonnel;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface RolepersonnelFacadeLocal {

    void create(Rolepersonnel rolepersonnel);

    void edit(Rolepersonnel rolepersonnel);

    void remove(Rolepersonnel rolepersonnel);

    Rolepersonnel find(Object id);

    List<Rolepersonnel> findAll();

    List<Rolepersonnel> findRange(int[] range);

    int count();
    
}
