/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Departement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class DepartementFacade extends AbstractFacade<Departement> implements DepartementFacadeLocal {

    private static final Logger LOG = Logger.getLogger(DepartementFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DepartementFacade() {
        super(Departement.class);
    }

    @Override
    public Departement findByNomDepartement(String nomdepat) {
        Departement departement;
        Query q;
        try {
            q = em.createQuery("SELECT d FROM Departement d WHERE d.nomdepat = ?1");
            q.setParameter(1, nomdepat);
            departement = (Departement) q.getSingleResult();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return departement;
    }

}
