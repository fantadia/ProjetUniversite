/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Postepersonnel;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface PostepersonnelFacadeLocal {

    void create(Postepersonnel postepersonnel);

    void edit(Postepersonnel postepersonnel);

    void remove(Postepersonnel postepersonnel);

    Postepersonnel find(Object id);

    List<Postepersonnel> findAll();

    List<Postepersonnel> findRange(int[] range);

    int count();

    public Postepersonnel findByPostepersonnel(String nom);
    
}
