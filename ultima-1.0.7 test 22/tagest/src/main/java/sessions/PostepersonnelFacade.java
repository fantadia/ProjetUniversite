/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Postepersonnel;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class PostepersonnelFacade extends AbstractFacade<Postepersonnel> implements PostepersonnelFacadeLocal {

    private static final Logger LOG = Logger.getLogger(PostepersonnelFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PostepersonnelFacade() {
        super(Postepersonnel.class);
    }

    @Override
    public Postepersonnel findByPostepersonnel(String nom) {
        Postepersonnel p;
        Query q;
        try {
            q = em.createQuery("SELECT p FROM Postepersonnel p WHERE p.nom =?1");
            q.setParameter(1, nom);
            p = (Postepersonnel) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return p;
    }

}
