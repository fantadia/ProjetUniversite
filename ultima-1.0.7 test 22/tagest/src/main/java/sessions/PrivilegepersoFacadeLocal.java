/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Privilegeperso;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface PrivilegepersoFacadeLocal {

    void create(Privilegeperso privilegeperso);

    void edit(Privilegeperso privilegeperso);

    void remove(Privilegeperso privilegeperso);

    Privilegeperso find(Object id);

    List<Privilegeperso> findAll();

    List<Privilegeperso> findRange(int[] range);

    int count();
    
}
