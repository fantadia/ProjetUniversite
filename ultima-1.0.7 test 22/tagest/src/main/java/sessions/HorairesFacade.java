/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Horaires;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class HorairesFacade extends AbstractFacade<Horaires> implements HorairesFacadeLocal {

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HorairesFacade() {
        super(Horaires.class);
    }

    @Override
    public Horaires findByIdHoraire(Integer id) {
        Horaires m;
        Query q;
        try {
            q = em.createQuery("SELECT h FROM Horaires h WHERE h.id = ?1");
            q.setParameter(1, id);
            m = (Horaires) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return m;
    }

}
