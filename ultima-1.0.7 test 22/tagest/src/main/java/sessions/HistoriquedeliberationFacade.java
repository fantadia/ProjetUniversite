/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Historiquedeliberation;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class HistoriquedeliberationFacade extends AbstractFacade<Historiquedeliberation> implements HistoriquedeliberationFacadeLocal {

    private static final Logger LOG = Logger.getLogger(HistoriquedeliberationFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HistoriquedeliberationFacade() {
        super(Historiquedeliberation.class);
    }

    @Override
    public List<Historiquedeliberation> findByNote(Integer idNote) {
        
        List<Historiquedeliberation> m = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT h FROM Historiquedeliberation h WHERE h.note = ?1");
            q.setParameter(1, idNote);
            m = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return m;
    
    }

    @Override
    public Historiquedeliberation findByIdHistDelibetation(Integer id) {
        
        Historiquedeliberation hd = null;
        Query q;
        try {
            q = em.createQuery("SELECT h FROM Historiquedeliberation h WHERE h.id =?1");
            q.setParameter(1, id);
            hd=(Historiquedeliberation) q.getSingleResult();
        } catch (Exception e) {
            return null;    
        }
        return hd;
    }
}
