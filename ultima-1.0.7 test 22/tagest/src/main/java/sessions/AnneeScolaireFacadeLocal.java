/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.AnneeScolaire;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface AnneeScolaireFacadeLocal {

    void create(AnneeScolaire anneeScolaire);

    void edit(AnneeScolaire anneeScolaire);

    void remove(AnneeScolaire anneeScolaire);

    AnneeScolaire find(Object id);

    List<AnneeScolaire> findAll();

    List<AnneeScolaire> findRange(int[] range);

    int count();

    AnneeScolaire findByAnnee (int annee ) ;
     public AnneeScolaire   findByAnneId(Integer id) ;
    
}
