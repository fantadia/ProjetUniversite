/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Uniteenseignements;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class UniteenseignementsFacade extends AbstractFacade<Uniteenseignements> implements UniteenseignementsFacadeLocal {

    private static final Logger LOG = Logger.getLogger(UniteenseignementsFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UniteenseignementsFacade() {
        super(Uniteenseignements.class);
    }

    @Override
    public List<Uniteenseignements> findByIdFilieres(Integer idfiliere) {
        List<Uniteenseignements> unitE = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT u FROM Uniteenseignements u WHERE u.filiere.id = ?1");
            q.setParameter(1, idfiliere);
            unitE = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return unitE;
    }

    @Override
    public Uniteenseignements findByNomUniteenseignements(String nom) {
        Uniteenseignements u;
        Query q;
        try {
            q = em.createQuery("SELECT u FROM Uniteenseignements u WHERE u.nom = ?1");
            q.setParameter(1, nom);
            u = (Uniteenseignements) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return u;
    }

}
