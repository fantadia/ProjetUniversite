/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Comptabilite;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class ComptabiliteFacade extends AbstractFacade<Comptabilite> implements ComptabiliteFacadeLocal {

    private static final Logger LOG = Logger.getLogger(ComptabiliteFacade.class.getName());

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ComptabiliteFacade() {
        super(Comptabilite.class);
    }

    @Override
    public List<Comptabilite> findByMois(Integer id) {
        List<Comptabilite> c;
        Query q;
        try {
            q = em.createQuery("SELECT c FROM Comptabilite c WHERE c.id=?1");
            q.setParameter(1, id);
            c = q.getResultList();

        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
            return null;
        }
        return c;
    }

}
