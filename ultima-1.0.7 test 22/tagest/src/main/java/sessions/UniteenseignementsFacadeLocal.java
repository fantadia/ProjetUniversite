/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Filieres;
import entities.Uniteenseignements;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface UniteenseignementsFacadeLocal {

    void create(Uniteenseignements uniteenseignements);

    void edit(Uniteenseignements uniteenseignements);

    void remove(Uniteenseignements uniteenseignements);

    Uniteenseignements find(Object id);

    List<Uniteenseignements> findAll();

    List<Uniteenseignements> findRange(int[] range);

    int count();

    public List<Uniteenseignements> findByIdFilieres(Integer idfiliere);

    public Uniteenseignements findByNomUniteenseignements(String nom);
    
}
