/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Notes;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class NotesFacade extends AbstractFacade<Notes> implements NotesFacadeLocal {

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;
    private static final Logger LOG = Logger.getLogger(NotesFacade.class.getName());

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NotesFacade() {
        super(Notes.class);
    }

    @Override
    public List<Notes> findByIdMatiersmodule(Integer idMatiere) {
        List<Notes> notes = new ArrayList<>();
        Query q;
        try {
            q = em.createQuery("SELECT n FROM Notes n WHERE n.matiere.id = ?1");
            q.setParameter(1, idMatiere);
            notes = q.getResultList();
        } catch (Exception e) {
            LOG.log(Level.WARNING, null, e);
        }
        return notes;
    }

    @Override
    public Notes findByIdNote(Integer idNote) {
        Notes note;
        Query q;
        try {
            q = em.createQuery("SELECT n FROM Notes n WHERE n.id = ?1");
            q.setParameter(1, idNote);
            note = (Notes) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return note;
    }

}
