/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.AnneeScolaire;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Fanta Diakité
 */
@Stateless
public class AnneeScolaireFacade extends AbstractFacade<AnneeScolaire> implements AnneeScolaireFacadeLocal {

    @PersistenceContext(unitName = "org.primefaces_ultima_war_1.0.7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AnneeScolaireFacade() {
        super(AnneeScolaire.class);
    }

    @Override
    public AnneeScolaire findByAnnee(int annee) {
        AnneeScolaire annee1;
        try {
            Query q = em.createQuery("SELECT a FROM AnneeScolaire a WHERE a.annee = ?1");
            q.setParameter(1, annee);
            annee1 = (AnneeScolaire) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return annee1;
    }

    @Override
    public AnneeScolaire findByAnneId(Integer id) {
        AnneeScolaire annee1;
        try {
            Query q = em.createQuery("SELECT a FROM AnneeScolaire a WHERE a.id = ?1");
            q.setParameter(1, id);
            annee1 = (AnneeScolaire) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return annee1;
    }
}
