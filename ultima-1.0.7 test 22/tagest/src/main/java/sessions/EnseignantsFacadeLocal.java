/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Enseignants;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface EnseignantsFacadeLocal {

    void create(Enseignants enseignants);

    void edit(Enseignants enseignants);

    void remove(Enseignants enseignants);

    Enseignants find(Object id);

    List<Enseignants> findAll();

    List<Enseignants> findRange(int[] range);
    
    Enseignants findByMatricule(String matricule);
    
    Enseignants findByIdEns(int id);

    int count();
    
}
