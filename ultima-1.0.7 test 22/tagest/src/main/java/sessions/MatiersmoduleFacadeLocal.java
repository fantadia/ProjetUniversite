/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Matiersmodule;
import entities.Uniteenseignements;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface MatiersmoduleFacadeLocal {

    void create(Matiersmodule matiersmodule);

    void edit(Matiersmodule matiersmodule);

    void remove(Matiersmodule matiersmodule);

    Matiersmodule find(Object id);

    List<Matiersmodule> findAll();

    List<Matiersmodule> findRange(int[] range);

    int count();
    
    public List<Matiersmodule> findByIdUniteenseignements(Integer iduniteEns);

    public Matiersmodule findByMatiersmodule(String nommatiere);
    public int NbreMatiereUE(Integer idUE);
    
}
