/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Comptabilite;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Fanta Diakité
 */
@Local
public interface ComptabiliteFacadeLocal {

    void create(Comptabilite comptabilite);

    void edit(Comptabilite comptabilite);

    void remove(Comptabilite comptabilite);

    Comptabilite find(Object id);

    List<Comptabilite> findAll();

    List<Comptabilite> findRange(int[] range);

    int count();

    public List<Comptabilite> findByMois(Integer id);
    
}
