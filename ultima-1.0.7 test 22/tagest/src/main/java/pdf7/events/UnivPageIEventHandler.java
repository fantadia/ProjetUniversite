/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf7.events;

import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.renderer.IRenderer;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import pdf7.utils.PdfPageUtil;

/**
 *
 * @author Ulrich Tiayo 
 */ 
public class UnivPageIEventHandler implements IEventHandler, Serializable {

//    public static final String FILIGRANE = FacesUtils.PATH + "/resources/images/filigrane.png";
    private PageSize size = PageSize.A4.rotate();
    private final IRenderer renderer;
    private final Calendar generationDate;

    public UnivPageIEventHandler(IRenderer renderer) {
        this.renderer = renderer;
        this.generationDate = Calendar.getInstance();
    }

    public void setSize(PageSize size) {
        this.size = size;
    }

    @Override
    public void handleEvent(Event event) {
        try {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            PdfDocument pdfDoc = docEvent.getDocument();
            PdfPage page = docEvent.getPage();
            int pageNumber = pdfDoc.getPageNumber(page);
            int numberOfPages = pdfDoc.getNumberOfPages();
            Document doc = new Document(pdfDoc);
            doc.add(PdfPageUtil.getHeaderTable(size, generationDate));
            doc.add(PdfPageUtil.getFooterTable(renderer, numberOfPages, pageNumber, size));

//            PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
//            addFlysoftLogo(pdfCanvas);
        } catch (IOException ex) {
            Logger.getLogger(UnivPageIEventHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Ajoute par défaut le logo de Flysoft au centre du document
     *
     * @param pdfCanvas
     * @throws MalformedURLException
     */
    private void addFlysoftLogo(PdfCanvas pdfCanvas) throws MalformedURLException {
//        ImageData img = ImageDataFactory.create(FILIGRANE);
//        double coef = (size.getWidth() - 50d) / img.getWidth();
//        float height = (float) (img.getHeight() * coef);
//        pdfCanvas.addImage(img, 25, (size.getHeight() - height) / 2, size.getWidth() - 50, false);
    }

}
