/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf7.renderer;

import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.renderer.DrawContext;
import com.itextpdf.layout.renderer.TableRenderer;

/**
 *
 * @author Ulrich TIAYO
 */
public class BackgroundLogoTableRenderer extends TableRenderer {

    private final Image image;
    private Float padding = 0f;

    public BackgroundLogoTableRenderer(Table modelElement, Image image) {
        super(modelElement);
        this.image = image;
    }

    public BackgroundLogoTableRenderer(Table modelElement, Image image, Float padding) {
        super(modelElement);
        this.image = image;
        this.padding = padding;
    }

    @Override
    public void draw(DrawContext drawContext) {
        float imgWidthToPrint = getOccupiedAreaBBox().getWidth() - 2 * padding;
        float reduceCoef = imgWidthToPrint / image.getImageWidth();
        float imgHeightToPrint = image.getImageHeight() * reduceCoef;
//        image.scaleToFit(getOccupiedAreaBBox().getWidth() - 2 * padding, getOccupiedAreaBBox().getHeight() - 2 * padding);
        drawContext.getCanvas().addXObject(image.getXObject(),
                getOccupiedAreaBBox().getX() + padding,
                getOccupiedAreaBBox().getY() + (getOccupiedAreaBBox().getHeight() - imgHeightToPrint) / 2,
                imgWidthToPrint);
//        drawContext.getCanvas().addXObject(image.getXObject(),
//                (getOccupiedAreaBBox().getWidth() - image.getImageWidth()) / 2 + getOccupiedAreaBBox().getX(),
//                (getOccupiedAreaBBox().getHeight() - image.getImageHeight()) / 2 + getOccupiedAreaBBox().getY());
        super.draw(drawContext);
    }
}
