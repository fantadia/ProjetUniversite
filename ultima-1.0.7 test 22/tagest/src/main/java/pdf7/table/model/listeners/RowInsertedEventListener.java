/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf7.table.model.listeners;

/**
 *
 * @author Ulrich TIAYO
 */
public interface RowInsertedEventListener {

    /**
     * Opération à exécuter à chaque ajout de ligne dans le tableau
     */
    public void rowInserted();

}
