package pdf7.table.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.itextpdf.layout.property.TextAlignment;
import entities.Etudiants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Ulrich TIAYO <ulrich.tiayo@lmtgroup.com>
 */
public class EtudiantPdfTableModel extends AbstractPdfTableModel {

    private float widths[] = {10f, 20f, 20f, 10f, 10f, 20f, 20f};
    private final TextAlignment columnTextAlignment[] = {TextAlignment.CENTER, TextAlignment.LEFT, TextAlignment.LEFT, TextAlignment.CENTER, TextAlignment.LEFT, TextAlignment.LEFT, TextAlignment.LEFT};
    private final String titles[] = {"N°", "Nom", "Prénom", "Sexe", "Age", "Nationalite", "Classe"};
    private final List<Etudiants> etudiants;

    public EtudiantPdfTableModel() {
        super();
        this.etudiants = new ArrayList<>();
    }

    public EtudiantPdfTableModel(List<Etudiants> etudiants) {
        super();
        this.etudiants = etudiants;
    }

    public void addNewRow(Etudiants etudiant) {
        etudiants.add(etudiant);
        notifyRowInsertedListeners();
    }

    @Override
    public int getRowCount() {
        return etudiants.size();
    }

    @Override
    public int getColumnCount() {
        return titles.length;
    }

    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return (rowIndex + 1) + "";
            case 1:
                return etudiants.get(rowIndex).getNom();
            case 2:
                return etudiants.get(rowIndex).getPremon();
            case 3:
                return etudiants.get(rowIndex).getSexe();
            case 4:
                return String.valueOf(Calendar.getInstance().get(Calendar.YEAR) - (1900 + etudiants.get(rowIndex).getDatenais().getYear()));
            case 5:
                return etudiants.get(rowIndex).getNationalite();
            case 6:
                return etudiants.get(rowIndex).getClasse().getDenomination();
        }
        return null;
    }

    @Override
    public TextAlignment getColumnTextAlignment(int columnIndex) {
        return columnTextAlignment[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return titles[column];
    }

    @Override
    public float getColumnWidth(int columnIndex) {
        return widths[columnIndex];
    }

    @Override
    public void setColumnWidth(int columnIndex, float width) {
        widths[columnIndex] = width;
        notifyStrucutreChangedListeners();
    }

    @Override
    public void setColumnWidths(float[] widths) {
        this.widths = widths;
    }

    @Override
    public float[] getColumnWidths() {
        return widths;
    }

}
