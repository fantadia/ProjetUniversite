/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf7.table.model;

import com.itextpdf.layout.property.TextAlignment;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pdf7.table.model.listeners.PdfTableEvents;
import pdf7.table.model.listeners.RowInsertedEventListener;
import pdf7.table.model.listeners.StructureChangedEventListener;

/**
 *
 * @author Ulrich TIAYO
 */
public abstract class AbstractPdfTableModel implements PdfTableModel, Serializable, PdfTableEvents {

    private final List<RowInsertedEventListener> rowInsertedEventListeners = new ArrayList<>();
    private final List<StructureChangedEventListener> structureChangedEventListeners = new ArrayList<>();

    /**
     * From JDK. Returns a default name for the column using spreadsheet
     * conventions: A, B, C, ... Z, AA, AB, etc. If <code>column</code> cannot
     * be found, returns an empty string.
     *
     * @param column the column being queried
     * @return a string containing the default name of <code>column</code>
     */
    @Override
    public String getColumnName(int column) {
        String result = "";
        for (; column >= 0; column = column / 26 - 1) {
            result = (char) ((char) (column % 26) + 'A') + result;
        }
        return result;
    }

    @Override
    public void addRowInsertedListener(RowInsertedEventListener rowInsertedEventListener) {
        rowInsertedEventListeners.add(rowInsertedEventListener);
    }

    @Override
    public void notifyRowInsertedListeners() {
        rowInsertedEventListeners.forEach((listener) -> {
            listener.rowInserted();
        });
    }

    @Override
    public void addStrucutreChangedListener(StructureChangedEventListener structureChangedEventListener) {
        structureChangedEventListeners.add(structureChangedEventListener);
    }

    @Override
    public void notifyStrucutreChangedListeners() {
        structureChangedEventListeners.forEach((listener) -> {
            listener.structureChanged();
        });
    }

    @Override
    public TextAlignment getColumnTextAlignment(int columnIndex) {
        return TextAlignment.LEFT;
    }

}
