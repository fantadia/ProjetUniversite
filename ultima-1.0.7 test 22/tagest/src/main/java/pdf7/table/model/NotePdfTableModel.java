package pdf7.table.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.itextpdf.layout.property.TextAlignment;
import entities.Notes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ulrich TIAYO <ulrich.tiayo@lmtgroup.com>
 */
public class NotePdfTableModel extends AbstractPdfTableModel {

    private float widths[] = {10f, 30f, 20f, 20f};
    private final TextAlignment columnTextAlignment[] = {TextAlignment.CENTER, TextAlignment.LEFT, TextAlignment.LEFT, TextAlignment.CENTER};
    private final String titles[] = {"N°", "Nom", "Matière", "Note"};
    private final List<Notes> notes;

    public NotePdfTableModel() {
        super();
        this.notes = new ArrayList<>();
    }

    public NotePdfTableModel(List<Notes> notes) {
        super();
        this.notes = notes;
    }

    public void addNewRow(Notes note) {
        notes.add(note);
        notifyRowInsertedListeners();
    }

    @Override
    public int getRowCount() {
        return notes.size();
    }

    @Override
    public int getColumnCount() {
        return titles.length;
    }

    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return (rowIndex + 1) + "";
            case 1:
                return notes.get(rowIndex).getEtudiant().getNom() + " " + notes.get(rowIndex).getEtudiant().getPremon();
            case 2:
                return notes.get(rowIndex).getMatiere().getNommatiere();
            case 3:
                return notes.get(rowIndex).getNote() + "";
        }
        return null;
    }

    @Override
    public TextAlignment getColumnTextAlignment(int columnIndex) {
        return columnTextAlignment[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return titles[column];
    }

    @Override
    public float getColumnWidth(int columnIndex) {
        return widths[columnIndex];
    }

    @Override
    public void setColumnWidth(int columnIndex, float width) {
        widths[columnIndex] = width;
        notifyStrucutreChangedListeners();
    }

    @Override
    public void setColumnWidths(float[] widths) {
        this.widths = widths;
    }

    @Override
    public float[] getColumnWidths() {
        return widths;
    }

}
