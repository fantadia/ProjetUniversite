/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf7.table;

import com.itextpdf.layout.Style;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import pdf7.table.model.AbstractPdfTableModel;
import pdf7.table.model.PdfTableModel;
import pdf7.table.model.listeners.RowInsertedEventListener;
import pdf7.table.model.listeners.StructureChangedEventListener;

/**
 *
 * @author Ulrich TIAYO <ulrich.tiayo@lmtgroup.com>
 */
public class UnivTable extends Table implements RowInsertedEventListener, StructureChangedEventListener {

    private final AbstractPdfTableModel tableModel;
    private Style styleEntete;

    public UnivTable(AbstractPdfTableModel tableModel) {
        super(tableModel.getColumnWidths());
        this.tableModel = tableModel;
        initComponent();
        addListeners();
    }

    public UnivTable(int numColumns, AbstractPdfTableModel tableModel) {
        super(numColumns);
        this.tableModel = tableModel;
        initComponent();
        addListeners();
    }

    public UnivTable(boolean largeTable, AbstractPdfTableModel tableModel) {
        super(tableModel.getColumnWidths(), largeTable);
        this.tableModel = tableModel;
        initComponent();
        addListeners();
    }

    public UnivTable(int numColumns, boolean largeTable, AbstractPdfTableModel tableModel) {
        super(numColumns, largeTable);
        this.tableModel = tableModel;
        initComponent();
        addListeners();
    }

    public PdfTableModel getTableModel() {
        return tableModel;
    }

    private void addListeners() {
        tableModel.addRowInsertedListener(this);
        tableModel.addStrucutreChangedListener(this);
    }

    private void initComponent() {
        styleEntete = new Style().setBold().setFontSize(13);

        //Entêtes
        for (int i = 0; i < tableModel.getColumnCount(); i++) {
            addCell(new Paragraph(tableModel.getColumnName(i)).addStyle(styleEntete)).setTextAlignment(TextAlignment.CENTER);
        }

        //Contenu
        Cell cell;
        for (int rowIndex = 0; rowIndex < tableModel.getRowCount(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
                cell = new Cell().add(new Paragraph(tableModel.getValueAt(rowIndex, columnIndex)));
                cell.setTextAlignment(tableModel.getColumnTextAlignment(columnIndex));
                addCell(cell);
            }
        }
    }

    @Override
    public void rowInserted() {
        Cell cell;
        int rowIndex = tableModel.getRowCount() - 1;
        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
            cell = new Cell().add(new Paragraph(tableModel.getValueAt(rowIndex, columnIndex)));
            cell.setTextAlignment(tableModel.getColumnTextAlignment(columnIndex));
            addCell(cell);
        }
    }

    @Override
    public void structureChanged() {
    }

}
