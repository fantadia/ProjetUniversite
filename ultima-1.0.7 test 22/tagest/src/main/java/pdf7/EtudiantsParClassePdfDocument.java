/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf7;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import entities.Etudiants;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import pdf7.table.UnivTable;
import pdf7.table.model.EtudiantPdfTableModel;
import utils.FacesUtils;

/**
 *
 * @author Ulrich TIAYO <ulrich.tiayo@lmtgroup.com>
 */
public class EtudiantsParClassePdfDocument implements Serializable {

    private String pdfFilePath;
    private String nomDuFichier;
    private final List<Etudiants> etudiants;

    public EtudiantsParClassePdfDocument(List<Etudiants> etudiants) {
        this.etudiants = etudiants;
    }
    
    public String print() throws FileNotFoundException {
        pdfFilePath = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Double rand = Math.random();
        nomDuFichier = "Etudiants" + "_" + sdf.format(new Date()) + "_" + (int) (rand * 10000) + ".pdf";
        pdfFilePath = FacesUtils.PATH + "/pdf/" + nomDuFichier;
        
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(pdfFilePath));
        try (UnivDocument univDoc = new UnivDocument(pdfDoc, PageSize.A4, "Impression de notes")) {
            EtudiantPdfTableModel model = new EtudiantPdfTableModel(etudiants);
            UnivTable table = new UnivTable(model);
            table.setWidthPercent(100);
            univDoc.add(table);
        }
        return nomDuFichier;
    }
    
}
