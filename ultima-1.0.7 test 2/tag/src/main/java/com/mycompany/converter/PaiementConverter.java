package com.mycompany.converter;

import com.mycompany.entities.Paiement;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "paiementConverter")
public class PaiementConverter implements Converter {
	  private static Map<String,Paiement> cache = new HashMap<String, Paiement>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valpaiement) {
        if (valpaiement == null || valpaiement.isEmpty()) {
            return null;
        }
        return cache.get(valpaiement.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valpaiement) {
        if (valpaiement == null) {
            return "";
        }
        String id = null;
        if (valpaiement instanceof Paiement) {
            Paiement model = (Paiement) valpaiement;
            id = String.valueOf(model.getCodepaiement());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
