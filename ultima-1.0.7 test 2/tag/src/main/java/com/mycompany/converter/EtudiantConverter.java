package com.mycompany.converter;

import com.mycompany.entities.Etudiant;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "etudiantConverter")
public class EtudiantConverter implements Converter {
	  private static Map<String,Etudiant> cache = new HashMap<String, Etudiant>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valetudiant) {
        if (valetudiant == null || valetudiant.isEmpty()) {
            return null;
        }
        return cache.get(valetudiant.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valetudiant) {
        if (valetudiant == null) {
            return "";
        }
        String id = null;
        if (valetudiant instanceof Etudiant) {
            Etudiant model = (Etudiant) valetudiant;
            id = String.valueOf(model.getCodeutilisateur());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
