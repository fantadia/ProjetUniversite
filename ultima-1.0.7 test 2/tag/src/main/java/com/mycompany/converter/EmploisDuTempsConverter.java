package com.mycompany.converter;

import com.mycompany.entities.EmploisDuTemps;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "emploisDuTempsConverter")
public class EmploisDuTempsConverter implements Converter {
	  private static Map<String,EmploisDuTemps> cache = new HashMap<String, EmploisDuTemps>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valemploisDuTemps) {
        if (valemploisDuTemps == null || valemploisDuTemps.isEmpty()) {
            return null;
        }
        return cache.get(valemploisDuTemps.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valemploisDuTemps) {
        if (valemploisDuTemps == null) {
            return "";
        }
        String id = null;
        if (valemploisDuTemps instanceof EmploisDuTemps) {
            EmploisDuTemps model = (EmploisDuTemps) valemploisDuTemps;
            id = String.valueOf(model.getCodeEmploisdutemps());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
