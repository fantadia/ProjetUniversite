package com.mycompany.converter;

import com.mycompany.entities.Niveau;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "niveauConverter")
public class NiveauConverter implements Converter {
	  private static Map<String,Niveau> cache = new HashMap<String, Niveau>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valniveau) {
        if (valniveau == null || valniveau.isEmpty()) {
            return null;
        }
        return cache.get(valniveau.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valniveau) {
        if (valniveau == null) {
            return "";
        }
        String id = null;
        if (valniveau instanceof Niveau) {
            Niveau model = (Niveau) valniveau;
            id = String.valueOf(model.getIdniveau());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
