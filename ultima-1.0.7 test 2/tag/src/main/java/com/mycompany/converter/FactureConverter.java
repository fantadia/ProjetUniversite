package com.mycompany.converter;

import com.mycompany.entities.Facture;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "factureConverter")
public class FactureConverter implements Converter {
	  private static Map<String,Facture> cache = new HashMap<String, Facture>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valfacture) {
        if (valfacture == null || valfacture.isEmpty()) {
            return null;
        }
        return cache.get(valfacture.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valfacture) {
        if (valfacture == null) {
            return "";
        }
        String id = null;
        if (valfacture instanceof Facture) {
            Facture model = (Facture) valfacture;
            id = String.valueOf(model.getCodefacture());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
