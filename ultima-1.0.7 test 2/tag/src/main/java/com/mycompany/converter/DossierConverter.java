package com.mycompany.converter;

import com.mycompany.entities.Dossier;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "dossierConverter")
public class DossierConverter implements Converter {
	  private static Map<String,Dossier> cache = new HashMap<String, Dossier>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valdossier) {
        if (valdossier == null || valdossier.isEmpty()) {
            return null;
        }
        return cache.get(valdossier.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valdossier) {
        if (valdossier == null) {
            return "";
        }
        String id = null;
        if (valdossier instanceof Dossier) {
            Dossier model = (Dossier) valdossier;
            id = String.valueOf(model.getCodedossier());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
