package com.mycompany.converter;

import com.mycompany.entities.Comptable;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "comptableConverter")
public class ComptableConverter implements Converter {
	  private static Map<String,Comptable> cache = new HashMap<String, Comptable>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valcomptable) {
        if (valcomptable == null || valcomptable.isEmpty()) {
            return null;
        }
        return cache.get(valcomptable.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valcomptable) {
        if (valcomptable == null) {
            return "";
        }
        String id = null;
        if (valcomptable instanceof Comptable) {
            Comptable model = (Comptable) valcomptable;
            id = String.valueOf(model.getCodeutilisateur());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
