package com.mycompany.converter;

import com.mycompany.entities.Semestre;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "semestreConverter")
public class SemestreConverter implements Converter {
	  private static Map<String,Semestre> cache = new HashMap<String, Semestre>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        return cache.get(value.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        String id = null;
        if (value instanceof Semestre) {
            Semestre model = (Semestre) value;
            id = String.valueOf(model.getCodesemestre());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
