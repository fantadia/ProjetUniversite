package com.mycompany.converter;

import com.mycompany.entities.Enseignant;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "enseignantConverter")
public class EnseignantConverter implements Converter {
	  private static Map<String,Enseignant> cache = new HashMap<String, Enseignant>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valenseignant) {
        if (valenseignant == null || valenseignant.isEmpty()) {
            return null;
        }
        return cache.get(valenseignant.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valenseignant) {
        if (valenseignant == null) {
            return "";
        }
        String id = null;
        if (valenseignant instanceof Enseignant) {
            Enseignant model = (Enseignant) valenseignant;
            id = String.valueOf(model.getCodeutilisateur());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
