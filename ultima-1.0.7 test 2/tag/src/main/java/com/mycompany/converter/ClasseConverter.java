package com.mycompany.converter;

import com.mycompany.entities.Classe;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "classeConverter")
public class ClasseConverter implements Converter 
{
	  private static Map<String,Classe> cache = new HashMap<String, Classe>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valclasse) {
        if (valclasse == null || valclasse.isEmpty()) {
            return null;
        }
        return cache.get(valclasse.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valclasse) {
        if (valclasse == null) {
            return "";
        }
        String id = null;
        if (valclasse instanceof Classe) {
            Classe model = (Classe) valclasse;
            id = String.valueOf(model.getCodeclasse());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
