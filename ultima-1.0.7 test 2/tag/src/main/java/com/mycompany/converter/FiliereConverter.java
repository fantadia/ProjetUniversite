package com.mycompany.converter;

import com.mycompany.entities.Filiere;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "filiereConverter")
public class FiliereConverter implements Converter {
	  private static Map<String,Filiere> cache = new HashMap<String, Filiere>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valfiliere) {
        if (valfiliere == null || valfiliere.isEmpty()) {
            return null;
        }
        return cache.get(valfiliere.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valfiliere) {
        if (valfiliere == null) {
            return "";
        }
        String id = null;
        if (valfiliere instanceof Filiere) {
            Filiere model = (Filiere) valfiliere;
            id = String.valueOf(model.getCodefiliere());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
