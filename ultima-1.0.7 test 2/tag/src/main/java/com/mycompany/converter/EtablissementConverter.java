package com.mycompany.converter;

import com.mycompany.entities.Etablissement;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "etablissementConverter")
public class EtablissementConverter implements Converter {
	  private static Map<String,Etablissement> cache = new HashMap<String, Etablissement>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valetablissement) {
        if (valetablissement == null || valetablissement.isEmpty()) {
            return null;
        }
        return cache.get(valetablissement.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valetablissement) {
        if (valetablissement == null) {
            return "";
        }
        String id = null;
        if (valetablissement instanceof Etablissement) {
            Etablissement model = (Etablissement) valetablissement;
            id = String.valueOf(model.getCodeetablissement());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
