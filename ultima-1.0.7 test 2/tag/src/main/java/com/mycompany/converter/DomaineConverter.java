package com.mycompany.converter;

import com.mycompany.entities.Domaine;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "domaineConverter")
public class DomaineConverter implements Converter {
	  private static Map<String,Domaine> cache = new HashMap<String, Domaine>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valdomaine) {
        if (valdomaine == null || valdomaine.isEmpty()) {
            return null;
        }
        return cache.get(valdomaine.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valdomaine) {
        if (valdomaine == null) {
            return "";
        }
        String id = null;
        if (valdomaine instanceof Domaine) {
            Domaine model = (Domaine) valdomaine;
            id = String.valueOf(model.getCodedomaine());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
