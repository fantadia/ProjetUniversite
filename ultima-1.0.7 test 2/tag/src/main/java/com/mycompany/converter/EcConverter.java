package com.mycompany.converter;

import com.mycompany.entities.Ec;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "ecConverter")
public class EcConverter implements Converter {
	  private static Map<String,Ec> cache = new HashMap<String, Ec>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valec) {
        if (valec == null || valec.isEmpty()) {
            return null;
        }
        return cache.get(valec.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valec) {
        if (valec == null) {
            return "";
        }
        String id = null;
        if (valec instanceof Ec) {
            Ec model = (Ec) valec;
            id = String.valueOf(model.getCodematiere());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
