package com.mycompany.converter;

import com.mycompany.entities.Cours;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "coursConverter")
public class CoursConverter implements Converter {
	  private static Map<String,Cours> cache = new HashMap<String, Cours>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valcours) {
        if (valcours == null || valcours.isEmpty()) {
            return null;
        }
        return cache.get(valcours.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valcours) {
        if (valcours == null) {
            return "";
        }
        String id = null;
        if (valcours instanceof Cours) {
            Cours model = (Cours) valcours;
            id = String.valueOf(model.getCodecours());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
