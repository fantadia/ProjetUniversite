package com.mycompany.converter;

import com.mycompany.entities.EnseignantResponsable;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "enseignantResponsableConverter")
public class EnseignantResponsableConverter implements Converter {
	  private static Map<String,EnseignantResponsable> cache = new HashMap<String, EnseignantResponsable>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valenseignantResponsable) {
        if (valenseignantResponsable == null || valenseignantResponsable.isEmpty()) {
            return null;
        }
        return cache.get(valenseignantResponsable.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valenseignantResponsable) {
        if (valenseignantResponsable == null) {
            return "";
        }
        String id = null;
        if (valenseignantResponsable instanceof EnseignantResponsable) {
            EnseignantResponsable model = (EnseignantResponsable) valenseignantResponsable;
            id = String.valueOf(model.getCodeutilisateur());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
