/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.converter;

import com.mycompany.entities.Absence;
import com.mycompany.entities.AbsenceId;
import java.util.HashMap;
import java.util.Map;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author macbookpro
 */
@FacesConverter(value = "absenceIdCourConverter")
public class AbsenceIdCourConverter implements Converter{

     private static Map<String,AbsenceId> cache = new HashMap<String, AbsenceId>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valabsence) {
        if (valabsence == null || valabsence.isEmpty()) {
            return null;
        }
        return cache.get(valabsence.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valabsence) {
        if (valabsence == null) {
            return "";
        }
        String id = null;
        if (valabsence instanceof AbsenceId) {
            AbsenceId model = (AbsenceId) valabsence;
            id = String.valueOf(model.getCodecours());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
    
}
