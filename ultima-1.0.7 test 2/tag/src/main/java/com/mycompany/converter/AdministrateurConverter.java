package com.mycompany.converter;

import com.mycompany.entities.Administrateur;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "administrateurConverter")
public class AdministrateurConverter implements Converter {
	  private static Map<String,Administrateur> cache = new HashMap<String, Administrateur>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valadministrateur) {
        if (valadministrateur == null || valadministrateur.isEmpty()) {
            return null;
        }
        return cache.get(valadministrateur.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valadministrateur) {
        if (valadministrateur == null) {
            return "";
        }
        String id = null;
        if (valadministrateur instanceof Administrateur) {
            Administrateur model = (Administrateur) valadministrateur;
            id = String.valueOf(model.getCodeutilisateur());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
