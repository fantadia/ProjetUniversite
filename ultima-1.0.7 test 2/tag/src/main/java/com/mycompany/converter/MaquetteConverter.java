package com.mycompany.converter;

import com.mycompany.entities.Maquette;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "maquetteConverter")
public class MaquetteConverter implements Converter {
	  private static Map<String,Maquette> cache = new HashMap<String, Maquette>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valmaquette) {
        if (valmaquette == null || valmaquette.isEmpty()) {
            return null;
        }
        return cache.get(valmaquette.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valmaquette) {
        if (valmaquette == null) {
            return "";
        }
        String id = null;
        if (valmaquette instanceof Maquette) {
            Maquette model = (Maquette) valmaquette;
            id = String.valueOf(model.getCodemaquette());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
