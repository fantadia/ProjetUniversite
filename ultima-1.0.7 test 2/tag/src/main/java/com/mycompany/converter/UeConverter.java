package com.mycompany.converter;

import com.mycompany.entities.Ue;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "ueConverter")
public class UeConverter implements Converter {
	  private static Map<String,Ue> cache = new HashMap<String, Ue>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        return cache.get(value.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        String id = null;
        if (value instanceof Ue) {
            Ue model = (Ue) value;
            id = String.valueOf(model.getCodeue());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
