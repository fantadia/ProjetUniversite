package com.mycompany.converter;

import com.mycompany.entities.Responsable;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "responsableConverter")
public class ResponsableConverter implements Converter {
	  private static Map<String,Responsable> cache = new HashMap<String, Responsable>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        return cache.get(value.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "";
        }
        String id = null;
        if (value instanceof Responsable) {
            Responsable model = (Responsable) value;
            id = String.valueOf(model.getCodeutilisateur());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
