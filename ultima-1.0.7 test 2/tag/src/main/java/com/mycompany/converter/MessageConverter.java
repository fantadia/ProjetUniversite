package com.mycompany.converter;

import com.mycompany.entities.Message;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "messageConverter")
public class MessageConverter implements Converter {
	  private static Map<String,Message> cache = new HashMap<String, Message>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valmessage) {
        if (valmessage == null || valmessage.isEmpty()) {
            return null;
        }
        return cache.get(valmessage.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valmessage) {
        if (valmessage == null) {
            return "";
        }
        String id = null;
        if (valmessage instanceof Message) {
            Message model = (Message) valmessage;
            id = String.valueOf(model.getCodemessage());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
