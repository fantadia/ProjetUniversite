package com.mycompany.converter;

import com.mycompany.entities.Utilisateur;

import java.util.HashMap;

import java.util.Map;


import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import javax.faces.convert.FacesConverter;

@FacesConverter(value = "utilisateurConverter")
public class UtilisateurConverter implements Converter {
	  private static Map<String,Utilisateur> cache = new HashMap<String, Utilisateur>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String valutilisateur) {
        if (valutilisateur == null || valutilisateur.isEmpty()) {
            return null;
        }
        return cache.get(valutilisateur.trim());
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object valutilisateur) {
        if (valutilisateur == null) {
            return "";
        }
        String id = null;
        if (valutilisateur instanceof Utilisateur) {
            Utilisateur model = (Utilisateur) valutilisateur;
            id = String.valueOf(model.getCodeutilisateur());
            if (id != null) {
                cache.put(id, model);
            }
        }
        return id;
    }
}
