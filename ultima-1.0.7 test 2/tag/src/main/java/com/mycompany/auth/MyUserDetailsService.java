package com.mycompany.auth;

import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.UtilisateurDao;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UtilisateurDao utilisateurDao;

    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {
        Utilisateur user = utilisateurDao.findByLogin(userName);
        if (user == null) {
            throw new UsernameNotFoundException("Aucun utilisateur avec pour Username " + userName);
        }
        UserDetails details = new User(userName, user.getPassword(), new ArrayList<>());
        return details;
    }

}
