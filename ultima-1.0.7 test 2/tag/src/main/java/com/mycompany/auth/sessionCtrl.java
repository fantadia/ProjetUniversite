/*
 * To Ulrich TIAYO NGNINGAHE change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.auth;

import com.mycompany.entities.Administrateur;
import com.mycompany.entities.Utilisateur;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
//import sessions.AdministrateurFacadeLocal;
import com.nycompany.dao.UtilisateurDao;

/**
 *
 * @author Fdiakite14@gmail.com
 */
@Named("sessionCtrl")
@SessionScoped
public class sessionCtrl implements Serializable {

    private static final Logger LOG = Logger.getLogger(sessionCtrl.class.getName());

    @EJB
    private UtilisateurDao utilisateurDao;

    private Utilisateur userAccount;
    
    private Administrateur userconnected;
    private String language = "fr";
    private String login;
    private String motDePasse;
    private boolean connecte = false;

    public sessionCtrl() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public void switch_fr() {
        language = "fr";
    }

    public void switch_en() {
        language = "en";
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Utilisateur getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(Utilisateur userAccount) {
        this.userAccount = userAccount;
    }

}
