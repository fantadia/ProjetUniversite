/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdf7.docs;

import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfNumber;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.renderer.IRenderer;
import com.itextpdf.layout.renderer.TextRenderer;
import com.mycompany.pdf7.events.FlysoftPageIEventHandler;
import com.mycompany.pdf7.utils.PageAngle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author <a href="mailto:tiayo.pro@gmail.com">Ulrich TIAYO NGNINGAHE</a>
 */
public class CustomDocument extends Document {

    private IRenderer pageNumberRenderer; 
    private PageOrientationsEventHandler pageOrientationsEventHandler;
    private FlysoftPageIEventHandler flysoftPageIEventHandler;
    private Style styleEntetes;
    private final String titre;
    public static PdfFont cnFont = null;

    public CustomDocument(PdfDocument pdfDoc, PageSize pageSize, String titre) {
        super(pdfDoc, pageSize, false);
        this.titre = titre;
        try {
            initComponent(pdfDoc);
        } catch (IOException ex) {
            Logger.getLogger(CustomDocument.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (pageSize == PageSize.A4) {
            flysoftPageIEventHandler.setSize(pdfDocument.getDefaultPageSize());
        } else if (pageSize == PageSize.A4.rotate()) {
            flysoftPageIEventHandler.setSize(pdfDocument.getDefaultPageSize().rotate());
        }
    }

    private void initComponent(PdfDocument pdfDoc) throws IOException {
        setTextAlignment(TextAlignment.JUSTIFIED);
        setFontSize(11);
        styleEntetes = new Style().setTextAlignment(TextAlignment.CENTER);

        Text totalPages = new Text("{totalPages}");
        pageNumberRenderer = new TextRenderer(totalPages);
        totalPages.setNextRenderer(pageNumberRenderer);
        pageOrientationsEventHandler = new PageOrientationsEventHandler();
        pdfDoc.addEventHandler(PdfDocumentEvent.START_PAGE, pageOrientationsEventHandler);
        flysoftPageIEventHandler = new FlysoftPageIEventHandler(pageNumberRenderer);
        pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, flysoftPageIEventHandler);
        getRenderer().addChild(pageNumberRenderer);

        Paragraph p = new Paragraph().addStyle(styleEntetes);

        Text t = new Text("Université\n").setFontSize(15);
        p.add(t);

        
        Paragraph logo = new Paragraph();
        Paragraph titreDoc = new Paragraph(titre).setTextAlignment(TextAlignment.CENTER);

        add(p);
        add(logo);
        add(titreDoc);
    }

    /**
     * Permet de spécifier dans chaque page créée le nombre total de pages
     * générées. permet de terminer le remplissage du pieds de page marquant
     * "page X/Y"
     *
     * @param numberOfPages
     */
    private void setNumberOfPages(int numberOfPages) {
        String total = pageNumberRenderer.toString().replace("{totalPages}",
                String.valueOf(numberOfPages));
        ((TextRenderer) pageNumberRenderer).setText(total);
        ((Text) pageNumberRenderer.getModelElement()).setNextRenderer(pageNumberRenderer);

        relayout();
    }

    public static class PageOrientationsEventHandler implements IEventHandler {

        private PageAngle orientation = PageAngle.PORTRAIT;

        public void setOrientation(PageAngle orientation) {
            this.orientation = orientation;
        }

        @Override
        public void handleEvent(Event event) {
            PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
            docEvent.getPage().put(PdfName.Rotate, new PdfNumber(orientation.getOrientationValue()));
        }
    }

    @Override
    public void close() {
        setNumberOfPages(pdfDocument.getNumberOfPages());
        flush();
        setFont(cnFont);
        pdfDocument.close();
        super.close(); //To change body of generated methods, choose Tools | Templates.
    }

}
