/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdf7.docs;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.mycompany.entities.Classe;
import com.mycompany.pdf7.table.CustomTable;
import com.mycompany.pdf7.table.model.ClassePdfTableModel;
import com.mycompany.utils.FacesUtils;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author <a href="mailto:tiayo.pro@gmail.com">Ulrich TIAYO NGNINGAHE</a>
 */
public class ClassesListDocument implements Serializable {

    private String pdfFilePath;
    private String nomDuFichier;
    private final List<Classe> classes;

    public ClassesListDocument(List<Classe> classes) {
        this.classes = classes;
    }

    public String print() throws FileNotFoundException {
        pdfFilePath = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Double rand = Math.random();
        nomDuFichier = "Stats" + "_" + sdf.format(new Date()) + "_" + (int) (rand * 10000) + ".pdf";
        pdfFilePath = FacesUtils.PATH + "/app/pdf/" + nomDuFichier;

        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(pdfFilePath));
        try (CustomDocument lmtDoc = new CustomDocument(pdfDoc, PageSize.A4, "Liste des classes disponibles")) {
            ClassePdfTableModel model = new ClassePdfTableModel(classes);
            CustomTable table = new CustomTable(model);
            table.setWidthPercent(100);
            lmtDoc.add(table);
        }
        return nomDuFichier;
    }

}
