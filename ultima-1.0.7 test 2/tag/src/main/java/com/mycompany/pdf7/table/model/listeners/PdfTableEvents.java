/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdf7.table.model.listeners;

/**
 *
 * @author Ulrich TIAYO
 */
public interface PdfTableEvents {
    
    public void addRowInsertedListener(RowInsertedEventListener rowInsertedEventListener);
    
    public void notifyRowInsertedListeners();
    
    public void addStrucutreChangedListener(StructureChangedEventListener structureChangedEventListener);
    
    public void notifyStrucutreChangedListeners();
    
}
