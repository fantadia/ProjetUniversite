/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdf7.utils;

/**
 *
 * @author <a href="mailto:tiayo.pro@gmail.com">Ulrich TIAYO NGNINGAHE</a>
 */
public enum PageAngle {

    PORTRAIT(0),
    LANDSCAPE(90);

    private int value = 0;

    public int getOrientationValue() {
        return value;
    }

    private PageAngle(int value) {
        this.value = value;
    }

}
