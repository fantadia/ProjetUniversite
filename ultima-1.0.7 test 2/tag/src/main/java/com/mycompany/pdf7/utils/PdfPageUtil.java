/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdf7.utils;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.renderer.IRenderer;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Calendar;

/**
 *
 * @author Ulrich
 */
public class PdfPageUtil {

    private final static String HEADER = "Fanta Team. Tous droits réservés";
    private final static String FOOTER = "Copyright \u00A9 2017 Fanta Team";

    private PdfPageUtil() throws IOException {
    }

    public static Table getHeaderTable(PageSize size, Calendar headDate) throws IOException {
        PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
        float[] widths = {30, 20};
        Table table = new Table(widths);
        table.setWidthPercent(100);
        table.setFixedPosition(25, size.getTop() - 14, size.getRight() - 50);
        table.setPaddingBottom(0);
        table.setBorderBottom(new SolidBorder(Color.BLACK, 0.6f));
        table.setHeight(10);

        Paragraph p = new Paragraph(HEADER);

        p.setFont(font).setFontSize(10).setFontColor(Color.GRAY).setItalic();
        Cell head = new Cell().add(p);
        head.setPadding(-3);
        head.setMarginLeft(5);
        head.setBorder(Border.NO_BORDER);
        table.addCell(head);

        Object[] obj = {headDate.getTimeInMillis()};
        p = new Paragraph(MessageFormat.format("A {0,time,medium}    Le  {0, date,long}.", obj));
        p.setFont(font).setFontSize(10).setItalic().setFontColor(Color.GRAY);
        head = new Cell().add(p);
        head.setPadding(-3);
        head.setMarginLeft(10);
        head.setTextAlignment(TextAlignment.RIGHT);
        head.setBorder(Border.NO_BORDER);
        table.addCell(head);
        return table;
    }

    public static Table getHeaderTable(PageSize size) throws IOException {
        return getHeaderTable(size, Calendar.getInstance());
    }
    
    public static Table getFooterTable(IRenderer renderer, int numberOfPages, int pageNumber, PageSize size) throws IOException {
        PdfFont font = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
        Table table = new Table(2);
        table.setWidthPercent(100);
        table.setFixedPosition(25, size.getBottom() + 10, size.getRight() - 50);
        table.setPaddingBottom(0);
        table.setHeight(15);
        table.setBorderTop(new SolidBorder(Color.BLACK, 0.6f));

        Paragraph p = new Paragraph(FOOTER);

        p.setFont(font).setFontSize(10).setFontColor(Color.GRAY).setItalic();
        Cell head = new Cell().add(p);
        head.setPadding(0);
        head.setMarginLeft(2);
        head.setBorder(Border.NO_BORDER);
        table.addCell(head);

        Text deb = new Text("Page : " + pageNumber + " sur ");
        Text totalPages = new Text("{totalPages}");

        p = new Paragraph();
        p.add(deb);
        p.add(totalPages);
        totalPages.setNextRenderer(renderer);
        head = new Cell().add(p);
        head.setPadding(0);
        head.setFont(font).setFontSize(10).setItalic().setFontColor(Color.GRAY);
        head.setMarginLeft(10);
        head.setBorder(Border.NO_BORDER);
        head.setTextAlignment(TextAlignment.RIGHT);
        head.setMarginRight(15);

        table.addCell(head);
        return table;
    }

}
