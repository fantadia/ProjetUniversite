/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdf7.table.model;

import com.itextpdf.layout.property.TextAlignment;

/**
 *
 * @author Ulrich TIAYO
 */
public interface PdfTableModel {

    /**
     * Fournis le nombre de lignes contenues dans le tableau
     *
     * @return
     */
    public int getRowCount();

    /**
     * Fournis le nombre de colones du tableau
     *
     * @return
     */
    public int getColumnCount();

    /**
     * Fournis le nom de la colone dans le tableau. Le nom de la colone sera
     * marqué comme entête dans la dite colone.
     *
     * @param columnIndex : Index de la colone
     * @return
     */
    public String getColumnName(int columnIndex);

    /**
     * Fournis la valeur du tableau, ligne i colone j
     *
     * @param rowIndex : Ligne du tableau
     * @param columnIndex : colone du tableau
     * @return
     */
    public String getValueAt(int rowIndex, int columnIndex);

    /**
     * Fournis la largeur de la colone spécifiée
     *
     * @param columnIndex : Index de la colone dont on souhaite avoir la largeur
     * @return
     */
    public float getColumnWidth(int columnIndex);

    /**
     * Spécifie la largeur de la colone dont l'index est fournis
     *
     * @param columnIndex : index de la colone
     * @param width
     */
    public void setColumnWidth(int columnIndex, float width);

    /**
     * Définis pour chaque colone l'alignement horizontal du texte dans la dite
     * colone.
     *
     * @param columnIndex
     * @return
     */
    public TextAlignment getColumnTextAlignment(int columnIndex);

    public void setColumnWidths(float[] widths);

    public float[] getColumnWidths();

}
