/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pdf7.table.model;

import com.itextpdf.layout.property.TextAlignment;
import com.mycompany.entities.Classe;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author <a href="mailto:tiayo.pro@gmail.com">Ulrich TIAYO NGNINGAHE</a>
 */
public class ClassePdfTableModel extends AbstractPdfTableModel {

    private float widths[] = {10f, 30f, 20f, 20f};
    private final TextAlignment columnTextAlignment[] = {TextAlignment.CENTER, TextAlignment.LEFT, TextAlignment.LEFT, TextAlignment.CENTER};
    private final String titles[] = {"N°", "Code", "Libellé", "Type"};
    private final List<Classe> classes;

    public ClassePdfTableModel() {
        super();
        this.classes = new ArrayList<>();
    }

    public ClassePdfTableModel(List<Classe> classes) {
        super();
        this.classes = classes;
    }

    public void addNewRow(Classe classe) {
        classes.add(classe);
        notifyRowInsertedListeners();
    }

    @Override
    public int getRowCount() {
        return classes.size();
    }

    @Override
    public int getColumnCount() {
        return titles.length;
    }

    @Override
    public String getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return (rowIndex + 1) + "";
            case 1:
                return classes.get(rowIndex).getCodeclasse();
            case 2:
                return classes.get(rowIndex).getLbelleClasse() == null ? "" : classes.get(rowIndex).getLbelleClasse();
            case 3:
                return classes.get(rowIndex).getType() == null ? "" : classes.get(rowIndex).getType();
        }
        return null;
    }

    @Override
    public TextAlignment getColumnTextAlignment(int columnIndex) {
        return columnTextAlignment[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return titles[column];
    }

    @Override
    public float getColumnWidth(int columnIndex) {
        return widths[columnIndex];
    }

    @Override
    public void setColumnWidth(int columnIndex, float width) {
        widths[columnIndex] = width;
        notifyStrucutreChangedListeners();
    }

    @Override
    public void setColumnWidths(float[] widths) {
        this.widths = widths;
    }

    @Override
    public float[] getColumnWidths() {
        return widths;
    }

}
