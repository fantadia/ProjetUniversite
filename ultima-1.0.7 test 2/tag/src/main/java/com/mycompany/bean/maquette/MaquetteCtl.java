/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.maquette;

import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Maquette;
import com.mycompany.entities.Semestre;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="maquetteCtl")
@ViewScoped

public class MaquetteCtl extends AbstractMaquetteCtl implements MaquetteInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public MaquetteCtl() {
    }
    
    @PostConstruct
    private void initMaquette()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        maquetteDao = (MaquetteDao) context.getBean("maquetteDao");
        semestreDao = (SemestreDao) context.getBean("semestreDao");
        
        classeDao = (ClasseDao) context.getBean("classeDao");
        selectedClasse = (Classe) context.getBean("classe");
        classes = classeDao.findAllClasse();
        
        maquette = (Maquette) context.getBean("maquette");
        selectMaquette = (Maquette) context.getBean("maquette");
        maquettes = maquetteDao.findAllMaquette();
    }

    @Override
    public void enregistrerMaquette() 
    {
        maquette.setCodemaquette("maq");
        maquetteDao.createMaquette(maquette);
        initMaquette();
        JsfUtil.addSuccessMessage("la maquette à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierMaquette() {
        
        if (selectMaquette == null || selectMaquette.getCodemaquette()== null) {
            return;
        }

       /*Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        maquetteDao.updateMaquette(selectMaquette);
        JsfUtil.addSuccessMessage("maquette Modifier avec succes");
    }

    @Override
    public void supprimerMaquette() {
        
         if(selectMaquette == null || selectMaquette.getCodemaquette()== null)
            return;
             
        List<Semestre> n = semestreDao.findSemestreByMaquette(selectMaquette.getCodemaquette());
       
        if (!n.isEmpty()) {
            JsfUtil.addErrorMessage("Impossible de supprimer cette maquette car il est deja utiliser");
         
       } else {

            maquetteDao.deleteMaquette(selectMaquette);
            JsfUtil.addErrorMessage("Cette maquette à été supprimée");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeMaquetteClasse() {
        if (selectedClasse == null) {
            maquettes = maquetteDao.findAllMaquette();
            return;
        }
        maquettes = maquetteDao.findMaquetteByClasse(selectedClasse.getCodeclasse());
        if (maquettes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune maquette enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
    }
        

    @Override
    public void imprimerMaquettePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerMaquetteHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
