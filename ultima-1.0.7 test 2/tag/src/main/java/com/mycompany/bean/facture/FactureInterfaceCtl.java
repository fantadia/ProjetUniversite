/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.facture;

import com.mycompany.bean.filiere.*;



/**
 *
 * @author macbookpro
 */
public interface FactureInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerFacture();
    public void modifierFacture();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerFacture();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerFacturePdf();
    
    public void imprimerFactureHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
