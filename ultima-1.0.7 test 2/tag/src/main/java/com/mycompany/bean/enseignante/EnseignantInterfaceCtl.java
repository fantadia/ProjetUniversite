/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.enseignante;

import com.mycompany.bean.comptable.*;







/**
 *
 * @author macbookpro
 */
public interface EnseignantInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerEnseignant();
    public void modifierEnseignant();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerEnseignant();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerEnseignantPdf();
    
    public void imprimerEnseignantHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
