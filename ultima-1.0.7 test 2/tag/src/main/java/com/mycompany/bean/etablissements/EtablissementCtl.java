/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.etablissements;

import beans.util.JsfUtil;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Etablissement;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="etablissementCtl")
@ViewScoped

public class EtablissementCtl extends AbstractEtablissementCtl implements EtablissementInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EtablissementCtl() {
    }
    
    @PostConstruct
    private void initEtablissement()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        domaineDao = (DomaineDao) context.getBean("domaineDao");
        etablissement = (Etablissement) context.getBean("etablissement");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
    }

    @Override
    public void enregistrerEtablissement() {
         Etablissement nv = etablissementDao.findEtablissementByLibelle(etablissement.getLibelleEtablissemant());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLibelleEtablissemant());
            modifier = supprimer = detail = true;
            return;

        }
        etablissement.setCodeetablissement("ETAB01");
        etablissementDao.createEtablissement(etablissement);
        initEtablissement();
        JsfUtil.addSuccessMessage("l'etablissement à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEtablissement() {
        
        if (selectedEtablissement == null || selectedEtablissement.getCodeetablissement()== null) {
            return;
        }

       /* List<Domaine> type = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        etablissementDao.updateEtablissement(selectedEtablissement);
        JsfUtil.addSuccessMessage("classe Modifier avec succes");
    }

    @Override
    public void supprimerEtablissement() {
        
         if(selectedEtablissement == null || selectedEtablissement.getCodeetablissement()== null)
            return;

            List<Domaine> n = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
            if (!n.isEmpty()) 
            {
             //utils.JsfUtil.addWarningMessage("Le ne peut pas etre supprime : il est deja utilise par une  operation");
            JsfUtil.addErrorMessage("Impossible de supprimer cet enregistrement car il est deja utilise");
        } else {

            etablissementDao.deleteEtablissement(selectedEtablissement);
            JsfUtil.addErrorMessage("Cette niveau à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerEtablissementPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEtablissementHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
