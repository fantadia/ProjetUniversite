/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.secretaire;





import com.mycompany.entities.Secretaire;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.DossierDao;
import com.nycompany.dao.SecretaireDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractSecretaireCtl {
    
    protected SecretaireDao secretaireDao ;
    protected UtilisateurDao utilisateurDao;
    protected DossierDao dossierDao;
    protected List<Secretaire> secretaires;
    
    protected StringBuffer secretairesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Secretaire selectedSecretaire;
    protected Secretaire secretaire;
    protected Utilisateur utilisateur;
    protected Utilisateur selectedUtilisateur;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public SecretaireDao getSecretaireDao() {
        return secretaireDao;
    }

    public void setSecretaireDao(SecretaireDao secretaireDao) {
        this.secretaireDao = secretaireDao;
    }

    public DossierDao getDossierDao() {
        return dossierDao;
    }

    public void setDossierDao(DossierDao dossierDao) {
        this.dossierDao = dossierDao;
    }

    public List<Secretaire> getSecretaires() {
        return secretaires;
    }

    public void setSecretaires(List<Secretaire> secretaires) {
        this.secretaires = secretaires;
    }

    public StringBuffer getSecretairesTableHtml() {
        return secretairesTableHtml;
    }

    public void setSecretairesTableHtml(StringBuffer secretairesTableHtml) {
        this.secretairesTableHtml = secretairesTableHtml;
    }

    public Secretaire getSelectedSecretaire() {
        return selectedSecretaire;
    }

    public void setSelectedSecretaire(Secretaire selectedSecretaire) {
        this.selectedSecretaire = selectedSecretaire;
        if (selectedSecretaire == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Secretaire getSecretaire() {
        return secretaire;
    }

    public void setSecretaire(Secretaire secretaire) {
        this.secretaire = secretaire;
    }
    public UtilisateurDao getUtilisateurDao() {
        return utilisateurDao;
    }

    public void setUtilisateurDao(UtilisateurDao utilisateurDao) {
        this.utilisateurDao = utilisateurDao;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Utilisateur getSelectedUtilisateur() {
        return selectedUtilisateur;
    }

    public void setSelectedUtilisateur(Utilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;   
    }
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = secretaireDao.findAllSecretaire().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
