/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.filiere;

import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Etablissement;

import com.mycompany.entities.Filiere;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.FiliereDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="filiereCtl")
@ViewScoped

public class FiliereCtl extends AbstractFiliereCtl implements FiliereInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public FiliereCtl() {
    }
    
    @PostConstruct
    private void initFiliere()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        filiereDao = (FiliereDao) context.getBean("filiereDao");
        classeDao = (ClasseDao) context.getBean("classeDao");
        filiere = (Filiere) context.getBean("filiere");
        selectedFiliere = (Filiere) context.getBean("filiere");
        filieres = filiereDao.findAllFiliere();
        
        domaineDao = (DomaineDao) context.getBean("domaineDao");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
    }

    @Override
    public void enregistrerFiliere() {
         Filiere nv = filiereDao.findFiliereByLibelle(filiere.getLibelleFiliere());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLibelleFiliere());
            modifier = supprimer = detail = true;
            return;

        }
        filiere.setCodefiliere("fil");
        filiereDao.createFiliere(filiere);
        initFiliere();
        JsfUtil.addSuccessMessage("la filier à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierFiliere() {
        
        if (selectedFiliere == null || selectedFiliere.getCodefiliere()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        filiereDao.updateFiliere(selectedFiliere);
        JsfUtil.addSuccessMessage("filiere Modifier avec succes");
    }

    @Override
    public void supprimerFiliere() {
        
         if(selectedFiliere == null || selectedFiliere.getCodefiliere()== null)
            return;
             
        List<Classe> n = classeDao.findClasseByIdFiliere(selectedFiliere.getCodefiliere());
        if (!n.isEmpty()) {
         //utils.JsfUtil.addWarningMessage("Le ne peut pas etre supprime : il est deja utilise par une  operation");
            JsfUtil.addErrorMessage("Impossible de supprimer cette filier car il est deja utiliser");
         
        } else {

            filiereDao.deleteFiliere(selectedFiliere);
            JsfUtil.addErrorMessage("Cette filiere à été supprimée");
            modifier = supprimer = detail = true;}
        
    }

     @Override
    public void listeDomainesEtablissement() 
    {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }
    
    @Override
    public void listeFilieresDomaine() {
        if (selectedDomaine == null) {
            filieres = filiereDao.findAllFiliere();
            return;
        }
        filieres = filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun filiere enregistré pour cette domaine");
            modifier = supprimer = detail = true;
        }
        
    } 
    
    @Override
    public void imprimerFilierePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerFiliereHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
