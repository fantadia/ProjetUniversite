/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.paiement;

import com.mycompany.bean.facture.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Facture;
import com.mycompany.entities.Filiere;
import com.mycompany.entities.Paiement;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.FactureDao;
import com.nycompany.dao.FiliereDao;
import com.nycompany.dao.PaiementDao;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="paiementCtl")
@ViewScoped

public class PaiementCtl extends AbstractPaiementCtl implements PaiementInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public PaiementCtl() {
    }
    
    @PostConstruct
    private void initPaiement()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        paiementDao = (PaiementDao) context.getBean("paiementDao");
        paiement = (Paiement) context.getBean("paiement");
        selectedPaiement = (Paiement) context.getBean("paiement");
        paiements = paiementDao.findAllPaiement();
        
        domaineDao = (DomaineDao) context.getBean("domaineDao");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        
        classeDao =(ClasseDao) context.getBean("classeDao");
        classes = classeDao.findAllClasse();
        selectedClasse = (Classe) context.getBean("classe");
        
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
        
        filiereDao =(FiliereDao) context.getBean("filiereDao");
        filieres =filiereDao.findAllFiliere();
        selectedFiliere = (Filiere) context.getBean("filiere");
        
        etudiantDao = (EtudiantDao) context.getBean("etudiantDao");
        selectedEtudiant = (Etudiant) context.getBean("etudiant");
        etudiants = etudiantDao.findAllEtudiant();
    }

    @Override
    public void enregistrerPaiement() {
       /*  Filiere nv = filiereDao.findFiliereByLibelle(filiere.getLibelleFiliere());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLibelleFiliere());
            modifier = supprimer = detail = true;
            return;

        }*/
        paiementDao.createPaiement(paiement);
        initPaiement();
        JsfUtil.addSuccessMessage("le Paiement à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierPaiement() {
        
        if (selectedPaiement == null || selectedPaiement.getCodepaiement()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        paiementDao.updatePaiement(selectedPaiement);
        JsfUtil.addSuccessMessage("Paiement Modifier avec succes");
    }

    @Override
    public void supprimerPaiement() {
        
         if(selectedPaiement == null || selectedPaiement.getCodepaiement()== null)
            return;
             

         paiementDao.deletePaiement(selectedPaiement);
         JsfUtil.addErrorMessage("Cette Paiement à été supprimée");
         modifier = supprimer = detail = true;}

  
     @Override
    public void listeEtudiantsClasse() {
        if (selectedClasse == null) {
            etudiants = etudiantDao.findAllEtudiant();
            return;
        }
        //etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        if (etudiants.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun etudiant enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
        
    }

    @Override
    public void listeClassesFiliere() {
        
        if (selectedFiliere == null) {
            classes = classeDao.findAllClasse();
            return;
        }
        classes = classeDao.findClasseByIdFiliere(selectedFiliere.getCodefiliere());
        if (classes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune Classe enregistré pour cette Filiere ");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void listeFilieresDomaine() {
        if (selectedDomaine == null) {
            filieres = filiereDao.findAllFiliere();
            return;
        }
        filieres = filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun filiere enregistré pour cette domaine");
            modifier = supprimer = detail = true;
        }
        
    } 

    @Override
    public void listeDomainesEtablissement() {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (domaines.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listePaiementsEtudiant() {
        if (selectedEtudiant == null) {
            paiements = paiementDao.findAllPaiement();
            return;
        }
        paiements = paiementDao.findPaiementByEtudiant(selectedEtudiant.getCodeutilisateur());
        if (paiements.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun paiement enregistré pour cette etudiant");
            modifier = supprimer = detail = true;
        }
    }
    @Override
    public void imprimerPaiementPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerPaiementHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
        
}
    

