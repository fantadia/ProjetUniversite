/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.emploisDuTemps;


import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.EmploisDuTemps;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Filiere;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.FiliereDao;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="emploisDuTempsCtl")
@ViewScoped

public class EmploisDuTempsCtl extends AbstractEmploisDuTempsCtl implements EmploisDuTempsInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EmploisDuTempsCtl() {
    }
    
    @PostConstruct
    private void initEmploisDuTemps()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        emploisDuTempsDao = (EmploisDuTempsDao) context.getBean("emploisdutempsDao");
        emploisDuTemps = (EmploisDuTemps) context.getBean("emploisdutemps");
        selectedEmploisDuTemps = (EmploisDuTemps) context.getBean("emploisdutemps");
        emploisDuTempses = emploisDuTempsDao.findAllEmploisDuTemps();
        
         domaineDao = (DomaineDao) context.getBean("domaineDao");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        
        classeDao =(ClasseDao) context.getBean("classeDao");
        classes = classeDao.findAllClasse();
        selectedClasse = (Classe) context.getBean("classe");
        
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
        
        filiereDao =(FiliereDao) context.getBean("filiereDao");
        filieres =filiereDao.findAllFiliere();
        selectedFiliere = (Filiere) context.getBean("filiere");
    }

    @Override
    public void enregistrerEmploisDuTemps() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        emploisDuTempsDao.createEmploisDuTemps(emploisDuTemps);
        initEmploisDuTemps();
        JsfUtil.addSuccessMessage("l' EmploisDuTemps à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEmploisDuTemps() {
        
        if (selectedEmploisDuTemps == null || selectedEmploisDuTemps.getCodeEmploisdutemps()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        emploisDuTempsDao.updateEmploisDuTemps(selectedEmploisDuTemps);
        JsfUtil.addSuccessMessage("Ec Modifier avec succes");
    }

    @Override
    public void supprimerEmploisDuTemps() {
        
         if(selectedEmploisDuTemps == null || selectedEmploisDuTemps.getCodeEmploisdutemps()== null)
            return;
       
            emploisDuTempsDao.deleteEmploisDuTemps(selectedEmploisDuTemps);
            JsfUtil.addErrorMessage("Ce Ec à été supprimée");
            modifier = supprimer = detail = true;
    }
    
     @Override
    public void listeEmploisDuTempsClasse() {
        
        if (selectedClasse == null) {
            emploisDuTempses = emploisDuTempsDao.findAllEmploisDuTemps();
            return;
        }
        emploisDuTempses = emploisDuTempsDao.findEmploisDuTempsByClasse(selectedClasse.getCodeclasse());
        if (classes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun Emplois du temps enregistré pour cette Classe ");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeClassesFiliere() {
        
        if (selectedFiliere == null) {
            classes = classeDao.findAllClasse();
            return;
        }
        classes = classeDao.findClasseByIdFiliere(selectedFiliere.getCodefiliere());
        if (classes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune Classe enregistré pour cette Filiere ");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void listeFilieresDomaine() {
        if (selectedDomaine == null) {
            filieres = filiereDao.findAllFiliere();
            return;
        }
        filieres = filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun filiere enregistré pour cette domaine");
            modifier = supprimer = detail = true;
        }
        
    } 

    @Override
    public void listeDomainesEtablissement() {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (domaines.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }
    
    @Override
    public void imprimerEmploisDuTempsPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEmploisDuTempsHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
