/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.classes;

import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.EmploisDuTemps;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Filiere;
import com.mycompany.entities.Maquette;
import com.mycompany.pdf7.docs.ClassesListDocument;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.FiliereDao;
import com.nycompany.dao.MaquetteDao;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name = "classeCtl")
@ViewScoped
public class ClasseCtl extends AbstractClasseCtl implements ClasseInterfaceCtl {

    private static final Logger LOG = Logger.getLogger(ClasseCtl.class.getName());

    /**
     * Creates a new instance of NiveauCtrl
     */
    public ClasseCtl() {
    }

    @PostConstruct
    private void initClasse() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        classeDao = (ClasseDao) context.getBean("classeDao");
        emploisDuTempsDao = (EmploisDuTempsDao) context.getBean("emploisdutempsDao");
        maquetteDao = (MaquetteDao) context.getBean("maquetteDao");
        classe = (Classe) context.getBean("classe");
        selectedClasse = (Classe) context.getBean("classe");
        classes = classeDao.findAllClasse();
        
        domaineDao = (DomaineDao) context.getBean("domaineDao");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
        
        filiereDao =(FiliereDao) context.getBean("filiereDao");
        selectedFiliere = (Filiere) context.getBean("filiere");
        filieres =filiereDao.findAllFiliere();
        
        
        
    }

    @Override
    public void enregistrerClasse() {
        Classe nv = classeDao.findClasseByLibelle(classe.getLbelleClasse());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLbelleClasse());
            modifier = supprimer = detail = true;
            return;

        }
        classe.setCodeclasse("classe");
        classeDao.createClasse(classe);
        initClasse();
        JsfUtil.addSuccessMessage("la classe à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierClasse() {

        if (selectedClasse == null || selectedClasse.getCodeclasse() == null) {
            return;
        }

        /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
         */
        classeDao.updateClasse(selectedClasse);
        JsfUtil.addSuccessMessage("classe Modifier avec succes");
    }

    @Override
    public void supprimerClasse() {

        if (selectedClasse == null || selectedClasse.getCodeclasse() == null) {
            return;
        }

        List<EmploisDuTemps> e = emploisDuTempsDao.findEmploisDuTempsByClasse(selectedClasse.getCodeclasse());
        List<Maquette> m = maquetteDao.findMaquetteByClasse(selectedClasse.getCodeclasse());
        if ((!e.isEmpty()) || (!m.isEmpty())) {
            JsfUtil.addErrorMessage("Impossible de supprimer cet enregistrement car il est deja utilise");

        } else {

            classeDao.deleteClasse(selectedClasse);
            JsfUtil.addErrorMessage("Cette niveau à été supprimée");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerClassePdf() {
        try {
            ClassesListDocument doc = new ClassesListDocument(classeDao.findAllClasse());
            pdfFileName = doc.print();
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void imprimerClasseHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
     @Override
    public void listeClassesFiliere() {
        
        if (selectedFiliere == null) {
            classes = classeDao.findAllClasse();
            return;
        }
        classes = classeDao.findClasseByIdFiliere(selectedFiliere.getCodefiliere());
        if (classes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune Classe enregistré pour cette Filiere ");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void listeFilieresDomaine() {
        if (selectedDomaine == null) {
            filieres = filiereDao.findAllFiliere();
            return;
        }
        filieres = filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun filiere enregistré pour cette domaine");
            modifier = supprimer = detail = true;
        }
        
    } 

    @Override
    public void listeDomainesEtablissement() {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }
    

}
