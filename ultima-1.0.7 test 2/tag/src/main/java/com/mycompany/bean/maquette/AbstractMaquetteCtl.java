/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.maquette;


import com.mycompany.entities.Classe;
import com.mycompany.entities.Filiere;
import com.mycompany.entities.Maquette;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.FiliereDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractMaquetteCtl {
    
    
    protected MaquetteDao maquetteDao ;
    protected SemestreDao semestreDao;
       
    protected List<Maquette> maquettes;
    
    protected ClasseDao classeDao;
    protected Classe selectedClasse;
    protected List<Classe> classes;

    
    protected StringBuffer maquettesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Maquette selectMaquette;
    protected Maquette maquette;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    
    
    
    

    public ClasseDao getClasseDao() {
        return classeDao;
    }

    public void setClasseDao(ClasseDao classeDao) {
        this.classeDao = classeDao;
    }

    public Classe getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classe selectedClasse) {
        this.selectedClasse = selectedClasse;
    }

    public List<Classe> getClasses() {
        return classes;
    }

    public void setClasses(List<Classe> classes) {
        this.classes = classes;
    }
    
    

    public MaquetteDao getMaquetteDao() {
        return maquetteDao;
    }

    public void setMaquetteDao(MaquetteDao maquetteDao) {
        this.maquetteDao = maquetteDao;
    }

    public List<Maquette> getMaquettes() {
        return maquettes;
    }

    public void setMaquettes(List<Maquette> maquettes) {
        this.maquettes = maquettes;
    }

    public StringBuffer getMaquettesTableHtml() {
        return maquettesTableHtml;
    }

    public void setMaquettesTableHtml(StringBuffer maquettesTableHtml) {
        this.maquettesTableHtml = maquettesTableHtml;
    }

    public Maquette getSelectMaquette() {
        return selectMaquette;
    }

    public void setSelectMaquette(Maquette selectMaquette) {
        this.selectMaquette = selectMaquette;
        if (selectMaquette == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Maquette getMaquette() {
        return maquette;
    }

    public void setMaquette(Maquette maquette) {
        this.maquette = maquette;
    }

    public SemestreDao getSemestreDao() {
        return semestreDao;
    }

    public void setSemestreDao(SemestreDao semestreDao) {
        this.semestreDao = semestreDao;
    }
    
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = maquetteDao.findAllMaquette().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
