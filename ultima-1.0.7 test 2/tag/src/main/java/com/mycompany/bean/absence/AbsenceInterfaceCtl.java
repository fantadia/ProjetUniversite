/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.absence;




/**
 *
 * @author macbookpro
 */
public interface AbsenceInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerAbsence();
    public void modifierAbsence();
    
    public void listeEtudiantsClasse();
    public void listeClassesFiliere();
    public void listeFilieresDomaine();
    public void listeDomainesEtablissement();
    public void listeAbsencesEtudiant();
      
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerAbsence();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerAbsencePdf();
    
    public void imprimerAbsenceHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
