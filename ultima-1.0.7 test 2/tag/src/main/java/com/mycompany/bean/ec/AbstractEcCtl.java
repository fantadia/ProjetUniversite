/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ec;




import com.mycompany.entities.Classe;
import com.mycompany.entities.Ec;
import com.mycompany.entities.Maquette;
import com.mycompany.entities.Semestre;
import com.mycompany.entities.Ue;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.EvaluerDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEcCtl {
    
   
    protected CoursDao coursDao;
    protected EvaluerDao evaluerDao;
    
    protected ClasseDao classeDao;
    protected Classe selectedClasse;
    protected List<Classe> classes;
    
    protected UeDao ueDao ;
    protected Ue selectUe;
    protected Ue ue;
    protected List<Ue> ues;
    
    protected MaquetteDao maquetteDao ;
    protected List<Maquette> maquettes;
    protected Maquette selectedMaquette;
    
    protected SemestreDao semestreDao;
    protected List<Semestre> semestres;
    protected Semestre selectedSemestre;
    
     
    protected EcDao ecDao;
    protected Ec selectedEc;
    protected List<Ec> ecs;
    
   
    
    protected StringBuffer ecsTableHtml = new StringBuffer("pas encore implementé") ;
    protected Ec ec;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public ClasseDao getClasseDao() {
        return classeDao;
    }

    public void setClasseDao(ClasseDao classeDao) {
        this.classeDao = classeDao;
    }

    public Classe getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classe selectedClasse) {
        this.selectedClasse = selectedClasse;
    }

    public List<Classe> getClasses() {
        return classes;
    }

    public void setClasses(List<Classe> classes) {
        this.classes = classes;
    }

    public UeDao getUeDao() {
        return ueDao;
    }

    public void setUeDao(UeDao ueDao) {
        this.ueDao = ueDao;
    }

    public Ue getSelectUe() {
        return selectUe;
    }

    public void setSelectUe(Ue selectUe) {
        this.selectUe = selectUe;
    }

    public Ue getUe() {
        return ue;
    }

    public void setUe(Ue ue) {
        this.ue = ue;
    }

    public List<Ue> getUes() {
        return ues;
    }

    public void setUes(List<Ue> ues) {
        this.ues = ues;
    }

    public MaquetteDao getMaquetteDao() {
        return maquetteDao;
    }

    public void setMaquetteDao(MaquetteDao maquetteDao) {
        this.maquetteDao = maquetteDao;
    }

    public List<Maquette> getMaquettes() {
        return maquettes;
    }

    public void setMaquettes(List<Maquette> maquettes) {
        this.maquettes = maquettes;
    }

    public Maquette getSelectedMaquette() {
        return selectedMaquette;
    }

    public void setSelectedMaquette(Maquette selectedMaquette) {
        this.selectedMaquette = selectedMaquette;
    }

    public SemestreDao getSemestreDao() {
        return semestreDao;
    }

    public void setSemestreDao(SemestreDao semestreDao) {
        this.semestreDao = semestreDao;
    }

    public List<Semestre> getSemestres() {
        return semestres;
    }

    public void setSemestres(List<Semestre> semestres) {
        this.semestres = semestres;
    }

    public Semestre getSelectedSemestre() {
        return selectedSemestre;
    }

    public void setSelectedSemestre(Semestre selectedSemestre) {
        this.selectedSemestre = selectedSemestre;
    }
    
    

    public EcDao getEcDao() {
        return ecDao;
    }

    public void setEcDao(EcDao ecDao) {
        this.ecDao = ecDao;
    }

    public CoursDao getCoursDao() {
        return coursDao;
    }

    public void setCoursDao(CoursDao coursDao) {
        this.coursDao = coursDao;
    }

    public EvaluerDao getEvaluerDao() {
        return evaluerDao;
    }

    public void setEvaluerDao(EvaluerDao evaluerDao) {
        this.evaluerDao = evaluerDao;
    }

    public List<Ec> getEcs() {
        return ecs;
    }

    public void setEcs(List<Ec> ecs) {
        this.ecs = ecs;
    }

    public StringBuffer getEcsTableHtml() {
        return ecsTableHtml;
    }

    public void setEcsTableHtml(StringBuffer ecsTableHtml) {
        this.ecsTableHtml = ecsTableHtml;
    }

    public Ec getSelectedEc() {
        return selectedEc;
    }

    public void setSelectedEc(Ec selectedEc) {
        this.selectedEc = selectedEc;
         if (selectedEc == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Ec getEc() {
        return ec;
    }

    public void setEc(Ec ec) {
        this.ec = ec;
    }

    
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = ecDao.findAllEc().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
