/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.responsable;





/**
 *
 * @author macbookpro
 */
public interface ResponsableInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerResponsable();
    public void modifierResponsable();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerResponsable();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerResponsablePdf();
    
    public void imprimerResponsableHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
