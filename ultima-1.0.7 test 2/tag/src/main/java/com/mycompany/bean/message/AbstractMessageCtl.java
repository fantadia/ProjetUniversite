/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.message;


import com.mycompany.entities.Message;
import com.nycompany.dao.MessageDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractMessageCtl {
    
    
    protected MessageDao messageDao;
  
   
    protected List<Message> messages;
    
    protected StringBuffer messagesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Message selectedMessage;
    protected Message message;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public MessageDao getMessageDao() {
        return messageDao;
    }

    public void setMessageDao(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    public List<Message> getMessages() {
        return messages;
    }
    

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public StringBuffer getMessagesTableHtml() {
        return messagesTableHtml;
    }

    public void setMessagesTableHtml(StringBuffer messagesTableHtml) {
        this.messagesTableHtml = messagesTableHtml;
    }

    public Message getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(Message selectedMessage) {
        this.selectedMessage = selectedMessage;
         if (selectedMessage == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
   
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = messageDao.findAllMessage().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
