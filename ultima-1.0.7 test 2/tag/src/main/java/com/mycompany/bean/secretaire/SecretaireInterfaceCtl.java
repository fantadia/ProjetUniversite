/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.secretaire;








/**
 *
 * @author macbookpro
 */
public interface SecretaireInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerSecretaire();
    public void modifierSecretaire();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerSecretaire();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerSecretairePdf();
    
    public void imprimerSecretaireHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
