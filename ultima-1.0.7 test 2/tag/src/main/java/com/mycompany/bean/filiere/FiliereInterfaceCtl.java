/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.filiere;



/**
 *
 * @author macbookpro
 */
public interface FiliereInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerFiliere();
    public void modifierFiliere();
    
    public void listeDomainesEtablissement() ;
    public void listeFilieresDomaine() ;
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerFiliere();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerFilierePdf();
    
    public void imprimerFiliereHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
