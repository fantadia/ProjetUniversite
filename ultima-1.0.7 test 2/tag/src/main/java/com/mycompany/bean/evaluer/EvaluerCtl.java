/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.evaluer;

import com.mycompany.bean.absence.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Absence;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Evaluer;
import com.mycompany.entities.EvaluerId;
import com.mycompany.entities.Filiere;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.EvaluerDao;
import com.nycompany.dao.FiliereDao;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="evaluerCtl")
@ViewScoped

public class EvaluerCtl extends AbstractEvaluerCtl implements EvaluerInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EvaluerCtl() {
    }
    
    @PostConstruct
    private void initAbsence()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        evaluerDao = (EvaluerDao) context.getBean("evaluerDao");
        evaluer = (Evaluer) context.getBean("evaluer");
        selectedEvaluer = (Evaluer) context.getBean("evaluer");
        evaluers = evaluerDao.findAllEvaluer();
        evaluer.setId((EvaluerId) context.getBean("evaluerId"));
       
        domaineDao = (DomaineDao) context.getBean("domaineDao");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        
        classeDao =(ClasseDao) context.getBean("classeDao");
        classes = classeDao.findAllClasse();
        selectedClasse = (Classe) context.getBean("classe");
        
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
        
        filiereDao =(FiliereDao) context.getBean("filiereDao");
        filieres =filiereDao.findAllFiliere();
        selectedFiliere = (Filiere) context.getBean("filiere");
        
        etudiantDao = (EtudiantDao) context.getBean("etudiantDao");
        selectedEtudiant = (Etudiant) context.getBean("etudiant");
        etudiants = etudiantDao.findAllEtudiant();
        
    }

    @Override
    public void enregistrerEvaluer() {
       /*  Filiere nv = filiereDao.findFiliereByLibelle(filiere.getLibelleFiliere());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLibelleFiliere());
            modifier = supprimer = detail = true;
            return;

        }*/
        evaluer.getId().setCodematiere(evaluer.getEc().getCodematiere());
        evaluer.getId().setCodeutilisateur(evaluer.getEtudiant().getCodeutilisateur());
        evaluerDao.createEvaluer(evaluer);
        initAbsence();
        JsfUtil.addSuccessMessage("evaluation créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEvaluer() {
        
        if (selectedEvaluer == null || selectedEvaluer.getId()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        evaluerDao.updateEvaluer(selectedEvaluer);
        JsfUtil.addSuccessMessage("evaluation Modifier avec succes");
    }

    @Override
    public void supprimerEvaluer() {
        
         if(selectedEvaluer == null || selectedEvaluer.getId()== null)
            return;
             

         evaluerDao.deleteEvaluer(selectedEvaluer);
         JsfUtil.addErrorMessage("Cette evaluation à été supprimée");
         modifier = supprimer = detail = true;}

     @Override
    public void listeEtudiantsClasse() {
        if (selectedClasse == null) {
            etudiants = etudiantDao.findAllEtudiant();
            return;
        }
        //etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        if (etudiants.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun etudiant enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
        
    }

    @Override
    public void listeClassesFiliere() {
        
        if (selectedFiliere == null) {
            classes = classeDao.findAllClasse();
            return;
        }
        classes = classeDao.findClasseByIdFiliere(selectedFiliere.getCodefiliere());
        if (classes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune Classe enregistré pour cette Filiere ");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void listeFilieresDomaine() {
        if (selectedDomaine == null) {
            filieres = filiereDao.findAllFiliere();
            return;
        }
        filieres = filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun filiere enregistré pour cette domaine");
            modifier = supprimer = detail = true;
        }
        
    } 

    @Override
    public void listeDomainesEtablissement() {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (domaines.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeEvaluersEtudiant() {
        if (selectedEtudiant == null) {
            evaluers = evaluerDao.findAllEvaluer();
            return;
        }
        evaluers = evaluerDao.findEvaluerByEtudiant(selectedEtudiant.getCodeutilisateur());
        if (evaluers.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune evaluation enregistré pour cette etudiant");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerEvaluerPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEvaluerHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
        
}
    

