/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.Domaine;



/**
 *
 * @author macbookpro
 */
public interface DomaineInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerDomaine();
    public void modifierDomaine();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerDomaine();
     public void listeDomainesEtablissement();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerDomainePdf();
    
    public void imprimerDomaineHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
