/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.semestre;





/**
 *
 * @author macbookpro
 */
public interface SemestreInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerSemestre();
    public void modifierSemestre();
    
     public void listeSemestresMaquette();
     public void listeMaquetteClasse();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerSemestre();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerSemestrePdf();
    
    public void imprimerSemestreHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
