/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.dossier;


import com.mycompany.bean.ec.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Dossier;
import com.mycompany.entities.Ec;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Filiere;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.DossierDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.FiliereDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="dossierCtl")
@ViewScoped

public class DossierCtl extends AbstractDossierCtl implements DossierInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public DossierCtl() {
    }
    
    @PostConstruct
    private void initDossier()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        dossierDao = (DossierDao) context.getBean("dossierDao");
        dossier = (Dossier) context.getBean("dossier");
        selectedDossier = (Dossier) context.getBean("dossier");
        dossiers = dossierDao.findAllDossier();
         domaineDao = (DomaineDao) context.getBean("domaineDao");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        
        classeDao =(ClasseDao) context.getBean("classeDao");
        classes = classeDao.findAllClasse();
        selectedClasse = (Classe) context.getBean("classe");
        
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
        
        filiereDao =(FiliereDao) context.getBean("filiereDao");
        filieres =filiereDao.findAllFiliere();
        selectedFiliere = (Filiere) context.getBean("filiere");
        
        etudiantDao = (EtudiantDao) context.getBean("etudiantDao");
        selectedEtudiant = (Etudiant) context.getBean("etudiant");
        etudiants = etudiantDao.findAllEtudiant();
        
    }

    @Override
    public void enregistrerDossier() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        dossier.setCodedossier("dossier");
        dossierDao.createDossier(dossier);
        initDossier();
        JsfUtil.addSuccessMessage("le dossier à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierDossier() {
        
        if (selectedDossier == null || selectedDossier.getCodedossier()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        dossierDao.updateDossier(selectedDossier);
        JsfUtil.addSuccessMessage("dossier Modifier avec succes");
    }

    @Override
    public void supprimerDossier() {
        
         if(selectedDossier == null || selectedDossier.getCodedossier()== null)
            return;
       
            dossierDao.deleteDossier(selectedDossier);
            JsfUtil.addErrorMessage("Ce dossier à été supprimée");
            modifier = supprimer = detail = true;
    }
    
     @Override
    public void listeEtudiantsClasse() {
        if (selectedClasse == null) {
            etudiants = etudiantDao.findAllEtudiant();
            return;
        }
        //etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        if (etudiants.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun etudiant enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
        
    }

    @Override
    public void listeClassesFiliere() {
        
        if (selectedFiliere == null) {
            classes = classeDao.findAllClasse();
            return;
        }
        classes = classeDao.findClasseByIdFiliere(selectedFiliere.getCodefiliere());
        if (classes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune Classe enregistré pour cette Filiere ");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void listeFilieresDomaine() {
        if (selectedDomaine == null) {
            filieres = filiereDao.findAllFiliere();
            return;
        }
        filieres = filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun filiere enregistré pour cette domaine");
            modifier = supprimer = detail = true;
        }
        
    } 

    @Override
    public void listeDomainesEtablissement() {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (domaines.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeDossiersEtudiant() {
        if (selectedEtudiant == null) {
            dossiers = dossierDao.findAllDossier();
            return;
        }
        dossiers = dossierDao.findDossierByEtudiant(selectedEtudiant.getCodeutilisateur());
        if (dossiers.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun dossier enregistré pour cette etudiant");
            modifier = supprimer = detail = true;
        }
    }
    @Override
    public void imprimerDossierPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerDossierHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
