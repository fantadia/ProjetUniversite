/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.facture;

import beans.util.JsfUtil;
import com.mycompany.entities.Facture;
import com.nycompany.dao.FactureDao;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="factureCtl")
@ViewScoped

public class FactureCtl extends AbstractFactureCtl implements FactureInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public FactureCtl() {
    }
    
    @PostConstruct
    private void initFacture()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        factureDao = (FactureDao) context.getBean("factureDao");
        facture = (Facture) context.getBean("facture");
        selectedFacture = (Facture) context.getBean("facture");
        factures = factureDao.findAllFacture();
    }

    @Override
    public void enregistrerFacture() {
       /*  Filiere nv = filiereDao.findFiliereByLibelle(filiere.getLibelleFiliere());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getLibelleFiliere());
            modifier = supprimer = detail = true;
            return;

        }*/
        factureDao.createFacture(facture);
        initFacture();
        JsfUtil.addSuccessMessage("la facture à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierFacture() {
        
        if (selectedFacture == null || selectedFacture.getCodefacture()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        factureDao.updateFacture(selectedFacture);
        JsfUtil.addSuccessMessage("facture Modifier avec succes");
    }

    @Override
    public void supprimerFacture() {
        
         if(selectedFacture == null || selectedFacture.getCodefacture()== null)
            return;
             

         factureDao.deleteFacture(selectedFacture);
         JsfUtil.addErrorMessage("Cette facture à été supprimée");
         modifier = supprimer = detail = true;}

    @Override
    public void imprimerFacturePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerFactureHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
}
    

