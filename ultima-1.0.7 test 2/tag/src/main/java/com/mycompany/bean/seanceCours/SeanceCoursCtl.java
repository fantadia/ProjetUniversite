/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.seanceCours;


import com.mycompany.bean.cours.*;
import com.mycompany.bean.ec.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Absence;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Ec;
import com.mycompany.entities.EmploisDuTemps;
import com.mycompany.entities.Filiere;
import com.mycompany.entities.SeanceCours;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.SeanceCoursDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="seanceCoursCtl")
@ViewScoped

public class SeanceCoursCtl extends AbstractSeanceCoursCtl implements SeanceCoursInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public SeanceCoursCtl() {
    }
    
    @PostConstruct
    private void initCours()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        seanceCoursDao = (SeanceCoursDao) context.getBean("seanceCoursDao");
        absenceDao = (AbsenceDao) context.getBean("absenceDao");
        seanceCours = (SeanceCours) context.getBean("seanceCours");
        selectedSeanceCours = (SeanceCours) context.getBean("seanceCours");
        seanceCourses = seanceCoursDao.findAllSeanceCours();
       
    }

    @Override
    public void enregistrerSeanceCours() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        seanceCours.getCours().setCodecours(seanceCours.getCodecours());
        seanceCoursDao.createSeanceCours(seanceCours);
        initCours();
        JsfUtil.addSuccessMessage("seance de cours creer avec succes");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierSeanceCours() {
        
        if (selectedSeanceCours == null || selectedSeanceCours.getCodecours()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        seanceCoursDao.updateSeanceCours(selectedSeanceCours);
        JsfUtil.addSuccessMessage("Cours Modifier avec succes");
    }

    @Override
    public void supprimerSeanceCours() {
        
         if(selectedSeanceCours == null || selectedSeanceCours.getCodecours()== null)
            return;
         List<Absence> absenc = absenceDao.findAbsenceBySeanceCours(selectedSeanceCours.getCodecours());
         if(!absenc.isEmpty()){
             JsfUtil.addErrorMessage("ce Seancecours ne peut pas etre supprimer,car il est utilise dans une autre operation");
         }
       
            seanceCoursDao.deleteSeanceCours(selectedSeanceCours);
            JsfUtil.addErrorMessage("Ce Seancecours à été supprimée");
            modifier = supprimer = detail = true;
    }
    @Override
    public void imprimerSeanceCoursPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerSeanceCoursHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
