/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.dossier;






/**
 *
 * @author macbookpro
 */
public interface DossierInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerDossier();
    public void modifierDossier();
    
    
    public void listeEtudiantsClasse();
    public void listeClassesFiliere();
    public void listeFilieresDomaine();
    public void listeDomainesEtablissement();
    public void listeDossiersEtudiant();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerDossier();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerDossierPdf();
    
    public void imprimerDossierHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
