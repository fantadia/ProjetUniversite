/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.seanceCours;

import com.mycompany.bean.cours.*;
import com.mycompany.bean.ec.*;






/**
 *
 * @author macbookpro
 */
public interface SeanceCoursInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerSeanceCours();
    public void modifierSeanceCours();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerSeanceCours();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerSeanceCoursPdf();
    
    public void imprimerSeanceCoursHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
