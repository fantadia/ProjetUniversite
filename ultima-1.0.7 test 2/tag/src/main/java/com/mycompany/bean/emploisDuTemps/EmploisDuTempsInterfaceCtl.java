/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.emploisDuTemps;







/**
 *
 * @author macbookpro
 */
public interface EmploisDuTempsInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerEmploisDuTemps();
    public void modifierEmploisDuTemps();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerEmploisDuTemps();
    public void listeClassesFiliere();
    public void listeFilieresDomaine();
    public void listeDomainesEtablissement();
    public void listeEmploisDuTempsClasse();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerEmploisDuTempsPdf();
    
    public void imprimerEmploisDuTempsHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
