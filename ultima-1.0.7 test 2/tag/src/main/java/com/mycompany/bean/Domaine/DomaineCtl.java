/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.Domaine;


import beans.util.JsfUtil;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Filiere;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.FiliereDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="domaineCtl")
@ViewScoped

public class DomaineCtl extends AbstractDomaineCtl implements DomaineInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public  DomaineCtl() {
    }
    
    @PostConstruct
    private void initDomaine()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        domaineDao = (DomaineDao) context.getBean("domaineDao");
        filiereDao  = (FiliereDao) context.getBean("filiereDao");
        domaine = (Domaine) context.getBean("domaine");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
    }

    @Override
    public void enregistrerDomaine() {
         Domaine nv = domaineDao.findDomaineByLibelle(domaine.getNomDomaine());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getNomDomaine());
            modifier = supprimer = detail = true;
            return;

        }
        domaine.setCodedomaine("dom");
        domaineDao.createDomainee(domaine);
        initDomaine();
        JsfUtil.addSuccessMessage("le domaine à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierDomaine() {
        
        if (selectedDomaine == null || selectedDomaine.getCodedomaine()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        domaineDao.updateDomaine(selectedDomaine);
        JsfUtil.addSuccessMessage("Domaine Modifier avec succes");
    }

    @Override
    public void supprimerDomaine() {
        
         if(selectedDomaine == null || selectedDomaine.getCodedomaine()== null)
            return;
             
        List<Filiere> n = (List<Filiere>) filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (!n.isEmpty()) 
        {
         //utils.JsfUtil.addWarningMessage("Le ne peut pas etre supprime : il est deja utilise par une  operation");
          JsfUtil.addErrorMessage("Impossible de supprimer ce domaine, car il est deja utilise");
        } 
        else{
            
            domaineDao.deleteDomaine(selectedDomaine);
            JsfUtil.addErrorMessage("domaine supprimée");
            modifier = supprimer = detail = true;
        }
            
    }
    
    
     @Override
    public void listeDomainesEtablissement() {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (domaines.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerDomainePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerDomaineHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
