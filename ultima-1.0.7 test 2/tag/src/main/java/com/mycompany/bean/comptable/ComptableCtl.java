/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.comptable;


import beans.util.JsfUtil;
import com.mycompany.entities.Comptable;import com.mycompany.entities.Facture;
;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.ComptableDao;
import com.nycompany.dao.FactureDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="comptableCtl")
@ViewScoped

public class ComptableCtl extends AbstractComptableCtl implements ComptableInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public ComptableCtl() {
    }
    
    @PostConstruct
    private void initComptable()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        comptableDao = (ComptableDao) context.getBean("comptableDao");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        utilisateur =(Utilisateur) context.getBean("utilisateur");
        selectedUtilisateur =(Utilisateur) context.getBean("utilisateur");
        comptable = (Comptable) context.getBean("comptable");
        selectedComptable = (Comptable) context.getBean("comptable");
        comptables = comptableDao.findAllComptable();
        factureDao = (FactureDao) context.getBean("factureDao");
    }

    @Override
    public void enregistrerComptable() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        /*
            Avant tout faut enregistrer ds utilisateur avec comme identifiant 700
            ce qui veut dire que dans la page jsf faut utiliser l'objet utilisateur
            pour preparer les donnees a inserer
            Ensuite l'objet enseignant sera creer a partir de utilisateur
        */
        utilisateur.setCodeutilisateur("600");
        utilisateurDao.createUtilisateur(utilisateur);
        //jecrase utilisateur et je le recupere a nouveau
        //depuis la BD 
        utilisateur=utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        //j'initialise l'enseignant a inserer
        comptable.setUtilisateur(utilisateur);
        comptableDao.createComptable(comptable);
        initComptable();
        JsfUtil.addSuccessMessage("le comptable à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierComptable() {
        
        if (selectedComptable == null || selectedComptable.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       selectedUtilisateur.setNom(selectedComptable.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedComptable.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedComptable.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedComptable.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedComptable.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedComptable.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedComptable.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedComptable.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedComptable.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedComptable.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedComptable.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedComptable.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedComptable.getUtilisateur().getBoitePostale());
       
       utilisateurDao.updateUtilisateur(selectedUtilisateur);
       //selectedEnseignant.setUtilisateur(selectedUtilisateur);
       //enseignantDao.updateEnseignant(selectedEnseignant);
       JsfUtil.addSuccessMessage("comptable modifier avec succes");
    }

    @Override
    public void supprimerComptable() {
        
         if(selectedUtilisateur == null || selectedUtilisateur.getCodeutilisateur()== null)
            return;
         
         List<Facture> factures=factureDao.findFactureByComptable(selectedUtilisateur.getCodeutilisateur());
         if(!factures.isEmpty())
         {
             JsfUtil.addErrorMessage("Ce comptable ne peut pas etre supprimée, il est deja utilise dans une operation");
         }
         
       selectedUtilisateur.setNom(selectedComptable.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedComptable.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedComptable.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedComptable.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedComptable.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedComptable.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedComptable.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedComptable.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedComptable.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedComptable.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedComptable.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedComptable.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedComptable.getUtilisateur().getBoitePostale());
       
         
           comptableDao.deleteComptable(selectedComptable);
           utilisateurDao.deleteUtilisateur(selectedUtilisateur);
          
           JsfUtil.addErrorMessage("Ce comptable à été supprimée");
           modifier = supprimer = detail = true;}

    @Override
    public void imprimerComptablePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerComptableHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
