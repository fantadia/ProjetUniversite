/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.cours;


import com.mycompany.bean.ec.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Absence;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Ec;
import com.mycompany.entities.EmploisDuTemps;
import com.mycompany.entities.Filiere;
import com.mycompany.entities.Maquette;
import com.mycompany.entities.Semestre;
import com.mycompany.entities.Ue;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="coursCtl")
@ViewScoped

public class CoursCtl extends AbstractCoursCtl implements CoursInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public CoursCtl() {
    }
    
    @PostConstruct
    private void initCours()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        coursDao = (CoursDao) context.getBean("coursDao");
        absenceDao = (AbsenceDao) context.getBean("absenceDao");
        cours = (Cours) context.getBean("cours");
        selectedCours = (Cours) context.getBean("cours");
        courses = coursDao.findAllCours();
        
        emploisDuTempsDao =(EmploisDuTempsDao) context.getBean("emploisdutempsDao");
        emploisDuTemps = (EmploisDuTemps) context.getBean("emploisdutemps");
        emploisDuTempses =emploisDuTempsDao.findAllEmploisDuTemps();
        
        ueDao = (UeDao) context.getBean("ueDao");
        selectUe = (Ue) context.getBean("ue");
        ues = ueDao.findAllUe();
    
        semestreDao = (SemestreDao) context.getBean("semestreDao");
        selectedSemestre = (Semestre) context.getBean("semestre");
        semestres = semestreDao.findAllSemestre();
        
        maquetteDao = (MaquetteDao) context.getBean("maquetteDao");
        selectedMaquette = (Maquette) context.getBean("maquette");
        maquettes = maquetteDao.findAllMaquette();
        
        classeDao = (ClasseDao) context.getBean("classeDao");
        selectedClasse = (Classe) context.getBean("classe");
        classes = classeDao.findAllClasse();
        
        ecDao = (EcDao) context.getBean("ecDao");
        selectedEc = (Ec) context.getBean("ec");
        ecs = ecDao.findAllEc();
    }

    @Override
    public void enregistrerCours() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        
        coursDao.createCours(cours);
        initCours();
        JsfUtil.addSuccessMessage("le Cours à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierCours() {
        
        if (selectedCours == null || selectedCours.getCodecours()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        coursDao.updateCours(selectedCours);
        JsfUtil.addSuccessMessage("Cours Modifier avec succes");
    }

    @Override
    public void supprimerCours() {
        
       
            coursDao.deleteCours(selectedCours);
            JsfUtil.addErrorMessage("Ce cours à été supprimée");
            modifier = supprimer = detail = true;
    }
    
    @Override
    public void listeSemestresMaquette() {
        if (selectedMaquette == null) {
            semestres = semestreDao.findAllSemestre();
            return;
        }
        semestres = semestreDao.findSemestreByMaquette(selectedMaquette.getCodemaquette());
        if (semestres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun semestre enregistré pour cette maquette");
            modifier = supprimer = detail = true;
        }
    }
    
    @Override
    public void listeUesSemestre() {
        if (selectedSemestre == null) {
            ues = ueDao.findAllUe();
            return;
        }
        ues = ueDao.findUeBySemestre(selectedSemestre.getCodesemestre());
        if (ues.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun ue enregistré pour cette semestre");
            modifier = supprimer = detail = true;
        }
    }
    
    
    @Override
    public void listeEcsUe() {
        if (selectUe == null) {
            ecs = ecDao.findAllEc();
            return;
        }
        ecs = ecDao.findEcByUe(selectUe.getCodeue());
        if (ecs.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune matiere enregistré pour cette unite d'enseignement");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeMaquetteClasse() {
        if (selectedClasse == null) {
            maquettes = maquetteDao.findAllMaquette();
            return;
        }
        maquettes = maquetteDao.findMaquetteByClasse(selectedClasse.getCodeclasse());
        if (maquettes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune maquette enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeCoursEc() {
        if (selectedEc == null) {
            courses = coursDao.findAllCours();
            return;
        }
        courses = coursDao.findCoursByEc(selectedEc.getCodematiere());
        if (courses.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun Cours enregistré pour cette matiere");
            modifier = supprimer = detail = true;
        }
    }
    @Override
    public void imprimerCoursPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerCoursHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
        
    }
    
