/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.evaluer;


import com.mycompany.bean.absence.*;
import com.mycompany.entities.Absence;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Evaluer;
import com.mycompany.entities.Filiere;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.EvaluerDao;
import com.nycompany.dao.FiliereDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractEvaluerCtl {
    
    
    protected EvaluerDao evaluerDao;
    
    protected List<Evaluer> evaluers;
    
    protected StringBuffer evaluersTableHtml = new StringBuffer("pas encore implementé") ;
    protected Evaluer selectedEvaluer;
    protected Evaluer evaluer;
    
    protected EtablissementDao etablissementDao;
    protected Etablissement selectedEtablissement;
    protected List<Etablissement> etablissements;
    
    protected ClasseDao classeDao;
    protected Classe selectedClasse;
    protected List<Classe> classes;
    
    protected FiliereDao filiereDao;
    protected Filiere selectedFiliere;
    protected List<Filiere> filieres;
    
    protected DomaineDao domaineDao;
    protected List<Domaine> domaines;
    protected Domaine selectedDomaine;
    
    protected EtudiantDao etudiantDao ;
    protected List<Etudiant> etudiants;
    protected Etudiant selectedEtudiant;
    

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    

    public EvaluerDao getEvaluerDao() {
        return evaluerDao;
    }

    public void setEvaluerDao(EvaluerDao evaluerDao) {
        this.evaluerDao = evaluerDao;
    }

    public List<Evaluer> getEvaluers() {
        return evaluers;
    }

    public void setEvaluers(List<Evaluer> evaluers) {
        this.evaluers = evaluers;
    }

    public StringBuffer getEvaluersTableHtml() {
        return evaluersTableHtml;
    }

    public void setEvaluersTableHtml(StringBuffer evaluersTableHtml) {
        this.evaluersTableHtml = evaluersTableHtml;
    }

    public Evaluer getSelectedEvaluer() {
        return selectedEvaluer;
    }

    public void setSelectedEvaluer(Evaluer selectedEvaluer) {
        this.selectedEvaluer = selectedEvaluer;
        if (selectedEvaluer == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Evaluer getEvaluer() {
        return evaluer;
    }

    public void setEvaluer(Evaluer evaluer) {
        this.evaluer = evaluer;
    }

    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = evaluerDao.findAllEvaluer().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    public EtablissementDao getEtablissementDao() {
        return etablissementDao;
    }

    public void setEtablissementDao(EtablissementDao etablissementDao) {
        this.etablissementDao = etablissementDao;
    }

    public Etablissement getSelectedEtablissement() {
        return selectedEtablissement;
    }

    public void setSelectedEtablissement(Etablissement selectedEtablissement) {
        this.selectedEtablissement = selectedEtablissement;
    }

    public List<Etablissement> getEtablissements() {
        return etablissements;
    }

    public void setEtablissements(List<Etablissement> etablissements) {
        this.etablissements = etablissements;
    }

    public ClasseDao getClasseDao() {
        return classeDao;
    }

    public void setClasseDao(ClasseDao classeDao) {
        this.classeDao = classeDao;
    }

    public Classe getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classe selectedClasse) {
        this.selectedClasse = selectedClasse;
    }

    public List<Classe> getClasses() {
        return classes;
    }

    public void setClasses(List<Classe> classes) {
        this.classes = classes;
    }

    public FiliereDao getFiliereDao() {
        return filiereDao;
    }

    public void setFiliereDao(FiliereDao filiereDao) {
        this.filiereDao = filiereDao;
    }

    public Filiere getSelectedFiliere() {
        return selectedFiliere;
    }

    public void setSelectedFiliere(Filiere selectedFiliere) {
        this.selectedFiliere = selectedFiliere;
    }

    public List<Filiere> getFilieres() {
        return filieres;
    }

    public void setFilieres(List<Filiere> filieres) {
        this.filieres = filieres;
    }

    public EtudiantDao getEtudiantDao() {
        return etudiantDao;
    }

    public void setEtudiantDao(EtudiantDao etudiantDao) {
        this.etudiantDao = etudiantDao;
    }

    public List<Etudiant> getEtudiants() {
        return etudiants;
    }

    public void setEtudiants(List<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }

    public Etudiant getSelectedEtudiant() {
        return selectedEtudiant;
    }

    public void setSelectedEtudiant(Etudiant selectedEtudiant) {
        this.selectedEtudiant = selectedEtudiant;
    }

    public DomaineDao getDomaineDao() {
        return domaineDao;
    }

    public void setDomaineDao(DomaineDao domaineDao) {
        this.domaineDao = domaineDao;
    }

    public List<Domaine> getDomaines() {
        return domaines;
    }

    public void setDomaines(List<Domaine> domaines) {
        this.domaines = domaines;
    }

    public Domaine getSelectedDomaine() {
        return selectedDomaine;
    }

    public void setSelectedDomaine(Domaine selectedDomaine) {
        this.selectedDomaine = selectedDomaine;
    }
    
    
}
