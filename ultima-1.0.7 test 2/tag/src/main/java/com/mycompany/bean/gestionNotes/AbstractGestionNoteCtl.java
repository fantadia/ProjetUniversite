/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.gestionNotes;


import com.mycompany.bean.evaluer.*;
import com.mycompany.bean.absence.*;
import com.mycompany.entities.Absence;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Evaluer;
import com.mycompany.entities.Filiere;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.EvaluerDao;
import com.nycompany.dao.FiliereDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractGestionNoteCtl {
    
    
    protected EvaluerDao evaluerDao;
    
    protected List<Evaluer> evaluers;
    
    protected StringBuffer evaluersTableHtml = new StringBuffer("pas encore implementé") ;
    protected Evaluer selectedEvaluer;
    protected Evaluer evaluer;
    

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;
    

    public EvaluerDao getEvaluerDao() {
        return evaluerDao;
    }

    public void setEvaluerDao(EvaluerDao evaluerDao) {
        this.evaluerDao = evaluerDao;
    }

    public List<Evaluer> getEvaluers() {
        return evaluers;
    }

    public void setEvaluers(List<Evaluer> evaluers) {
        this.evaluers = evaluers;
    }

    public StringBuffer getEvaluersTableHtml() {
        return evaluersTableHtml;
    }

    public void setEvaluersTableHtml(StringBuffer evaluersTableHtml) {
        this.evaluersTableHtml = evaluersTableHtml;
    }

    public Evaluer getSelectedEvaluer() {
        return selectedEvaluer;
    }

    public void setSelectedEvaluer(Evaluer selectedEvaluer) {
        this.selectedEvaluer = selectedEvaluer;
        if (selectedEvaluer == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Evaluer getEvaluer() {
        return evaluer;
    }

    public void setEvaluer(Evaluer evaluer) {
        this.evaluer = evaluer;
    }

    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = evaluerDao.findAllEvaluer().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
    
}
