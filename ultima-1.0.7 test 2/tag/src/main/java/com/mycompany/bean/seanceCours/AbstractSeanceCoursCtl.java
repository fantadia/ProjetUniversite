/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.seanceCours;

import com.mycompany.entities.SeanceCours;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.SeanceCoursDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractSeanceCoursCtl {
    
    
    protected SeanceCoursDao seanceCoursDao;
    protected AbsenceDao absenceDao;
    
   
    protected List<SeanceCours> seanceCourses;
    
    protected StringBuffer seanceCoursTableHtml = new StringBuffer("pas encore implementé") ;
    protected SeanceCours selectedSeanceCours;
    protected SeanceCours seanceCours;
    
    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public SeanceCoursDao getSeanceCoursDao() {
        return seanceCoursDao;
    }

    public void setSeanceCoursDao(SeanceCoursDao seanceCoursDao) {
        this.seanceCoursDao = seanceCoursDao;
    }

    public AbsenceDao getAbsenceDao() {
        return absenceDao;
    }

    public void setAbsenceDao(AbsenceDao absenceDao) {
        this.absenceDao = absenceDao;
    }

    public List<SeanceCours> getSeanceCourses() {
        return seanceCourses;
    }

    public void setSeanceCourses(List<SeanceCours> seanceCourses) {
        this.seanceCourses = seanceCourses;
    }

    public StringBuffer getSeanceCoursTableHtml() {
        return seanceCoursTableHtml;
    }

    public void setSeanceCoursTableHtml(StringBuffer seanceCoursTableHtml) {
        this.seanceCoursTableHtml = seanceCoursTableHtml;
    }

    public SeanceCours getSelectedSeanceCours() {
        return selectedSeanceCours;
    }

    public void setSelectedSeanceCours(SeanceCours selectedSeanceCours) {
        this.selectedSeanceCours = selectedSeanceCours;
        if (selectedSeanceCours == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public SeanceCours getSeanceCours() {
        return seanceCours;
    }

    public void setSeanceCours(SeanceCours seanceCours) {
        this.seanceCours = seanceCours;
    }

    

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = seanceCoursDao.findAllSeanceCours().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
