/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.gestionNotes;

import com.mycompany.entities.Ec;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Ue;

/**
 *
 * @author macbookpro
 */
public interface gestionNotesDao {
    
    public double moyenneMatiere(String idEtudiant,String idEc);
    public double moyenneUe(String idEtudiant,String idUe);
    
    
}
