/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.facture;


import com.mycompany.entities.Facture;
import com.mycompany.entities.Filiere;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.FactureDao;
import com.nycompany.dao.FiliereDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractFactureCtl {
    
    
    protected FactureDao factureDao ;
    
    protected List<Facture> factures;
    
    protected StringBuffer facturesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Facture selectedFacture;
    protected Facture facture;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public FactureDao getFactureDao() {
        return factureDao;
    }

    public void setFactureDao(FactureDao factureDao) {
        this.factureDao = factureDao;
    }

    public List<Facture> getFactures() {
        return factures;
    }

    public void setFactures(List<Facture> factures) {
        this.factures = factures;
    }

    public StringBuffer getFacturesTableHtml() {
        return facturesTableHtml;
    }

    public void setFacturesTableHtml(StringBuffer facturesTableHtml) {
        this.facturesTableHtml = facturesTableHtml;
    }

    public Facture getSelectedFacture() {
        return selectedFacture;
    }

    public void setSelectedFacture(Facture selectedFacture) {
        this.selectedFacture = selectedFacture;
        if (selectedFacture == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Facture getFacture() {
        return facture;
    }

    public void setFacture(Facture facture) {
        this.facture = facture;
    }

   
   
    
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = factureDao.findAllFacture().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
