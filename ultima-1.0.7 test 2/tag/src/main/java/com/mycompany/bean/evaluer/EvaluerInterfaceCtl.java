/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.evaluer;

import com.mycompany.bean.absence.*;




/**
 *
 * @author macbookpro
 */
public interface EvaluerInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerEvaluer();
    public void modifierEvaluer();
    
    public void listeEtudiantsClasse();
    public void listeClassesFiliere();
    public void listeFilieresDomaine();
    public void listeDomainesEtablissement();
    public void listeEvaluersEtudiant();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerEvaluer();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerEvaluerPdf();
    
    public void imprimerEvaluerHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
