/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.cours;





import com.mycompany.entities.Classe;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Ec;
import com.mycompany.entities.EmploisDuTemps;
import com.mycompany.entities.Maquette;
import com.mycompany.entities.Semestre;
import com.mycompany.entities.Ue;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractCoursCtl {
    
    
    protected CoursDao coursDao;
    protected AbsenceDao absenceDao;
    
   
    protected List<Cours> courses;
    
    protected StringBuffer coursesTableHtml = new StringBuffer("pas encore implementé") ;
    protected Cours selectedCours;
    protected Cours cours;
    
    protected ClasseDao classeDao;
    protected Classe selectedClasse;
    protected List<Classe> classes;
    
    protected UeDao ueDao ;
    protected Ue selectUe;
    protected List<Ue> ues;
    
    protected MaquetteDao maquetteDao ;
    protected List<Maquette> maquettes;
    protected Maquette selectedMaquette;
    
    protected SemestreDao semestreDao;
    protected List<Semestre> semestres;
    protected Semestre selectedSemestre;
    
     
    protected EcDao ecDao;
    protected Ec selectedEc;
    protected List<Ec> ecs;

    protected EmploisDuTempsDao emploisDuTempsDao;
    protected EmploisDuTemps emploisDuTemps;
    protected List<EmploisDuTemps> emploisDuTempses;
    
    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    
    public EmploisDuTempsDao getEmploisDuTempsDao() {
        return emploisDuTempsDao;
    }

    public void setEmploisDuTempsDao(EmploisDuTempsDao emploisDuTempsDao) {
        this.emploisDuTempsDao = emploisDuTempsDao;
    }

    public EmploisDuTemps getEmploisDuTemps() {
        return emploisDuTemps;
    }

    public void setEmploisDuTemps(EmploisDuTemps emploisDuTemps) {
        this.emploisDuTemps = emploisDuTemps;
    }

    public List<EmploisDuTemps> getEmploisDuTempses() {
        return emploisDuTempses;
    }

    public void setEmploisDuTempses(List<EmploisDuTemps> emploisDuTempses) {
        this.emploisDuTempses = emploisDuTempses;
    }
    

    public CoursDao getCoursDao() {
        return coursDao;
    }

    public void setCoursDao(CoursDao coursDao) {
        this.coursDao = coursDao;
    }

    public AbsenceDao getAbsenceDao() {
        return absenceDao;
    }

    public void setAbsenceDao(AbsenceDao absenceDao) {
        this.absenceDao = absenceDao;
    }
    
    

    public List<Cours> getCourses() {
        return courses;
    }

    public void setCourses(List<Cours> courses) {
        this.courses = courses;
    }

    public StringBuffer getCoursesTableHtml() {
        return coursesTableHtml;
    }

    public void setCoursesTableHtml(StringBuffer coursesTableHtml) {
        this.coursesTableHtml = coursesTableHtml;
    }

    public Cours getSelectedCours() {
        return selectedCours;
    }

    public void setSelectedCours(Cours selectedCours) {
        this.selectedCours = selectedCours;
        if (selectedCours == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = coursDao.findAllCours().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }

    public ClasseDao getClasseDao() {
        return classeDao;
    }

    public void setClasseDao(ClasseDao classeDao) {
        this.classeDao = classeDao;
    }

    public Classe getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classe selectedClasse) {
        this.selectedClasse = selectedClasse;
    }

    public List<Classe> getClasses() {
        return classes;
    }

    public void setClasses(List<Classe> classes) {
        this.classes = classes;
    }

    public UeDao getUeDao() {
        return ueDao;
    }

    public void setUeDao(UeDao ueDao) {
        this.ueDao = ueDao;
    }

    public Ue getSelectUe() {
        return selectUe;
    }

    public void setSelectUe(Ue selectUe) {
        this.selectUe = selectUe;
    }

    public List<Ue> getUes() {
        return ues;
    }

    public void setUes(List<Ue> ues) {
        this.ues = ues;
    }

    public MaquetteDao getMaquetteDao() {
        return maquetteDao;
    }

    public void setMaquetteDao(MaquetteDao maquetteDao) {
        this.maquetteDao = maquetteDao;
    }

    public List<Maquette> getMaquettes() {
        return maquettes;
    }

    public void setMaquettes(List<Maquette> maquettes) {
        this.maquettes = maquettes;
    }

    public Maquette getSelectedMaquette() {
        return selectedMaquette;
    }

    public void setSelectedMaquette(Maquette selectedMaquette) {
        this.selectedMaquette = selectedMaquette;
    }

    public SemestreDao getSemestreDao() {
        return semestreDao;
    }

    public void setSemestreDao(SemestreDao semestreDao) {
        this.semestreDao = semestreDao;
    }

    public List<Semestre> getSemestres() {
        return semestres;
    }

    public void setSemestres(List<Semestre> semestres) {
        this.semestres = semestres;
    }

    public Semestre getSelectedSemestre() {
        return selectedSemestre;
    }

    public void setSelectedSemestre(Semestre selectedSemestre) {
        this.selectedSemestre = selectedSemestre;
    }

    public EcDao getEcDao() {
        return ecDao;
    }

    public void setEcDao(EcDao ecDao) {
        this.ecDao = ecDao;
    }

    public Ec getSelectedEc() {
        return selectedEc;
    }

    public void setSelectedEc(Ec selectedEc) {
        this.selectedEc = selectedEc;
    }

    public List<Ec> getEcs() {
        return ecs;
    }

    public void setEcs(List<Ec> ecs) {
        this.ecs = ecs;
    }
    
    
}
