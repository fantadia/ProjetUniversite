/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.gestionNotes;

import com.mycompany.entities.Ec;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Evaluer;
import com.mycompany.entities.Ue;
import com.nycompany.dao.EvaluerDao;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
public class gestionNotesDaoImpl extends AbstractGestionNoteCtl implements gestionNotesDao{
    
    @PostConstruct
    private void initNote(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        evaluerDao = (EvaluerDao) context.getBean("evaluerDao");
        evaluer = (Evaluer) context.getBean("evaluer");
        selectedEvaluer = (Evaluer) context.getBean("evaluer");
        evaluers = evaluerDao.findAllEvaluer();
    
    }

    @Override
    public double moyenneMatiere(String idEtudiant, String idEc) {
        List<Evaluer> evaluers = new ArrayList<>();
        evaluers = evaluerDao.findEvaluerByEtudiantMatiere(idEtudiant, idEc);
        int som = 0;
        int cpt = 0;
        double moyenne = 0.0;
        for(int i = 0; i < evaluers.size(); i++) 
        {
            som = som + evaluers.get(i).getNote();
            cpt = cpt+1;
        }
        if(cpt!=0)
        {
            moyenne = som/cpt;
            return moyenne;
        }
        else
            return moyenne;
           
    }

    @Override
    public double moyenneUe(String idEtudiant, String idEc) {
        
        return 0.0;
    }
    
}
