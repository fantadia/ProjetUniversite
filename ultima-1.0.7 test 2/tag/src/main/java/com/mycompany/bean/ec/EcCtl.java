/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ec;


import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Ec;
import com.mycompany.entities.Evaluer;
import com.mycompany.entities.Maquette;
import com.mycompany.entities.Semestre;
import com.mycompany.entities.Ue;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.EvaluerDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="ecCtl")
@ViewScoped

public class EcCtl extends AbstractEcCtl implements EcInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public EcCtl() {
    }
    
    @PostConstruct
    private void initEc()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        ecDao = (EcDao) context.getBean("ecDao");
        evaluerDao = (EvaluerDao) context.getBean("evaluerDao");
        coursDao = (CoursDao) context.getBean("coursDao");
        ec = (Ec) context.getBean("ec");
        selectedEc = (Ec) context.getBean("ec");
        ecs = ecDao.findAllEc();
        
        ueDao = (UeDao) context.getBean("ueDao");
        ue = (Ue) context.getBean("ue");
        selectUe = (Ue) context.getBean("ue");
        ues = ueDao.findAllUe();
    
        semestreDao = (SemestreDao) context.getBean("semestreDao");
        selectedSemestre = (Semestre) context.getBean("semestre");
        semestres = semestreDao.findAllSemestre();
        
        maquetteDao = (MaquetteDao) context.getBean("maquetteDao");
        selectedMaquette = (Maquette) context.getBean("maquette");
        maquettes = maquetteDao.findAllMaquette();
        
        classeDao = (ClasseDao) context.getBean("classeDao");
        selectedClasse = (Classe) context.getBean("classe");
        classes = classeDao.findAllClasse();
    }

    @Override
    public void enregistrerEc() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        Ec e = ecDao.findEcByLibelle(ec.getLibelleMatiere());
        if(e==null){
            ecDao.createEc(ec);
            initEc();
            JsfUtil.addSuccessMessage("l' Ec à été bien créé");
            modifier = supprimer = detail = true;
        }
        else
            JsfUtil.addSuccessMessage("l'Ec existe deja");
        
    }

    @Override
    public void modifierEc() {
        
        if (selectedEc == null || selectedEc.getCodematiere()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       Ec e = ecDao.findEcByLibelle(selectedEc.getLibelleMatiere());
        if(e==null){
             ecDao.updateEc(selectedEc);
             JsfUtil.addSuccessMessage("Ec Modifier avec succes");
        }
        else
            JsfUtil.addSuccessMessage("l'Ec existe deja");
        
       
    }

    @Override
    public void supprimerEc() {
        
         if(selectedEc == null || selectedEc.getCodematiere()== null)
            return;
        List<Cours>  cours= coursDao.findCoursByEc(selectedEc.getCodematiere());
        List<Evaluer> evaluers = evaluerDao.findEvaluerByEc(selectedEc.getCodematiere());
         if((!cours.isEmpty())||(!evaluers.isEmpty()))
         {
             JsfUtil.addErrorMessage("ce Ec ne peut pas etre supprimer,car il est utilise dans une autre operation");
         }
       
            ecDao.deleteEc(selectedEc);
            JsfUtil.addErrorMessage("Ce Ec à été supprimée");
            modifier = supprimer = detail = true;
    }
    
    @Override
    public void listeEcsUe() {
        if (selectUe == null) {
            ecs = ecDao.findAllEc();
            return;
        }
        ecs = ecDao.findEcByUe(selectUe.getCodeue());
        if (semestres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun Ec enregistré pour cette Ue");
            modifier = supprimer = detail = true;
        }
    }
    
    @Override
    public void listeSemestresMaquette() {
        if (selectedMaquette == null) {
            semestres = semestreDao.findAllSemestre();
            return;
        }
        semestres = semestreDao.findSemestreByMaquette(selectedMaquette.getCodemaquette());
        if (semestres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun semestre enregistré pour cette maquette");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeUesSemestre() {
        if (selectedSemestre == null) {
            ues = ueDao.findAllUe();
            return;
        }
        ues = ueDao.findUeBySemestre(selectedSemestre.getCodesemestre());
        if (ues.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun ue enregistré pour cette semestre");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeMaquetteClasse() {
        if (selectedClasse == null) {
            maquettes = maquetteDao.findAllMaquette();
            return;
        }
        maquettes = maquetteDao.findMaquetteByClasse(selectedClasse.getCodeclasse());
        if (maquettes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune maquette enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
    }
    @Override
    public void imprimerEcPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEcHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
