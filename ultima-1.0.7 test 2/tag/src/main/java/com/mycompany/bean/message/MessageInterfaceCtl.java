/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.message;

import com.mycompany.bean.ec.*;






/**
 *
 * @author macbookpro
 */
public interface MessageInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerMessage();
    public void modifierMessage();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerMessage();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerMessagePdf();
    
    public void imprimerMessageHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
