/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.responsable;


import beans.util.JsfUtil;
import com.mycompany.entities.EmploisDuTemps;
import com.mycompany.entities.Etablissement;

import com.mycompany.entities.Responsable;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.ResponsableDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="responsableCtl")
@ViewScoped

public class ResponsableCtl extends AbstractResponsableCtl implements ResponsableInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public ResponsableCtl() {
    }
    
    @PostConstruct
    private void initResponsable()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        utilisateur =(Utilisateur) context.getBean("utilisateur");
        responsableDao = (ResponsableDao) context.getBean("responsableDao");
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedUtilisateur =(Utilisateur) context.getBean("utilisateur");
        emploisDuTempsDao = (EmploisDuTempsDao) context.getBean("emploisdutempsDao");
        responsable = (Responsable) context.getBean("responsable");
        selectedResponsable = (Responsable) context.getBean("responsable");
        responsables = responsableDao.findAll();
    }

    @Override
    public void enregistrerResponsable() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        utilisateur.setCodeutilisateur("500");
        utilisateurDao.createUtilisateur(utilisateur);
        //jecrase utilisateur et je le recupere a nouveau
        //depuis la BD 
        utilisateur=utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        //j'initialise le responsable a inserer
        responsable.setUtilisateur(utilisateur);
        
        responsableDao.saveResponsable(responsable);
        initResponsable();
        JsfUtil.addSuccessMessage("le responsable à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierResponsable() {
        
        if (selectedResponsable == null || selectedResponsable.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       selectedUtilisateur.setNom(selectedResponsable.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedResponsable.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedResponsable.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedResponsable.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedResponsable.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedResponsable.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedResponsable.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedResponsable.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedResponsable.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedResponsable.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedResponsable.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedResponsable.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedResponsable.getUtilisateur().getBoitePostale());
       
      
        utilisateurDao.updateUtilisateur(selectedUtilisateur);
        
        
       // responsableDao.updateResponsable(selectedResponsable);
       //selectedEnseignant.setUtilisateur(selectedUtilisateur);
       //enseignantDao.updateEnseignant(selectedEnseignant);
       JsfUtil.addSuccessMessage("Responsable modifier avec succes");
    }

    @Override
    public void supprimerResponsable() {
        
         if(selectedResponsable == null || selectedResponsable.getCodeutilisateur()== null)
            return;
         
         List<EmploisDuTemps> emp = emploisDuTempsDao.findEmploisDuTempsByResponsable(selectedResponsable.getCodeutilisateur());
         List<Etablissement> etps = etablissementDao.findEtablissementByIdResponsable(selectedResponsable.getCodeutilisateur());
         if((!emp.isEmpty())||(!etps.isEmpty()))
         {
              JsfUtil.addErrorMessage("Cet responsable ne peut pas etre supprimée, il est deja utilise dans une operation");
         }
            
            selectedUtilisateur.setNom(selectedResponsable.getUtilisateur().getNom());
            selectedUtilisateur.setPrenom(selectedResponsable.getUtilisateur().getPrenom());
            selectedUtilisateur.setCin(selectedResponsable.getUtilisateur().getCin());
            selectedUtilisateur.setEmail(selectedResponsable.getUtilisateur().getEmail());
            selectedUtilisateur.setPays(selectedResponsable.getUtilisateur().getPays());
            selectedUtilisateur.setLogin(selectedResponsable.getUtilisateur().getLogin());
            selectedUtilisateur.setAdresse(selectedResponsable.getUtilisateur().getAdresse());
            selectedUtilisateur.setPassword(selectedResponsable.getUtilisateur().getPassword());
            selectedUtilisateur.setVille(selectedResponsable.getUtilisateur().getVille());
            selectedUtilisateur.setSituationMatrimonial(selectedResponsable.getUtilisateur().getSituationMatrimonial());
            selectedUtilisateur.setTelephone(selectedResponsable.getUtilisateur().getTelephone());
            selectedUtilisateur.setCodeutilisateur(selectedResponsable.getUtilisateur().getCodeutilisateur());
            selectedUtilisateur.setBoitePostale(selectedResponsable.getUtilisateur().getBoitePostale());
         
         
            responsableDao.deleteResponsable(selectedResponsable);
            
            utilisateurDao.deleteUtilisateur(selectedUtilisateur);

            JsfUtil.addErrorMessage("Cette responsable à été supprimé");
            modifier = supprimer = detail = true;}

    @Override
    public void imprimerResponsablePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerResponsableHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
