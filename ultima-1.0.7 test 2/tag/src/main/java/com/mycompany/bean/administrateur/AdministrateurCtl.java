/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.administrateur;

import beans.util.JsfUtil;
import com.mycompany.entities.Administrateur;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.AdministrateurDao;
import com.nycompany.dao.UtilisateurDao;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="administrateurCtl")
@ViewScoped

public class AdministrateurCtl extends AbstractAdministrateurCtl implements AdministrateurInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public AdministrateurCtl() {
    }
    
    @PostConstruct
    private void initAdmin()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        administrateurDao = (AdministrateurDao) context.getBean("administrateurDao");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        utilisateur =(Utilisateur) context.getBean("utilisateur");
        selectedUtilisateur =(Utilisateur) context.getBean("utilisateur");
        administrateur = (Administrateur) context.getBean("administrateur");
        selectedAdministrateur = (Administrateur) context.getBean("administrateur");
        administrateurs = administrateurDao.findAllAdministrateur();
    }

    @Override
    public void enregistrerAdministrateur() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        /*
            Avant tout faut enregistrer ds utilisateur avec comme identifiant 700
            ce qui veut dire que dans la page jsf faut utiliser l'objet utilisateur
            pour preparer les donnees a inserer
            Ensuite l'objet enseignant sera creer a partir de utilisateur
        */
        utilisateur.setCodeutilisateur("900");
        utilisateurDao.createUtilisateur(utilisateur);
        //jecrase utilisateur et je le recupere a nouveau
        //depuis la BD 
        utilisateur=utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        //j'initialise l'enseignant a inserer
        administrateur.setUtilisateur(utilisateur);
        administrateurDao.createAdministrateur(administrateur);
        initAdmin();
        JsfUtil.addSuccessMessage("l'administrateur à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierAdministrateur() {
        
        if (selectedAdministrateur == null || selectedAdministrateur.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       selectedUtilisateur.setNom(selectedAdministrateur.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedAdministrateur.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedAdministrateur.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedAdministrateur.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedAdministrateur.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedAdministrateur.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedAdministrateur.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedAdministrateur.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedAdministrateur.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedAdministrateur.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedAdministrateur.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedAdministrateur.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedAdministrateur.getUtilisateur().getBoitePostale());
       
       utilisateurDao.updateUtilisateur(selectedUtilisateur);
       //selectedEnseignant.setUtilisateur(selectedUtilisateur);
       //enseignantDao.updateEnseignant(selectedEnseignant);
       JsfUtil.addSuccessMessage("Admin modifier avec succes");
    }

    @Override
    public void supprimerAdministrateur() {
        
         if(selectedAdministrateur == null || selectedAdministrateur.getCodeutilisateur()== null)
            return;
         
         
       selectedUtilisateur.setNom(selectedAdministrateur.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedAdministrateur.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedAdministrateur.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedAdministrateur.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedAdministrateur.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedAdministrateur.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedAdministrateur.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedAdministrateur.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedAdministrateur.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedAdministrateur.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedAdministrateur.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedAdministrateur.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedAdministrateur.getUtilisateur().getBoitePostale());
         
           
           administrateurDao.deleteAdministrateur(selectedAdministrateur);
           utilisateurDao.deleteUtilisateur(selectedUtilisateur);
          
           JsfUtil.addErrorMessage("l'Administrateur à été supprimé");
           modifier = supprimer = detail = true;}

    @Override
    public void imprimerAdministrateurPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerAdministrateurHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
