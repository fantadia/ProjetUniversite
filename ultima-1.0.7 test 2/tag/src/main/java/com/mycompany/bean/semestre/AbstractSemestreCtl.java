/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.semestre;




import com.mycompany.entities.Classe;
import com.mycompany.entities.Maquette;
import com.mycompany.entities.Semestre;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.EmploisDuTempsDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractSemestreCtl {
    
    
    protected SemestreDao semestreDao ;
    protected UeDao ueDao;
    protected EmploisDuTempsDao emploisDuTempsDao;
    
    protected MaquetteDao maquetteDao ;
    protected List<Maquette> maquettes;
    protected Maquette selectedMaquette;
    
    protected ClasseDao classeDao;
    protected Classe selectedClasse;
    protected List<Classe> classes;
    
   
    protected List<Semestre> semestres;
    
    protected StringBuffer semestresTableHtml = new StringBuffer("pas encore implementé") ;
    protected Semestre selectedSemestre;
    protected Semestre semestre;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public ClasseDao getClasseDao() {
        return classeDao;
    }

    public void setClasseDao(ClasseDao classeDao) {
        this.classeDao = classeDao;
    }

    public Classe getSelectedClasse() {
        return selectedClasse;
    }

    public void setSelectedClasse(Classe selectedClasse) {
        this.selectedClasse = selectedClasse;
    }

    public List<Classe> getClasses() {
        return classes;
    }

    public void setClasses(List<Classe> classes) {
        this.classes = classes;
    }
    
    

    public MaquetteDao getMaquetteDao() {
        return maquetteDao;
    }

    public void setMaquetteDao(MaquetteDao maquetteDao) {
        this.maquetteDao = maquetteDao;
    }

    public List<Maquette> getMaquettes() {
        return maquettes;
    }

    public void setMaquettes(List<Maquette> maquettes) {
        this.maquettes = maquettes;
    }

    public Maquette getSelectedMaquette() {
        return selectedMaquette;
    }

    public void setSelectedMaquette(Maquette selectedMaquette) {
        this.selectedMaquette = selectedMaquette;
    }
    
    

    public SemestreDao getSemestreDao() {
        return semestreDao;
    }

    public void setSemestreDao(SemestreDao semestreDao) {
        this.semestreDao = semestreDao;
    }

    public EmploisDuTempsDao getEmploisDuTempsDao() {
        return emploisDuTempsDao;
    }

    public void setEmploisDuTempsDao(EmploisDuTempsDao emploisDuTempsDao) {
        this.emploisDuTempsDao = emploisDuTempsDao;
    }

    public List<Semestre> getSemestres() {
        return semestres;
    }

    public void setSemestres(List<Semestre> semestres) {
        this.semestres = semestres;
    }

    public StringBuffer getSemestresTableHtml() {
        return semestresTableHtml;
    }

    public void setSemestresTableHtml(StringBuffer semestresTableHtml) {
        this.semestresTableHtml = semestresTableHtml;
    }

    public Semestre getSelectedSemestre() {
        return selectedSemestre;
    }

    public void setSelectedSemestre(Semestre selectedSemestre) 
    {
        this.selectedSemestre = selectedSemestre;
        if (selectedSemestre == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public UeDao getUeDao() {
        return ueDao;
    }

    public void setUeDao(UeDao ueDao) {
        this.ueDao = ueDao;
    }
    
    

    public Semestre getSemestre() {
        return semestre;
    }

    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }

    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = semestreDao.findAllSemestre().isEmpty();
        return imprimer;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
