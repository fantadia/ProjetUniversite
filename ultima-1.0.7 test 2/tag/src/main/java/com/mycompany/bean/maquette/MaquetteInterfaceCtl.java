/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.maquette;





/**
 *
 * @author macbookpro
 */
public interface MaquetteInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerMaquette();
    public void modifierMaquette();
    
    public void listeMaquetteClasse();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerMaquette();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerMaquettePdf();
    
    public void imprimerMaquetteHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
