/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.paiement;

import com.mycompany.bean.facture.*;
import com.mycompany.bean.filiere.*;



/**
 *
 * @author macbookpro
 */
public interface PaiementInterfaceCtl {
    
    /**
     * cette fonction permet de save les Niveau
     */
    public void enregistrerPaiement();
    public void modifierPaiement();
    
    
    public void listeEtudiantsClasse();
    public void listeClassesFiliere();
    public void listeFilieresDomaine();
    public void listeDomainesEtablissement();
    public void listePaiementsEtudiant();
    /**
     * 
     * Supprimer les Niveau
     * 
     */
    public void supprimerPaiement();
    /**
     * 
     * Méthode d'impression
     *
     */
    public void imprimerPaiementPdf();
    
    public void imprimerPaiementHtml();
    
    public String getComponentMessages(String filiereComponent, String defaultMessage);
    
}
