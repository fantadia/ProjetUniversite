/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.message;


import com.mycompany.bean.ec.*;
import beans.util.JsfUtil;
import com.mycompany.entities.Cours;
import com.mycompany.entities.Ec;
import com.mycompany.entities.Message;
import com.nycompany.dao.CoursDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.MessageDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="messageCtl")
@ViewScoped

public class MessageCtl extends AbstractMessageCtl implements MessageInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public MessageCtl() {
    }
    
    @PostConstruct
    private void initEc()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        messageDao = (MessageDao) context.getBean("messageDao");
        message = (Message) context.getBean("message");
        selectedMessage = (Message) context.getBean("message");
        messages = messageDao.findAllMessage();
    }

    @Override
    public void enregistrerMessage() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        messageDao.createMessage(message);
        initEc();
        JsfUtil.addSuccessMessage("le message à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierMessage() {
        
        if (selectedMessage == null || selectedMessage.getCodemessage()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        messageDao.updateMessage(selectedMessage);
        JsfUtil.addSuccessMessage("Message Modifier avec succes");
    }

    @Override
    public void supprimerMessage() {
        
         if(selectedMessage == null || selectedMessage.getCodemessage()== null)
            return;
         
         messageDao.deleteMessage(selectedMessage);
         JsfUtil.addErrorMessage("Ce Message à été supprimée");
         modifier = supprimer = detail = true;
    }
    @Override
    public void imprimerMessagePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerMessageHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    }
    
