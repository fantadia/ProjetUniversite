/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.etudiant;


import beans.util.JsfUtil;
import com.mycompany.entities.Absence;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Domaine;
import com.mycompany.entities.Dossier;
import com.mycompany.entities.Etablissement;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Evaluer;
import com.mycompany.entities.Filiere;
import com.mycompany.entities.Utilisateur;
import com.nycompany.dao.AbsenceDao;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.DomaineDao;
import com.nycompany.dao.DossierDao;
import com.nycompany.dao.EtablissementDao;
import com.nycompany.dao.EtudiantDao;
import com.nycompany.dao.EvaluerDao;
import com.nycompany.dao.FiliereDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="etudiantCtl")
@ViewScoped
public class EtudiantCtl extends AbstractEtudiantCtl implements EtudiantInterfaceCtl {
    
    /**
     * Creates a new instance of EnseignantCtrl
     */
    public EtudiantCtl() {
    }
    
    @PostConstruct
    private void initEtudiant()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        etudiantDao = (EtudiantDao) context.getBean("etudiantDao");
        selectedEtudiant = (Etudiant) context.getBean("etudiant");
        etudiants = etudiantDao.findAllEtudiant();
        
        dossierDao =(DossierDao) context.getBean("dossierDao");
       
       
        absenceDao = (AbsenceDao) context.getBean("absenceDao");
        evaluerDao = (EvaluerDao) context.getBean("evaluerDao");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        utilisateur =(Utilisateur) context.getBean("utilisateur");
        selectedUtilisateur =(Utilisateur) context.getBean("utilisateur");
        etudiant = (Etudiant) context.getBean("etudiant");
        
        filiereDao =(FiliereDao) context.getBean("filiereDao");
        filieres =filiereDao.findAllFiliere();
        selectedFiliere = (Filiere) context.getBean("filiere");
        
        domaineDao = (DomaineDao) context.getBean("domaineDao");
        selectedDomaine = (Domaine) context.getBean("domaine");
        domaines = domaineDao.findAllDomaine();
        
        classeDao =(ClasseDao) context.getBean("classeDao");
        classes = classeDao.findAllClasse();
        selectedClasse = (Classe) context.getBean("classe");
        
        etablissementDao = (EtablissementDao) context.getBean("etablissementDao");
        selectedEtablissement = (Etablissement) context.getBean("etablissement");
        etablissements = etablissementDao.findAllEtablissement();
        
    }

    @Override
    public void enregistrerEtudiant() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        /*
            Avant tout faut enregistrer ds utilisateur avec comme identifiant 700
            ce qui veut dire que dans la page jsf faut utiliser l'objet utilisateur
            pour preparer les donnees a inserer
            Ensuite l'objet enseignant sera creer a partir de utilisateur
        */
        utilisateur.setCodeutilisateur("saliou");
 
        utilisateurDao.createUtilisateur(utilisateur);
        //jecrase utilisateur et je le recupere a nouveau
        //depuis la BD 
        
        
        utilisateur=utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        //j'initialise l'enseignant a inserer
        etudiant.setUtilisateur(utilisateur);
        //etudiant.setCodeutilisateur(utilisateur.getCodeutilisateur());
        etudiantDao.createEtudiant(etudiant);
        initEtudiant();
        JsfUtil.addSuccessMessage("l'etudiant à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierEtudiant() {
        
        if (selectedEtudiant == null || selectedEtudiant.getCodeutilisateur()== null) {
            return;
        }

       /*Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       selectedUtilisateur.setNom(selectedEtudiant.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedEtudiant.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedEtudiant.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedEtudiant.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedEtudiant.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedEtudiant.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedEtudiant.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedEtudiant.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedEtudiant.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedEtudiant.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedEtudiant.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedEtudiant.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedEtudiant.getUtilisateur().getBoitePostale());
       
       utilisateurDao.updateUtilisateur(selectedUtilisateur);
       etudiantDao.updateEtudiant(selectedEtudiant);
       //enseignantDao.updateEnseignant(selectedEnseignant);
       JsfUtil.addSuccessMessage("etudiant Modifier avec succes");
    }

    @Override
    public void supprimerEtudiant() {
        
         if(selectedEtudiant == null || selectedEtudiant.getCodeutilisateur()== null)
            return;
         List<Dossier> dossiers =  dossierDao.findDossierByEtudiant(selectedUtilisateur.getCodeutilisateur());
         List<Absence> absenc = absenceDao.findAbsenceByEtudiant(selectedUtilisateur.getCodeutilisateur());
         List<Evaluer> evaluer = evaluerDao.findEvaluerByEtudiant(selectedUtilisateur.getCodeutilisateur());
         if((!dossiers.isEmpty())||(!absenc.isEmpty())||(!evaluer.isEmpty()))
         {
             JsfUtil.addErrorMessage("Cet etudiant ne peut pas etre supprimée, il est deja utilise dans une operation");
         }
         
            selectedUtilisateur.setNom(selectedEtudiant.getUtilisateur().getNom());
            selectedUtilisateur.setPrenom(selectedEtudiant.getUtilisateur().getPrenom());
            selectedUtilisateur.setCin(selectedEtudiant.getUtilisateur().getCin());
            selectedUtilisateur.setEmail(selectedEtudiant.getUtilisateur().getEmail());
            selectedUtilisateur.setPays(selectedEtudiant.getUtilisateur().getPays());
            selectedUtilisateur.setLogin(selectedEtudiant.getUtilisateur().getLogin());
            selectedUtilisateur.setAdresse(selectedEtudiant.getUtilisateur().getAdresse());
            selectedUtilisateur.setPassword(selectedEtudiant.getUtilisateur().getPassword());
            selectedUtilisateur.setVille(selectedEtudiant.getUtilisateur().getVille());
            selectedUtilisateur.setSituationMatrimonial(selectedEtudiant.getUtilisateur().getSituationMatrimonial());
            selectedUtilisateur.setTelephone(selectedEtudiant.getUtilisateur().getTelephone());
            selectedUtilisateur.setCodeutilisateur(selectedEtudiant.getUtilisateur().getCodeutilisateur());
            selectedUtilisateur.setBoitePostale(selectedEtudiant.getUtilisateur().getBoitePostale());
         
         
          // selectedEtudiant.setUtilisateur(selectedUtilisateur);
          
           etudiantDao.deleteEtudiant(selectedEtudiant);
          
           utilisateurDao.deleteUtilisateur(selectedUtilisateur);
           
          
           JsfUtil.addErrorMessage("Cet etudiant à été supprimée");
           modifier = supprimer = detail = true;}

    @Override
    public void imprimerEtudiantPdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerEtudiantHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void listeEtudiantsClasse() {
        if (selectedClasse == null) {
            etudiants = etudiantDao.findAllEtudiant();
            return;
        }
        //etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        etudiants = etudiantDao.findEtudiantByClasse(selectedClasse.getCodeclasse());
        if (etudiants.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun etudiant enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
        
    }

    @Override
    public void listeClassesFiliere() {
        
        if (selectedFiliere == null) {
            classes = classeDao.findAllClasse();
            return;
        }
        classes = classeDao.findClasseByIdFiliere(selectedFiliere.getCodefiliere());
        if (classes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune Classe enregistré pour cette Filiere ");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void listeFilieresDomaine() {
        if (selectedDomaine == null) {
            filieres = filiereDao.findAllFiliere();
            return;
        }
        filieres = filiereDao.findFiliereByCodeDomaine(selectedDomaine.getCodedomaine());
        if (filieres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun filiere enregistré pour cette domaine");
            modifier = supprimer = detail = true;
        }
        
    } 

    @Override
    public void listeDomainesEtablissement() {
        if (selectedEtablissement == null) {
            domaines = domaineDao.findAllDomaine();
            return;
        }
        domaines = domaineDao.findDomaineByEtablissement(selectedEtablissement.getCodeetablissement());
        if (domaines.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun domaine enregistré pour cette etablissement");
            modifier = supprimer = detail = true;
        }
    }
    
   
        
 }
    
