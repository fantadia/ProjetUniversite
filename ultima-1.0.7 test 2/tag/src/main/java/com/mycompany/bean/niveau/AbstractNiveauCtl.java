/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.niveau;

import com.mycompany.entities.Niveau;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.NiveauDao;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public abstract class AbstractNiveauCtl {
    
    
    protected NiveauDao niveauDao ;
    
   
    protected ClasseDao classDao;
    
    protected List<Niveau> niveaux;
    
    protected StringBuffer filieresTableHtml = new StringBuffer("pas encore implementé") ;
    protected Niveau selectedNiveau;
    protected Niveau NiveauModifiee;
    protected Niveau niveau;

    protected boolean creer = true;
    protected boolean detail = false;
    protected boolean modifier = false;
    protected boolean supprimer = false;
    protected boolean imprimer = false;

    public NiveauDao getNiveauDao() {
        return niveauDao;
    }

    public void setNiveauDao(NiveauDao niveauDao) {
        this.niveauDao = niveauDao;
    }

    public List<Niveau> getNiveaux() {
        return niveaux;
    }

    public void setNiveaux(List<Niveau> niveaux) {
        this.niveaux = niveaux;
    }

    public Niveau getSelectedNiveau() {
        return selectedNiveau;
    }

    public void setSelectedNiveau(Niveau selectedNiveau) {
        this.selectedNiveau = selectedNiveau;
        if (selectedNiveau == null) {
            detail = false;
            modifier = false;
            supprimer = false;
            return;
        }
        detail = true;
        modifier = true;
        supprimer = true;
    }

    public Niveau getNiveauModifiee() {
        return NiveauModifiee;
    }

    public void setNiveauModifiee(Niveau NiveauModifiee) {
        this.NiveauModifiee = NiveauModifiee;
    }

    public Niveau getNiveau() {
        return niveau;
    }

    public void setNiveau(Niveau niveau) {
        this.niveau = niveau;
    }

    public ClasseDao getClassDao() {
        return classDao;
    }

    public void setClassDao(ClasseDao classDao) {
        this.classDao = classDao;
    }
   
    
    
    public boolean isDetail() {
        return !detail;
    }

    public boolean isModifier() {
        return !modifier;
    }

    public boolean isSupprimer() {
        return !supprimer;
    }

    public boolean isImprimer() {
        imprimer = niveauDao.findAll().isEmpty();
        return imprimer;
    }
    
    public StringBuffer getFilieresTableHtml() {
        return filieresTableHtml;
    }

    public boolean getCreer() {
        return creer;
    }

    public void setCreer(boolean creer) {
        this.creer = creer;
    }
}
