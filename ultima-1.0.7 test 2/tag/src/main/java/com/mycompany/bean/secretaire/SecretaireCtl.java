/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.secretaire;  


import beans.util.JsfUtil;
import com.mycompany.entities.Dossier;
import com.mycompany.entities.Secretaire;
import com.mycompany.entities.Utilisateur; 
import com.nycompany.dao.DossierDao;
import com.nycompany.dao.SecretaireDao;
import com.nycompany.dao.UtilisateurDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="secretaireCtl")
@ViewScoped

public class SecretaireCtl extends AbstractSecretaireCtl implements SecretaireInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public SecretaireCtl() {
    }
    
    @PostConstruct
    private void initSecretaire()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        secretaireDao = (SecretaireDao) context.getBean("secretaireDao");
        dossierDao =(DossierDao) context.getBean("dossierDao");
        utilisateurDao = (UtilisateurDao) context.getBean("utilisateurDao");
        utilisateur =(Utilisateur) context.getBean("utilisateur");
        selectedUtilisateur =(Utilisateur) context.getBean("utilisateur");
        secretaire = (Secretaire) context.getBean("secretaire");
        selectedSecretaire = (Secretaire) context.getBean("secretaire");
        secretaires = secretaireDao.findAllSecretaire();
    }

    @Override
    public void enregistrerSecretaire() {
        /*Responsable nv = responsableDao.findResponsableById(responsable.getCodeutilisateur());
        if (nv != null) {
            JsfUtil.addErrorMessage("enregistrement existant" + "'" + nv.getUtilisateur().getNom());
            modifier = supprimer = detail = true;
            return;
        }*/
        /*
            Avant tout faut enregistrer ds utilisateur avec comme identifiant 700
            ce qui veut dire que dans la page jsf faut utiliser l'objet utilisateur
            pour preparer les donnees a inserer
            Ensuite l'objet enseignant sera creer a partir de utilisateur
        */
        utilisateur.setCodeutilisateur("800");
        utilisateurDao.createUtilisateur(utilisateur);
        //jecrase utilisateur et je le recupere a nouveau
        //depuis la BD 
        utilisateur=utilisateurDao.findUtilisateurByCIN(utilisateur.getCin());
        //j'initialise le secretaire a inserer
        secretaire.setUtilisateur(utilisateur);
        secretaireDao.createSecretaire(secretaire);
        initSecretaire();
        JsfUtil.addSuccessMessage("le secretaire à été bien créé");
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierSecretaire() {
        
        if (selectedSecretaire == null || selectedSecretaire.getCodeutilisateur()== null) {
            return;
        }

       /* Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
       selectedUtilisateur.setNom(selectedSecretaire.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedSecretaire.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedSecretaire.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedSecretaire.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedSecretaire.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedSecretaire.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedSecretaire.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedSecretaire.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedSecretaire.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedSecretaire.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedSecretaire.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedSecretaire.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedSecretaire.getUtilisateur().getBoitePostale());
         
       utilisateurDao.updateUtilisateur(selectedUtilisateur);
       //selectedSecretaire.setUtilisateur(selectedUtilisateur);
       //secretaireDao.updateSecretaire(selectedSecretaire);
       JsfUtil.addSuccessMessage("secretaire Modifier avec succes");
    }

    @Override
    public void supprimerSecretaire() {
        
         if(selectedSecretaire == null || selectedSecretaire.getCodeutilisateur()== null)
            return;
         List<Dossier> dossiers =  dossierDao.findDossierBySecretaire(selectedUtilisateur.getCodeutilisateur());
         if(!dossiers.isEmpty()){
             JsfUtil.addErrorMessage("Cet enseignant ne peut pas etre supprimée, il est deja utilise dans une operation");
         }
         
       selectedUtilisateur.setNom(selectedSecretaire.getUtilisateur().getNom());
       selectedUtilisateur.setPrenom(selectedSecretaire.getUtilisateur().getPrenom());
       selectedUtilisateur.setCin(selectedSecretaire.getUtilisateur().getCin());
       selectedUtilisateur.setEmail(selectedSecretaire.getUtilisateur().getEmail());
       selectedUtilisateur.setPays(selectedSecretaire.getUtilisateur().getPays());
       selectedUtilisateur.setLogin(selectedSecretaire.getUtilisateur().getLogin());
       selectedUtilisateur.setAdresse(selectedSecretaire.getUtilisateur().getAdresse());
       selectedUtilisateur.setPassword(selectedSecretaire.getUtilisateur().getPassword());
       selectedUtilisateur.setVille(selectedSecretaire.getUtilisateur().getVille());
       selectedUtilisateur.setSituationMatrimonial(selectedSecretaire.getUtilisateur().getSituationMatrimonial());
       selectedUtilisateur.setTelephone(selectedSecretaire.getUtilisateur().getTelephone());
       selectedUtilisateur.setCodeutilisateur(selectedSecretaire.getUtilisateur().getCodeutilisateur());
       selectedUtilisateur.setBoitePostale(selectedSecretaire.getUtilisateur().getBoitePostale());
         
         
        //selectedSecretaire.setUtilisateur(selectedUtilisateur);
        secretaireDao.deleteSecretaire(selectedSecretaire);
        utilisateurDao.deleteUtilisateur(selectedUtilisateur);
          
        JsfUtil.addErrorMessage("Cet secretaire à été supprimée");
        modifier = supprimer = detail = true;}

    @Override
    public void imprimerSecretairePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerSecretaireHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
        
    }
    
