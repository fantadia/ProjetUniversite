/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bean.ue;

import beans.util.JsfUtil;
import com.mycompany.entities.Classe;
import com.mycompany.entities.Ec;
import com.mycompany.entities.Maquette;

import com.mycompany.entities.Semestre;
import com.mycompany.entities.Ue;
import com.nycompany.dao.ClasseDao;
import com.nycompany.dao.EcDao;
import com.nycompany.dao.MaquetteDao;
import com.nycompany.dao.SemestreDao;
import com.nycompany.dao.UeDao;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author macbookpro
 */
@ManagedBean(name="ueCtl")
@ViewScoped

public class UeCtl extends AbstractUeCtl implements UeInterfaceCtl {
    
    /**
     * Creates a new instance of NiveauCtrl
     */
    public UeCtl() {
    }
    
    @PostConstruct
    private void initUe()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
       
        ecDao = (EcDao) context.getBean("ecDao");
        
        ueDao = (UeDao) context.getBean("ueDao");
        ue = (Ue) context.getBean("ue");
        selectUe = (Ue) context.getBean("ue");
        ues = ueDao.findAllUe();
    
        semestreDao = (SemestreDao) context.getBean("semestreDao");
        selectedSemestre = (Semestre) context.getBean("semestre");
        semestres = semestreDao.findAllSemestre();
        
        maquetteDao = (MaquetteDao) context.getBean("maquetteDao");
        selectedMaquette = (Maquette) context.getBean("maquette");
        maquettes = maquetteDao.findAllMaquette();
        
        classeDao = (ClasseDao) context.getBean("classeDao");
        selectedClasse = (Classe) context.getBean("classe");
        classes = classeDao.findAllClasse();
    }

    @Override
    public void enregistrerUe() {
        Ue unites = ueDao.findUeByLibelle(ue.getLibelleUe());
        if(unites==null){
            ue.setCodeue("ue");
            ue.setCodeue("ue");
            ueDao.createUe(ue);
            initUe();
            JsfUtil.addSuccessMessage("l'ue à été bien créé");
        }
        else
            JsfUtil.addSuccessMessage("Ce ue existe deja");
        
        modifier = supprimer = detail = true;
    }

    @Override
    public void modifierUe() {
        
        if (selectUe == null || selectUe.getCodeue()== null) {
            return;
        }

       /*Niveau type = filieresFacadeLocal.findByNomFilieres(selectedFilieres.getNomfiliere());
        if (type != null && !Objects.equals(type.getId(), selectedFilieres.getId())) {
            JsfUtil.addErrorMessage("la filiere '" + selectedFilieres.getNomfiliere() + "'" + "a été mis à jour");
        }
        */
        Ue unit = ueDao.findUeByLibelle(selectUe.getLibelleUe());
        if(unit==null){
            ueDao.updateUe(selectUe);
            JsfUtil.addSuccessMessage("ue Modifier avec succes");
        }
        else
             JsfUtil.addSuccessMessage("Ce ue existe deja");
    }

    @Override
    public void supprimerUe() {
        
         if(selectUe == null || selectUe.getCodeue()== null)
            return;
      
        List<Ec> n = ecDao.findEcByUe(selectUe.getCodeue());
        if (!n.isEmpty()) {
         JsfUtil.addErrorMessage("L'ue ne peut pas etre supprime : il est deja utilise par une  operation");
         
       } else {
            ueDao.deleteUe(selectUe);
            JsfUtil.addErrorMessage("Cette ue à été supprimée");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeSemestresMaquette() {
        if (selectedMaquette == null) {
            semestres = semestreDao.findAllSemestre();
            return;
        }
        semestres = semestreDao.findSemestreByMaquette(selectedMaquette.getCodemaquette());
        if (semestres.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun semestre enregistré pour cette maquette");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeUesSemestre() {
        if (selectedSemestre == null) {
            ues = ueDao.findAllUe();
            return;
        }
        ues = ueDao.findUeBySemestre(selectedSemestre.getCodesemestre());
        if (ues.isEmpty()) {
            JsfUtil.addErrorMessage("Aucun ue enregistré pour cette semestre");
            modifier = supprimer = detail = true;
        }
    }
    
     @Override
    public void listeMaquetteClasse() {
        if (selectedClasse == null) {
            maquettes = maquetteDao.findAllMaquette();
            return;
        }
        maquettes = maquetteDao.findMaquetteByClasse(selectedClasse.getCodeclasse());
        if (maquettes.isEmpty()) {
            JsfUtil.addErrorMessage("Aucune maquette enregistré pour cette classe");
            modifier = supprimer = detail = true;
        }
    }

    @Override
    public void imprimerUePdf() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimerUeHtml() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getComponentMessages(String filiereComponent, String defaultMessage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
