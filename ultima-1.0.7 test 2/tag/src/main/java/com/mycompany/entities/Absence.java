package com.mycompany.entities;
// Generated 16 nov. 2017 12:44:30 by Hibernate Tools 4.3.1


import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Absence generated by hbm2java
 */
@Entity
@Table(name="absence"
    ,schema="public"
)
public class Absence  implements java.io.Serializable {


     private AbsenceId id;
     private Etudiant etudiant;
     private SeanceCours seanceCours;
     private String type;

    public Absence() {
    }

	
    public Absence(AbsenceId id, Etudiant etudiant, SeanceCours seanceCours) {
        this.id = id;
        this.etudiant = etudiant;
        this.seanceCours = seanceCours;
    }
    public Absence(AbsenceId id, Etudiant etudiant, SeanceCours seanceCours, String type) {
       this.id = id;
       this.etudiant = etudiant;
       this.seanceCours = seanceCours;
       this.type = type;
    }
   
     @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="codeutilisateur", column=@Column(name="codeutilisateur", nullable=false, length=254) ), 
        @AttributeOverride(name="codecours", column=@Column(name="codecours", nullable=false, length=254) ) } )
    public AbsenceId getId() {
        return this.id;
    }
    
    public void setId(AbsenceId id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="codeutilisateur", nullable=false, insertable=false, updatable=false)
    public Etudiant getEtudiant() {
        return this.etudiant;
    }
    
    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="codecours", nullable=false, insertable=false, updatable=false)
    public SeanceCours getSeanceCours() {
        return this.seanceCours;
    }
    
    public void setSeanceCours(SeanceCours seanceCours) {
        this.seanceCours = seanceCours;
    }

    
    @Column(name="type", length=254)
    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Absence other = (Absence) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    

    


}


