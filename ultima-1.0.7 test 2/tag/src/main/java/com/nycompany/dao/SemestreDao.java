/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Semestre;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface SemestreDao {
    public void createSemestre(Semestre semestre);
    public void updateSemestre(Semestre semestre);
    public void deleteSemestre(Semestre semestre);
    public Semestre findSemestreById(String id);
    public List<Semestre> findSemestreByMaquette(String codeMaquette);
    public List<Semestre> findAllSemestre(); 
}
