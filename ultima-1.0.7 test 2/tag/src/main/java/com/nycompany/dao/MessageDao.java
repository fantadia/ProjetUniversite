/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Message;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface MessageDao 
{
    public void createMessage(Message message);
    public void updateMessage(Message message);
    public void deleteMessage(Message message);
    public Message findMessageById(String id);
    public List<Message> findMessageByUtilisateur(String codeUtilisateur);
    public List<Message> findAllMessage(); 
    
}
