/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Etudiant;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class EtudiantDaoImpl implements EtudiantDao{

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void createEtudiant(Etudiant etudiant) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(etudiant);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateEtudiant(Etudiant etudiant) {
         
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(etudiant);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteEtudiant(Etudiant etudiant) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Etudiant etu = (Etudiant) session.load(Etudiant.class, etudiant.getUtilisateur().getCodeutilisateur());
        
            session.delete(etu);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Etudiant findEtudiantById(String id) {
       Etudiant etudiant = null;
       Transaction trns = null;
       Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Etudiant where utilisateur = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            etudiant = (Etudiant) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etudiant;
    }

    @Override
    public List<Etudiant> findAllEtudiant() {
        List<Etudiant> etudiants = new ArrayList<Etudiant>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            etudiants = session.createQuery("from Etudiant").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etudiants;  
    }

    @Override
    public List<Etudiant> findEtudiantByClasse(String idClasse) {
        List<Etudiant> etudiants = new ArrayList<Etudiant>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Etudiant where classe = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idClasse);
            etudiants = (List<Etudiant>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etudiants;  
    }
    
    
    @Override
    public List<Etudiant> findEtudiantBySpecification(String idClasse,String idDomaine,String idFiliere,String idEtab) {
        List<Etudiant> etudiants = new ArrayList<Etudiant>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "select e from Etudiant as e join e.Classe as cl join  cl.Filiere as f "
                           + "join f.Domaine as d join d.Etablissement as etab where cl.codeclasse = :lib and f.codefiliere = :lib2 and d.codedomaine=:lib3 and etab.codeetablissement=:lib4";
            Query query = session.createQuery(queryString);
            query.setString("lib", idClasse);
            query.setString("lib2", idFiliere);
            query.setString("lib3", idDomaine);
            query.setString("lib4", idEtab);
            
            etudiants = (List<Etudiant>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etudiants;  
    }
}
