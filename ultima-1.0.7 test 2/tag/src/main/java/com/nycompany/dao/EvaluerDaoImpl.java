/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Ec;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Evaluer;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class EvaluerDaoImpl implements EvaluerDao{
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createEvaluer(Evaluer evaluer) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(evaluer);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateEvaluer(Evaluer evaluer) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(evaluer);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteEvaluer(Evaluer evaluer) {
       Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Evaluer abs = (Evaluer) session.load(Evaluer.class, evaluer.getId());
        
            session.delete(abs);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Evaluer findEvaluerByEvaluerId(String id) {
        Evaluer evaluer = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Evaluer where id = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            evaluer = (Evaluer) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return evaluer;
    }

    @Override
    public List<Evaluer> findAllEvaluer() 
    {
        List<Evaluer> evaluers = new ArrayList<Evaluer>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            evaluers = session.createQuery("from Evaluer").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return evaluers;  
    }

    @Override
    public List<Evaluer> findEvaluerByEtudiant(String idetud) 
    {
        List<Evaluer> evaluers = new ArrayList<Evaluer>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Evaluer where etudiant = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idetud);
            evaluers = (List<Evaluer>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return evaluers; 
    }

    @Override
    public List<Evaluer> findEvaluerByEc(String idec) 
    {
        List<Evaluer> evaluers = new ArrayList<Evaluer>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Evaluer where ec = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idec);
            evaluers = (List<Evaluer>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return evaluers;
    }

    @Override
    public List<Evaluer> findEvaluerByEtudiantMatiere(String idEtudiant, String idEc) {
        List<Evaluer> evaluers = new ArrayList<Evaluer>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Evaluer where etudiant = :lib And ec=:param";
            Query query = session.createQuery(queryString);
            query.setString("lib", idEtudiant);
            query.setString("param", idEc);
            evaluers = (List<Evaluer>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return evaluers; 
    }
    
}