/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Secretaire;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface SecretaireDao 
{
    public void createSecretaire(Secretaire secretaire);
    public void updateSecretaire(Secretaire secretaire);
    public void deleteSecretaire(Secretaire secretairet);
    public Secretaire findSecretaireById(String id);
    public List<Secretaire> findAllSecretaire(); 
}
