/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Maquette;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class MaquetteDaoImpl implements MaquetteDao{

     private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void createMaquette(Maquette maquette) {
       
         Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(maquette);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateMaquette(Maquette maquette) {
      Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(maquette);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteMaquette(Maquette maquette) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Maquette cl = (Maquette) session.load(Maquette.class, maquette.getCodemaquette());
        
            session.delete(cl);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Maquette findMaquetteById(String id) {
        
        Maquette maquette = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Maquette where codemaquette = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            maquette = (Maquette) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return maquette;
    }

    @Override
    public List<Maquette> findAllMaquette() {
        
        List<Maquette> maquettes = new ArrayList<Maquette>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            maquettes = session.createQuery("from Maquette").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return maquettes;   
    }

    @Override
    public List<Maquette> findMaquetteByClasse(String idclassse) {
        
        List<Maquette> maquettes = new ArrayList<Maquette>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Maquette where classe = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idclassse);
            maquettes = (List<Maquette>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return maquettes; 
    }

    
}
