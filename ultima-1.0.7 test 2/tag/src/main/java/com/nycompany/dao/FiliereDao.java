/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Filiere;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface FiliereDao {
    public void createFiliere(Filiere filiere);
    public void updateFiliere(Filiere filiere);
    public void deleteFiliere(Filiere filiere);
    public Filiere findFiliereById(String id);
    public List<Filiere> findFiliereByCodeDomaine(String libelle);
    public List<Filiere> findFiliereByCodeEnsResp(String idEnsResp);
    public Filiere findFiliereByLibelle(String libelle);
    public List<Filiere> findAllFiliere(); 
}
