/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Enseignant;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface EnseignantDao {
    
    public void createEnseignant(Enseignant enseignant);
    public void updateEnseignant(Enseignant enseignant);
    public void deleteEnseignant(Enseignant enseignant);
    public Enseignant findEnseignantById(String id);
    public List<Enseignant> findEnseignantByUtilisateur(String idUser);
    public List<Enseignant> findAll(); 
}
