/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Ec;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface EcDao {
    
    public void createEc(Ec ec);
    public void updateEc(Ec ec);
    public void deleteEc(Ec ec);
    public Ec findEcById(String id);
    public Ec findEcByLibelle(String lib);
    public List<Ec> findEcByUe(String codeUe);
    public List<Ec> findAllEc(); 
    
}
