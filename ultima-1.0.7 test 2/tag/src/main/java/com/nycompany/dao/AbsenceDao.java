/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Absence;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface AbsenceDao {
    
    public void createAbsence(Absence absence);
    public void updateAbsence(Absence absence);
    public void deleteAbsence(Absence absence);
    public Absence findAbsenceByAbsenceId(String id);
    public List<Absence> findAbsenceByEtudiant(String idetud);
    public List<Absence> findAbsenceBySeanceCours(String idcours);
    public List<Absence> findAllAbsence();
}
