/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Facture;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface FactureDao {
    
    public void createFacture(Facture facture);
    public void updateFacture(Facture facture);
    public void deleteFacture(Facture facture);
    public Facture findFactureById(String idFacture);
    public List<Facture> findFactureByComptable(String idComptable);
    public List<Facture> findAllFacture(); 
}
