/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Ec;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class EcDaoImpl implements EcDao{

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void createEc(Ec ec) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(ec);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateEc(Ec ec) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(ec);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteEc(Ec ec) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Ec resp = (Ec) session.load(Ec.class, ec.getCodematiere());
        
            session.delete(resp);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Ec findEcById(String id) {
        
        Ec ec = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Ec where codematiere = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            ec = (Ec) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return ec;
    }

    @Override
    public List<Ec> findEcByUe(String codeUe) {
        List<Ec> ecs = new ArrayList<Ec>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Ec where ue = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", codeUe);
            ecs = (List<Ec>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return ecs; 
    }

    @Override
    public List<Ec> findAllEc() {
        
        List<Ec> ecs = new ArrayList<Ec>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            ecs = session.createQuery("from Ec").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return ecs;   
    }

    @Override
    public Ec findEcByLibelle(String lib) {
        Ec ec = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Ec where libelleMatiere = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", lib);
            ec = (Ec) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return ec;
    }
    
}
