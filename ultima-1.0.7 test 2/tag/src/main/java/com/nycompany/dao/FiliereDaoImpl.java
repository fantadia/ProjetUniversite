/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Filiere;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class FiliereDaoImpl implements FiliereDao {
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createFiliere(Filiere filiere) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(filiere);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateFiliere(Filiere filiere) {
        
         
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(filiere);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteFiliere(Filiere filiere) {
       
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Filiere fil = (Filiere) session.load(Filiere.class, filiere.getCodefiliere());
        
            session.delete(fil);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Filiere findFiliereById(String idFiliere) {
         
        Filiere filiere = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Filiere where codefiliere = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", idFiliere);
            filiere = (Filiere) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return filiere;
    }

    @Override
    public Filiere findFiliereByLibelle(String libelle) {
        Filiere filiere = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Filiere where libelle_filiere = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", libelle);
            filiere = (Filiere) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return filiere;
    }

    @Override
    public List<Filiere> findAllFiliere() {
        
        List<Filiere> filieres = new ArrayList<Filiere>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            filieres = session.createQuery("from Filiere").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return filieres;   
    }

    @Override
    public List<Filiere> findFiliereByCodeDomaine(String codeDomaine) {
        List<Filiere> filieres = new ArrayList<Filiere>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Filiere where domaine = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", codeDomaine);
            filieres = (List<Filiere>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return filieres;  
    }

    @Override
    public List<Filiere> findFiliereByCodeEnsResp(String idEnsResp) {
       
        List<Filiere> filieres = new ArrayList<Filiere>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Filiere where enseignantResponsable = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idEnsResp);
            filieres = (List<Filiere>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return filieres;  
    }
    
}
