/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Dossier;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface DossierDao {
    
    public void createDossier(Dossier dossier);
    public void updateDossier(Dossier dossier);
    public void deleteDossier(Dossier dossier);
    public Dossier findDossierById(String id);
    public List<Dossier> findDossierByEtudiant(String codeUe);
     public List<Dossier> findDossierBySecretaire(String codeUe);
    public List<Dossier> findAllDossier(); 
    
}
