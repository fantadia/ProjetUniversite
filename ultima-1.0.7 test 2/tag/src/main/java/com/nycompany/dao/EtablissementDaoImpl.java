/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Etablissement;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class EtablissementDaoImpl implements EtablissementDao{
     private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createEtablissement(Etablissement etablissement) {
       Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(etablissement);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateEtablissement(Etablissement etablissement) {
        
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(etablissement);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteEtablissement(Etablissement etablissement) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Etablissement cl = (Etablissement) session.load(Etablissement.class, etablissement.getCodeetablissement());
        
            session.delete(cl);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Etablissement findEtablissementById(String idEtablissement) {
       Etablissement etablissement = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Etablissement where codeetablissement = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", idEtablissement);
            etablissement = (Etablissement) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etablissement;
    }

    @Override
    public Etablissement findEtablissementByLibelle(String libelle) {
        
        Etablissement etablissement = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Etablissement where libelleEtablissemant = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", libelle);
            etablissement = (Etablissement) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etablissement;
    }

    @Override
    public List<Etablissement> findAllEtablissement() {
        
         List<Etablissement> etablissements = new ArrayList<Etablissement>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            etablissements = session.createQuery("from Etablissement").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etablissements;  
    }

    @Override
    public List<Etablissement> findEtablissementByIdResponsable(String idResp) {
        
        List<Etablissement> etablissements = new ArrayList<Etablissement>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Etablissement where responsable = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idResp);
            etablissements = (List<Etablissement>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return etablissements;  
    }
    
}
