/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.EmploisDuTemps;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface EmploisDuTempsDao {
    
    public void createEmploisDuTemps(EmploisDuTemps ept);
    public void updateEmploisDuTemps(EmploisDuTemps ept);
    public void deleteEmploisDuTemps(EmploisDuTemps ept);
    public EmploisDuTemps findEmploiDuTempsById(String id);
    public List<EmploisDuTemps> findEmploisDuTempsByClasse(String codeClasse);
    public List<EmploisDuTemps> findEmploisDuTempsByResponsable(String codeResp);
    public List<EmploisDuTemps> findEmploisDuTempsBySemestre(String codeSemestre);
    public List<EmploisDuTemps> findAllEmploisDuTemps(); 
}
