/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Dossier;
import com.mycompany.entities.Ec;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class DossierDaoImpl implements DossierDao{

     private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void createDossier(Dossier dossier) {
      
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(dossier);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateDossier(Dossier dossier) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(dossier);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteDossier(Dossier dossier) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Dossier d = (Dossier) session.load(Dossier.class, dossier.getCodedossier());
        
            session.delete(d);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Dossier findDossierById(String id) {
       Dossier dossier = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Dossier where codedossier = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            dossier = (Dossier) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return dossier;
    }

    @Override
    public List<Dossier> findDossierByEtudiant(String codeUe) {
        List<Dossier> dossiers = new ArrayList<Dossier>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Dossier where etudiant = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", codeUe);
            dossiers = (List<Dossier>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return dossiers; 
    }

    @Override
    public List<Dossier> findAllDossier() {
        List<Dossier> dossiers = new ArrayList<Dossier>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            dossiers = session.createQuery("from Dossier").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return dossiers;   
    }

    @Override
    public List<Dossier> findDossierBySecretaire(String codeSec) {
        
        List<Dossier> dossiers = new ArrayList<Dossier>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Dossier where secretaire = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", codeSec);
            dossiers = (List<Dossier>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return dossiers; 
    }
    
}
