/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Cours;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface CoursDao {
    
    public void createCours(Cours cour);
    public void updateCours(Cours cour);
    public void deleteCours(Cours cour);
    public Cours findCoursById(String id);
    public List<Cours> findCoursByEc(String codeEc);
    public List<Cours> findCoursByEnseignant(String codeEns);
    public List<Cours> findAllCours(); 
}
