/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Ue;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface UeDao {
    
    public void createUe(Ue ue);
    public void updateUe(Ue ue);
    public void deleteUe(Ue ue);
    public Ue findUeById(String id);
    public Ue findUeByLibelle(String lib);
    public List<Ue> findUeBySemestre(String codeMaquette);
    public List<Ue> findAllUe(); 
}
