/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Ec;
import com.mycompany.entities.Etudiant;
import com.mycompany.entities.Evaluer;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface EvaluerDao 
{
    public void createEvaluer(Evaluer evaluer);
    public void updateEvaluer(Evaluer evaluer);
    public void deleteEvaluer(Evaluer evaluer);
    public Evaluer findEvaluerByEvaluerId(String id);
    public List<Evaluer> findEvaluerByEtudiant(String idetud);
    public List<Evaluer> findEvaluerByEc(String idec);
    public List<Evaluer> findAllEvaluer();
    public List<Evaluer> findEvaluerByEtudiantMatiere(String idEtudiant,String idEc);
}
