/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Paiement;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface PaiementDao {
    
    public void createPaiement(Paiement paiement);
    public void updatePaiement(Paiement paiement);
    public void deletePaiement(Paiement paiement);
    public Paiement findPaiementById(String idpaiement);
    public List<Paiement> findPaiementByEtudiant(String codeEtudiant);
    public List<Paiement> findAllPaiement(); 
}
