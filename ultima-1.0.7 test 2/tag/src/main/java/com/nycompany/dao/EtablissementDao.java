/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Etablissement;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface EtablissementDao {
    
    public void createEtablissement(Etablissement etablissement);
    public void updateEtablissement(Etablissement etablissement);
    public void deleteEtablissement(Etablissement etablissement);
    public Etablissement findEtablissementById(String id);
    public List<Etablissement> findEtablissementByIdResponsable(String idResp);
    public Etablissement findEtablissementByLibelle(String libelle);
    public List<Etablissement> findAllEtablissement(); 
    
}
