/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Niveau;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface NiveauDao
{
    
    public void saveNiveau(Niveau niveau);
    public void updateNiveau(Niveau niveau);
    public void deleteNiveau(Niveau niveau);
    public Niveau findNiveauById(String id);
    public Niveau findNiveauByLibelle(String libelle);
    public List<Niveau> findAll(); 
}
