/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Niveau;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
/**
 *
 * @author macbookpro
 */
public class NiveauDaoImpl implements NiveauDao{
 
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    

    @Override
    public void saveNiveau(Niveau niveau) 
    {
        try {
            Session session = this.sessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            session.persist(niveau);
            tx.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public List<Niveau> findAll() 
    {
        List<Niveau> niveaux = new ArrayList<Niveau>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            niveaux = session.createQuery("from Niveau").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return niveaux;   
    }
    

    @Override
    public void updateNiveau(Niveau niveau) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(niveau);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        
       
    }

    @Override
    public void deleteNiveau(Niveau niveau) {
        
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Niveau niv = (Niveau) session.load(Niveau.class, niveau.getIdniveau());
        
            session.delete(niv);
            session.getTransaction().commit();
           
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public Niveau findNiveauById(String idNiveau) {
        
        Niveau niveau = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Niveau where idniveau = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", idNiveau);
            niveau = (Niveau) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return niveau;
    }

    @Override
    public Niveau findNiveauByLibelle(String libelle) {
        
        Niveau niveau = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Niveau where libelle = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", libelle);
            niveau = (Niveau) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return niveau;
    }
    
}
