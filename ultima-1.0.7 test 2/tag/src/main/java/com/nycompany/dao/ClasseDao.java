/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Classe;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface ClasseDao 
{
    public void createClasse(Classe classe);
    public void updateClasse(Classe classe);
    public void deleteClasse(Classe classe);
    public Classe findClasseById(String id);
    public Classe findClasseByLibelle(String libelle);
    //prend t-il un objet niveau en parametre
    public List<Classe> findClasseByIdNiveau(String idNiveau);
    public List<Classe> findClasseByIdFiliere(String idFiliere);
    public List<Classe> findAllClasse(); 
    
}
