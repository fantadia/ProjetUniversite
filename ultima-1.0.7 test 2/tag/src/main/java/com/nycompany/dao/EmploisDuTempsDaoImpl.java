/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.EmploisDuTemps;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class EmploisDuTempsDaoImpl implements EmploisDuTempsDao{

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void createEmploisDuTemps(EmploisDuTemps ept) {
       
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(ept);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateEmploisDuTemps(EmploisDuTemps ept) {
       
         
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(ept);
            //trns.commit();
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteEmploisDuTemps(EmploisDuTemps ept) {
        
         
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            EmploisDuTemps empTps = (EmploisDuTemps) session.load(EmploisDuTemps.class, ept.getCodeEmploisdutemps());
        
            session.delete(empTps);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public EmploisDuTemps findEmploiDuTempsById(String id) {
        EmploisDuTemps emploisDuTemps = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from EmploisDuTemps where codeEmploisdutemps = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            emploisDuTemps = (EmploisDuTemps) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return emploisDuTemps;
    }

    @Override
    public List<EmploisDuTemps> findEmploisDuTempsByClasse(String codeClasse) {
        
       List<EmploisDuTemps> emploisDuTempses = new ArrayList<EmploisDuTemps>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from EmploisDuTemps where classe = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", codeClasse);
            emploisDuTempses = (List<EmploisDuTemps>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return emploisDuTempses; 
    }

    @Override
    public List<EmploisDuTemps> findEmploisDuTempsByResponsable(String codeResp) {
        
        List<EmploisDuTemps> emploisDuTempses = new ArrayList<EmploisDuTemps>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from EmploisDuTemps where responsable = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", codeResp);
            emploisDuTempses = (List<EmploisDuTemps>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return emploisDuTempses; 
    }

    @Override
    public List<EmploisDuTemps> findEmploisDuTempsBySemestre(String codeSemestre) {
         
        List<EmploisDuTemps> emploisDuTempses = new ArrayList<EmploisDuTemps>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from EmploisDuTemps where semestre = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", codeSemestre);
            emploisDuTempses = (List<EmploisDuTemps>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return emploisDuTempses; 
    }

    @Override
    public List<EmploisDuTemps> findAllEmploisDuTemps() {
          
        List<EmploisDuTemps> emploisDuTempses = new ArrayList<EmploisDuTemps>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            emploisDuTempses = session.createQuery("from EmploisDuTemps").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return emploisDuTempses;   
    }
    
}
