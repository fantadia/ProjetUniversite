/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Cours;
import com.mycompany.entities.SeanceCours;
import java.util.List;

/**
 *
 * @author macbookpro
 */
public interface SeanceCoursDao {
    
    public void createSeanceCours(SeanceCours seanceCours);
    public void updateSeanceCours(SeanceCours seanceCours);
    public void deleteSeanceCours(SeanceCours seanceCours);
    public SeanceCours findSeanceCoursById(String id);
    public List<SeanceCours> findAllSeanceCours(); 
}
