/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nycompany.dao;

import com.mycompany.entities.Absence;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author macbookpro
 */
public class AbsenceDaoImpl implements AbsenceDao{
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createAbsence(Absence absence) {
       Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.save(absence);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void updateAbsence(Absence absence) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            session.update(absence);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }

    @Override
    public void deleteAbsence(Absence absence) {
        Transaction trns = null;
        Session session = this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            Absence abs = (Absence) session.load(Absence.class, absence.getId());
        
            session.delete(abs);
            session.getTransaction().commit();
            
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
    }
   
    @Override
    public List<Absence> findAllAbsence() {
        List<Absence> absences = new ArrayList<Absence>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            absences = session.createQuery("from Absence").list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return absences;  
    }

    @Override
    public Absence findAbsenceByAbsenceId(String idAbsence) {
        Absence absence = null;
        Transaction trns = null;
        Session session =this.sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Absence where id = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", idAbsence);
            absence = (Absence) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return absence;
    }

    @Override
    public List<Absence> findAbsenceByEtudiant(String idetud) 
    {
        List<Absence> absences = new ArrayList<Absence>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Absence where etudiant = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idetud);
            absences = (List<Absence>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return absences; 
    }

    @Override
    public List<Absence> findAbsenceBySeanceCours(String idcours) 
    {
        List<Absence> absences = new ArrayList<Absence>();
        Transaction trns = null;
        Session session = sessionFactory.openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Absence where seanceCours = :lib";
            Query query = session.createQuery(queryString);
            query.setString("lib", idcours);
            absences = (List<Absence>)query.list();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
        }
        return absences; 
    }

   
}
