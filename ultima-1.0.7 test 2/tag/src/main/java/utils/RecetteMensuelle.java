/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author Bommeu
 */
public class RecetteMensuelle {

    private Integer numero ;
    private String reference ;
    private Double loyerBase ;
    private String nomsPrenoms ;
    private Double aeres ;
    private Double energies ;
    private Double cummul ;
    private String observation ;
    private String contacts ;

    public RecetteMensuelle() {
        
    }

    /**
     * bommeu
     * @since 02 09 2015
     * @param numero
     * @param reference
     * @param loyerBase
     * @param nomsPrenoms
     * @param aeres
     * @param energies
     * @param cummul
     * @param contacts 
     */
    public RecetteMensuelle(Integer numero, String reference, Double loyerBase, String nomsPrenoms, Double aeres, Double energies, Double cummul, String contacts) {
        this.numero = numero;
        this.reference = reference;
        this.loyerBase = loyerBase;
        this.nomsPrenoms = nomsPrenoms;
        this.aeres = aeres;
        this.energies = energies;
        this.cummul = cummul;
        this.contacts = contacts;
    }

    /**
     * Ulrich
     * @param numero
     * @param reference
     * @param loyerBase
     * @param nomsPrenoms
     * @param aeres
     * @param energies
     * @param cummul
     * @param observation
     * @param contacts 
     */
    public RecetteMensuelle(Integer numero, String reference, Double loyerBase, String nomsPrenoms, Double aeres, Double energies, Double cummul, String observation, String contacts) {
        this.numero = numero;
        this.reference = reference;
        this.loyerBase = loyerBase;
        this.nomsPrenoms = nomsPrenoms;
        this.aeres = aeres;
        this.energies = energies;
        this.cummul = cummul;
        this.observation = observation;
        this.contacts = contacts;
    }

    
    
    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
    
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getLoyerBase() {
        return loyerBase;
    }

    public void setLoyerBase(Double loyerBase) {
        this.loyerBase = loyerBase;
    }

    public String getNomsPrenoms() {
        return nomsPrenoms;
    }

    public void setNomsPrenoms(String nomsPrenoms) {
        this.nomsPrenoms = nomsPrenoms;
    }

    public Double getAeres() {
        return aeres;
    }

    public void setAeres(Double aeres) {
        this.aeres = aeres;
    }

    public Double getEnergies() {
        return energies;
    }

    public void setEnergies(Double energies) {
        this.energies = energies;
    }

    public Double getCummul() {
        return cummul;
    }

    public void setCummul(Double cummul) {
        this.cummul = cummul;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }
}
